#pragma once
#include "BotServer.h"
#include "NtlSessionFactory.h"

#ifndef CBOTSESSION_FACTORY_H
#define CBOTSESSION_FACTORY_H

class CBotSessionFactory : public CNtlSessionFactory
{
public:
	CBotSessionFactory();
	~CBotSessionFactory();
	CNtlSession * CreateSession(SESSIONTYPE sessionType);
	void DestroySession(CNtlSession * pSession);
};
#endif

