#pragma once
#include "BotServer.h"
#include "NtlSession.h"
#include "NtlPacketEncoder_RandKey.h"
///////////////////////////////////////RESPONSE PACKETS///////////////////////////
//System Tool -> Master Server
#include "NtlPacketSM.h"
//System Tool -> Operating Server
#include "NtlPacketSP.h"
/////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////REQUEST PACKETS///////////////////////////
//Master Server -> System Tool
#include "NtlPacketMS.h"
//Operating Server -> System Tool
#include "NtlPacketPS.h"
/////////////////////////////////////////////////////////////////////////////////
#include "NtlPacketUtil.h"

#ifndef CBOTSESSION_H
#define CBOTSESSION_H

class CClientSession :	public CNtlSession
{
public:
	CClientSession(bool bAliveCheck = true, bool bOpcodeCheck = true);
	~CClientSession();
	int							OnAccept();
	void						OnClose();
	int							OnDispatch(CNtlPacket * pPacket);
	// Packet functions
	void						SendBotAgentEnterReq(CNtlPacket* pPacket);
	// End Packet functions
private:
	CNtlPacketEncoder_RandKey	m_packetEncoder;
};

#endif