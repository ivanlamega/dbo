#include "stdafx.h"
#include "AuthServer.h"
#include "ClientSession.h"


CClientSession::CClientSession(bool bAliveCheck, bool bOpcodeCheck) :CNtlSession(SESSION_CLIENT)
{
	SetControlFlag(CONTROL_FLAG_USE_SEND_QUEUE);

	if (bAliveCheck)
	{
		SetControlFlag(CONTROL_FLAG_CHECK_ALIVE);
	}
	if (bOpcodeCheck)
	{
		SetControlFlag(CONTROL_FLAG_CHECK_OPCODE);
	}

	SetPacketEncoder(&m_packetEncoder);
}

//-----------------------------------------------------------------------------------
//		클라이언트 소멸
//-----------------------------------------------------------------------------------
CClientSession::~CClientSession()
{
	NTL_PRINT(PRINT_APP, "CClientSession Destructor Called");

	CAuthServer * app = (CAuthServer*)NtlSfxGetApp();
	app->Remove(this->GetHandle());
}

int CClientSession::OnAccept()
{
	NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
	return NTL_SUCCESS;
}


void CClientSession::OnClose()
{
	NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
}

int CClientSession::OnDispatch(CNtlPacket * pPacket)
{
	CAuthServer * app = (CAuthServer*)NtlSfxGetApp();

	sNTLPACKETHEADER * pHeader = (sNTLPACKETHEADER *)pPacket->GetPacketData();
	switch (pHeader->wOpCode)
	{
		case UA_LOGIN_REQ:
		{
			CClientSession::SendAuthLoginEnterReq(pPacket);
		}
		break;
		case UA_LOGIN_DISCONNECT_REQ:
		{
			CClientSession::SendAuthLoginDiscoReq(pPacket);
		}
		break;
		case QA_LOGIN_RES:
		{
			CClientSession::SendAuthLoginRes(pPacket);
		}
		break;
		default:
		{
			NTL_PRINT(PRINT_APP, "The following packet was not handled: %s", NtlGetPacketName(pHeader->wOpCode));
			return CNtlSession::OnDispatch(pPacket);
		}
	}

	return NTL_SUCCESS;
}
void CClientSession::SendAuthLoginEnterReq(CNtlPacket* pPacket)
{
	CAuthServer* app = (CAuthServer*)NtlSfxGetApp();
	sUA_LOGIN_REQ* req = (sUA_LOGIN_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sAQ_LOGIN_REQ));
	sAQ_LOGIN_REQ* res = (sAQ_LOGIN_REQ*)packet.GetPacketData();
	memcpy(res->awchUserId, req->awchUserId, NTL_MAX_SIZE_USERID_UNICODE+1);
	memcpy(res->awchPasswd, req->awchUserId, NTL_MAX_SIZE_USERID_UNICODE+1);
	memcpy(res->abyMacAddress, req->abyMacAddress, DBO_MAX_ADAPTER_ADDRESS_LENGTH);
	res->wOpCode = AQ_LOGIN_REQ;
	packet.SetPacketLen(sizeof(sAQ_LOGIN_REQ));
	app->Send(this->GetHandle(), &packet);	
}
void CClientSession::SendAuthLoginDiscoReq(CNtlPacket* pPacket)
{
	CAuthServer* app = (CAuthServer*)NtlSfxGetApp();
	sUA_LOGIN_DISCONNECT_REQ* req = (sUA_LOGIN_DISCONNECT_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sAU_LOGIN_DISCONNECT_RES));
	sAU_LOGIN_DISCONNECT_RES* res = (sAU_LOGIN_DISCONNECT_RES*)packet.GetPacketData();
	res->wOpCode = AU_LOGIN_DISCONNECT_RES;
	packet.SetPacketLen(sizeof(sAU_LOGIN_DISCONNECT_RES));
	app->Send(this->GetHandle(), &packet);
}
void CClientSession::SendAuthLoginRes(CNtlPacket* pPacket)
{
	CAuthServer* app = (CAuthServer*)NtlSfxGetApp();
	sQA_LOGIN_RES* req = (sQA_LOGIN_RES*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sAU_LOGIN_RES));
	sAU_LOGIN_RES* res = (sAU_LOGIN_RES*)packet.GetPacketData();
	strcpy_s((char*)res->abyAuthKey, NTL_MAX_SIZE_AUTH_KEY, "Dbo");
	res->accountId = req->accountId;
	memcpy(res->awchUserId, req->awchUserId, NTL_MAX_SIZE_USERID_UNICODE + 1);
	res->lastServerFarmId = req->lastServerFarmId;
	res->byServerInfoCount = app->byServerInfoCount;
	for (int i = 0; i < 10; i++)
		res->aServerInfo[i] = app->aServerInfo[i];
	res->wResultCode = req->wResultCode;
	res->wOpCode = AU_LOGIN_RES;	
	packet.SetPacketLen(sizeof(sAU_LOGIN_RES));
	app->Send(this->GetHandle(), &packet);
}