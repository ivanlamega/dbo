#include "stdafx.h"
#include "AuthSessionFactory.h"


CAuthSessionFactory::CAuthSessionFactory()
{
}


CAuthSessionFactory::~CAuthSessionFactory()
{
}

CNtlSession * CAuthSessionFactory::CreateSession(SESSIONTYPE sessionType)
{
	CNtlSession * pSession = NULL;
	switch (sessionType)
	{
		case SESSION_CLIENT:
		case SESSION_AUTH_SERVER:
		{
			pSession = new CAuthSession(sessionType);
		}
		break;
		case SESSION_MASTER_SERVER:
		{
			pSession = new CNtlSession(sessionType);
		}
		break;
		case SESSION_QUERY_SERVER:
		{
			pSession = new CNtlSession(sessionType);
		}
		break;
	default:
		break;
	}

	return pSession;
}



void CAuthSessionFactory::DestroySession(CNtlSession * pSession)
{
	switch (pSession->GetSessionType())
	{
	case SESSION_CLIENT:
	case SESSION_MASTER_SERVER:
	case SESSION_QUERY_SERVER:
	case SESSION_AUTH_SERVER:
	{
		SAFE_DELETE(pSession);
	}
	break;
	default:
		break;
	}
}