#include "stdafx.h"
#include "AuthServer.h"
#include "AuthSession.h"


CAuthSession::CAuthSession(bool bAliveCheck, bool bOpcodeCheck) :CNtlSession(SESSION_AUTH_SERVER)
{
	SetControlFlag(CONTROL_FLAG_USE_SEND_QUEUE);

	if (bAliveCheck)
	{
		SetControlFlag(CONTROL_FLAG_CHECK_ALIVE);
	}
	if (bOpcodeCheck)
	{
		SetControlFlag(CONTROL_FLAG_CHECK_OPCODE);
	}

	SetPacketEncoder(&m_packetEncoder);
}

//-----------------------------------------------------------------------------------
//		클라이언트 소멸
//-----------------------------------------------------------------------------------
CAuthSession::~CAuthSession()
{
	NTL_PRINT(PRINT_APP, "CAuthSession Destructor Called");

	CAuthServer * app = (CAuthServer*)NtlSfxGetApp();
	app->Remove(this->GetHandle());
}

int CAuthSession::OnAccept()
{
	NTL_PRINT(PRINT_APP, "%s", __FUNCTION__);	
	CAuthServer* app = (CAuthServer*)NtlSfxGetApp();
	for (int i = 0; i < app->GetNetwork()->GetSessionList()->GetMaxCount(); i++)
	{
		CNtlSession* first = app->GetNetwork()->GetSessionList()->Find(i);
		if (first)
		{
			if (first->GetSessionType() == SESSION_QUERY_SERVER)
			{
				app->SetQuerySession((CQuerySession*)first);
			}
			if (first->GetSessionType() == SESSION_MASTER_SERVER)
			{
				app->SetMasterSession((CMasterSession*)first);
			}
		}
	}
	if ((app->GetQuerySession() != NULL) && (!app->IsQueryServerConnected()))
	{
		CNtlPacket packetA(sizeof(sAQ_HEARTBEAT));
		sAQ_HEARTBEAT* msgA = (sAQ_HEARTBEAT *)packetA.GetPacketData();
		msgA->wOpCode = AQ_HEARTBEAT;
		packetA.SetPacketLen(sizeof(sAQ_HEARTBEAT));
		app->GetQuerySession()->PushPacket(&packetA);
		app->SetQueryServerConnected(true);
	}
	if ((app->GetMasterSession() != NULL) && (!app->IsMasterServerConnected()))
	{
		CNtlPacket packetM(sizeof(sAM_HEARTBEAT));
		sAM_HEARTBEAT* msgM = (sAM_HEARTBEAT*)packetM.GetPacketData();
		msgM->wOpCode = AM_HEARTBEAT;
		packetM.SetPacketLen(sizeof(sAM_HEARTBEAT));
		app->GetMasterSession()->PushPacket(&packetM);
		app->SetMasterServerConnected(true);
	}
	return NTL_SUCCESS;
}


void CAuthSession::OnClose()
{
	NTL_PRINT(PRINT_APP, "%s", __FUNCTION__);
}
int CAuthSession::OnConnect()
{
	NTL_PRINT(PRINT_APP, "%s", __FUNCTION__);
	CAuthServer* app = (CAuthServer*)NtlSfxGetApp();
	do{
		this->OnAccept();
		Sleep(5000);
	} while ((!app->GetQuerySession()) ||
		(!app->GetMasterSession()));
	return 0;
}
int CAuthSession::OnDispatch(CNtlPacket * pPacket)
{
	CAuthServer * app = (CAuthServer*)NtlSfxGetApp();
	if ((!app->GetQuerySession()) ||
		(!app->GetMasterSession()))
		this->OnConnect();

	sNTLPACKETHEADER * pHeader = (sNTLPACKETHEADER *)pPacket->GetPacketData();
	switch (pHeader->wOpCode)
	{
		case UA_LOGIN_REQ:
		{
			CAuthSession::SendAuthLoginEnterReq(pPacket);
		}
		break;
		case UA_LOGIN_DISCONNECT_REQ:
		{
			CAuthSession::SendAuthLoginDiscoReq(pPacket);
		}
		break;
		case QA_LOGIN_RES:
		{
			CAuthSession::SendAuthLoginRes(pPacket);
		}
		break;
		case QA_HEARTBEAT:
		{
			if (app->GetQuerySession() != NULL)
				app->GetQuerySession()->ResetAliveTime();
		}
		break;
		case QA_CHARACTER_EXIT:
		{
			CAuthSession::SendQueryLoginExit(pPacket);
		}
		break;
		case MA_HEARTBEAT:
		{
			if (app->GetMasterSession() != NULL)
				app->GetMasterSession()->ResetAliveTime();
		}
		break;
		default:
		{
			if (pHeader->wOpCode != SYS_ALIVE)
			{
				NTL_PRINT(PRINT_APP, "The following packet was not handled: %s", NtlGetPacketName(pHeader->wOpCode));
			}
			return CNtlSession::OnDispatch(pPacket);
		}
	}

	return NTL_SUCCESS;
}
void CAuthSession::SendAuthLoginEnterReq(CNtlPacket* pPacket)
{
	CAuthServer* app = (CAuthServer*)NtlSfxGetApp();
	sUA_LOGIN_REQ* req = (sUA_LOGIN_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sAQ_LOGIN_REQ));
	sAQ_LOGIN_REQ* res = (sAQ_LOGIN_REQ*)packet.GetPacketData();
	memcpy(res->awchUserId, req->awchUserId, NTL_MAX_SIZE_USERID_UNICODE+1);
	memcpy(res->awchPasswd, req->awchUserId, NTL_MAX_SIZE_USERID_UNICODE+1);
	memcpy(res->abyMacAddress, req->abyMacAddress, DBO_MAX_ADAPTER_ADDRESS_LENGTH);
	res->wOpCode = AQ_LOGIN_REQ;
	packet.SetPacketLen(sizeof(sAQ_LOGIN_REQ));
	app->AcquireQuerySession()->PushPacket(&packet);
	app->Add(req->awchUserId, this->GetHandle());
}
void CAuthSession::SendAuthLoginDiscoReq(CNtlPacket* pPacket)
{
	CAuthServer* app = (CAuthServer*)NtlSfxGetApp();
	sUA_LOGIN_DISCONNECT_REQ* req = (sUA_LOGIN_DISCONNECT_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sAU_LOGIN_DISCONNECT_RES));
	sAU_LOGIN_DISCONNECT_RES* res = (sAU_LOGIN_DISCONNECT_RES*)packet.GetPacketData();
	res->wOpCode = AU_LOGIN_DISCONNECT_RES;
	packet.SetPacketLen(sizeof(sAU_LOGIN_DISCONNECT_RES));
	app->Send(app->GetHSession(req->awchUserId), &packet);
	if (!req->bIsEnteringCharacterServer)
		app->RemoveUserLogged(0, req->awchUserId);
}
void CAuthSession::SendAuthLoginRes(CNtlPacket* pPacket)
{
	CAuthServer* app = (CAuthServer*)NtlSfxGetApp();
	sQA_LOGIN_RES* req = (sQA_LOGIN_RES*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sAU_LOGIN_RES));
	sAU_LOGIN_RES* res = (sAU_LOGIN_RES*)packet.GetPacketData();
	strcpy_s((char*)res->abyAuthKey, NTL_MAX_SIZE_AUTH_KEY, "Dbo");
	res->accountId = req->accountId;
	wcsncpy_s(res->awchUserId, req->awchUserId, NTL_MAX_SIZE_USERID_UNICODE + 1);
	res->lastServerFarmId = req->lastServerFarmId;
	res->byServerInfoCount = app->byServerInfoCount;
	if (app->GetConfigFileEnabledMultipleServers() == "true")
	{
		for (int i = 0; i < 10; i++)
		{
			res->aServerInfo[i] = app->aServerInfo[i];
		}
	}
	else
	{
		memcpy(res->aServerInfo, app->aServerInfo, sizeof(res->aServerInfo));
	}
	if(req->wResultCode == AUTH_SUCCESS)
	{
		if (app->AddUserLogged(req->accountId,req->awchUserId))
			res->wResultCode = req->wResultCode;
		else
			res->wResultCode = AUTH_USER_EXIST_IN_CHARACTER_SERVER;
	}
	else
		res->wResultCode = req->wResultCode;
	res->wOpCode = AU_LOGIN_RES;	
	packet.SetPacketLen(sizeof(sAU_LOGIN_RES));
	app->Send(app->GetHSession(req->awchUserId), &packet);	
}

void CAuthSession::SendQueryLoginExit(CNtlPacket* pPacket)
{
	sQA_CHARACTER_EXIT* req = (sQA_CHARACTER_EXIT*)pPacket->GetPacketData();
	CAuthServer* app = (CAuthServer*)NtlSfxGetApp();
	app->RemoveUserLogged(req->accountId,NULL);
}