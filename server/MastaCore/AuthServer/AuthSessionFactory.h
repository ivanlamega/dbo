#pragma once
#include "AuthServer.h"
#include "NtlSessionFactory.h"

#ifndef CAUTHSESSION_FACTORY_H
#define CAUTHSESSION_FACTORY_H

class CAuthSessionFactory : public CNtlSessionFactory
{
public:
	CAuthSessionFactory();
	~CAuthSessionFactory();
	CNtlSession * CreateSession(SESSIONTYPE sessionType);
	void DestroySession(CNtlSession * pSession);
};
#endif

