#pragma once
#include "AuthServer.h"
#include "NtlSession.h"
#include "NtlPacketEncoder_RandKey.h"
///////////////////////////////////////RESPONSE PACKETS///////////////////////////
//Auth Server -> Master Server
#include "NtlPacketAM.h"
//Auth Server -> Operating Server
#include "NtlPacketAP.h"
//Auth Server -> Client(EXE)
#include "NtlPacketAU.h"
//Auth Server -> Query Server
#include "NtlPacketAQ.h"
/////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////REQUEST PACKETS///////////////////////////
//Master Server -> Auth Server
#include "NtlPacketMA.h"
//Operating Server -> Auth Server
#include "NtlPacketPA.h"
//Client(EXE) -> Auth Server
#include "NtlPacketUA.h"
//Query Server -> Auth Server
#include "NtlPacketQA.h"
/////////////////////////////////////////////////////////////////////////////////

#include "NtlPacketUtil.h"

#ifndef CAUTHSESSION_H
#define CAUTHSESSION_H

class CClientSession :	public CNtlSession
{
public:
	CClientSession(bool bAliveCheck = false, bool bOpcodeCheck = false);
	~CClientSession();
	int							OnAccept();
	void						OnClose();
	int							OnDispatch(CNtlPacket * pPacket);
	// Packet functions
	void						SendAuthLoginEnterReq(CNtlPacket* pPacket);
	void						SendAuthLoginDiscoReq(CNtlPacket* pPacket);
	void						SendAuthLoginRes(CNtlPacket* pPacket);
	// End Packet functions
private:
	CNtlPacketEncoder_RandKey	m_packetEncoder;
};

#endif