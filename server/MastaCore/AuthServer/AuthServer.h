#pragma once
#include "NtlSfx.h"
#include "AuthSession.h"
#include "AuthSessionFactory.h"

#ifndef CAUTH_SERVER_H
#define CAUTH_SERVER_H

class CMasterSession;
class CQuerySession;

#include "MasterSession.h"
#include "QuerySession.h"

class CAuthServer :	public CNtlServerApp
{
	enum STATE
	{
		MASTER_SERVER_STATE_CONNECTED,
		MASTER_SERVER_STATE_JOINED,
		MASTER_SERVER_STATE_DISCONNECTED,
		OPERATING_SERVER_STATE_CONNECTED,
		OPERATING_SERVER_STATE_JOINED,
		OPERATING_SERVER_STATE_DISCONNECTED,
		QUERY_SERVER_STATE_CONNECTED,
		QUERY_SERVER_STATE_JOINED,
		QUERY_SERVER_STATE_DISCONNECTED,
	};
	//Members
public:
	CNtlString				ServersConfig[99][2];//For Config of multiple server 0->{Ip,Port} - Luiz45
	BYTE					byServerInfoCount;
	sSERVER_INFO			aServerInfo[10];
private:	
	//Auth Session ptr refresh
	CAuthSession*				m_pAuthSession;
	//Client Session
	CAuthSession*				m_pClientSession;
	//Master Server Connection
	CNtlConnector				m_masterServerConnector;
	CNtlMutex					m_masterServerMutex;
	CMasterSession*				m_pMasterServerSession;
	//Operating Server Connection
	CNtlConnector				m_operatingServerConnector;
	CNtlMutex					m_operatingServerMutex;
	//CServerSession *			m_pOperatingServerSession;
	//Query Server Connection
	CNtlConnector				m_queryServerConnector;
	CNtlMutex					m_queryServerMutex;
	CQuerySession*			    m_pQueryServerSession;

	CNtlAcceptor			m_clientAcceptor;
	CNtlLog  				m_log;
	sSERVERCONFIG			m_config;
	CNtlString				EnableMultipleServers;//Added for enabling multiple server - Luiz45

	typedef std::map<WCHAR*, ACCOUNTID> LOGGED_USERS;
	typedef std::map<WCHAR*, ACCOUNTID>::iterator LOGGED_USERS_IT;

	AUTH_CLIENT_SESSION		m_sessionList;
	LOGGED_USERS			m_userList;//User in Char server
	//Methods
	//Connected
	bool					m_bMasterServerConnected;
	bool					m_bQueryServerConnected;
	void					DoAliveTime();
public:	
	//For Multiple Server - Luiz45
	const std::string		GetConfigFileEnabledMultipleServers();
	const DWORD				GetConfigFileMaxServers();	
	int						OnInitApp();
	int						OnCreate();
	void					OnDestroy();
	int						OnCommandArgument(int argc, _TCHAR* argv[]);
	int						OnConfiguration(const char * lpszConfigFile);
	int						OnAppStart();
	int						GetSessionCount();
	void					Run();
	bool					Add(WCHAR* userAcc,HSESSION hSession);
	void					Remove(WCHAR* userAcc);
	void					Remove(HSESSION session);
	bool					Find(WCHAR* userAcc);
	HSESSION				GetHSession(WCHAR* userAcc);
	bool					AddUserLogged(ACCOUNTID accID, WCHAR* userName);
	bool					IsUserLogged(ACCOUNTID accID);
	void					RemoveUserLogged(ACCOUNTID accID, WCHAR* userName);
	void					SetMasterSession(CMasterSession* pServerSession);
	CMasterSession*			AcquireMasterSession();
	CMasterSession*			GetMasterSession();
	void					SetQuerySession(CQuerySession* pServerSession);
	CQuerySession*			AcquireQuerySession();
	CQuerySession*			GetQuerySession();
	void					SetClientSession(CAuthSession* pClientSession);
	CAuthSession*			GetClientSession();
	void					SetMasterServerConnected(bool c){ m_bMasterServerConnected = c; }
	void					SetQueryServerConnected(bool c){ m_bQueryServerConnected = c; }
	bool					IsMasterServerConnected(){ return m_bMasterServerConnected; }
	bool					IsQueryServerConnected(){ return m_bQueryServerConnected; }
};

#endif