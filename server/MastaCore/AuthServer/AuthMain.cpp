// AuthMain.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "NtlSfx.h"
#include "AuthServer.h"
#include "NtlFile.h"


int _tmain(int argc, _TCHAR* argv[])
{
	NtlSetPrintFlag(PRINT_APP);

	CAuthServer app;
	CNtlFileStream traceFileStream;

	// LOG FILE
	std::cout << "Loading Server.ini... \n";
	int rc = traceFileStream.Create("AuthServerLog");
	rc = app.Create(argc, argv, ".\\Server.ini");
	NtlSetPrintStream(traceFileStream.GetFilePtr());
	NtlSetPrintFlag(PRINT_APP | PRINT_SYSTEM);

	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "log file CreateFile error %d(%s)", rc, NtlGetErrorMessage(rc));
		return rc;
	}
	std::cout << "Loaded!\n";

	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "Server Application Create Fail %d(%s)", rc, NtlGetErrorMessage(rc));
		return rc;
	}
	app.Start();
	std::cout << "Running... \n";
	app.WaitForTerminate();


	return 0;
}

