#pragma once
#include "AuthServer.h"
#include "NtlSession.h"
#include "NtlPacketEncoder_RandKey.h"
///////////////////////////////////////RESPONSE PACKETS///////////////////////////
//Auth Server -> Master Server
#include "NtlPacketAM.h"
//Auth Server -> Operating Server
#include "NtlPacketAP.h"
//Auth Server -> Client(EXE)
#include "NtlPacketAU.h"
//Auth Server -> Query Server
#include "NtlPacketAQ.h"
/////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////REQUEST PACKETS///////////////////////////
//Master Server -> Auth Server
#include "NtlPacketMA.h"
//Operating Server -> Auth Server
#include "NtlPacketPA.h"
//Client(EXE) -> Auth Server
#include "NtlPacketUA.h"
//Query Server -> Auth Server
#include "NtlPacketQA.h"
/////////////////////////////////////////////////////////////////////////////////
#include "NtlPacketSYS.h"
#include "NtlPacketUtil.h"

class CAuthSession :	public CNtlSession
{
public:
	CAuthSession(bool bAliveCheck = true, bool bOpcodeCheck = true);
	~CAuthSession();
	int							OnAccept();
	void						OnClose();
	int							OnConnect();
	int							OnDispatch(CNtlPacket * pPacket);
	// Packet functions
	void						SendAuthLoginEnterReq(CNtlPacket* pPacket);
	void						SendAuthLoginDiscoReq(CNtlPacket* pPacket);
	void						SendAuthLoginRes(CNtlPacket* pPacket);
	void						SendQueryLoginExit(CNtlPacket* pPacket);
	// End Packet functions
private:
	CNtlPacketEncoder_RandKey	m_packetEncoder;
};