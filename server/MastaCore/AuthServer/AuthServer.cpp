#include "stdafx.h"
#include "AuthServer.h"

//For Multiple Server - Luiz45
const std::string CAuthServer::GetConfigFileEnabledMultipleServers()
{
	return EnableMultipleServers.GetString();
}
const DWORD CAuthServer::GetConfigFileMaxServers()
{
	return MAX_NUMOF_AUTH_SERVER;
}
int	CAuthServer::OnInitApp()
{
	m_nMaxSessionCount = MAX_NUMOF_AUTH_SESSION;

	m_pSessionFactory = new CAuthSessionFactory;
	if (NULL == m_pSessionFactory)
	{
		return NTL_ERR_SYS_MEMORY_ALLOC_FAIL;
	}
	m_pMasterServerSession = NULL;
	m_pQueryServerSession = NULL;
	m_pAuthSession = (CAuthSession*)m_pSessionFactory->CreateSession(SESSION_AUTH_SERVER);
	return NTL_SUCCESS;
}

int	CAuthServer::OnCreate()
{
	int rc = NTL_SUCCESS;

	rc = m_clientAcceptor.Create(m_config.strAuthServerIP.c_str(), m_config.wAuthServerPort, SESSION_AUTH_SERVER,
		MAX_NUMOF_AUTH_GAME_CLIENT, 5, 15, MAX_NUMOF_AUTH_GAME_CLIENT);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_clientAcceptor, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_masterServerConnector.Create(m_config.strMasterServerIP.c_str(), m_config.wMasterServerPort, SESSION_MASTER_SERVER);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_masterServerConnector, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_queryServerConnector.Create(m_config.strQueryServerIP.c_str(), m_config.wQueryServerPort, SESSION_QUERY_SERVER);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_queryServerConnector, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	return NTL_SUCCESS;

}

void	CAuthServer::OnDestroy()
{
	SAFE_DELETE(m_pSessionFactory);
}

int	CAuthServer::OnCommandArgument(int argc, _TCHAR* argv[])
{
	return NTL_SUCCESS;
}

int	CAuthServer::OnConfiguration(const char * lpszConfigFile)
{
	CNtlIniFile file;

	int rc = file.Create(lpszConfigFile);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}
	//For multiple servers - Luiz45
	if (!file.Read("ServerOptions", "EnableMultipleServers", EnableMultipleServers))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	CNtlString sTmpCountServer;
	if (!file.Read("ServerOptions", "MaxServerAllowed", sTmpCountServer))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (GetConfigFileEnabledMultipleServers() == "true")
	{
		int maxServer = atoi(sTmpCountServer.c_str());
		int i = 0;
		for (i = 0; i < 10;i++)
		{
			if (!file.Read("CharServer" + i, "Address", ServersConfig[i][0]))
			{
				return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
			}
			if (!file.Read("CharServer" + 1, "Port", ServersConfig[i][1]))
			{
				return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
			}
			strcpy_s(aServerInfo[i].szCharacterServerIP, NTL_MAX_LENGTH_OF_IP, ServersConfig[i][0].c_str());
			aServerInfo[i].wCharacterServerPortForClient = atoi(ServersConfig[i][1].GetString().c_str());
			aServerInfo[i].dwLoad = 0;
		}
		byServerInfoCount = i;
		
	}
	else
	{
		if (!file.Read("CharServer", "Address", m_config.strCharServerIP))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("CharServer", "Port", m_config.wCharServerPort))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		byServerInfoCount = 1;
		strcpy_s(aServerInfo[0].szCharacterServerIP, NTL_MAX_LENGTH_OF_IP, m_config.strCharServerIP.c_str());
		aServerInfo[0].wCharacterServerPortForClient = m_config.wCharServerPort;
		aServerInfo[0].dwLoad = 0;
	}
	if (!file.Read("MasterServer", "Address", m_config.strMasterServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("MasterServer", "Port", m_config.wMasterServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("AuthServer", "Address", m_config.strAuthServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("AuthServer", "Port", m_config.wAuthServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("OperatingServer", "Address", m_config.strOperatingServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("OperatingServer", "Port", m_config.wOperatingServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("QueryServer", "Address", m_config.strQueryServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("QueryServer", "Port", m_config.wQueryServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	return NTL_SUCCESS;
}

int	CAuthServer::OnAppStart()
{
	return NTL_SUCCESS;
}

void	CAuthServer::Run()
{
	DWORD dwTickCur, dwTickOld = ::GetTickCount();

	while (IsRunnable())
	{
		dwTickCur = ::GetTickCount();		
		if (dwTickCur - dwTickOld >= 100000)
		{
			//NTL_PRINT(PRINT_APP, "Auth Server Run()");
			DoAliveTime();
			dwTickOld = dwTickCur;
			if (!m_pQueryServerSession)
				this->m_pAuthSession->OnAccept();
			if (!m_pMasterServerSession)
				this->m_pAuthSession->OnAccept();
		}
		Sleep(2);
		DoAliveTime();
	}
}

bool	CAuthServer::Add(WCHAR* userAcc, HSESSION hSession)
{
	if (Find(userAcc))
		return false;

	m_sessionList.insert(std::pair<WCHAR*, HSESSION>(userAcc, hSession));

	return true;
}

void	CAuthServer::Remove(WCHAR* userAcc)
{
	for (AUTH_CLIENT_IT it = m_sessionList.begin(); it != m_sessionList.end(); it++)
	{
		if (wcscmp(userAcc, it->first) == NTL_SUCCESS)
		{
			m_sessionList.erase(it->first);
			break;
		}
	}
}

void	CAuthServer::Remove(HSESSION session)
{
	for (AUTH_CLIENT_IT it = m_sessionList.begin(); it != m_sessionList.end(); it++)
	{
		if (session == it->second)
		{
			m_sessionList.erase(it);
			break;
		}
	}
}

bool	CAuthServer::Find(WCHAR* accUser)
{
	for (AUTH_CLIENT_IT it = m_sessionList.begin(); it != m_sessionList.end(); it++)
	{
		if (wcscmp(accUser, it->first) == NTL_SUCCESS)
		{
			return true;
		}
	}

	return false;
}

HSESSION CAuthServer::GetHSession(WCHAR* accUser)
{
	for (AUTH_CLIENT_IT it = m_sessionList.begin(); it != m_sessionList.end(); it++)
	{
		if (wcscmp(accUser, it->first) == NTL_SUCCESS)
		{
			return it->second;
		}
	}
	return INVALID_HSESSION;
}

bool	CAuthServer::AddUserLogged(ACCOUNTID accID, WCHAR* userName)
{
	if (IsUserLogged(accID))
		return false;
	m_userList.insert(std::pair<WCHAR*, ACCOUNTID>(userName, accID));
	return true;
}
bool	CAuthServer::IsUserLogged(ACCOUNTID accID)
{
	for (LOGGED_USERS_IT it = m_userList.begin(); it != m_userList.end(); it++)
	{
		if (accID == it->second)
			return true;
	}
	return false;
}
void	CAuthServer::RemoveUserLogged(ACCOUNTID accID, WCHAR* userName)
{
	for (LOGGED_USERS_IT it = m_userList.begin(); it != m_userList.end(); ++it)
	{
		if (NULL != userName)
		{
			if (wcscmp(userName, it->first) == NTL_SUCCESS)
			{
				m_userList.erase(it->first);
				break;
			}
		}
		else
		{
			if (accID == it->second)
			{
				m_userList.erase(it->first);
				break;
			}
		}
	}
}

void	CAuthServer::SetMasterSession(CMasterSession * pServerSession)
{
	CNtlAutoMutex mutex(&m_masterServerMutex);
	mutex.Lock();

	if (NULL == pServerSession)
	{
		m_pMasterServerSession->Release();
		m_pMasterServerSession = pServerSession;
	}
	else
	{
		pServerSession->Acquire();
		m_pMasterServerSession = pServerSession;
	}
}

CMasterSession * CAuthServer::AcquireMasterSession()
{
	CNtlAutoMutex mutex(&m_masterServerMutex);
	mutex.Lock();

	if (NULL == m_pMasterServerSession) return NULL;

	m_pMasterServerSession->Acquire();
	return m_pMasterServerSession;
}

CMasterSession * CAuthServer::GetMasterSession()
{
	return m_pMasterServerSession;
}
void	CAuthServer::SetQuerySession(CQuerySession * pServerSession)
{
	CNtlAutoMutex mutex(&m_queryServerMutex);
	mutex.Lock();

	if (NULL == pServerSession)
	{
		m_pQueryServerSession->Release();
		m_pQueryServerSession = pServerSession;
	}
	else
	{
		pServerSession->Acquire();
		m_pQueryServerSession = pServerSession;
	}
}

CQuerySession * CAuthServer::AcquireQuerySession()
{
	CNtlAutoMutex mutex(&m_queryServerMutex);
	mutex.Lock();

	if (NULL == m_pQueryServerSession) return NULL;

	m_pQueryServerSession->Acquire();
	return m_pQueryServerSession;
}
CQuerySession * CAuthServer::GetQuerySession()
{	
	return m_pQueryServerSession;
}
void	CAuthServer::SetClientSession(CAuthSession* pSession)
{
	this->m_pClientSession = pSession;
}
CAuthSession* CAuthServer::GetClientSession()
{
	return this->m_pClientSession;
}
void CAuthServer::DoAliveTime()
{
	for (int i = 0; i < this->GetNetwork()->GetSessionList()->GetMaxCount(); i++)
	{
		CNtlSession* first = this->GetNetwork()->GetSessionList()->Find(i);
		if (first)
		{
			if (first->GetSessionType() != SESSION_AUTH_SERVER)
			{
				PACKETDATA res;
				res.wOpCode = SYS_ALIVE;
				CNtlPacket packet((BYTE*)&res, sizeof(res));
				first->PushPacket(&packet);
			}
		}
	}
}