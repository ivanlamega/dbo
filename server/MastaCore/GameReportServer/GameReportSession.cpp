#include "stdafx.h"
#include "GameReportServer.h"
#include "GameReportSession.h"


CGameReportSession::CGameReportSession(bool bAliveCheck, bool bOpcodeCheck) :CNtlSession(SESSION_CLIENT)
{
	SetControlFlag(CONTROL_FLAG_USE_SEND_QUEUE);

	if (bAliveCheck)
	{
		SetControlFlag(CONTROL_FLAG_CHECK_ALIVE);
	}
	if (bOpcodeCheck)
	{
		SetControlFlag(CONTROL_FLAG_CHECK_OPCODE);
	}

	SetPacketEncoder(&m_packetEncoder);
}

//-----------------------------------------------------------------------------------
//		클라이언트 소멸
//-----------------------------------------------------------------------------------
CGameReportSession::~CGameReportSession()
{
	NTL_PRINT(PRINT_APP, "CClientSession Destructor Called");

	CBotServer * app = (CBotServer*)NtlSfxGetApp();
	app->Remove(this->GetHandle());
}

int CGameReportSession::OnAccept()
{
	NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
	return NTL_SUCCESS;
}


void CGameReportSession::OnClose()
{
	NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
}

int CGameReportSession::OnDispatch(CNtlPacket * pPacket)
{
	CBotServer * app = (CBotServer*)NtlSfxGetApp();

	sNTLPACKETHEADER * pHeader = (sNTLPACKETHEADER *)pPacket->GetPacketData();
	switch (pHeader->wOpCode)
	{
		default:
		{
			NTL_PRINT(PRINT_APP, "The following packet was not handled: %s", NtlGetPacketName(pHeader->wOpCode));
			return CNtlSession::OnDispatch(pPacket);
		}
	}

	return NTL_SUCCESS;
}