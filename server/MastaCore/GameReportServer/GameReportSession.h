#pragma once
#include "BotServer.h"
#include "NtlSession.h"
#include "NtlPacketEncoder_RandKey.h"
///////////////////////////////////////RESPONSE PACKETS///////////////////////////
//Game Report Server -> Master Server
#include "NtlPacketRM.h"
/////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////REQUEST PACKETS///////////////////////////
//Master Server -> Game Report Server
#include "NtlPacketMR.h"
/////////////////////////////////////////////////////////////////////////////////
#include "NtlPacketUtil.h"

#ifndef CBOTSESSION_H
#define CBOTSESSION_H

class CGameReportSession :	public CNtlSession
{
public:
	CGameReportSession(bool bAliveCheck = true, bool bOpcodeCheck = true);
	~CGameReportSession();
	int							OnAccept();
	void						OnClose();
	int							OnDispatch(CNtlPacket * pPacket);
	// Packet functions
	void						SendBotAgentEnterReq(CNtlPacket* pPacket);
	// End Packet functions
private:
	CNtlPacketEncoder_RandKey	m_packetEncoder;
};

#endif