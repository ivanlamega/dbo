// GameMain.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "NtlSfx.h"
#include "NtlFile.h"
#include "NtlFileSerializer.h"
#include "NtlStringHandler.h"
#include "NtlBitFlagManager.h"
#include "TableContainer.h"
#include "NtlBitFlagManager.h"
#include "TableFileNameList.h"
//List Tables
#include "ExpTable.h"
#include "HelpTable.h"
#include "MerchantTable.h"
#include "MobTable.h"
#include "NewbieTable.h"
#include "NPCTable.h"
#include "PCTable.h"
#include "StatusTransformTable.h"
#include "GameManiaTimeTable.h"
#include "GuideHintTable.h"
#include "BasicDropTable.h"
#include "ItemOptionTable.h"
#include "ItemTable.h"
#include "NormalDropTable.h"
#include "LegendaryDropTable.h"
#include "SuperiorDropTable.h"
#include "UseItemTable.h"
#include "SetItemTable.h"
#include "EachDropTable.h"
#include "TypeDropTable.h"
#include "ExcellentDropTable.h"
#include "DragonBallTable.h"
#include "DragonBallRewardTable.h"
#include "ActionTable.h"
#include "ChatCommandTable.h"
#include "DirectionLinkTable.h"
#include "FormulaTable.h"
#include "CharmTable.h"
#include "QuestDropTable.h"
#include "QuestItemTable.h"
#include "QuestProbabilityTable.h"
#include "QuestTextDataTable.h"
#include "QuestRewardTable.h"
#include "HTBSetTable.h"
#include "SkillTable.h"
#include "SystemEffectTable.h"
#include "TextAllTable.h"
#include "ChattingFilterTable.h"
#include "TextServerTable.h"
#include "ObjectTable.h"
#include "SpawnTable.h"
#include "WorldTable.h"
#include "WorldZoneTable.h"
#include "WorldMapTable.h"
#include "LandMarkTable.h"
#include "WorldPathTable.h"
#include "WorldPlayTable.h"
#include "PortalTable.h"
#include "NpcSpeechTable.h"
#include "SetItemTable.h"
#include "TimeQuestTable.h"
#include "RankBattleTable.h"
#include "BudokaiTable.h"
#include "ScriptLinkTable.h"
#include "DungeonTable.h"
#include "ModelToolCharDataTable.h"
#include "ModelToolObjDataTable.h"
#include "QuestNarrationTable.h"
#include "VehicleTable.h"
#include "ItemRecipeTable.h"
#include "DynamicObjectTable.h"
#include "MobMovePatternTable.h"
#include "DojoTable.h"
#include "ItemUpgradeTable.h"
#include "ItemMixMachineTable.h"
#include "HLSItemTable.h"
#include "HLSMerchantTable.h"
//END TABLES


int _tmain(int argc, char* argv[])
{
	std::cout << "EDF To RDF File Decrypter by -Luiz45\n";
	std::cout << "REMEMBER: The decrypted files will be at 'decrypted\\file' \n";	
	std::cout << "REMEMBER2: Make sure you have two dir called 'encrypted' 'decrypted' if you dont have it create NOW! \n";
	std::cout << "Loading....\n";
	CNtlFileSerializer serializer;
	serializer.Refresh();																	
	if (true)
	{							
		std::wstring wstrFullPathDecrypt;																				
		wstrFullPathDecrypt += L"decrypted";
		wstrFullPathDecrypt += L"\\";
		wstrFullPathDecrypt += Ntl_MB2WC(argv[1]);
		wstrFullPathDecrypt += L".rdf";
		std::wstring wstrFullPathEncrypt;
		wstrFullPathEncrypt += L"encrypted";
		wstrFullPathEncrypt += L"\\";
		wstrFullPathEncrypt += Ntl_MB2WC(argv[1]);
		wstrFullPathEncrypt += L".edf";
		std::ifstream infile((WCHAR*)wstrFullPathEncrypt.c_str());
		bool exis = infile.good();
		std::ifstream infile2((WCHAR*)wstrFullPathDecrypt.c_str());
		bool ex = infile2.good();
		serializer.LoadFile((WCHAR*)wstrFullPathEncrypt.c_str(), true, L"KEY_FOR_GAME_DATA_TABLE");
		serializer.SaveFile((WCHAR*)wstrFullPathDecrypt.c_str(), false);		
	}																					
	else																				
	{																					
		std::string filename;
		filename += ".edf";
		CNtlFileSerializer dataSerializer;		
		int nDataSize = dataSerializer.GetDataSize();										
		serializer << nDataSize;															
		serializer.In(dataSerializer.GetData(), nDataSize);									
		serializer.SaveFile((WCHAR*)(filename.c_str()), true, L"KEY_FOR_GAME_DATA_TABLE");
	}
	std::cout << "WELL DONE!\n";
	system("pause");

	return 0;
}

