-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.5.34 - MySQL Community Server (GPL)
-- OS do Servidor:               Win32
-- HeidiSQL Versão:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura do banco de dados para mastacore
CREATE DATABASE IF NOT EXISTS `mastacore` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `mastacore`;


-- Copiando estrutura para tabela mastacore.account
CREATE TABLE IF NOT EXISTS `account` (
  `AccountID` int(10) NOT NULL AUTO_INCREMENT,
  `Username` varchar(13) DEFAULT NULL,
  `Password` varchar(13) DEFAULT NULL,
  `LastServer` int(10) DEFAULT NULL,
  `AdminAcc` int(1) DEFAULT '0',
  PRIMARY KEY (`AccountID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mastacore.account: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` (`AccountID`, `Username`, `Password`, `LastServer`, `AdminAcc`) VALUES
	(1, '1', '1', 0, 1);
INSERT INTO `account` (`AccountID`, `Username`, `Password`, `LastServer`, `AdminAcc`) VALUES
	(2, '2', '2', 0, 0);
INSERT INTO `account` (`AccountID`, `Username`, `Password`, `LastServer`, `AdminAcc`) VALUES
	(3, '3', '3', 0, 0);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;


-- Copiando estrutura para tabela mastacore.bufflist
CREATE TABLE IF NOT EXISTS `bufflist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `charId` int(11) DEFAULT NULL,
  `dwTblidx` int(255) DEFAULT NULL,
  `dwInitialDuration` int(255) DEFAULT NULL,
  `dwTimeRemaining` int(255) DEFAULT NULL,
  `afEffectValue0` float(255,0) DEFAULT NULL,
  `afEffectValue1` float(255,0) DEFAULT NULL,
  `byBuffGroup` int(255) DEFAULT NULL,
  `bySourceType` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mastacore.bufflist: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `bufflist` DISABLE KEYS */;
/*!40000 ALTER TABLE `bufflist` ENABLE KEYS */;


-- Copiando estrutura para procedure mastacore.CharacterCreate
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `CharacterCreate`(IN `m_dwAccountID` int,IN `m_dwServerID` int,IN `m_awchName` varchar(17),IN `m_byRace` int,IN `m_byClass` int,IN `m_byGender` int,IN `m_byFace` int,IN `m_byHair` int,IN `m_byHairColor` int,IN `m_bySkinColor` int,IN `m_byLevel` int,IN `m_byBindType` int,IN `m_byBlood` int,IN `m_byMarking` int,IN `m_bIsAdult` tinyint,IN `m_worldTblidx` int,IN `m_worldId` int,IN `m_bTutorialFlag` tinyint,IN `m_bNeedNameChange` tinyint,IN `m_bChangeClass` tinyint,IN `m_fPositionX` double,IN `m_fPositionY` double,IN `m_fPositionZ` double,IN `m_fDirX` double,IN `m_fDirY` double,IN `m_fDirZ` double,IN `m_fBindPositionX` double,IN `m_fBindPositionY` double,IN `m_fBindPositionZ` double,IN `m_fBindDirX` double,IN `m_fBindDirY` double,IN `m_fBindDirZ` double,IN `m_dwBindObjectTblidx` int,IN `m_dwBindWorldID` int,IN `m_dwMoney` int,IN `m_dwMoneyBank` int,IN `m_dwMapInfoIndex` int,IN `m_dwLP` int,IN `m_dwEP` int,IN `m_dwRP` int,IN `m_dwExp` int,IN `m_dwReputationPoint` int,IN `m_dwMudosaPoint` int,IN `m_dwSpPoint` int,IN `m_dwTutorialHint` int)
BEGIN
	update charCreated	set charID = (select generatedID from (select max(charID)+1 as generatedID from charCreated) as a);
	INSERT INTO characters(AccountID,
							ServerID,
							Online,
							awchName,
							byRace,
							byClass,
							byGender,
							byFace,
							byHair,
							byHairColor,
							bySkinColor,
							byLevel,
							byBindType,
							byBlood,
							byMarking,
							bIsAdult,
							worldTblidx,
							worldId,
							bTutorialFlag,
							bNeedNameChange,
							bChangeClass,
							fPositionX,
							fPositionY,
							fPositionZ,
							fDirX,
							fDirY,
							fDirZ,
							fBindPositionX,
							fBindPositionY,
							fBindPositionZ,
							fBindDirX,
							fBindDirY,
							fBindDirZ,
							dwBindObjectTblidx,
							dwBindWorldID,
							dwMoney,
							dwMoneyBank,
							dwMapInfoIndex,
							dwLP,
							dwEP,
							dwRP,
							dwExp,
							dwReputationPoint,
							dwMudosaPoint,
							dwSpPoint,
							dwTutorialHint)
	VALUES(m_dwAccountID,
			m_dwServerID,
			0,
			m_awchName,
			m_byRace,
			m_byClass,
			m_byGender,
			m_byFace,
			m_byHair,
			m_byHairColor,
			m_bySkinColor,
			m_byLevel,
			m_byBindType,
			m_byBlood,
			m_byMarking,
			m_bIsAdult,
			m_worldTblidx,
			m_worldId,
			m_bTutorialFlag,
			m_bNeedNameChange,
			m_bChangeClass,
			m_fPositionX,
			m_fPositionY,
			m_fPositionZ,
			m_fDirX,
			m_fDirY,
			m_fDirZ,
			m_fBindPositionX,
			m_fBindPositionY,
			m_fBindPositionZ,
			m_fBindDirX,
			m_fBindDirY,
			m_fBindDirZ,
			m_dwBindObjectTblidx,
			m_dwBindWorldID,
			m_dwMoney,
			m_dwMoneyBank,
			m_dwMapInfoIndex,
			m_dwLP,
			m_dwEP,
			m_dwRP,
			m_dwExp,
			m_dwReputationPoint,
			m_dwMudosaPoint,
			m_dwSpPoint,
			m_dwTutorialHint);	
END//
DELIMITER ;


-- Copiando estrutura para tabela mastacore.characters
CREATE TABLE IF NOT EXISTS `characters` (
  `CharID` int(11) NOT NULL AUTO_INCREMENT,
  `AccountID` int(11) DEFAULT NULL,
  `ServerID` int(10) DEFAULT NULL,
  `awchName` varchar(17) DEFAULT NULL,
  `byRace` int(10) DEFAULT NULL,
  `byClass` int(10) DEFAULT NULL,
  `byGender` int(10) DEFAULT NULL,
  `byFace` int(10) DEFAULT NULL,
  `byHair` int(10) DEFAULT NULL,
  `byHairColor` int(10) DEFAULT NULL,
  `bySkinColor` int(10) DEFAULT NULL,
  `byLevel` int(90) DEFAULT NULL,
  `byBindType` int(10) DEFAULT NULL,
  `byBlood` int(10) DEFAULT NULL,
  `byMarking` int(10) DEFAULT NULL,
  `bIsAdult` tinyint(2) DEFAULT '0',
  `worldTblidx` int(255) DEFAULT NULL,
  `worldId` int(255) DEFAULT NULL,
  `bTutorialFlag` tinyint(2) DEFAULT '1',
  `bNeedNameChange` tinyint(2) DEFAULT '0',
  `bChangeClass` tinyint(2) DEFAULT '0',
  `fPositionX` double(150,0) DEFAULT NULL,
  `fPositionY` double(150,0) DEFAULT NULL,
  `fPositionZ` double(150,0) DEFAULT NULL,
  `fDirX` double(150,0) DEFAULT NULL,
  `fDirY` double(150,0) DEFAULT NULL,
  `fDirZ` double(150,0) DEFAULT NULL,
  `fBindPositionX` double(150,0) DEFAULT NULL,
  `fBindPositionY` double(150,0) DEFAULT NULL,
  `fBindPositionZ` double(150,0) DEFAULT NULL,
  `fBindDirX` double(150,0) DEFAULT NULL,
  `fBindDirY` double(150,0) DEFAULT NULL,
  `fBindDirZ` double(150,0) DEFAULT NULL,
  `dwBindObjectTblidx` int(255) DEFAULT NULL,
  `dwBindWorldID` int(255) DEFAULT NULL,
  `dwMoney` int(255) DEFAULT NULL,
  `dwMoneyBank` int(255) DEFAULT NULL,
  `dwMapInfoIndex` int(255) DEFAULT NULL,
  `dwLP` int(255) DEFAULT NULL,
  `dwEP` int(255) DEFAULT NULL,
  `dwRP` int(255) DEFAULT NULL,
  `dwExp` int(255) DEFAULT NULL,
  `dwReputationPoint` int(255) DEFAULT NULL,
  `dwMudosaPoint` int(255) DEFAULT NULL,
  `dwSpPoint` int(255) DEFAULT NULL,
  `dwTutorialHint` int(255) DEFAULT NULL,
  `Online` smallint(2) DEFAULT NULL,
  `dwNetPy` int(11) DEFAULT '0',
  PRIMARY KEY (`CharID`),
  KEY `AccountID` (`AccountID`),
  CONSTRAINT `characters_ibfk_1` FOREIGN KEY (`AccountID`) REFERENCES `account` (`AccountID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mastacore.characters: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `characters` DISABLE KEYS */;
INSERT INTO `characters` (`CharID`, `AccountID`, `ServerID`, `awchName`, `byRace`, `byClass`, `byGender`, `byFace`, `byHair`, `byHairColor`, `bySkinColor`, `byLevel`, `byBindType`, `byBlood`, `byMarking`, `bIsAdult`, `worldTblidx`, `worldId`, `bTutorialFlag`, `bNeedNameChange`, `bChangeClass`, `fPositionX`, `fPositionY`, `fPositionZ`, `fDirX`, `fDirY`, `fDirZ`, `fBindPositionX`, `fBindPositionY`, `fBindPositionZ`, `fBindDirX`, `fBindDirY`, `fBindDirZ`, `dwBindObjectTblidx`, `dwBindWorldID`, `dwMoney`, `dwMoneyBank`, `dwMapInfoIndex`, `dwLP`, `dwEP`, `dwRP`, `dwExp`, `dwReputationPoint`, `dwMudosaPoint`, `dwSpPoint`, `dwTutorialHint`, `Online`, `dwNetPy`) VALUES
	(8, 1, 0, 'teste234', 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 255, 0, 10000, 10000, 0, 0, 0, -78, 47, -169, -1, 0, -0., 4690, 0, 4472, 1, 0, 0, 1, 1, 10000, 500, 200101000, 100, 100, 100, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `characters` (`CharID`, `AccountID`, `ServerID`, `awchName`, `byRace`, `byClass`, `byGender`, `byFace`, `byHair`, `byHairColor`, `bySkinColor`, `byLevel`, `byBindType`, `byBlood`, `byMarking`, `bIsAdult`, `worldTblidx`, `worldId`, `bTutorialFlag`, `bNeedNameChange`, `bChangeClass`, `fPositionX`, `fPositionY`, `fPositionZ`, `fDirX`, `fDirY`, `fDirZ`, `fBindPositionX`, `fBindPositionY`, `fBindPositionZ`, `fBindDirX`, `fBindDirY`, `fBindDirZ`, `dwBindObjectTblidx`, `dwBindWorldID`, `dwMoney`, `dwMoneyBank`, `dwMapInfoIndex`, `dwLP`, `dwEP`, `dwRP`, `dwExp`, `dwReputationPoint`, `dwMudosaPoint`, `dwSpPoint`, `dwTutorialHint`, `Online`, `dwNetPy`) VALUES
	(9, 1, 0, 'tesf323', 1, 3, 2, 1, 1, 1, 1, 1, 0, 0, 255, 0, 14000, 14000, 0, 0, 0, -78, 47, -169, -1, 0, -0., 2902, 0, -2370, 1, 0, 0, 1, 1, 10000, 10000, 200105000, 100, 100, 100, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `characters` (`CharID`, `AccountID`, `ServerID`, `awchName`, `byRace`, `byClass`, `byGender`, `byFace`, `byHair`, `byHairColor`, `bySkinColor`, `byLevel`, `byBindType`, `byBlood`, `byMarking`, `bIsAdult`, `worldTblidx`, `worldId`, `bTutorialFlag`, `bNeedNameChange`, `bChangeClass`, `fPositionX`, `fPositionY`, `fPositionZ`, `fDirX`, `fDirY`, `fDirZ`, `fBindPositionX`, `fBindPositionY`, `fBindPositionZ`, `fBindDirX`, `fBindDirY`, `fBindDirZ`, `dwBindObjectTblidx`, `dwBindWorldID`, `dwMoney`, `dwMoneyBank`, `dwMapInfoIndex`, `dwLP`, `dwEP`, `dwRP`, `dwExp`, `dwReputationPoint`, `dwMudosaPoint`, `dwSpPoint`, `dwTutorialHint`, `Online`, `dwNetPy`) VALUES
	(10, 1, 0, 'tesfa1', 2, 5, 0, 1, 1, 1, 1, 1, 0, 0, 255, 0, 29000, 29000, 0, 0, 0, -78, 47, -169, -1, 0, -0., 6057, 0, -4216, -1, 0, 0, 1, 1, 10000, 10000, 200101000, 100, 100, 100, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `characters` (`CharID`, `AccountID`, `ServerID`, `awchName`, `byRace`, `byClass`, `byGender`, `byFace`, `byHair`, `byHairColor`, `bySkinColor`, `byLevel`, `byBindType`, `byBlood`, `byMarking`, `bIsAdult`, `worldTblidx`, `worldId`, `bTutorialFlag`, `bNeedNameChange`, `bChangeClass`, `fPositionX`, `fPositionY`, `fPositionZ`, `fDirX`, `fDirY`, `fDirZ`, `fBindPositionX`, `fBindPositionY`, `fBindPositionZ`, `fBindDirX`, `fBindDirY`, `fBindDirZ`, `dwBindObjectTblidx`, `dwBindWorldID`, `dwMoney`, `dwMoneyBank`, `dwMapInfoIndex`, `dwLP`, `dwEP`, `dwRP`, `dwExp`, `dwReputationPoint`, `dwMudosaPoint`, `dwSpPoint`, `dwTutorialHint`, `Online`, `dwNetPy`) VALUES
	(11, 2, 0, 'tesfa', 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 255, 0, 10000, 10000, 0, 0, 0, -78, 47, -169, -1, 0, -0., 4690, 0, 4472, 1, 0, 0, 1, 1, 10000, 500, 200101000, 100, 100, 100, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `characters` (`CharID`, `AccountID`, `ServerID`, `awchName`, `byRace`, `byClass`, `byGender`, `byFace`, `byHair`, `byHairColor`, `bySkinColor`, `byLevel`, `byBindType`, `byBlood`, `byMarking`, `bIsAdult`, `worldTblidx`, `worldId`, `bTutorialFlag`, `bNeedNameChange`, `bChangeClass`, `fPositionX`, `fPositionY`, `fPositionZ`, `fDirX`, `fDirY`, `fDirZ`, `fBindPositionX`, `fBindPositionY`, `fBindPositionZ`, `fBindDirX`, `fBindDirY`, `fBindDirZ`, `dwBindObjectTblidx`, `dwBindWorldID`, `dwMoney`, `dwMoneyBank`, `dwMapInfoIndex`, `dwLP`, `dwEP`, `dwRP`, `dwExp`, `dwReputationPoint`, `dwMudosaPoint`, `dwSpPoint`, `dwTutorialHint`, `Online`, `dwNetPy`) VALUES
	(12, 1, 0, 'mastaT', 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 255, 0, 1, 1, 0, 0, 0, 4690, 0, 4472, 1, 0, 0, 4690, 0, 4472, 1, 0, 0, 1, 1, 10000, 10000, 200101000, 100, 100, 100, 0, 0, 0, 0, 0, 0, 0);
/*!40000 ALTER TABLE `characters` ENABLE KEYS */;


-- Copiando estrutura para tabela mastacore.charcreated
CREATE TABLE IF NOT EXISTS `charcreated` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `charID` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mastacore.charcreated: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `charcreated` DISABLE KEYS */;
INSERT INTO `charcreated` (`id`, `charID`) VALUES
	(1, 12);
/*!40000 ALTER TABLE `charcreated` ENABLE KEYS */;


-- Copiando estrutura para tabela mastacore.htblist
CREATE TABLE IF NOT EXISTS `htblist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `charID` int(11) DEFAULT NULL,
  `bySlotID` int(11) DEFAULT NULL,
  `dwTimeCoolDown` int(11) DEFAULT NULL,
  `skillId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mastacore.htblist: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `htblist` DISABLE KEYS */;
/*!40000 ALTER TABLE `htblist` ENABLE KEYS */;


-- Copiando estrutura para tabela mastacore.inventory
CREATE TABLE IF NOT EXISTS `inventory` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `charID` int(10) DEFAULT NULL,
  `itemId` int(255) DEFAULT NULL,
  `tblidx` int(255) DEFAULT NULL,
  `byPlace` int(11) DEFAULT NULL,
  `byPosition` int(11) DEFAULT NULL,
  `byStackCount` int(11) DEFAULT NULL,
  `byRank` int(11) DEFAULT NULL,
  `byCurrentDurability` int(11) DEFAULT NULL,
  `bNeedToIdentify` tinyint(2) DEFAULT NULL,
  `byGrade` int(255) DEFAULT NULL,
  `byBattleAttribute` int(11) DEFAULT NULL,
  `byRestrictType` int(11) DEFAULT NULL,
  `OptionTblidx1` int(255) DEFAULT NULL,
  `OptionTblidx2` int(255) DEFAULT NULL,
  `byDurationType` int(11) DEFAULT NULL,
  `nUseStartTime` int(255) DEFAULT NULL,
  `nUseEndTime` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `char_ivt` (`charID`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mastacore.inventory: ~30 rows (aproximadamente)
/*!40000 ALTER TABLE `inventory` DISABLE KEYS */;
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(43, 8, 0, 19901, 0, 0, 1, 0, 255, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(44, 8, 0, 10045, 7, 0, 1, 1, 100, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(45, 8, 0, 14001, 7, 2, 1, 1, 100, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(46, 8, 0, 14002, 7, 3, 1, 1, 100, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(47, 8, 0, 14003, 7, 4, 1, 1, 100, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(48, 8, 0, 17001, 7, 5, 1, 1, 50, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(49, 9, 0, 19901, 0, 0, 1, 0, 255, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(50, 9, 0, 500045, 7, 0, 1, 1, 100, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(51, 9, 0, 13001, 7, 2, 1, 1, 100, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(52, 9, 0, 13002, 7, 3, 1, 1, 100, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(53, 9, 0, 13003, 7, 4, 1, 1, 100, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(54, 9, 0, 17001, 7, 5, 1, 1, 50, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(55, 10, 0, 19901, 0, 0, 1, 0, 255, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(56, 10, 0, 500081, 7, 0, 1, 1, 100, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(57, 10, 0, 15001, 7, 2, 1, 1, 100, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(58, 10, 0, 15002, 7, 3, 1, 1, 100, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(59, 10, 0, 15003, 7, 4, 1, 1, 100, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(60, 10, 0, 17001, 7, 5, 1, 1, 50, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(61, 11, 0, 19901, 0, 0, 1, 0, 255, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(62, 11, 0, 10045, 7, 0, 1, 1, 100, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(63, 11, 0, 14001, 7, 2, 1, 1, 100, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(64, 11, 0, 14002, 7, 3, 1, 1, 100, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(65, 11, 0, 14003, 7, 4, 1, 1, 100, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(66, 11, 0, 17001, 7, 5, 1, 1, 50, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(67, 12, 0, 19901, 0, 0, 1, 0, 255, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(68, 12, 0, 10045, 7, 0, 1, 1, 100, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(69, 12, 0, 14001, 7, 2, 1, 1, 100, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(70, 12, 0, 14002, 7, 3, 1, 1, 100, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(71, 12, 0, 14003, 7, 4, 1, 1, 100, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
INSERT INTO `inventory` (`id`, `charID`, `itemId`, `tblidx`, `byPlace`, `byPosition`, `byStackCount`, `byRank`, `byCurrentDurability`, `bNeedToIdentify`, `byGrade`, `byBattleAttribute`, `byRestrictType`, `OptionTblidx1`, `OptionTblidx2`, `byDurationType`, `nUseStartTime`, `nUseEndTime`) VALUES
	(72, 12, 0, 17001, 7, 5, 1, 1, 50, 0, 0, 0, 0, -1, -1, 0, NULL, NULL);
/*!40000 ALTER TABLE `inventory` ENABLE KEYS */;


-- Copiando estrutura para procedure mastacore.InventoryCreate
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `InventoryCreate`(IN `m_charID` int,IN `m_tblidx` int,IN `m_byPlace` int,IN `m_byPosition` int,IN `m_byStackCount` int,IN `m_byRank` int,IN `m_byCurrentDurability` int,IN `m_bNeedToIdentify` tinyint,IN `m_byGrade` int,IN `m_byBattleAttribute` int,IN `m_byRestrictType` int,IN `m_OptionTblidx1` int,IN `m_OptionTblidx2` int,IN `m_byDurationType` int)
BEGIN
	INSERT INTO inventory (charID, itemId, tblidx, byPlace, byPosition, byStackCount, byRank, byCurrentDurability, bNeedToIdentify, byGrade, byBattleAttribute, byRestrictType, OptionTblidx1, OptionTblidx2, byDurationType)
	VALUES(m_charID,
				0,
				m_tblidx,
				m_byPlace, 
				m_byPosition,
				m_byStackCount,
				m_byRank,
				m_byCurrentDurability,
				m_bNeedToIdentify,
				m_byGrade,
				m_byBattleAttribute,
				m_byRestrictType,
				m_OptionTblidx1,
				m_OptionTblidx2,
				m_byDurationType);
				

END//
DELIMITER ;


-- Copiando estrutura para tabela mastacore.quickslot
CREATE TABLE IF NOT EXISTS `quickslot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `charID` int(20) DEFAULT NULL,
  `bySlot` int(15) DEFAULT NULL,
  `byType` int(15) DEFAULT NULL,
  `dwItemTblidx` int(255) DEFAULT NULL,
  `dwSkillTblidx` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mastacore.quickslot: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `quickslot` DISABLE KEYS */;
/*!40000 ALTER TABLE `quickslot` ENABLE KEYS */;


-- Copiando estrutura para tabela mastacore.skills
CREATE TABLE IF NOT EXISTS `skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `charID` int(11) DEFAULT NULL,
  `bySlot` int(11) DEFAULT NULL,
  `byRpBonusType` int(11) DEFAULT NULL,
  `bIsRpBonusAuto` int(11) DEFAULT NULL,
  `nRemainSec` int(255) DEFAULT NULL,
  `nExp` int(255) DEFAULT NULL,
  `dwSkillTblidx` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mastacore.skills: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `skills` DISABLE KEYS */;
INSERT INTO `skills` (`id`, `charID`, `bySlot`, `byRpBonusType`, `bIsRpBonusAuto`, `nRemainSec`, `nExp`, `dwSkillTblidx`) VALUES
	(8, 8, 0, 0, 0, 0, 0, '10111');
INSERT INTO `skills` (`id`, `charID`, `bySlot`, `byRpBonusType`, `bIsRpBonusAuto`, `nRemainSec`, `nExp`, `dwSkillTblidx`) VALUES
	(9, 9, 0, 0, 0, 0, 0, '310111');
INSERT INTO `skills` (`id`, `charID`, `bySlot`, `byRpBonusType`, `bIsRpBonusAuto`, `nRemainSec`, `nExp`, `dwSkillTblidx`) VALUES
	(10, 10, 0, 0, 0, 0, 0, '510111');
INSERT INTO `skills` (`id`, `charID`, `bySlot`, `byRpBonusType`, `bIsRpBonusAuto`, `nRemainSec`, `nExp`, `dwSkillTblidx`) VALUES
	(11, 11, 0, 0, 0, 0, 0, '10111');
INSERT INTO `skills` (`id`, `charID`, `bySlot`, `byRpBonusType`, `bIsRpBonusAuto`, `nRemainSec`, `nExp`, `dwSkillTblidx`) VALUES
	(12, 12, 0, 0, 0, 0, 0, '10111');
/*!40000 ALTER TABLE `skills` ENABLE KEYS */;


-- Copiando estrutura para tabela mastacore.warehouse
CREATE TABLE IF NOT EXISTS `warehouse` (
  `Account` int(11) DEFAULT NULL,
  `CharID` int(11) DEFAULT NULL,
  `ServerID` int(11) DEFAULT NULL,
  `tblidx` int(11) DEFAULT NULL,
  `byPlace` int(11) DEFAULT NULL COMMENT 'eCONTAINER_TYPE',
  `byPos` int(11) DEFAULT NULL,
  `byStackcount` int(11) DEFAULT NULL,
  `byRank` int(11) DEFAULT NULL,
  `byCurDur` int(11) DEFAULT NULL,
  `awchMaker` varchar(50) DEFAULT NULL COMMENT 'Who Crafted the Item?',
  `bNeedIndentify` int(11) DEFAULT NULL,
  `byGrade` int(11) DEFAULT NULL,
  `byBattleAttribute` int(11) DEFAULT NULL COMMENT 'NtlBattle.h eBATTLE_ATTRIBUTE',
  `byRestrictType` int(11) DEFAULT NULL COMMENT 'eITEM_RESTRICT_TYPE',
  `aOptionTblidx1` int(11) DEFAULT NULL,
  `aOptionTblidx2` int(11) DEFAULT NULL,
  `byDurationType` int(11) DEFAULT NULL COMMENT 'eDURATIONTYPE',
  `nUseStartTime` int(11) DEFAULT NULL,
  `nUseEndTime` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mastacore.warehouse: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `warehouse` DISABLE KEYS */;
/*!40000 ALTER TABLE `warehouse` ENABLE KEYS */;


-- Copiando estrutura para tabela mastacore.warfog
CREATE TABLE IF NOT EXISTS `warfog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `charId` int(11) DEFAULT NULL,
  `achWarFogFlag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mastacore.warfog: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `warfog` DISABLE KEYS */;
/*!40000 ALTER TABLE `warfog` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
