#include "stdafx.h"
#include "NPCServer.h"
#include "NpcSession.h"


CNpcSession::CNpcSession(bool bAliveCheck, bool bOpcodeCheck) :CNtlSession(SESSION_NPC_SERVER)
{
	SetControlFlag(CONTROL_FLAG_USE_SEND_QUEUE);

	if (bAliveCheck)
	{
		SetControlFlag(CONTROL_FLAG_CHECK_ALIVE);
	}
	if (bOpcodeCheck)
	{
		SetControlFlag(CONTROL_FLAG_CHECK_OPCODE);
	}

	SetPacketEncoder(&m_packetEncoder);
}

//-----------------------------------------------------------------------------------
//		클라이언트 소멸
//-----------------------------------------------------------------------------------
CNpcSession::~CNpcSession()
{
	NTL_PRINT(PRINT_APP, "CNpcSession Destructor Called");

	CNPCServer * app = (CNPCServer*)NtlSfxGetApp();
	app->Remove(this->GetHandle());
}

int CNpcSession::OnAccept()
{
	NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
	CNPCServer * app = (CNPCServer*)NtlSfxGetApp();
	for (int i = 0; i < app->GetNetwork()->GetSessionList()->GetMaxCount(); i++)
	{
		CNtlSession* first = app->GetNetwork()->GetSessionList()->Find(i);
		if (first)
		{			
			if (first->GetSessionType() == SESSION_GAME_SERVER)
			{
				app->SetGameSession((CGameSession*)first);
			}
			if (first->GetSessionType() == SESSION_MASTER_SERVER)
			{
				app->SetMasterSession((CMasterSession*)first);
			}
		}
	}
	if ((app->AcquireGameSession() != NULL) && (!app->IsGameServerConnected()))
	{
		CNtlPacket packetQ(sizeof(sNG_GAME_DATA_REQ));
		sNG_GAME_DATA_REQ* msgQ = (sNG_GAME_DATA_REQ *)packetQ.GetPacketData();
		msgQ->wOpCode = NG_GAME_DATA_REQ;
		packetQ.SetPacketLen(sizeof(sNG_GAME_DATA_REQ));
		app->AcquireGameSession()->PushPacket(&packetQ);
		app->SetGameServerConnected(true);
	}
	return NTL_SUCCESS;
}


void CNpcSession::OnClose()
{
	NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
}

int CNpcSession::OnDispatch(CNtlPacket * pPacket)
{
	CNPCServer * app = (CNPCServer*)NtlSfxGetApp();

	sNTLPACKETHEADER * pHeader = (sNTLPACKETHEADER *)pPacket->GetPacketData();
	switch (pHeader->wOpCode)
	{
		//Game Server
		case GN_GAME_DATA_RES:
		{			
			if (app->GetGameSession() != NULL)
				app->GetGameSession()->ResetAliveTime();
		}
		break;
		case GN_CHAR_SPAWN_REQ:
		{
		}
		break;
		case GN_NPC_SPAWN_REQ:
		{
			CNpcSession::SendNpcSpawnRes(pPacket);
		}
		break;
		case GN_MOB_SPAWN_REQ:
		{
			CNpcSession::SendMobSpawnRes(pPacket);
		}
		break;
		case GN_NPC_CHECK_REQ:
		{
			CNpcSession::SendNpcCheckRes(pPacket);
		}
		break;
		case GN_MOB_CHECK_REQ:
		{
			CNpcSession::SendMobCheckRes(pPacket);
		}
		break;
		case GN_ADMIN_NPC_SPAWN:
		{
			CNpcSession::SendAdminNpcSpawn(pPacket);
		}
		break;
		case GN_ADMIN_MOB_SPAWN:
		{
			CNpcSession::SendAdminMobSpawn(pPacket);
		}
		break;
		case GN_NPC_SHOP_BUY:
		{
			CNpcSession::SendShopResult(pPacket);
		}
		break;
		case GN_WORLD_CREATED_NFY:
		{
		}
		break;
		case GN_WORLD_DESTROYED_NFY:
		{
		}
		break;
		case GN_OBJECT_CREATED_NFY:
		{
		}
		break;
		case GN_OBJECT_DESTROYED_NFY:
		{
		}
		break;
		case GN_OBJECT_WORLD_ENTERED:
		{
		}
		break;
		case GN_OBJECT_WORLD_LEAVED:
		{
		}
		break;
		case GN_PREPARE_ENTER_GAME:
		{
		}
		break;
		case GN_ENTER_GAME_RES:
		{
		}
		break;
		case GN_LEAVE_GAME_RES:
		{
		}
		break;
		case GN_ENTER_WORLD_RES:
		{
		}
		break;
		case GN_LEAVE_WORLD_RES:
		{
		}
		break;
		case MN_HEARTBEAT:
		{
			if (app->GetMasterSession() != NULL)
				app->GetMasterSession()->ResetAliveTime();
		}
		break;
		default:
		{
			if (pHeader->wOpCode != SYS_ALIVE)
			{
				NTL_PRINT(PRINT_APP, "The following packet was not handled: %s", NtlGetPacketName(pHeader->wOpCode));
			}
			return CNtlSession::OnDispatch(pPacket);
		}
	}

	return NTL_SUCCESS;
}

void CNpcSession::SendNpcSpawnRes(CNtlPacket* pPacket)
{
	CNPCServer * app = (CNPCServer*)NtlSfxGetApp();
	sGN_NPC_SPAWN_REQ* req = (sGN_NPC_SPAWN_REQ*)pPacket->GetPacketData();	
	for (NPC_LIST_IT it = app->m_npcList.begin(); it != app->m_npcList.end(); it++)
	{
		sNPC_FULLDATA* npc = (sNPC_FULLDATA*)it->second;
		if (npc->worldId == req->worldTblidx)
		{
			if (NtlGetDistance(req->vPlayerPos.x, req->vPlayerPos.z, npc->vSpawnLoc.x, npc->vSpawnLoc.z) < 120)
			{
				CNtlPacket packet(sizeof(sNG_NPC_SPAWN_RES));
				sNG_NPC_SPAWN_RES* res = (sNG_NPC_SPAWN_RES*)packet.GetPacketData();
				res->hNpcHandle = npc->hHandler;
				res->hPlayerHandle = req->hPlayerHandle;				
				res->sNpcBrief = npc->sNpcData;
				res->sNpcState.sCharStateBase.bFightMode = npc->bFightMode;
				res->sNpcState.sCharStateBase.byStateID = npc->byAspectID;
				res->sNpcState.sCharStateBase.vCurDir = npc->vSpawnDir;
				res->sNpcState.sCharStateBase.vCurLoc = npc->vSpawnLoc;
				res->wOpCode = NG_NPC_SPAWN_RES;
				packet.SetPacketLen(sizeof(sNG_NPC_SPAWN_RES));
				app->GetGameSession()->PushPacket(&packet);
				app->AddSpawnObj(req->hPlayerHandle, npc->hHandler);
			}
		}
	}
	
}

void CNpcSession::SendMobSpawnRes(CNtlPacket* pPacket)
{
	CNPCServer * app = (CNPCServer*)NtlSfxGetApp();
	sGN_MOB_SPAWN_REQ* req = (sGN_MOB_SPAWN_REQ*)pPacket->GetPacketData();		
	for (MOB_LIST_IT it = app->m_mobList.begin(); it != app->m_mobList.end(); it++)
	{
		sMOB_FULLDATA* mob = (sMOB_FULLDATA*)it->second;
		if (it->second->worldId == req->worldTblidx)
		{
			float fDistance = NtlGetDistance(req->vPlayerPos.x, req->vPlayerPos.z, mob->vSpawnLoc.x, mob->vSpawnLoc.z);
			if ( fDistance < 400 )
			{
				CNtlPacket packet(sizeof(sNG_MOB_SPAWN_RES));
				sNG_MOB_SPAWN_RES* res = (sNG_MOB_SPAWN_RES*)packet.GetPacketData();
				res->hMobHandle = mob->hHandler;
				res->hPlayerHandle = req->hPlayerHandle;
				res->sMobBrief = mob->sMobData;
				res->sMobState.sCharStateBase.bFightMode = mob->bFightMode;
				res->sMobState.sCharStateBase.byStateID = mob->byAspectID;
				res->sMobState.sCharStateBase.vCurDir = mob->vSpawnDir;
				res->sMobState.sCharStateBase.vCurLoc = mob->vSpawnLoc;
				res->wOpCode = NG_MOB_SPAWN_RES;
				packet.SetPacketLen(sizeof(sNG_MOB_SPAWN_RES));
				app->GetGameSession()->PushPacket(&packet);
				app->AddSpawnMobObj(req->hPlayerHandle, mob->hHandler);
			}
		}
	}
}

void CNpcSession::SendMobCheckRes(CNtlPacket* pPacket)
{
	CNPCServer * app = (CNPCServer*)NtlSfxGetApp();
	sGN_MOB_CHECK_REQ* req = (sGN_MOB_CHECK_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sNG_MOB_SPAWN_RES));
	sNG_MOB_SPAWN_RES* res = (sNG_MOB_SPAWN_RES*)packet.GetPacketData();
	res->hPlayerHandle = req->hPlayerHandle;
	for (MOB_LIST_IT it = app->m_mobList.begin(); it != app->m_mobList.end(); it++)
	{
		sMOB_FULLDATA* mob = (sMOB_FULLDATA*)it->second;
		if (it->second->worldId == req->worldTblidx)
		{
			float fDistance = NtlGetDistance(req->vPlayerPos.x, req->vPlayerPos.z, mob->vSpawnLoc.x, mob->vSpawnLoc.z);
			if ( fDistance < 400 )
			{
				if (!app->FindSpawnMobObj(req->hPlayerHandle, mob->hHandler))
				{
					CNtlPacket packet(sizeof(sNG_MOB_SPAWN_RES));
					sNG_MOB_SPAWN_RES* res = (sNG_MOB_SPAWN_RES*)packet.GetPacketData();
					res->hMobHandle = mob->hHandler;
					res->hPlayerHandle = req->hPlayerHandle;
					res->sMobBrief = mob->sMobData;
					res->sMobState.sCharStateBase.bFightMode = mob->bFightMode;
					res->sMobState.sCharStateBase.byStateID = mob->byAspectID;
					res->sMobState.sCharStateBase.vCurDir = mob->vSpawnDir;
					res->sMobState.sCharStateBase.vCurLoc = mob->vSpawnLoc;
					res->wOpCode = NG_MOB_SPAWN_RES;
					packet.SetPacketLen(sizeof(sNG_MOB_SPAWN_RES));
					app->GetGameSession()->PushPacket(&packet);
					app->AddSpawnMobObj(req->hPlayerHandle, mob->hHandler);
				}
			}
			else
			{
				if (app->FindSpawnMobObj(req->hPlayerHandle, mob->hHandler))
				{
					CNtlPacket packet(sizeof(sNG_MOB_UNSPAWN_RES));
					sNG_MOB_UNSPAWN_RES* res = (sNG_MOB_UNSPAWN_RES*)packet.GetPacketData();
					res->hPlayerHandle = req->hPlayerHandle;
					res->hMobHandle = mob->hHandler;
					res->wOpCode = NG_MOB_UNSPAWN_RES;
					packet.SetPacketLen(sizeof(sNG_MOB_UNSPAWN_RES));
					app->GetGameSession()->PushPacket(&packet);
					app->RemoveSpawnMobObj(req->hPlayerHandle, mob->hHandler);
				}
			}
		}
	}
}

void CNpcSession::SendNpcCheckRes(CNtlPacket* pPacket)
{
	CNPCServer * app = (CNPCServer*)NtlSfxGetApp();
	sGN_NPC_CHECK_REQ* req = (sGN_NPC_CHECK_REQ*)pPacket->GetPacketData();
	for (NPC_LIST_IT it = app->m_npcList.begin(); it != app->m_npcList.end(); it++)
	{
		sNPC_FULLDATA* npc = (sNPC_FULLDATA*)it->second;
		if (npc->worldId == req->worldTblidx)
		{
			float fDistance = NtlGetDistance(req->vPlayerPos.x, req->vPlayerPos.z, npc->vSpawnLoc.x, npc->vSpawnLoc.z);
			if ( fDistance < 120 )
			{
				if (!app->FindSpawnObj(req->hPlayerHandle, npc->hHandler))
				{
					CNtlPacket packet(sizeof(sNG_NPC_SPAWN_RES));
					sNG_NPC_SPAWN_RES* res = (sNG_NPC_SPAWN_RES*)packet.GetPacketData();
					res->hNpcHandle = npc->hHandler;
					res->hPlayerHandle = req->hPlayerHandle;
					res->sNpcBrief = npc->sNpcData;
					res->sNpcState.sCharStateBase.bFightMode = npc->bFightMode;
					res->sNpcState.sCharStateBase.byStateID = npc->byAspectID;
					res->sNpcState.sCharStateBase.vCurDir = npc->vSpawnDir;
					res->sNpcState.sCharStateBase.vCurLoc = npc->vSpawnLoc;
					res->wOpCode = NG_NPC_SPAWN_RES;
					packet.SetPacketLen(sizeof(sNG_NPC_SPAWN_RES));
					app->GetGameSession()->PushPacket(&packet);
					app->AddSpawnObj(req->hPlayerHandle, npc->hHandler);
				}
			}
			else
			{
				if (app->FindSpawnObj(req->hPlayerHandle, npc->hHandler))
				{
					CNtlPacket packet(sizeof(sNG_NPC_UNSPAWN_RES));
					sNG_NPC_UNSPAWN_RES* res = (sNG_NPC_UNSPAWN_RES*)packet.GetPacketData();
					res->hPlayerHandle = req->hPlayerHandle;
					res->hNpcHandle = npc->hHandler;
					res->wOpCode = NG_NPC_UNSPAWN_RES;
					packet.SetPacketLen(sizeof(sNG_NPC_UNSPAWN_RES));
					app->GetGameSession()->PushPacket(&packet);
					app->RemoveSpawnObj(req->hPlayerHandle, npc->hHandler);
				}
			}
		}
	}
}

void	CNpcSession::SendAdminNpcSpawn(CNtlPacket* pPacket)
{
	CNPCServer * app = (CNPCServer*)NtlSfxGetApp();
	sGN_ADMIN_NPC_SPAWN* req = (sGN_ADMIN_NPC_SPAWN*)pPacket->GetPacketData();
	NPC_LIST_IT it = app->m_npcList.find(req->npcID);
	sNPC_BRIEF sNpcBrief;
	sCHARSTATE sNpcState;
	if (it != app->m_npcList.end())
	{
		sNpcBrief = it->second->sNpcData;
	}
	sNpcState.sCharStateBase.bFightMode = false;
	sNpcState.sCharStateBase.byStateID = CHARSTATE_SPAWNING;
	sNpcState.sCharStateBase.vCurLoc = req->vPlayerPos;
	sNpcState.sCharStateBase.vCurDir = req->vPlayerDir;

	CNtlPacket packet(sizeof(sNG_ADMIN_NPC_SPAWN));
	sNG_ADMIN_NPC_SPAWN* res = (sNG_ADMIN_NPC_SPAWN*)packet.GetPacketData();
	res->hNpcHandle = req->hLastSerial;
	res->sNpcBrief = sNpcBrief;
	res->sNpcState = sNpcState;
	res->wOpCode = NG_ADMIN_NPC_SPAWN;
	packet.SetPacketLen(sizeof(sNG_ADMIN_NPC_SPAWN));
	app->GetGameSession()->PushPacket(&packet);
}

void	CNpcSession::SendAdminMobSpawn(CNtlPacket* pPacket)
{
	CNPCServer * app = (CNPCServer*)NtlSfxGetApp();
	sGN_ADMIN_MOB_SPAWN* req = (sGN_ADMIN_MOB_SPAWN*)pPacket->GetPacketData();
	sMOB_BRIEF sMobBrief;
	sCHARSTATE sMobState;
	MOB_LIST_IT it = app->m_mobList.find(req->mobID);
	if (it != app->m_mobList.end())
	{
		sMobBrief = it->second->sMobData;
	}
	sMobState.sCharStateBase.bFightMode = true;
	sMobState.sCharStateBase.byStateID = CHARSTATE_SPAWNING;
	sMobState.sCharStateBase.vCurLoc = req->vPlayerPos;
	sMobState.sCharStateBase.vCurDir = req->vPlayerDir;

	CNtlPacket packet(sizeof(sNG_ADMIN_MOB_SPAWN));
	sNG_ADMIN_MOB_SPAWN* res = (sNG_ADMIN_MOB_SPAWN*)packet.GetPacketData();
	res->hMobHandle = req->hLastSerial;
	res->sMobBrief = sMobBrief;
	res->sMobState = sMobState;
	res->wOpCode = NG_ADMIN_MOB_SPAWN;
	packet.SetPacketLen(sizeof(sNG_ADMIN_MOB_SPAWN));
	app->GetGameSession()->PushPacket(&packet);
}

void	CNpcSession::SendShopResult(CNtlPacket* pPacket)
{
	CNPCServer * app = (CNPCServer*)NtlSfxGetApp();
	sGN_NPC_SHOP_BUY* req = (sGN_NPC_SHOP_BUY*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sNG_SHOP_BUY));
	sNG_SHOP_BUY* res = (sNG_SHOP_BUY*)packet.GetPacketData();
	sNPC_FULLDATA* npcData = app->GetNpcData(req->handle);
	sNPC_TBLDAT* pNPCtData = reinterpret_cast<sNPC_TBLDAT*>(app->GetTableContainer()->GetNpcTable()->FindData(npcData->sNpcData.tblidx));
	for (int i = 0; i < req->byBuyCount; i++)
	{
		sMERCHANT_TBLDAT* pMerchantData = reinterpret_cast<sMERCHANT_TBLDAT*>(app->GetTableContainer()->GetMerchantTable()->FindData(pNPCtData->amerchant_Tblidx[req->sBuyData[i].byMerchantTab]));
		sITEM_TBLDAT* pItemTbldat = reinterpret_cast<sITEM_TBLDAT*>(app->GetTableContainer()->GetItemTable()->FindData(pMerchantData->aitem_Tblidx[req->sBuyData[i].byItemPos]));
		res->sItemTblidx[i] = pItemTbldat->tblidx;
		res->byOrderedStack[i] = req->sBuyData[i].byStack;		
	}
	res->byItemsCount = req->byBuyCount;	
	res->hNpc = req->handle;
	res->hPlayer = req->hPlayer;
	res->wOpCode = NG_SHOP_BUY;
	packet.SetPacketLen(sizeof(sNG_SHOP_BUY));
	app->GetGameSession()->PushPacket(&packet);
}