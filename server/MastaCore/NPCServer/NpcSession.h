#pragma once
#include "NPCServer.h"
#include "NtlSession.h"
#include "NtlPacketEncoder_RandKey.h"
///////////////////////////////////////RESPONSE PACKETS///////////////////////////
//NPC Server -> Game Server
#include "NtlPacketNG.h"
//NPC Server -> Master Server
#include "NtlPacketNM.h"
/////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////REQUEST PACKETS///////////////////////////
//Master Server -> NPC Server
#include "NtlPacketMN.h"
//Game Server -> NPC Server
#include "NtlPacketGN.h"
/////////////////////////////////////////////////////////////////////////////////
#include "NtlPacketUtil.h"

#ifndef CNPCSESSION_H
#define CNPCSESSION_H

class CNpcSession :	public CNtlSession
{
public:
	CNpcSession(bool bAliveCheck = true, bool bOpcodeCheck = true);
	~CNpcSession();
	int							OnAccept();
	void						OnClose();
	int							OnDispatch(CNtlPacket * pPacket);
	// Packet functions
	void						SendNpcSpawnRes(CNtlPacket* pPacket);
	void						SendMobSpawnRes(CNtlPacket* pPacket);
	void						SendNpcCheckRes(CNtlPacket* pPacket);
	void						SendMobCheckRes(CNtlPacket* pPacket);
	void						SendAdminNpcSpawn(CNtlPacket* pPacket);
	void						SendAdminMobSpawn(CNtlPacket* pPacket);
	void						SendShopResult(CNtlPacket* pPacket);
	void						SendLastSerial();
	// End Packet functions
private:
	CNtlPacketEncoder_RandKey	m_packetEncoder;
};

#endif