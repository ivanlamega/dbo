#include "stdafx.h"
#include "NpcSessionFactory.h"


CNpcSessionFactory::CNpcSessionFactory()
{
}


CNpcSessionFactory::~CNpcSessionFactory()
{
}

CNtlSession * CNpcSessionFactory::CreateSession(SESSIONTYPE sessionType)
{
	CNtlSession * pSession = NULL;
	switch (sessionType)
	{
	case SESSION_NPC_SERVER:
	{
		pSession = new CNpcSession(sessionType);
	}
	break;
	case SESSION_MASTER_SERVER:
	{
		pSession = new CNtlSession(sessionType);
	}
	break;
	case SESSION_GAME_SERVER:
	{
		pSession = new CNtlSession(sessionType);
	}
	break;

	default:
		break;
	}

	return pSession;
}



void CNpcSessionFactory::DestroySession(CNtlSession * pSession)
{
	switch (pSession->GetSessionType())
	{
	case SESSION_GAME_SERVER:
	case SESSION_NPC_SERVER:
	case SESSION_MASTER_SERVER:
	{
		SAFE_DELETE(pSession);
	}
	break;

	default:
		break;
	}
}