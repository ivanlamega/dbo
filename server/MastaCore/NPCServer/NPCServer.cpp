#include "stdafx.h"
#include "NPCServer.h"

//For Multiple Server - Luiz45
const std::string CNPCServer::GetConfigFileEnabledMultipleServers()
{
	return EnableMultipleServers.GetString();
}
const DWORD CNPCServer::GetConfigFileMaxServers()
{
	return MAX_NUMOF_NPC_SERVER;
}
int	CNPCServer::OnInitApp()
{
	m_nMaxSessionCount = MAX_NUMOF_NPC_SESSION;

	m_pSessionFactory = new CNpcSessionFactory;
	if (NULL == m_pSessionFactory)
	{
		return NTL_ERR_SYS_MEMORY_ALLOC_FAIL;
	}

	m_masterSession = NULL;
	m_gameSession = NULL;
	m_pNpcSession = (CNpcSession*)m_pSessionFactory->CreateSession(SESSION_NPC_SERVER);
	return NTL_SUCCESS;
}

int	CNPCServer::OnCreate()
{
	int rc = NTL_SUCCESS;
	rc = m_clientAcceptor.Create(m_config.strNpcServerIP.c_str(), m_config.wNpcServerPort, SESSION_NPC_SERVER,
		MAX_NUMOF_NPC_GAME_CLIENT, 5, 15, MAX_NUMOF_NPC_GAME_CLIENT);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_clientAcceptor, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_gameConnector.Create(m_config.strGameServerIP.c_str(), m_config.wGameServerPort, SESSION_GAME_SERVER);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_gameConnector, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_masterConnector.Create(m_config.strMasterServerIP.c_str(), m_config.wMasterServerPort, SESSION_MASTER_SERVER);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_masterConnector, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	return NTL_SUCCESS;

}

void	CNPCServer::OnDestroy()
{
	SAFE_DELETE(m_pSessionFactory);
}

int	CNPCServer::OnCommandArgument(int argc, _TCHAR* argv[])
{
	return NTL_SUCCESS;
}

int	CNPCServer::OnConfiguration(const char * lpszConfigFile)
{
	CNtlIniFile file;

	int rc = file.Create(lpszConfigFile);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}
	//For multiple servers - Luiz45
	if (!file.Read("ServerOptions", "EnableMultipleServers", EnableMultipleServers))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("NpcServer", "Address", m_config.strNpcServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("NpcServer", "Port", m_config.wNpcServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("MasterServer", "Address", m_config.strMasterServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("MasterServer", "Port", m_config.wMasterServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("GameServer", "Address", m_config.strGameServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("GameServer", "Port", m_config.wGameServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	return NTL_SUCCESS;
}

int	CNPCServer::OnAppStart()
{
	return NTL_SUCCESS;
}

void	CNPCServer::Run()
{
	DWORD dwTickCur, dwTickOld = ::GetTickCount();

	while (IsRunnable())
	{
		dwTickCur = ::GetTickCount();
		if (dwTickCur - dwTickOld >= 10000)
		{
			//	NTL_PRINT(PRINT_APP, "Auth Server Run()");
			DoAliveTime();
			dwTickOld = dwTickCur;
			if (!m_masterSession)
				this->m_pNpcSession->OnAccept();
			if (!m_gameSession)
				this->m_pNpcSession->OnAccept();
		}
		Sleep(2);
		DoAliveTime();
	}
}

bool	CNPCServer::Add(HSESSION hSession)
{
	if (Find(hSession))
		return false;

	m_sessionList.push_back(hSession);

	return true;
}

void	CNPCServer::Remove(HSESSION hSession)
{
	for (SESSIONIT it = m_sessionList.begin(); it != m_sessionList.end(); it++)
	{
		if (hSession == *it)
		{
			m_sessionList.erase(it);
			break;
		}
	}
}

bool	CNPCServer::Find(HSESSION hSession)
{
	for (SESSIONIT it = m_sessionList.begin(); it != m_sessionList.end(); it++)
	{
		if (hSession == *it)
		{
			return true;
		}
	}

	return false;
}

void	CNPCServer::DoAliveTime()
{
	for (int i = 0; i < this->GetNetwork()->GetSessionList()->GetMaxCount(); i++)
	{
		CNtlSession* first = this->GetNetwork()->GetSessionList()->Find(i);
		if (first)
		{
			if (first->GetSessionType() != SESSION_NPC_SERVER)
			{
				PACKETDATA res;
				res.wOpCode = SYS_ALIVE;
				CNtlPacket packet((BYTE*)&res, sizeof(res));
				first->PushPacket(&packet);
			}
		}
	}
}

void	CNPCServer::SetGameSession(CGameSession * pServerSession)
{
	CNtlAutoMutex mutex(&m_gameServerMutex);
	mutex.Lock();

	if (NULL == pServerSession)
	{
		m_gameSession->Release();
		m_gameSession = pServerSession;
	}
	else
	{
		pServerSession->Acquire();
		m_gameSession = pServerSession;
	}
}

CGameSession* CNPCServer::AcquireGameSession()
{
	CNtlAutoMutex mutex(&m_gameServerMutex);
	mutex.Lock();

	if (NULL == m_gameSession) return NULL;

	m_gameSession->Acquire();
	return m_gameSession;
}

CGameSession* CNPCServer::GetGameSession()
{
	return m_gameSession;
}

void CNPCServer::SetMasterSession(CMasterSession * pServerSession)
{
	CNtlAutoMutex mutex(&m_masterServerMutex);
	mutex.Lock();

	if (NULL == pServerSession)
	{
		m_masterSession->Release();
		m_masterSession = pServerSession;
	}
	else
	{
		pServerSession->Acquire();
		m_masterSession = pServerSession;
	}
}

CMasterSession * CNPCServer::AcquireMasterSession()
{
	CNtlAutoMutex mutex(&m_masterServerMutex);
	mutex.Lock();

	if (NULL == m_masterSession) return NULL;

	m_masterSession->Acquire();
	return m_masterSession;
}

CMasterSession * CNPCServer::GetMasterSession()
{
	return m_masterSession;
}

int		CNPCServer::CreateTableContainer()
{
	CNtlBitFlagManager flagManager;
	if (false == flagManager.Create(CTableContainer::TABLE_COUNT))
	{
		return false;
	}

	CTableFileNameList fileNameList;
	if (false == fileNameList.Create())
	{
		return false;
	}


	flagManager.Set(CTableContainer::TABLE_WORLD);
	flagManager.Set(CTableContainer::TABLE_PC);
	flagManager.Set(CTableContainer::TABLE_MOB);
	flagManager.Set(CTableContainer::TABLE_NPC);
	flagManager.Set(CTableContainer::TABLE_ITEM);
	flagManager.Set(CTableContainer::TABLE_ITEM_OPTION);
	flagManager.Set(CTableContainer::TABLE_SKILL);
	flagManager.Set(CTableContainer::TABLE_SYSTEM_EFFECT);
	flagManager.Set(CTableContainer::TABLE_NEWBIE);
	flagManager.Set(CTableContainer::TABLE_MERCHANT);
	flagManager.Set(CTableContainer::TABLE_HTB_SET);
	flagManager.Set(CTableContainer::TABLE_USE_ITEM);
	flagManager.Set(CTableContainer::TABLE_SET_ITEM);
	flagManager.Set(CTableContainer::TABLE_CHARM);
	flagManager.Set(CTableContainer::TABLE_ACTION);
	flagManager.Set(CTableContainer::TABLE_CHAT_COMMAND);
	flagManager.Set(CTableContainer::TABLE_QUEST_ITEM);
	flagManager.Set(CTableContainer::TABLE_QUEST_TEXT_DATA);
	flagManager.Set(CTableContainer::TABLE_TEXT_ALL);
	flagManager.Set(CTableContainer::TABLE_OBJECT);
	flagManager.Set(CTableContainer::TABLE_WORLD_MAP);
	flagManager.Set(CTableContainer::TABLE_LAND_MARK);
	flagManager.Set(CTableContainer::TABLE_HELP);
	flagManager.Set(CTableContainer::TABLE_GUIDE_HINT);
	flagManager.Set(CTableContainer::TABLE_DRAGONBALL);
	flagManager.Set(CTableContainer::TABLE_DRAGONBALL_REWARD);
	flagManager.Set(CTableContainer::TABLE_TIMEQUEST);
	flagManager.Set(CTableContainer::TABLE_BUDOKAI);
	flagManager.Set(CTableContainer::TABLE_RANKBATTLE);
	flagManager.Set(CTableContainer::TABLE_DIRECTION_LINK);
	flagManager.Set(CTableContainer::TABLE_CHATTING_FILTER);
	flagManager.Set(CTableContainer::TABLE_PORTAL);
	flagManager.Set(CTableContainer::TABLE_SPEECH);
	flagManager.Set(CTableContainer::TABLE_SCRIPT_LINK);
	flagManager.Set(CTableContainer::TABLE_QUEST_NARRATION);
	flagManager.Set(CTableContainer::TABLE_VEHICLE);
	flagManager.Set(CTableContainer::TABLE_DUNGEON);
	flagManager.Set(CTableContainer::TABLE_MOB_MOVE_PATTERN);
	flagManager.Set(CTableContainer::TABLE_DYNAMIC_OBJECT);
	flagManager.Set(CTableContainer::TABLE_ITEM_RECIPE);
	flagManager.Set(CTableContainer::TABLE_ITEM_UPGRADE);
	flagManager.Set(CTableContainer::TABLE_MIX_MACHINE);
	flagManager.Set(CTableContainer::TABLE_DOJO);
	flagManager.Set(CTableContainer::TABLE_QUEST_REWARD);
	flagManager.Set(CTableContainer::TABLE_WORLD_ZONE);
	flagManager.Set(CTableContainer::TABLE_NPC_SPAWN);
	flagManager.Set(CTableContainer::TABLE_MOB_SPAWN);
	flagManager.Set(CTableContainer::TABLE_FORMULA);
	flagManager.Set(CTableContainer::TABLE_GAME_MANIA_TIME);

	fileNameList.SetFileName(CTableContainer::TABLE_WORLD, "Table_World_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_PC, "Table_PC_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_MOB, "Table_MOB_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_NPC, "Table_NPC_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_ITEM, "Table_Item_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_ITEM_OPTION, "Table_Item_Option_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_SKILL, "Table_Skill_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_SYSTEM_EFFECT, "Table_System_Effect_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_NEWBIE, "Table_Newbie_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_MERCHANT, "Table_Merchant_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_HTB_SET, "Table_HTB_Set_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_USE_ITEM, "Table_Use_Item_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_SET_ITEM, "Table_Set_Item_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_CHARM, "Table_Charm_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_ACTION, "Table_Action_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_CHAT_COMMAND, "Table_Chat_Command_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_QUEST_ITEM, "Table_Quest_Item_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_QUEST_TEXT_DATA, "Table_Quest_Text_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_TEXT_ALL, "Table_Text_All_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_OBJECT, "Table_Object");
	fileNameList.SetFileName(CTableContainer::TABLE_WORLD_MAP, "Table_Worldmap_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_LAND_MARK, "Table_Landmark_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_HELP, "Table_Help_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_GUIDE_HINT, "Table_Guide_Hint_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_DRAGONBALL, "Table_Dragon_Ball_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_DRAGONBALL_REWARD, "Table_DB_Reward_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_TIMEQUEST, "Table_TMQ_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_BUDOKAI, "Table_Tenkaichibudokai_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_RANKBATTLE, "Table_RankBattle_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_DIRECTION_LINK, "Table_Direction_Link_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_CHATTING_FILTER, "Table_Chatting_Filter_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_PORTAL, "Table_Portal_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_SPEECH, "Table_NPC_Speech_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_SCRIPT_LINK, "Table_Script_Link_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_QUEST_NARRATION, "Table_Quest_Narration_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_VEHICLE, "Table_Vehicle_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_DUNGEON, "Table_Dungeon_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_MOB_MOVE_PATTERN, "Table_Mob_Move_Pattern_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_DYNAMIC_OBJECT, "Table_Dynamic_Object_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_ITEM_RECIPE, "Table_Item_Recipe_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_ITEM_UPGRADE, "Table_Item_Upgrade_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_MIX_MACHINE, "Table_Item_Mix_Machine_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_DOJO, "Table_Dojo_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_QUEST_REWARD, "Table_Quest_Reward_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_WORLD_ZONE, "Table_World_Zone_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_FORMULA, "TD_Formula");
	fileNameList.SetFileName(CTableContainer::TABLE_GAME_MANIA_TIME, "Table_GameManiaTime_Data");

	g_pTableContainer = new CTableContainer;

	std::string str;
	CTable::eLOADING_METHOD eLoadMethod = (CTable::eLOADING_METHOD)1;
	str = "data";

	bool bResult = FALSE;
	bResult = g_pTableContainer->Create(flagManager, (char*)str.c_str(), &fileNameList, eLoadMethod, GetACP(), NULL);
	return bResult;
}

void	CNPCServer::LoadNpcs()
{
	for (CTableContainer::SPAWNTABLEIT it = GetTableContainer()->BeginNpcSpawnTable(); it != GetTableContainer()->EndNpcSpawnTable(); it++)
	{
		CSpawnTable* pSpawnTable = GetTableContainer()->GetNpcSpawnTable(it->first);
		if (pSpawnTable)
		{
			for (CTable::TABLEIT pi = pSpawnTable->Begin(); pi != pSpawnTable->End(); pi++)
			{
				sNPC_FULLDATA* anNpc = new sNPC_FULLDATA;
				sSPAWN_TBLDAT* npcSpawn = reinterpret_cast<sSPAWN_TBLDAT*>(pi->second);
				sNPC_TBLDAT* npcData = reinterpret_cast<sNPC_TBLDAT*>(GetTableContainer()->GetNpcTable()->FindData(npcSpawn->mob_Tblidx));
				if (npcData)
				{			
					anNpc->worldId = it->first;
					anNpc->hHandler = AcquireSerialId();
					anNpc->vSpawnDir.x = npcSpawn->vSpawn_Dir.x;
					anNpc->vSpawnDir.y = npcSpawn->vSpawn_Dir.y;
					anNpc->vSpawnDir.z = npcSpawn->vSpawn_Dir.z;
					anNpc->vSpawnLoc.x = npcSpawn->vSpawn_Loc.x;
					anNpc->vSpawnLoc.y = npcSpawn->vSpawn_Loc.y;
					anNpc->vSpawnLoc.z = npcSpawn->vSpawn_Loc.z;
					anNpc->bFightMode = false;
					anNpc->byAspectID = CHARSTATE_SPAWNING;
					anNpc->sNpcData.wCurEP = npcData->wBasic_EP;
					anNpc->sNpcData.wCurLP = npcData->wBasic_LP;
					anNpc->sNpcData.wMaxEP = npcData->wBasic_EP;
					anNpc->sNpcData.wMaxLP = npcData->wBasic_LP;
					anNpc->sNpcData.tblidx = npcSpawn->mob_Tblidx;
					anNpc->sNpcData.fLastRunningSpeed = npcData->fRun_Speed;
					anNpc->sNpcData.fLastWalkingSpeed = npcData->fWalk_Speed;
					anNpc->sNpcData.wAttackSpeedRate = npcData->wAttack_Speed_Rate;					
					AddNpc(anNpc, npcSpawn->mob_Tblidx, it->first);
				}
			}
		}
	}
}

void	CNPCServer::LoadMobs()
{
	for (CTableContainer::SPAWNTABLEIT it = GetTableContainer()->BeginMobSpawnTable(); it != GetTableContainer()->EndMobSpawnTable(); it++)
	{
		CSpawnTable* pSpawnTable = GetTableContainer()->GetMobSpawnTable(it->first);
		if (pSpawnTable)
		{
			for (CTable::TABLEIT pi = pSpawnTable->Begin(); pi != pSpawnTable->End(); pi++)
			{
				sMOB_FULLDATA* anMob = new sMOB_FULLDATA;
				sSPAWN_TBLDAT* mobSpawn = reinterpret_cast<sSPAWN_TBLDAT*>(pi->second);
				sMOB_TBLDAT* mobData = reinterpret_cast<sMOB_TBLDAT*>(GetTableContainer()->GetMobTable()->FindData(mobSpawn->mob_Tblidx));
				if (mobData)
				{
					anMob->hHandler = AcquireSerialId();
					anMob->worldId = it->first;
					anMob->sMobData.tblidx = mobData->tblidx;
					anMob->sMobData.fLastRunningSpeed = mobData->fRun_Speed;
					anMob->sMobData.fLastWalkingSpeed = mobData->fWalk_Speed;					
					anMob->sMobData.wAttackSpeedRate = mobData->wAttack_Speed_Rate;
					anMob->sMobData.wCurEP = mobData->wBasic_EP;
					anMob->sMobData.wCurLP = mobData->wBasic_LP;
					anMob->sMobData.wMaxEP = mobData->wBasic_EP;
					anMob->sMobData.wMaxLP = mobData->wBasic_LP;
					anMob->bFightMode = false;
					anMob->byAspectID = CHARSTATE_SPAWNING;
					anMob->vSpawnDir.x = mobSpawn->vSpawn_Dir.x;
					anMob->vSpawnDir.y = mobSpawn->vSpawn_Dir.y;
					anMob->vSpawnDir.z = mobSpawn->vSpawn_Dir.z;
					anMob->vSpawnLoc.x = mobSpawn->vSpawn_Loc.x;
					anMob->vSpawnLoc.y = mobSpawn->vSpawn_Loc.y;
					anMob->vSpawnLoc.z = mobSpawn->vSpawn_Loc.z;
					AddMob(anMob, mobData->tblidx, it->first);
				}
			}
		}	
	}
}

bool CNPCServer::AddNpc(sNPC_FULLDATA* npc,TBLIDX tblidx,TBLIDX worldID)
{
	if (FindNpc(tblidx,worldID))
		return false;

	m_npcList.insert(std::pair<TBLIDX, sNPC_FULLDATA*>(tblidx, npc));
	return true;
}
bool CNPCServer::FindNpc(TBLIDX tblidx, TBLIDX worldID)
{
	for (NPC_LIST_IT it = m_npcList.begin(); it != m_npcList.end(); it++)
	{
		if (worldID == it->second->worldId)
			if (tblidx == it->first)
				return true;
	}
	return false;
}
bool CNPCServer::AddMob(sMOB_FULLDATA* mob, TBLIDX tblidx, TBLIDX worldID)
{
	if (FindMob(tblidx, worldID))
		return false;

	m_mobList.insert(std::pair<TBLIDX, sMOB_FULLDATA*>(tblidx, mob));
	return true;
}
bool CNPCServer::FindMob(TBLIDX tblidx, TBLIDX worldID)
{
	for (MOB_LIST_IT it = m_mobList.begin(); it != m_mobList.end(); it++)
	{
		if (worldID == it->second->worldId)
			if (tblidx == it->first)
				return true;
	}
	return false;
}

HOBJECT	CNPCServer::AcquireSerialId(void)
{
	if (m_uiSerialId++)
	{
		if (m_uiSerialId == 0xffffffff)
			m_uiSerialId = 0;
	}

	return m_uiSerialId;
}

bool	CNPCServer::AddSpawnObj(HOBJECT hPlayer, HOBJECT npc)
{
	if (FindSpawnObj(hPlayer, npc))
		return false;

	m_pSpawnList.insert(std::pair<HOBJECT, HOBJECT>(hPlayer, npc));

	return true;
}

void	CNPCServer::RemoveSpawnObj(HOBJECT hPlayer, HOBJECT npc)
{
	for (SPAWNMAP_IT it = m_pSpawnList.begin(); it != m_pSpawnList.end(); it++)
	{
		if (it->first == hPlayer)
		{
			if (it->second == npc)
			{
				m_pSpawnList.erase(it);
				break;
			}
		}
	}
}

bool	CNPCServer::FindSpawnObj(HOBJECT hPlayer, HOBJECT npc)
{
	for (SPAWNMAP_IT it = m_pSpawnList.begin(); it != m_pSpawnList.end(); it++)
	{
		if (hPlayer == it->first)
		{
			if (it->second == npc)
				return true;
		}
	}

	return false;
}

bool	CNPCServer::AddSpawnMobObj(HOBJECT hPlayer, HOBJECT mob)
{
	if (FindSpawnMobObj(hPlayer, mob))
		return false;

	m_pSpawnMobList.insert(std::pair<HOBJECT, HOBJECT>(hPlayer, mob));

	return true;
}

void	CNPCServer::RemoveSpawnMobObj(HOBJECT hPlayer, HOBJECT mob)
{
	for (SPAWNMAP_IT it = m_pSpawnMobList.begin(); it != m_pSpawnMobList.end(); it++)
	{
		if (it->first == hPlayer)
		{
			if (it->second == mob)
			{
				m_pSpawnMobList.erase(it);
				break;
			}
		}
	}
}

bool	CNPCServer::FindSpawnMobObj(HOBJECT hPlayer, HOBJECT mob)
{
	for (SPAWNMAP_IT it = m_pSpawnMobList.begin(); it != m_pSpawnMobList.end(); it++)
	{
		if (hPlayer == it->first)
		{
			if (it->second == mob)
				return true;
		}
	}

	return false;
}

void	CNPCServer::RefreshSerial()
{
	CNtlPacket packet2(sizeof(sNG_REFRESH_SERIAL));
	sNG_REFRESH_SERIAL* req = (sNG_REFRESH_SERIAL*)packet2.GetPacketData();
	req->hLastSerial = m_uiSerialId;
	req->wOpCode = NG_REFRESH_SERIAL;
	packet2.SetPacketLen(sizeof(sNG_REFRESH_SERIAL));
	GetGameSession()->PushPacket(&packet2);
}

sNPC_FULLDATA* CNPCServer::GetNpcData(HOBJECT hNpc)
{
	for (NPC_LIST_IT it = m_npcList.begin(); it != m_npcList.end(); it++)
	{
		if (hNpc == it->second->hHandler)
				return it->second;
	}
	return NULL;
}