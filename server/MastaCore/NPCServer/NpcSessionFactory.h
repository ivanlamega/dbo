#pragma once
#include "NPCServer.h"
#include "NtlSessionFactory.h"

#ifndef CNPCSESSION_FACTORY_H
#define CNPCSESSION_FACTORY_H

class CNpcSessionFactory : public CNtlSessionFactory
{
public:
	CNpcSessionFactory();
	~CNpcSessionFactory();
	CNtlSession * CreateSession(SESSIONTYPE sessionType);
	void DestroySession(CNtlSession * pSession);
};
#endif

