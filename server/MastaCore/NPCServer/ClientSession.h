#pragma once
#include "BotServer.h"
#include "NtlSession.h"
#include "NtlPacketEncoder_RandKey.h"
///////////////////////////////////////RESPONSE PACKETS///////////////////////////
//NPC Server -> Game Server
#include "NtlPacketNG.h"
//NPC Server -> Master Server
#include "NtlPacketNM.h"
/////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////REQUEST PACKETS///////////////////////////
//Master Server -> NPC Server
#include "NtlPacketMN.h"
//Game Server -> NPC Server
#include "NtlPacketGN.h"
/////////////////////////////////////////////////////////////////////////////////
#include "NtlPacketUtil.h"

#ifndef CBOTSESSION_H
#define CBOTSESSION_H

class CClientSession :	public CNtlSession
{
public:
	CClientSession(bool bAliveCheck = false, bool bOpcodeCheck = false);
	~CClientSession();
	int							OnAccept();
	void						OnClose();
	int							OnDispatch(CNtlPacket * pPacket);
	// Packet functions
	void						SendBotAgentEnterReq(CNtlPacket* pPacket);
	// End Packet functions
private:
	CNtlPacketEncoder_RandKey	m_packetEncoder;
};

#endif