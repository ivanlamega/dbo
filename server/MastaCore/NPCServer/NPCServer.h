#pragma once
#include "NtlSfx.h"
#include "NpcSession.h"
#include "NpcSessionFactory.h"

#ifndef CNPC_SERVER_H
#define CNPC_SERVER_H

class CMasterSession;
class CGameSession;
class CTableContainer;
class CNtlBitFlagManager;
class CTableFileNameList;

#include "MasterSession.h"
#include "GameSession.h"

#include "TableContainer.h"
#include "NtlBitFlagManager.h"
#include "TableFileNameList.h"
//List Tables
#include "MerchantTable.h"
#include "MobTable.h"
#include "NewbieTable.h"
#include "NPCTable.h"
#include "PCTable.h"
#include "BasicDropTable.h"
#include "ItemOptionTable.h"
#include "ItemTable.h"
#include "NormalDropTable.h"
#include "LegendaryDropTable.h"
#include "SuperiorDropTable.h"
#include "UseItemTable.h"
#include "SetItemTable.h"
#include "EachDropTable.h"
#include "TypeDropTable.h"
#include "ExcellentDropTable.h"
#include "ActionTable.h"
#include "FormulaTable.h"
#include "SkillTable.h"
#include "SpawnTable.h"
#include "NpcSpeechTable.h"
#include "SetItemTable.h"
#include "TimeQuestTable.h"
#include "MobMovePatternTable.h"
#include "ItemUpgradeTable.h"
#include "ItemMixMachineTable.h"
#include "HLSItemTable.h"
#include "HLSMerchantTable.h"
#include "ObjectTable.h"
#include "WorldTable.h"
//End of Tables

#define HTRIGGER_OBJECT_WORLD 100000

class CNPCServer :	public CNtlServerApp
{
	//Members
public:
	CNtlString				ServersConfig[99][2];//For Config of multiple server 0->{Ip,Port} - Luiz45
private:
	CNtlAcceptor			m_clientAcceptor;
	//Master Server
	CNtlMutex				m_masterServerMutex;
	CNtlConnector			m_masterConnector;
	CMasterSession*			m_masterSession;
	//Game Server
	CNtlMutex				m_gameServerMutex;
	CNtlConnector			m_gameConnector;
	CGameSession*			m_gameSession;
	//Auth Session ptr refresh
	CNpcSession*			m_pNpcSession;

	CNtlLog  				m_log;
	sSERVERCONFIG			m_config;
	CNtlString				EnableMultipleServers;//Added for enabling multiple server - Luiz45

	typedef std::list<HSESSION> SESSIONLIST;
	typedef SESSIONLIST::iterator SESSIONIT;
	
	SESSIONLIST				m_sessionList;
	SPAWNMAP				m_pSpawnList;
	SPAWNMAP				m_pSpawnMobList;
	//Methods
	void					DoAliveTime();
	bool					m_bGameServerConnected;
	CTableContainer*		g_pTableContainer;
	DWORD					m_uiSerialId;	
public:	
	NPC_LIST				m_npcList;
	MOB_LIST				m_mobList;
	//For Multiple Server - Luiz45
	const std::string		GetConfigFileEnabledMultipleServers();
	const DWORD				GetConfigFileMaxServers();
	int						OnInitApp();
	int						OnCreate();
	void					OnDestroy();
	int						CreateTableContainer();
	int						OnCommandArgument(int argc, _TCHAR* argv[]);
	int						OnConfiguration(const char * lpszConfigFile);
	int						OnAppStart();
	int						GetSessionCount();
	void					Run();
	bool					Add(HSESSION hSession);
	void					Remove(HSESSION hSession);
	bool					Find(HSESSION hSession);
	bool					AddSpawnObj(HOBJECT hPlayer, HOBJECT npc);
	void					RemoveSpawnObj(HOBJECT hPlayer, HOBJECT npc);
	bool					FindSpawnObj(HOBJECT hPlayer, HOBJECT npc);
	bool					AddSpawnMobObj(HOBJECT hPlayer, HOBJECT mob);
	void					RemoveSpawnMobObj(HOBJECT hPlayer, HOBJECT mob);
	bool					FindSpawnMobObj(HOBJECT hPlayer, HOBJECT mob);
	void					SetMasterSession(CMasterSession * pServerSession);
	void					LoadNpcs();
	void					LoadMobs();
	CMasterSession*			AcquireMasterSession();
	CMasterSession*			GetMasterSession();
	void					SetGameSession(CGameSession * pServerSession);
	CGameSession*			AcquireGameSession();
	CGameSession*			GetGameSession();
	HOBJECT					AcquireSerialId(void);
	sOBJECT_TBLDAT*			GetObjectTbl(HOBJECT hObject);
	sWORLD_TBLDAT*			GetWorld(WORLDID WorldId);
	CTableContainer*		GetTableContainer();
	void					SetGameServerConnected(bool c){ m_bGameServerConnected = c; }
	bool					IsGameServerConnected() { return m_bGameServerConnected; }
	sNPC_FULLDATA*			GetNpcData(HOBJECT hNpc);
	bool					AddNpc(sNPC_FULLDATA* npc, TBLIDX tblidx, TBLIDX worldID);
	void					RefreshSerial();
	bool					FindNpc(TBLIDX tblidx, TBLIDX worldID);
	bool					AddMob(sMOB_FULLDATA*	 npc, TBLIDX tblidx, TBLIDX worldID);
	bool					FindMob(TBLIDX tblidx, TBLIDX worldID);
	HOBJECT					GetLastSerialId() {return m_uiSerialId; }

};

#endif

inline CTableContainer* CNPCServer::GetTableContainer()
{
	return g_pTableContainer;
}