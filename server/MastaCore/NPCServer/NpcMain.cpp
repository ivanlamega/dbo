// NpcMain.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "NtlSfx.h"
#include "NPCServer.h"
#include "NtlFile.h"


int _tmain(int argc, _TCHAR* argv[])
{
	NtlSetPrintFlag(PRINT_APP);

	CNPCServer app;
	CNtlFileStream traceFileStream;

	// LOG FILE
	std::cout << "Loading Server.ini... \n";
	int rc = traceFileStream.Create("NpcServerLog");
	rc = app.Create(argc, argv, ".\\Server.ini");
	NtlSetPrintStream(traceFileStream.GetFilePtr());
	NtlSetPrintFlag(PRINT_APP | PRINT_SYSTEM);

	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "log file CreateFile error %d(%s)", rc, NtlGetErrorMessage(rc));
		return rc;
	}
	std::cout << "Loaded!\n";

	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "Server Application Create Fail %d(%s)", rc, NtlGetErrorMessage(rc));
		return rc;
	}
	app.Start();
	std::cout << "Loading Tables...\n";
	app.CreateTableContainer();
	std::cout << "Loading Tables - DONE\n";
	std::cout << "Loading NPCS...\n";
	app.LoadNpcs();
	std::cout << "Loading NPCS - DONE\n";
	std::cout << "Loading MOBS...\n";
	app.LoadMobs();
	std::cout << "Loading MOBS - DONE\n";	
	std::cout << "Running... \n";
	app.RefreshSerial();
	app.WaitForTerminate();


	return 0;
}

