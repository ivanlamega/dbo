#pragma once
#include "NtlSfx.h"
#include "NtlSfxDB.h"
#include "QuerySession.h"
#include "QueryServerUnitHelper.h"
#include "QuerySessionFactory.h"

#ifndef CQUERY_SERVER_H
#define CQUERY_SERVER_H

class CMasterSession;
class CAuthSession;
class CCharSession;
class CCommunitySession;
class CGameSession;
class CNtlEvent;
#include "AuthSession.h"
#include "CharSession.h"
#include "MasterSession.h"
#include "CommunitySession.h"
#include "GameSession.h"
#include "QuerysDefinition.h"
//Tables - MySQL
#include "TableAccount.h"
#include "TableBuffList.h"
#include "TableCharacters.h"
#include "TableHTBList.h"
#include "TableInventory.h"
#include "TableQuickSlot.h"
#include "TableSkills.h"
#include "TableWarFog.h"
/////////////////////////////
#include "NtlResultCode.h"

class CQueryServer :	public CNtlServerApp
{
	enum STATE
	{
		MASTER_SERVER_STATE_CONNECTED,
		MASTER_SERVER_STATE_JOINED,
		MASTER_SERVER_STATE_DISCONNECTED,
		OPERATING_SERVER_STATE_CONNECTED,
		OPERATING_SERVER_STATE_JOINED,
		OPERATING_SERVER_STATE_DISCONNECTED,
		QUERY_SERVER_STATE_CONNECTED,
		QUERY_SERVER_STATE_JOINED,
		QUERY_SERVER_STATE_DISCONNECTED,
	};
	//Members
public:
	CNtlString				ServersConfig[99][2];//For Config of multiple server 0->{Ip,Port} - Luiz45	
private:
	//Query Server ptr Refresh
	CQuerySession*				m_pQuerySession;
	//Master Server Connection
	CNtlConnector				m_masterServerConnector;
	CNtlMutex					m_masterServerMutex;
	CMasterSession*				m_pMasterServerSession;	
	//Operating Server Connection
	CNtlConnector				m_operatingServerConnector;
	CNtlMutex					m_operatingServerMutex;
	//CServerSession *			m_pOperatingServerSession;
	//Auth Server Connection
	CNtlConnector				m_authServerConnector;
	CNtlMutex					m_authServerMutex;
	CAuthSession*			    m_pAuthServerSession;
	//Char Server Connection
	CNtlConnector				m_charServerConnector;
	CNtlMutex					m_charServerMutex;
	CCharSession*				m_pCharServerSession;
	//Community Server Connection
	CNtlConnector				m_communityServerConnector;
	CNtlMutex					m_communityServerMutex;
	CCommunitySession*			m_pCommunityServerSession;
	//Game Server Connection
	CNtlConnector				m_gameServerConnector;
	CNtlMutex					m_gameServerMutex;
	CGameSession*				m_pGameServerSession;

	CNtlAcceptor			m_clientAcceptor;
	CNtlLog  				m_log;
	sSERVERCONFIG   		m_config;
	CNtlString				EnableMultipleServers;//Added for enabling multiple server - Luiz45

	typedef std::list<HSESSION> SESSIONLIST;
	typedef SESSIONLIST::iterator SESSIONIT;

	SESSIONLIST				m_sessionList;
	CNtlDatabaseManager		databaseManager;
	HDATABASE				hDB;
	//Connected
	bool					m_bAuthServerConnected;
	bool					m_bMasterServerConnected;
	bool					m_bCharServerConnected;
	bool					m_bCommunityServerConnected;
	bool					m_bGameServerConnected;
	void					DoAliveTime();
	//Methods
public:	
	//For Multiple Server - Luiz45
	const std::string		GetConfigFileEnabledMultipleServers();
	const DWORD				GetConfigFileMaxServers();	
	int						OnInitApp();
	int						OnCreate();
	void					OnDestroy();
	int						OnCommandArgument(int argc, _TCHAR* argv[]);
	int						OnConfiguration(const char * lpszConfigFile);
	int						OnAppStart();
	int						GetSessionCount();
	void					Run();
	bool					Add(HSESSION hSession);
	void					Remove(HSESSION hSession);
	bool					Find(HSESSION hSession);
	CNtlDatabaseManager*	GetDatabaseManager(){ return &databaseManager; }
	HDATABASE				GetHDatabase(){ return hDB; }
	void					SetHDatabase(HDATABASE hDB){ this->hDB = hDB; }
	void					SetAuthSession(CAuthSession * pServerSession);
	CAuthSession*			AcquireAuthSession();
	CAuthSession*			GetAuthSession();
	void					SetCharSession(CCharSession * pServerSession);
	CCharSession*			AcquireCharSession();
	CCharSession*			GetCharSession();
	void					SetMasterSession(CMasterSession * pServerSession);
	CMasterSession*			AcquireMasterSession();
	CMasterSession*			GetMasterSession();
	void					SetCommunitySession(CCommunitySession * pServerSession);
	CCommunitySession*		AcquireCommunitySession();
	CCommunitySession*		GetCommunitySession();
	void					SetGameSession(CGameSession * pServerSession);
	CGameSession*			AcquireGameSession();
	CGameSession*			GetGameSession();
	sSERVERCONFIG*			GetQueryServerConfig(){ return &m_config; }
	void					SetAuthServerConnected(bool c){ m_bAuthServerConnected = c; }
	void					SetMasterServerConnected(bool c){ m_bMasterServerConnected = c; }
	void					SetCharServerConnected(bool c){ m_bCharServerConnected = c; }
	void					SetCommunityServerConnected(bool c){ m_bCommunityServerConnected = c; }
	void					SetGameServerConnected(bool c){ m_bGameServerConnected = c; }
	bool					IsAuthServerConnected() { return m_bAuthServerConnected; }
	bool					IsMasterServerConnected(){ return m_bMasterServerConnected; }
	bool					IsCharServerConnected(){ return m_bCharServerConnected; }
	bool					IsCommunityServerConnected(){ return m_bCommunityServerConnected; }
	bool					IsGameServerConnected(){ return m_bGameServerConnected; }
	void					ConvertDBToStruct(sPC_SUMMARY* create,
												int				charID,
												WCHAR*			awchName,
												BYTE			byRace,
												BYTE			byClass,
												bool			bIsAdult,
												BYTE			byGender,
												BYTE			byFace,
												BYTE			byHair,
												BYTE			byHairColor,
												BYTE			bySkinColor,
												BYTE			byLevel,
												TBLIDX			worldTblidx,
												WORLDID			worldId,
												float			fPositionX,
												float			fPositionY,
												float			fPositionZ,
												DWORD			dwMoney,
												DWORD			dwMoneyBank,
												DWORD			dwMapInfoIndex,
												bool			bTutorialFlag,
												bool			bNeedNameChange);
	void					ConvertStructTODB(sPC_SUMMARY* from, 
											  CSqlUnit_SP_CharacterCreate* to,
											  BYTE byBlood,
											  BYTE byBindType,
											  float fDirX,
											  float fDirY,
											  float fDirZ,
											  float fBindLocX,
											  float fBindLocY,
											  float fBindLocZ,
											  float fBindDirX,
											  float fBindDirY,
											  float fBindDirZ,
											  DWORD dwBindObjectTblidx,
											  DWORD dwBindWorldID,
											  DWORD dwInitEP,
											  DWORD dwInitLP,
											  DWORD dwInitRP);
	void					ConvertStructTODB(CSqlUnit_SP_CharacterSelect* to, sPC_SUMMARY* from);
	void					ConvertInventoryTMPToInventory(sITEM_SUMMARY* tmp, sITEM_SUMMARY* pcSummary);
	void					ConvertStructInvTODB(int* charID, CSqlUnit_SP_InventoryCreate* pSqlUnit, sITEM_DATA* sm);
	void					ConvertDBInventoryToSummary(int* charID, CSqlUnit_SP_InventorySelect* pSqlUnit, sITEM_SUMMARY* sm);
	void					CreateChar(int pos,
										sPC_SUMMARY* create,
										int				charID,
										WCHAR*			awchName,
										BYTE			byRace,
										BYTE			byClass,
										BYTE			byGender,
										BYTE			byFace,
										BYTE			byHair,
										BYTE			byHairColor,
										BYTE			bySkinColor,
										TBLIDX			worldTblidx,
										WORLDID			worldId,
										float			fPositionX,
										float			fPositionY,
										float			fPositionZ,
										sITEM_SUMMARY*	sItem,
										DWORD			dwMapInfoIndex
									  );
	int						UpdateChar(sPC_SUMMARY* update,
										int				charID,
										WCHAR*			awchName,
										BYTE			byRace,
										BYTE			byClass,
										BYTE			byGender,
										BYTE			byFace,
										BYTE			byHair,
										BYTE			byHairColor,
										BYTE			bySkinColor,
										TBLIDX			worldTblidx,
										WORLDID			worldId,
										float			fPositionX,
										float			fPositionY,
										float			fPositionZ,
										sITEM_SUMMARY*	sItem,
										DWORD			dwMapInfoIndex
										);
	CNtlEvent				g_exitEvent;
};

#endif