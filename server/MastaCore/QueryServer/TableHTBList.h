#pragma once
#include "QueryServer.h"
#include "NtlQuery.h"
class CTableHTBList :	public CNtlQuery
{
private:
	SQL_ID  eSqlID;
	DWORD			m_charId;
	DWORD			m_skillId;
	BYTE			m_bySlotId;
	DWORD			m_dwTimeRemaining;
public:
	CTableHTBList();
	~CTableHTBList();
	int ExecuteQuery(CNtlDatabaseConnection * pConnection);
	int ExecuteResult();
	void    SetSqlID(SQL_ID ID_SQL) { this->eSqlID = ID_SQL; }
	void	SetCharID(DWORD charId) { this->m_charId = charId; }
};

