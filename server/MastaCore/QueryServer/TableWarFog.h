#pragma once
#include "QueryServer.h"
#include "NtlQuery.h"
class CTableWarFog :	public CNtlQuery
{
private:
	SQL_ID		eSqlID;
	DWORD		charID;
	char		m_achWarFogFlag[NTL_MAX_SIZE_WAR_FOG];
public:
	CTableWarFog();
	~CTableWarFog();
	int ExecuteQuery(CNtlDatabaseConnection * pConnection);
	int ExecuteResult();
	void    SetSqlID(SQL_ID ID_SQL) { this->eSqlID = ID_SQL; }
	void	SetCharID(DWORD charId) { this->charID = charId; }
};

