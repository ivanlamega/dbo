#pragma once
#include "QueryServer.h"
#include "NtlQuery.h"
class CTableBuffList :	public CNtlQuery
{
private:
	SQL_ID			eSqlID;
	DWORD			charID;
	DWORD			m_dwTblidx;
	BYTE			m_bySourceType;			// eDBO_OBJECT_SOURCE
	BYTE			m_byBuffGroup;
	DWORD			m_dwInitialDuration;
	DWORD			m_dwTimeRemaining;		// in millisecs.
	float			m_afEffectValue0;
	float			m_afEffectValue1;
public:
	CTableBuffList();
	~CTableBuffList();
	int ExecuteQuery(CNtlDatabaseConnection * pConnection);
	int ExecuteResult();
	void    SetSqlID(SQL_ID ID_SQL) { this->eSqlID = ID_SQL; }
	void	SetCharID(DWORD charId) { this->charID = charId; }
};

