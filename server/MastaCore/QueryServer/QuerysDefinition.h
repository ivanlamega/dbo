#include "QueryServer.h"
/**
This will be storing all SQL QUERYS that server will be executing
To create a New query or New Table you need create a Table in Database(SQL)
next you need go to NtlShared->ServerGeneralDef.h
add a SP_TableFunction
Example of CRUD:
SP_TableSelect
SP_TableCreate
SP_TableUpdate
SP_TableDelete
After defined in ServerGeneralDef.h
you need go to Here and make your query(just look below the examples)
Use "BEGIN_DECLARE_SQLUNIT2" for: Select,Update,Create,Delete
Use "BEGIN_DECLARE_SQLUNIT" for Store Procedure or Function: CharCreate
And after that you need create 2 files called
CTable_Your_Awesome_Table (Example CTableAccounts: TableAccounts.h,TableAccounts.cpp)
Put then into "Tables Folder" in this project
for last
you need go to Unity Helper and add your "SP_NewThing"

You can search each Specific Query just doing a CTRL+F and searching for the table name
**/
//---------------------------------------------------------ACCOUNT-----------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_AccountSelect)

	DEFINE_QUERY("Select AccountID, Username, Password, LastServer from Account where Username = ?")

	BEGIN_VARIABLE()
		DWORD	m_dwAccountID;
		char	m_szUserID[MAX_SIZE_USER_ID + 1];
		char	m_szUserPW[MAX_SIZE_USER_PW + 1];
		DWORD	m_dwLastFarmID;
	END_VARIABLE()

	// Column Must be in the same order as Query
	BEGIN_COLUMN(4)
		COLUMN_ENTRY(m_dwAccountID)
		COLUMN_ENTRY_STR(m_szUserID)
		COLUMN_ENTRY_STR(m_szUserPW)
		COLUMN_ENTRY(m_dwLastFarmID)
	END_PARAM()

	// Where condition
	BEGIN_PARAM(1)
		PARAM_ENTRY_STR(SQL_PARAM_INPUT, m_szUserID)
	END_PARAM()

END_DECLARE_SQLUNIT()
//-----------------------------------------------------------CHARACTERS---------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_CharacterSelect)
	DEFINE_QUERY("Select awchName,byRace,byClass,byGender,byFace,byHair,byHairColor,bySkinColor,byLevel,byBindType,byBlood,byMarking,bIsAdult,worldTblidx,worldId,bTutorialFlag,bNeedNameChange,bChangeClass,fPositionX,fPositionY,fPositionZ,fDirX,fDirY,fDirZ,fBindPositionX,fBindPositionY,fBindPositionZ,fBindDirX,fBindDirY,fBindDirZ,AccountID,ServerID,CharID,dwBindObjectTblidx,dwBindWorldID,dwMoney,dwMoneyBank,dwMapInfoIndex,dwLP,dwEP,dwRP,dwExp,dwReputationPoint,dwMudosaPoint,dwSpPoint,dwTutorialHint from Characters where AccountID = ? AND ServerID = ?")
	BEGIN_VARIABLE()
		char							m_awchName[NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1];
		BYTE							m_byRace;
		BYTE							m_byClass;
		BYTE							m_byGender;
		BYTE							m_byFace;
		BYTE							m_byHair;
		BYTE							m_byHairColor;
		BYTE							m_bySkinColor;
		BYTE							m_byLevel;
		BYTE							m_byBindType;
		BYTE							m_byBlood;
		BYTE							m_byMarking;//eMARKING_TYPE
		int								m_bIsAdult;
		int								m_worldTblidx;
		int								m_worldId;
		int								m_bTutorialFlag;
		int								m_bNeedNameChange;
		int								m_bChangeClass;
		float							m_fPositionX;
		float							m_fPositionY;
		float							m_fPositionZ;
		float							m_fDirX;
		float							m_fDirY;
		float							m_fDirZ;
		float							m_fBindPositionX;
		float							m_fBindPositionY;
		float							m_fBindPositionZ;
		float							m_fBindDirX;
		float							m_fBindDirY;
		float							m_fBindDirZ;
		DWORD							m_dwAccountID;
		DWORD							m_dwServerID;
		DWORD							m_dwCharID;
		DWORD							m_dwBindObjectTblidx;
		DWORD							m_dwBindWorldID;
		DWORD							m_dwMoney;
		DWORD							m_dwMoneyBank;
		DWORD							m_dwMapInfoIndex;
		DWORD							m_dwLP;
		DWORD							m_dwEP;
		DWORD							m_dwRP;
		DWORD							m_dwEXP;
		DWORD							m_dwReputationPoint;
		DWORD							m_dwMudosaPoint;
		DWORD							m_dwSpPoint;
		DWORD							m_dwTutorialHint;
	END_VARIABLE()
	// Column Must be in the same order as Query to receive the values
	BEGIN_COLUMN(46)
			COLUMN_ENTRY_STR(m_awchName)
			COLUMN_ENTRY(m_byRace)
			COLUMN_ENTRY(m_byClass)			
			COLUMN_ENTRY(m_byGender)
			COLUMN_ENTRY(m_byFace)
			COLUMN_ENTRY(m_byHair)
			COLUMN_ENTRY(m_byHairColor)
			COLUMN_ENTRY(m_bySkinColor)
			COLUMN_ENTRY(m_byLevel)
			COLUMN_ENTRY(m_byBindType)
			COLUMN_ENTRY(m_byBlood)
			COLUMN_ENTRY(m_byMarking)
			COLUMN_ENTRY(m_bIsAdult)
			COLUMN_ENTRY(m_worldTblidx)
			COLUMN_ENTRY(m_worldId)
			COLUMN_ENTRY(m_bTutorialFlag)
			COLUMN_ENTRY(m_bNeedNameChange)
			COLUMN_ENTRY(m_bChangeClass)
			COLUMN_ENTRY(m_fPositionX)
			COLUMN_ENTRY(m_fPositionY)
			COLUMN_ENTRY(m_fPositionZ)
			COLUMN_ENTRY(m_fDirX)
			COLUMN_ENTRY(m_fDirY)
			COLUMN_ENTRY(m_fDirZ)
			COLUMN_ENTRY(m_fBindPositionX)
			COLUMN_ENTRY(m_fBindPositionY)
			COLUMN_ENTRY(m_fBindPositionZ)
			COLUMN_ENTRY(m_fBindDirX)
			COLUMN_ENTRY(m_fBindDirY)
			COLUMN_ENTRY(m_fBindDirZ)
			COLUMN_ENTRY(m_dwAccountID)
			COLUMN_ENTRY(m_dwServerID)
			COLUMN_ENTRY(m_dwCharID)
			COLUMN_ENTRY(m_dwBindObjectTblidx)
			COLUMN_ENTRY(m_dwBindWorldID)
			COLUMN_ENTRY(m_dwMoney)
			COLUMN_ENTRY(m_dwMoneyBank)
			COLUMN_ENTRY(m_dwMapInfoIndex)
			COLUMN_ENTRY(m_dwLP)
			COLUMN_ENTRY(m_dwEP)
			COLUMN_ENTRY(m_dwRP)
			COLUMN_ENTRY(m_dwEXP)
			COLUMN_ENTRY(m_dwReputationPoint)
			COLUMN_ENTRY(m_dwMudosaPoint)
			COLUMN_ENTRY(m_dwSpPoint)
			COLUMN_ENTRY(m_dwTutorialHint)
	END_PARAM()
	// Where condition
	BEGIN_PARAM(2)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwAccountID)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwServerID)
	END_PARAM()
END_DECLARE_SQLUNIT()
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_CharacterSelectByID)
	DEFINE_QUERY("Select chars.awchName,chars.byRace,chars.byClass,chars.byGender,chars.byFace,chars.byHair,chars.byHairColor,chars.bySkinColor,chars.byLevel,chars.byBindType,chars.byBlood,chars.byMarking,chars.bIsAdult,chars.worldTblidx,chars.worldId,chars.bTutorialFlag,chars.bNeedNameChange,chars.bChangeClass,chars.fPositionX,chars.fPositionY,chars.fPositionZ,chars.fDirX,chars.fDirY,chars.fDirZ,chars.fBindPositionX,chars.fBindPositionY,chars.fBindPositionZ,chars.fBindDirX,chars.fBindDirY,chars.fBindDirZ,chars.AccountID,chars.ServerID,chars.CharID,chars.dwBindObjectTblidx,chars.dwBindWorldID,chars.dwMoney,chars.dwMoneyBank,chars.dwMapInfoIndex,chars.dwLP,chars.dwEP,chars.dwRP,chars.dwExp,chars.dwReputationPoint,chars.dwMudosaPoint,chars.dwSpPoint,chars.dwTutorialHint,acc.AdminAcc,chars.dwNetPy from Characters chars left join account acc on acc.AccountID = chars.AccountID where chars.AccountID = ? AND chars.charID = ?")
	BEGIN_VARIABLE()
		char							m_awchName[NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1];
		BYTE							m_byRace;
		BYTE							m_byClass;
		BYTE							m_byGender;
		BYTE							m_byFace;
		BYTE							m_byHair;
		BYTE							m_byHairColor;
		BYTE							m_bySkinColor;
		BYTE							m_byLevel;
		BYTE							m_byBindType;
		BYTE							m_byBlood;
		BYTE							m_byMarking;//eMARKING_TYPE
		int								m_bIsAdult;
		int								m_worldTblidx;
		int								m_GMAcc;
		int								m_worldId;
		int								m_bTutorialFlag;
		int								m_bNeedNameChange;
		int								m_bChangeClass;
		float							m_fPositionX;
		float							m_fPositionY;
		float							m_fPositionZ;
		float							m_fDirX;
		float							m_fDirY;
		float							m_fDirZ;
		float							m_fBindPositionX;
		float							m_fBindPositionY;
		float							m_fBindPositionZ;
		float							m_fBindDirX;
		float							m_fBindDirY;
		float							m_fBindDirZ;
		DWORD							m_dwAccountID;
		DWORD							m_dwServerID;
		DWORD							m_dwCharID;
		DWORD							m_dwBindObjectTblidx;
		DWORD							m_dwBindWorldID;
		DWORD							m_dwMoney;
		DWORD							m_dwMoneyBank;
		DWORD							m_dwMapInfoIndex;
		DWORD							m_dwLP;
		DWORD							m_dwEP;
		DWORD							m_dwRP;
		DWORD							m_dwEXP;
		DWORD							m_dwReputationPoint;
		DWORD							m_dwMudosaPoint;
		DWORD							m_dwSpPoint;
		DWORD							m_dwTutorialHint;
		DWORD							m_dwNetP;
	END_VARIABLE()
		// Column Must be in the same order as Query to receive the values
		BEGIN_COLUMN(48)
		COLUMN_ENTRY_STR(m_awchName)
		COLUMN_ENTRY(m_byRace)
		COLUMN_ENTRY(m_byClass)
		COLUMN_ENTRY(m_byGender)
		COLUMN_ENTRY(m_byFace)
		COLUMN_ENTRY(m_byHair)
		COLUMN_ENTRY(m_byHairColor)
		COLUMN_ENTRY(m_bySkinColor)
		COLUMN_ENTRY(m_byLevel)
		COLUMN_ENTRY(m_byBindType)
		COLUMN_ENTRY(m_byBlood)
		COLUMN_ENTRY(m_byMarking)
		COLUMN_ENTRY(m_bIsAdult)
		COLUMN_ENTRY(m_worldTblidx)
		COLUMN_ENTRY(m_worldId)
		COLUMN_ENTRY(m_bTutorialFlag)
		COLUMN_ENTRY(m_bNeedNameChange)
		COLUMN_ENTRY(m_bChangeClass)
		COLUMN_ENTRY(m_fPositionX)
		COLUMN_ENTRY(m_fPositionY)
		COLUMN_ENTRY(m_fPositionZ)
		COLUMN_ENTRY(m_fDirX)
		COLUMN_ENTRY(m_fDirY)
		COLUMN_ENTRY(m_fDirZ)
		COLUMN_ENTRY(m_fBindPositionX)
		COLUMN_ENTRY(m_fBindPositionY)
		COLUMN_ENTRY(m_fBindPositionZ)
		COLUMN_ENTRY(m_fBindDirX)
		COLUMN_ENTRY(m_fBindDirY)
		COLUMN_ENTRY(m_fBindDirZ)
		COLUMN_ENTRY(m_dwAccountID)
		COLUMN_ENTRY(m_dwServerID)
		COLUMN_ENTRY(m_dwCharID)
		COLUMN_ENTRY(m_dwBindObjectTblidx)
		COLUMN_ENTRY(m_dwBindWorldID)
		COLUMN_ENTRY(m_dwMoney)
		COLUMN_ENTRY(m_dwMoneyBank)
		COLUMN_ENTRY(m_dwMapInfoIndex)
		COLUMN_ENTRY(m_dwLP)
		COLUMN_ENTRY(m_dwEP)
		COLUMN_ENTRY(m_dwRP)
		COLUMN_ENTRY(m_dwEXP)
		COLUMN_ENTRY(m_dwReputationPoint)
		COLUMN_ENTRY(m_dwMudosaPoint)
		COLUMN_ENTRY(m_dwSpPoint)
		COLUMN_ENTRY(m_dwTutorialHint)
		COLUMN_ENTRY(m_GMAcc)
		COLUMN_ENTRY(m_dwNetP)
	END_PARAM()
		// Where condition
	BEGIN_PARAM(2)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwAccountID)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwCharID)
	END_PARAM()
END_DECLARE_SQLUNIT()

//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT(SP_CharacterCreate, "{ call CharacterCreate(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }")

	BEGIN_VARIABLE()
		char							m_awchName[NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1];
		BYTE							m_byRace;
		BYTE							m_byClass;
		BYTE							m_byGender;
		BYTE							m_byFace;
		BYTE							m_byHair;
		BYTE							m_byHairColor;
		BYTE							m_bySkinColor;
		BYTE							m_byLevel;
		BYTE							m_byBindType;
		BYTE							m_byBlood;
		BYTE							m_byMarking;//eMARKING_TYPE
		int								m_bIsAdult;
		int								m_worldTblidx;
		int								m_worldId;
		int								m_bTutorialFlag;
		int								m_bNeedNameChange;
		int								m_bChangeClass;
		float							m_fPositionX;
		float							m_fPositionY;
		float							m_fPositionZ;
		float							m_fDirX;
		float							m_fDirY;
		float							m_fDirZ;
		float							m_fBindPositionX;
		float							m_fBindPositionY;
		float							m_fBindPositionZ;
		float							m_fBindDirX;
		float							m_fBindDirY;
		float							m_fBindDirZ;
		DWORD							m_dwAccountID;
		DWORD							m_dwServerID;
		DWORD							m_dwCharID;
		DWORD							m_dwBindObjectTblidx;
		DWORD							m_dwBindWorldID;
		DWORD							m_dwMoney;
		DWORD							m_dwMoneyBank;
		DWORD							m_dwMapInfoIndex;
		DWORD							m_dwLP;
		DWORD							m_dwEP;
		DWORD							m_dwRP;
		DWORD							m_dwEXP;
		DWORD							m_dwReputationPoint;
		DWORD							m_dwMudosaPoint;
		DWORD							m_dwSpPoint;
		DWORD							m_dwTutorialHint;
	END_VARIABLE()

	BEGIN_PARAM(45)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwAccountID)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwServerID)
		PARAM_ENTRY_STR(SQL_PARAM_INPUT, m_awchName)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byRace)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byClass)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byGender)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byFace)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byHair)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byHairColor)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bySkinColor)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byLevel)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byBindType)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byBlood)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byMarking)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bIsAdult)		
		PARAM_ENTRY(SQL_PARAM_INPUT, m_worldTblidx)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_worldId)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bTutorialFlag)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bNeedNameChange)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bChangeClass)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_fPositionX)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_fPositionY)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_fPositionZ)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_fDirX)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_fDirY)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_fDirZ)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_fBindPositionX)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_fBindPositionY)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_fBindPositionZ)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_fBindDirX)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_fBindDirY)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_fBindDirZ)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwBindObjectTblidx)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwBindWorldID)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwMoney)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwMoneyBank)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwMapInfoIndex)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwLP)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwEP)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwRP)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwEXP)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwReputationPoint)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwMudosaPoint)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwSpPoint)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwTutorialHint)
	END_PARAM()

END_DECLARE_SQLUNIT()
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_CharacterUpdate)
	DEFINE_QUERY("Update Characters set awchName = ?,	byRace = ?,	byClass = ?, byGender = ?, byFace = ?, byHair = ?, byHairColor = ?, bySkinColor = ?, byLevel = ?, byBindType = ?, byBlood = ?, byMarking = ?, bIsAdult = ?, worldTblidx = ?, worldId = ?, bTutorialFlag = ?, bNeedNameChange = ?, bChangeClass = ?, fPositionX = ?, fPositionY = ?, fPositionZ = ?, fDirX = ?, fDirY = ?, fDirZ = ?, fBindPositionX = ?, fBindPositionY = ?, fBindPositionZ = ?, fBindDirX = ?, fBindDirY = ?, fBindDirZ = ?, dwBindObjectTblidx = ?, dwBindWorldID = ?, dwMoney = ?, dwMoneyBank = ?, dwMapInfoIndex = ?, dwLP = ?, dwEP = ?, dwRP = ?, dwExp = ?, dwReputationPoint = ?, dwMudosaPoint = ?, dwSpPoint = ?, dwTutorialHint = ? where AccountID = ? AND ServerID = ?")
	BEGIN_VARIABLE()
		char							m_awchName[NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1];
		BYTE							m_byRace;
		BYTE							m_byClass;
		BYTE							m_byGender;
		BYTE							m_byFace;
		BYTE							m_byHair;
		BYTE							m_byHairColor;
		BYTE							m_bySkinColor;
		BYTE							m_byLevel;
		BYTE							m_byBindType;
		BYTE							m_byBlood;
		BYTE							m_byMarking;//eMARKING_TYPE
		int								m_bIsAdult;
		int								m_worldTblidx;
		int								m_worldId;
		int								m_bTutorialFlag;
		int								m_bNeedNameChange;
		int								m_bChangeClass;
		float							m_fPositionX;
		float							m_fPositionY;
		float							m_fPositionZ;
		float							m_fDirX;
		float							m_fDirY;
		float							m_fDirZ;
		float							m_fBindPositionX;
		float							m_fBindPositionY;
		float							m_fBindPositionZ;
		float							m_fBindDirX;
		float							m_fBindDirY;
		float							m_fBindDirZ;
		DWORD							m_dwAccountID;
		DWORD							m_dwServerID;
		DWORD							m_dwCharID;
		DWORD							m_dwBindObjectTblidx;
		DWORD							m_dwBindWorldID;
		DWORD							m_dwMoney;
		DWORD							m_dwMoneyBank;
		DWORD							m_dwMapInfoIndex;
		DWORD							m_dwLP;
		DWORD							m_dwEP;
		DWORD							m_dwRP;
		DWORD							m_dwEXP;
		DWORD							m_dwReputationPoint;
		DWORD							m_dwMudosaPoint;
		DWORD							m_dwSpPoint;
		DWORD							m_dwTutorialHint;
	END_VARIABLE()	
	// Where condition
	BEGIN_PARAM(45)
		PARAM_ENTRY_STR(SQL_PARAM_INPUT, m_awchName)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byRace)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byClass)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byGender)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byFace)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byHair)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byHairColor)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bySkinColor)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byLevel)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byBindType)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byBlood)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byMarking)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bIsAdult)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_worldTblidx)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_worldId)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bTutorialFlag)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bNeedNameChange)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bChangeClass)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_fPositionX)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_fPositionY)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_fPositionZ)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_fDirX)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_fDirY)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_fDirZ)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_fBindPositionX)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_fBindPositionY)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_fBindPositionZ)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_fBindDirX)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_fBindDirY)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_fBindDirZ)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwBindObjectTblidx)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwBindWorldID)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwMoney)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwMoneyBank)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwMapInfoIndex)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwLP)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwEP)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwRP)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwEXP)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwReputationPoint)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwMudosaPoint)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwSpPoint)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwTutorialHint)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwAccountID)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwServerID)
	END_PARAM()
END_DECLARE_SQLUNIT()
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_CharacterOnline)
	DEFINE_QUERY("Update Characters set Online = ? where AccountID = ? AND ServerID = ?")
	BEGIN_VARIABLE()
		int     m_bIsOnline;
		DWORD	m_dwAccountID;
		DWORD	m_dwServerID;
	END_VARIABLE()
	// Where condition
	BEGIN_PARAM(3)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bIsOnline)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwAccountID)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwServerID)
	END_PARAM()
END_DECLARE_SQLUNIT()
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_CharacterMaxID)
	DEFINE_QUERY("Select max(charID) from charCreated")
	BEGIN_VARIABLE()
		int		m_MaxCharID;
	END_VARIABLE()
	BEGIN_COLUMN(1)
		COLUMN_ENTRY(m_MaxCharID)
	END_COLUMN()
END_DECLARE_SQLUNIT()
//---------------------------------------------------------------------INVENTORY-----------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_InventorySelect)
	DEFINE_QUERY("Select charID, tblidx, byPlace, byPosition, byStackCount, byRank, byCurrentDurability, bNeedToIdentify, byGrade, byBattleAttribute, byRestrictType, OptionTblidx1, OptionTblidx2, byDurationType, nUseStartTime, nUseEndTime from inventory where charID = ?")
	BEGIN_VARIABLE()
		DWORD	m_charID;
		//DWORD	m_itemId; // this will be randomly generated by servers
		DWORD	m_tblidx;
		BYTE	m_byPlace,
				m_byPosition,
				m_byStackCount,
				m_byRank,
				m_byCurrentDurability,
				m_byGrade,
				m_byBattleAttribute,
				m_byRestrictType,
				m_byDurationType;
		int		m_bNeedToIdentify, m_OptionTblidx1, m_OptionTblidx2;
		DWORD	m_nUseEndTime;
		DWORD	m_nUseStartTime;
	END_VARIABLE()
	BEGIN_COLUMN(16)
		COLUMN_ENTRY(m_charID);
		//COLUMN_ENTRY(m_itemId); // this will be randomly generated by servers
		COLUMN_ENTRY(m_tblidx);
		COLUMN_ENTRY(m_byPlace);
		COLUMN_ENTRY(m_byPosition);
		COLUMN_ENTRY(m_byStackCount);
		COLUMN_ENTRY(m_byRank);
		COLUMN_ENTRY(m_byCurrentDurability);
		COLUMN_ENTRY(m_bNeedToIdentify);
		COLUMN_ENTRY(m_byGrade);
		COLUMN_ENTRY(m_byBattleAttribute);
		COLUMN_ENTRY(m_byRestrictType);
		COLUMN_ENTRY(m_OptionTblidx1);
		COLUMN_ENTRY(m_OptionTblidx2);
		COLUMN_ENTRY(m_byDurationType);
		COLUMN_ENTRY(m_nUseStartTime);
		COLUMN_ENTRY(m_nUseEndTime);
	END_COLUMN()
	BEGIN_PARAM(1)
		PARAM_ENTRY(SQL_PARAM_INPUT,m_charID)
	END_PARAM()
END_DECLARE_SQLUNIT()
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_InventoryInsertItem)
	DEFINE_QUERY("INSERT INTO inventory(charID, tblidx, byPlace, byPosition, byStackCount, byRank, byCurrentDurability, bNeedToIdentify, byGrade, byBattleAttribute, byRestrictType, OptionTblidx1, OptionTblidx2, byDurationType, nUseStartTime, nUseEndTime) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
	BEGIN_VARIABLE()
		DWORD	m_charID;
		//DWORD	m_itemId; // this will be randomly generated by servers
		DWORD	m_tblidx;
		BYTE	m_byPlace,
			m_byPosition,
			m_byStackCount,
			m_byRank,
			m_byCurrentDurability,
			m_byGrade,
			m_byBattleAttribute,
			m_byRestrictType,
			m_byDurationType;
		int		m_bNeedToIdentify, m_OptionTblidx1, m_OptionTblidx2;
		DWORD	m_nUseEndTime;
		DWORD	m_nUseStartTime;
	END_VARIABLE()
	BEGIN_PARAM(16)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_charID)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_tblidx)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byPlace)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byPosition)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byStackCount)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byRank)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byCurrentDurability)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bNeedToIdentify)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byGrade)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byBattleAttribute)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byRestrictType)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_OptionTblidx1)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_OptionTblidx2)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byDurationType)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_nUseStartTime)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_nUseEndTime)
	END_PARAM()
END_DECLARE_SQLUNIT()
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_InventoryDeleteItem)
	DEFINE_QUERY("DELETE FROM inventory WHERE charID = ? AND byPlace = ? AND byPosition = ?")
	BEGIN_VARIABLE()
		DWORD	m_charID;
		BYTE	m_byPlace;
		BYTE	m_byPosition;
	END_VARIABLE()
	BEGIN_PARAM(3)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_charID)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byPlace)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byPosition)
	END_PARAM()
END_DECLARE_SQLUNIT()
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_InventoryItemMove)
	DEFINE_QUERY("UPDATE inventory set byPlace = ?, byPosition = ? WHERE charID = ? AND byPlace = ? AND byPosition = ?")
	BEGIN_VARIABLE()
		DWORD	m_charID;
		BYTE	m_byPlaceDest;
		BYTE	m_byPositionDest;
		BYTE	m_byPlaceSource;
		BYTE	m_byPositionSource;
	END_VARIABLE()
	BEGIN_PARAM(5)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_charID)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byPlaceDest)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byPositionDest)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byPlaceSource)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byPositionSource)
	END_PARAM()
END_DECLARE_SQLUNIT()
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT(SP_InventoryCreate,"{ call InventoryCreate(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }")
	BEGIN_VARIABLE()
		DWORD	m_charID;
		//DWORD	m_itemId; // this will be generated by server randomly
		DWORD	m_tblidx;
		BYTE	m_byPlace,
			m_byPosition,
			m_byStackCount,
			m_byRank,
			m_byCurrentDurability,
			m_byGrade,
			m_byBattleAttribute,
			m_byRestrictType,
			m_byDurationType;
		int		m_bNeedToIdentify, m_OptionTblidx1, m_OptionTblidx2;
	END_VARIABLE()
	BEGIN_PARAM(14)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_charID);
		//PARAM_ENTRY(SQL_PARAM_INPUT, m_itemId); //this will be generated by server randomly
		PARAM_ENTRY(SQL_PARAM_INPUT, m_tblidx);
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byPlace);
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byPosition);
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byStackCount);
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byRank);
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byCurrentDurability);
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bNeedToIdentify);
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byGrade);
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byBattleAttribute);
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byRestrictType);
		PARAM_ENTRY(SQL_PARAM_INPUT, m_OptionTblidx1);
		PARAM_ENTRY(SQL_PARAM_INPUT, m_OptionTblidx2);
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byDurationType);
	END_PARAM()
END_DECLARE_SQLUNIT()
//----------------------------------------------------------------QUICKSLOT-------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_QuickSlotSelect)
	DEFINE_QUERY("Select bySlot, dwItemTblidx, dwSkillTblidx, byType from quickSlot where charID = ?")
	BEGIN_VARIABLE()
		BYTE		m_bySlot;
		DWORD		m_dwItemTblidx;
		DWORD		m_dwSkillTblidx;
		BYTE		m_byType;
		DWORD		m_charID;
	END_VARIABLE()
	BEGIN_COLUMN(4)
		COLUMN_ENTRY(m_bySlot)
		COLUMN_ENTRY(m_dwItemTblidx)
		COLUMN_ENTRY(m_dwSkillTblidx)		
		COLUMN_ENTRY(m_byType)
	END_COLUMN()
	BEGIN_PARAM(1)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_charID)
	END_PARAM()
END_DECLARE_SQLUNIT()
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_QuickSlotUpdate)
	DEFINE_QUERY("UPDATE quickslot set bySlot = ?, byType = ?, dwItemTblidx = ?, dwSkillTblidx = ? WHERE charID = ? AND bySlot = ?")
	BEGIN_VARIABLE()
		BYTE		m_bySlot;
		DWORD		m_dwItemTblidx;
		DWORD		m_dwSkillTblidx;
		BYTE		m_byType;
		DWORD		m_charID;
		DWORD		m_bySlotSearch;
	END_VARIABLE()
	BEGIN_PARAM(6)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bySlot)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byType)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwItemTblidx)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwSkillTblidx)		
		PARAM_ENTRY(SQL_PARAM_INPUT, m_charID)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bySlotSearch)
	END_PARAM()
END_DECLARE_SQLUNIT()
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_QuickSlotCreate)
	DEFINE_QUERY("insert into quickslot(charID,bySlot,byType,dwItemTblidx,dwSkillTblidx) VALUES(?,?,?,?,?)")
	BEGIN_VARIABLE()
		BYTE		m_bySlot;
		DWORD		m_dwItemTblidx;
		DWORD		m_dwSkillTblidx;
		BYTE		m_byType;
		DWORD		m_charID;
	END_VARIABLE()
	BEGIN_PARAM(5)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_charID)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bySlot)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byType)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwItemTblidx)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwSkillTblidx)
	END_PARAM()
END_DECLARE_SQLUNIT()
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_QuickSlotDelete)
	DEFINE_QUERY("DELETE FROM quickslot where charID = ? and bySlot = ?")
	BEGIN_VARIABLE()
		BYTE		m_bySlot;
		DWORD		m_charID;
	END_VARIABLE()
	BEGIN_PARAM(2)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_charID)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bySlot)
	END_PARAM()
END_DECLARE_SQLUNIT()
//----------------------------------------------------------------SKILLS-------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_SkillsMaxSlotByChar)
	DEFINE_QUERY("Select MAX(bySlot) from skills where charID = ?")
	BEGIN_VARIABLE()
		BYTE		m_bySlot;
		DWORD		m_charID;
	END_VARIABLE()
	BEGIN_COLUMN(1)
		COLUMN_ENTRY(m_bySlot)
	END_COLUMN()
	BEGIN_PARAM(1)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_charID)
	END_PARAM()
END_DECLARE_SQLUNIT()
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_SkillsSelectAll)
	DEFINE_QUERY("Select bySlot, dwSkillTblidx, nRemainSec, nExp, byRpBonusType, bIsRpBonusAuto from skills where charID = ?")
	BEGIN_VARIABLE()
		BYTE		m_bySlot;
		DWORD		m_dwSkillTblidx;
		DWORD		m_nRemainSec;
		DWORD		m_nExp;
		BYTE		m_byRpBonusType;
		int			m_bIsRpBonusAuto;
		DWORD		m_charID;
	END_VARIABLE()
	BEGIN_COLUMN(6)
		COLUMN_ENTRY(m_bySlot)
		COLUMN_ENTRY(m_dwSkillTblidx)
		COLUMN_ENTRY(m_nRemainSec)
		COLUMN_ENTRY(m_nExp)
		COLUMN_ENTRY(m_byRpBonusType)
		COLUMN_ENTRY(m_bIsRpBonusAuto)
	END_COLUMN()
	BEGIN_PARAM(1)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_charID)
	END_PARAM()
END_DECLARE_SQLUNIT()
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_SkillsSelect)
	DEFINE_QUERY("select bySlot, dwSkillTblidx, nRemainSec, nExp, byRpBonusType, bIsRpBonusAuto from skills where charID = ? AND dwSkillTblidx = ?")
	BEGIN_VARIABLE()
		BYTE		m_bySlot;
		DWORD		m_dwSkillTblidx;
		DWORD		m_nRemainSec;
		DWORD		m_nExp;
		BYTE		m_byRpBonusType;
		int			m_bIsRpBonusAuto;
		DWORD		m_charID;
	END_VARIABLE()
	BEGIN_COLUMN(6)
		COLUMN_ENTRY(m_bySlot)
		COLUMN_ENTRY(m_dwSkillTblidx)
		COLUMN_ENTRY(m_nRemainSec)
		COLUMN_ENTRY(m_nExp)
		COLUMN_ENTRY(m_byRpBonusType)
		COLUMN_ENTRY(m_bIsRpBonusAuto)
	END_COLUMN()
	BEGIN_PARAM(2)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_charID)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwSkillTblidx)
	END_PARAM()
END_DECLARE_SQLUNIT()
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_SkillsInsert)
	DEFINE_QUERY("INSERT INTO Skills(charID, bySlot, dwSkillTblidx, nRemainSec, nExp, byRpBonusType, bIsRpBonusAuto) VALUES(?,?,?,?,?,?,?)")
	BEGIN_VARIABLE()
		BYTE		m_bySlot;
		DWORD		m_dwSkillTblidx;
		DWORD		m_nRemainSec;
		DWORD		m_nExp;
		BYTE		m_byRpBonusType;
		int			m_bIsRpBonusAuto;
		DWORD		m_charID;
	END_VARIABLE()
	BEGIN_PARAM(7)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_charID)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bySlot)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwSkillTblidx)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_nRemainSec)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_nExp)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byRpBonusType)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bIsRpBonusAuto)
	END_PARAM()
END_DECLARE_SQLUNIT()
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_SkillsDelete)
	DEFINE_QUERY("DELETE FROM Skills WHERE charID = ? AND dwSkillTblidx = ?")
	BEGIN_VARIABLE()
		DWORD		m_dwSkillTblidx;
		DWORD		m_charID;
	END_VARIABLE()
	BEGIN_PARAM(2)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_charID)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwSkillTblidx)
	END_PARAM()
END_DECLARE_SQLUNIT()
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_SkillsUpdate)
	DEFINE_QUERY("UPDATE Skills set bySlot = ?, dwSkillTblidx = ?, nRemainSec = ?, nExp = ?, byRpBonusType = ?, bIsRpBonusAuto = ? WHERE charID = ? AND dwSkillTblidx = ?")
	BEGIN_VARIABLE()
		BYTE		m_bySlot;
		DWORD		m_dwSkillTblidxSource;
		DWORD		m_dwSkillTblidxDest;
		DWORD		m_nRemainSec;
		DWORD		m_nExp;
		BYTE		m_byRpBonusType;
		int			m_bIsRpBonusAuto;
		DWORD		m_charID;
	END_VARIABLE()
	BEGIN_PARAM(8)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bySlot)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwSkillTblidxDest)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_nRemainSec)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_nExp)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byRpBonusType)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bIsRpBonusAuto)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_charID)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwSkillTblidxSource)
	END_PARAM()
END_DECLARE_SQLUNIT()
//--------------------------------------------------------------------HTBLIST---------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_HTBSkillInsert)
	DEFINE_QUERY("INSERT INTO htblist(charId,skillId,bySlotID,dwTimeCoolDown) VALUES(?,?,?,?)")
	BEGIN_VARIABLE()
		DWORD			m_charId;
		DWORD			m_skillId;
		BYTE			m_bySlotId;
		DWORD			m_dwTimeRemaining;
	END_VARIABLE()
	BEGIN_PARAM(4)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_charId)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_skillId)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bySlotId)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwTimeRemaining)
	END_PARAM()
END_DECLARE_SQLUNIT()
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_HTBSkillSelect)
	DEFINE_QUERY("Select skillId, bySlotID, dwTimeCoolDown from htblist where charID = ?")
	BEGIN_VARIABLE()
		DWORD			m_charId;
		DWORD			m_skillId;
		BYTE			m_bySlotId;
		DWORD			m_dwTimeRemaining;
	END_VARIABLE()
	BEGIN_COLUMN(3)
		COLUMN_ENTRY(m_skillId)
		COLUMN_ENTRY(m_bySlotId)
		COLUMN_ENTRY(m_dwTimeRemaining)//Cooldown for next use
	END_COLUMN()
	BEGIN_PARAM(1)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_charId)
	END_PARAM()
END_DECLARE_SQLUNIT()
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_HTBSkillUpdate)
	DEFINE_QUERY("UPDATE htblist set skillId = ?, bySlotID = ?, dwTimeCoolDown = ? where charID = ?")
	BEGIN_VARIABLE()
		DWORD			m_charId;
		DWORD			m_skillId;
		BYTE			m_bySlotId;
		DWORD			m_dwTimeRemaining;
	END_VARIABLE()
	BEGIN_PARAM(4)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_skillId)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bySlotId)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwTimeRemaining)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_charId)
	END_PARAM()
END_DECLARE_SQLUNIT()
//-------------------------------------------------------------------BuffList-------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_BuffListCreate)
	DEFINE_QUERY("INSERT INTO bufflist(charId,dwTblidx,bySourceType,dwInitialDuration,dwTimeRemaining,afEffectValue0,afEffectValue1,byBuffGroup) VALUES(?,?,?,?,?,?,?,?)")
	BEGIN_VARIABLE()
		DWORD			m_dwTblidx;
		BYTE			m_bySourceType;			// eDBO_OBJECT_SOURCE
		BYTE			m_byBuffGroup;
		DWORD			m_dwInitialDuration;
		DWORD			m_dwTimeRemaining;		// in millisecs.
		float			m_afEffectValue0;
		float			m_afEffectValue1;
		DWORD			m_charID;
	END_VARIABLE()
	BEGIN_PARAM(8)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_charID)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwTblidx)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bySourceType)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwInitialDuration)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwTimeRemaining)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_afEffectValue0)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_afEffectValue1)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byBuffGroup)
	END_PARAM()
END_DECLARE_SQLUNIT();
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_BuffListUpdate)
	DEFINE_QUERY("UPDATE bufflist set bySourceType = ?,dwInitialDuration = ?,dwTimeRemaining = ?,afEffectValue0 = ?,afEffectValue1 = ?,byBuffGroup = ? where charID = ? and dwTblidx = ?")
	BEGIN_VARIABLE()
		DWORD			m_dwTblidx;
		BYTE			m_bySourceType;			// eDBO_OBJECT_SOURCE
		BYTE			m_byBuffGroup;
		DWORD			m_dwInitialDuration;
		DWORD			m_dwTimeRemaining;		// in millisecs.
		float			m_afEffectValue0;
		float			m_afEffectValue1;
		DWORD			m_charID;
	END_VARIABLE()
	BEGIN_PARAM(8)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bySourceType)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwInitialDuration)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwTimeRemaining)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_afEffectValue0)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_afEffectValue1)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_byBuffGroup)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_charID)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwTblidx)
	END_PARAM()
END_DECLARE_SQLUNIT();
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_BuffListDelete)
	DEFINE_QUERY("DELETE FROM buffList where charId = ? and dwTblidx = ? and bySourceType = ?")
	BEGIN_VARIABLE()
		DWORD			m_dwTblidx;
		BYTE			m_bySourceType;			// eDBO_OBJECT_SOURCE
		DWORD			m_charID;
	END_VARIABLE()
	BEGIN_PARAM(3)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_charID)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_dwTblidx)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_bySourceType)
	END_PARAM()
END_DECLARE_SQLUNIT();
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_BuffListSelect)
	DEFINE_QUERY("Select dwTblidx,bySourceType,dwInitialDuration,dwTimeRemaining,afEffectValue0,afEffectValue1,byBuffGroup from bufflist where charId = ?")
	BEGIN_VARIABLE()
		DWORD			m_dwTblidx;
		BYTE			m_bySourceType;			// eDBO_OBJECT_SOURCE
		BYTE			m_byBuffGroup;
		DWORD			m_dwInitialDuration;
		DWORD			m_dwTimeRemaining;		// in millisecs.
		float			m_afEffectValue0;
		float			m_afEffectValue1;
		DWORD			m_charID;
	END_VARIABLE()
	BEGIN_COLUMN(7)
		COLUMN_ENTRY(m_dwTblidx)
		COLUMN_ENTRY(m_bySourceType)
		COLUMN_ENTRY(m_dwInitialDuration)
		COLUMN_ENTRY(m_dwTimeRemaining)
		COLUMN_ENTRY(m_afEffectValue0)
		COLUMN_ENTRY(m_afEffectValue1)
		COLUMN_ENTRY(m_byBuffGroup)
	END_COLUMN()
	BEGIN_PARAM(1)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_charID)
	END_PARAM()
END_DECLARE_SQLUNIT();
//-------------------------------------------------------------------WarFog---------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_WarFogInsert)
	DEFINE_QUERY("INSERT INTO warfog(charId,achWarFogFlag) VALUES(?,?)")
	BEGIN_VARIABLE()
		DWORD		m_charId;
		char		m_achWarFogFlag[NTL_MAX_SIZE_WAR_FOG];
	END_VARIABLE()
	BEGIN_PARAM(2)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_charId)
		PARAM_ENTRY_STR(SQL_PARAM_INPUT, m_achWarFogFlag)
	END_PARAM()
END_DECLARE_SQLUNIT();
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_WarFogSelect)
	DEFINE_QUERY("Select achWarFogFlag from warfog where charId = ?")
	BEGIN_VARIABLE()
		DWORD		m_charId;
		char		m_achWarFogFlag[NTL_MAX_SIZE_WAR_FOG];
	END_VARIABLE()
	BEGIN_PARAM(1)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_charId)
	END_PARAM()
END_DECLARE_SQLUNIT();
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN_DECLARE_SQLUNIT2(SP_WarFogUpdate)
	DEFINE_QUERY("UPDATE warfog set achWarFogFlag = ? where charId = ?")
	BEGIN_VARIABLE()
		DWORD		m_charId;
		char		m_achWarFogFlag[NTL_MAX_SIZE_WAR_FOG];
	END_VARIABLE()
	BEGIN_PARAM(2)
		PARAM_ENTRY_STR(SQL_PARAM_INPUT, m_achWarFogFlag)
		PARAM_ENTRY(SQL_PARAM_INPUT, m_charId)
	END_PARAM()
END_DECLARE_SQLUNIT();
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------