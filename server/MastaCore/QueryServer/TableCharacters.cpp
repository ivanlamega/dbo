#include "stdafx.h"
#include "TableCharacters.h"


CTableCharacters::CTableCharacters()
{
}


CTableCharacters::~CTableCharacters()
{
}

int CTableCharacters::ExecuteQuery(CNtlDatabaseConnection * pConnection)
{
	SQLLEN nRowCount = 0;

	switch (eSqlID)
	{
		case SP_CharacterSelect:
		{
			FIND_SQLUNIT(SP_CharacterSelect, pConnection, pSqlUnit);
			if (NULL == pSqlUnit)
			{
				return NTL_FAIL;
			}
			pSqlUnit->m_dwAccountID = this->m_dwAccountID;
			pSqlUnit->m_dwServerID = this->m_dwServerID;
			if (pSqlUnit->Store())
			{
				if (pSqlUnit->Fetch())
				{
					SetAccountID(pSqlUnit->m_dwAccountID);
					SetServerID(pSqlUnit->m_dwServerID);
					SetCharID(pSqlUnit->m_dwCharID);
					//Set Char Information
					memcpy(this->m_awchName, pSqlUnit->m_awchName, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1);
					this->m_bIsAdult = pSqlUnit->m_bIsAdult;
					this->m_bNeedNameChange = pSqlUnit->m_bNeedNameChange;
					this->m_bTutorialFlag = pSqlUnit->m_bTutorialFlag;
					this->m_byClass = pSqlUnit->m_byClass;
					this->m_byFace = pSqlUnit->m_byFace;
					this->m_byHair = pSqlUnit->m_byHair;
					this->m_byHairColor = pSqlUnit->m_byHairColor;
					this->m_byGender = pSqlUnit->m_byGender;
					this->m_byLevel = pSqlUnit->m_byLevel;
					this->m_byRace = pSqlUnit->m_byRace;
					this->m_bySkinColor = pSqlUnit->m_bySkinColor;
					this->m_fPositionX = pSqlUnit->m_fPositionX;
					this->m_fPositionY = pSqlUnit->m_fPositionY;
					this->m_fPositionZ = pSqlUnit->m_fPositionZ;
					this->m_dwMoney = pSqlUnit->m_dwMoney;
					this->m_dwMoneyBank = pSqlUnit->m_dwMoneyBank;
					this->m_worldId = pSqlUnit->m_worldId;
					this->m_worldTblidx = pSqlUnit->m_worldTblidx;
					this->m_dwMapInfoIndex = pSqlUnit->m_dwMapInfoIndex;
				}
				else
				{
					pSqlUnit->m_dwAccountID = 0;
					pSqlUnit->m_dwServerID = 0;
				}
			}
			
		}
			break;
		case SP_CharacterSelectByID:
		{
			FIND_SQLUNIT(SP_CharacterSelectByID, pConnection, pSqlUnit);
			if (NULL == pSqlUnit)
			{
				return NTL_FAIL;
			}
			pSqlUnit->m_dwAccountID = this->m_dwAccountID;
			pSqlUnit->m_dwCharID = this->m_dwCharID;
			if (pSqlUnit->Store())
			{
				if (pSqlUnit->Fetch())
				{
					SetAccountID(pSqlUnit->m_dwAccountID);
					SetServerID(pSqlUnit->m_dwServerID);
					SetCharID(pSqlUnit->m_dwCharID);
					//Set Char Information
					memcpy(this->m_awchName, pSqlUnit->m_awchName, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1);
					this->m_bIsAdult = pSqlUnit->m_bIsAdult;
					this->m_bNeedNameChange = pSqlUnit->m_bNeedNameChange;
					this->m_bTutorialFlag = pSqlUnit->m_bTutorialFlag;
					this->m_byClass = pSqlUnit->m_byClass;
					this->m_byFace = pSqlUnit->m_byFace;
					this->m_byHair = pSqlUnit->m_byHair;
					this->m_byHairColor = pSqlUnit->m_byHairColor;
					this->m_byGender = pSqlUnit->m_byGender;
					this->m_byLevel = pSqlUnit->m_byLevel;
					this->m_byRace = pSqlUnit->m_byRace;
					this->m_bySkinColor = pSqlUnit->m_bySkinColor;
					this->m_fPositionX = pSqlUnit->m_fPositionX;
					this->m_fPositionY = pSqlUnit->m_fPositionY;
					this->m_fPositionZ = pSqlUnit->m_fPositionZ;
					this->m_dwMoney = pSqlUnit->m_dwMoney;
					this->m_dwMoneyBank = pSqlUnit->m_dwMoneyBank;
					this->m_worldId = pSqlUnit->m_worldId;
					this->m_worldTblidx = pSqlUnit->m_worldTblidx;
					this->m_dwMapInfoIndex = pSqlUnit->m_dwMapInfoIndex;
				}
				else
				{
					pSqlUnit->m_dwAccountID = 0;
					pSqlUnit->m_dwServerID = 0;
				}
			}

		}
			break;
		case SP_CharacterCreate:
		{
			FIND_SQLUNIT( SP_CharacterCreate, pConnection, pSqlUnit2 );
			if( NULL == pSqlUnit2 )
			{
				return NTL_FAIL;
			}

			pSqlUnit2->Exec( &nRowCount );
		}
		case SP_CharacterUpdate:
		{
			FIND_SQLUNIT(SP_CharacterUpdate, pConnection, pSqlUnit3);
			if (NULL == pSqlUnit3)
			{
				return NTL_FAIL;
			}

			pSqlUnit3->Exec(&nRowCount);
		}
		break;
		case SP_CharacterOnline:
		{
			FIND_SQLUNIT(SP_CharacterOnline, pConnection, pSqlUnit4);
			if (NULL == pSqlUnit4)
			{
				return NTL_FAIL;
			}

			pSqlUnit4->Exec(&nRowCount);
		}
		break;
		case SP_CharacterMaxID:
		{
			FIND_SQLUNIT(SP_CharacterMaxID, pConnection, pSqlUnit5);
			if (NULL == pSqlUnit5)
			{
				return NTL_FAIL;
			}
			if (pSqlUnit5->Store())
			{
				if (pSqlUnit5->Fetch())
				{
					SetCharID(pSqlUnit5->m_MaxCharID);
				}
			}
		}
		break;
	}

	NTL_PRINT(PRINT_APP, "ExecuteQuery Done");

	return NTL_SUCCESS;
}


int CTableCharacters::ExecuteResult()
{
	NTL_PRINT(PRINT_APP, "ExecuteResult Done");
	CQueryServer* app = (CQueryServer*)NtlSfxGetApp();
	app->g_exitEvent.Notify();
	return NTL_SUCCESS;
}