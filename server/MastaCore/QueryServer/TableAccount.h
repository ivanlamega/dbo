#pragma once
#include "QueryServer.h"
#include "NtlQuery.h"
class CTableAccount :	public CNtlQuery
{
private:
	DWORD	m_dwAccountID;
	char	m_szUserID[MAX_SIZE_USER_ID + 1];
	char	m_szUserPW[MAX_SIZE_USER_PW + 1];
	DWORD	m_dwLastFarmID;
	SQL_ID  eSqlID;
public:
	CTableAccount();
	~CTableAccount();
	int ExecuteQuery(CNtlDatabaseConnection * pConnection);
	int ExecuteResult();
	DWORD   GetAccountID() { return m_dwAccountID; }
	char*	GetUsername() { return m_szUserID; }
	char*   GetPassword() { return m_szUserPW; }
	DWORD	GetLastFarmID(){ return m_dwLastFarmID; }
	void	SetAccountID(DWORD accountID){ this->m_dwAccountID = accountID; }
	void	SetUsername(char* m_szUserID){ strncpy_s(this->m_szUserID, m_szUserID, MAX_SIZE_USER_ID); }
	void	SetPassword(char* m_szUserPW){ strncpy_s(this->m_szUserPW, m_szUserPW, MAX_SIZE_USER_PW); }
	void	SetLastFarmID(DWORD farmID){ this->m_dwLastFarmID = farmID; }
	void    SetSqlID(SQL_ID ID_SQL) { this->eSqlID = ID_SQL; }
	
};