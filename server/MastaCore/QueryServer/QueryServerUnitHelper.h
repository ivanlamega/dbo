#pragma once

#include "NtlSqlUnitHelper.h"
class CQueryServerUnitHelper :	public CNtlSqlUnitHelper
{
public:
	CQueryServerUnitHelper(SQLUNITID maxSqlUnitID);
	~CQueryServerUnitHelper();
	virtual BOOL	PreCreateSqlUnit(CNtlDatabaseConnection * pConnection);
};

