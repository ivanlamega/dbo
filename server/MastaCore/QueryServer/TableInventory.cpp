#include "stdafx.h"
#include "TableInventory.h"


CTableInventory::CTableInventory()
{
}


CTableInventory::~CTableInventory()
{
}

int CTableInventory::ExecuteQuery(CNtlDatabaseConnection * pConnection)
{
	SQLLEN nRowCount = 0;

	switch (eSqlID)
	{
		case SP_InventorySelect:
		{
			FIND_SQLUNIT(SP_InventorySelect, pConnection, pSqlUnit);
			if (NULL == pSqlUnit)
			{
				return NTL_FAIL;
			}
			pSqlUnit->m_charID = this->charID;
			if (pSqlUnit->Store())
			{
				if (pSqlUnit->Fetch())
				{
					this->charID = pSqlUnit->m_charID;
				}
				else
				{
					pSqlUnit->m_charID = 0;
					//pSqlUnit->m_itemId = 0;
				}
			}
		}
		break;
		case SP_InventoryCreate:
		{
			FIND_SQLUNIT(SP_InventoryCreate, pConnection, pSqlUnit3);
			if (NULL == pSqlUnit3)
			{
				return NTL_FAIL;
			}

			pSqlUnit3->Exec(&nRowCount);
		}
		break;
		case SP_InventoryInsertItem:
		{
			FIND_SQLUNIT(SP_InventoryInsertItem, pConnection, pSqlUnit5);
			if (NULL == pSqlUnit5)
			{
				return NTL_FAIL;
			}

			pSqlUnit5->Exec(&nRowCount);
		}
		break;
		case SP_InventoryDeleteItem:
		{
			FIND_SQLUNIT(SP_InventoryDeleteItem, pConnection, pSqlUnit4);
			if (NULL == pSqlUnit4)
			{
				return NTL_FAIL;
			}

			pSqlUnit4->Exec(&nRowCount);
		}
		break;
		case SP_InventoryItemMove:
		{
			FIND_SQLUNIT(SP_InventoryItemMove, pConnection, pSqlUnit6);
			if (NULL == pSqlUnit6)
			{
				return NTL_FAIL;
			}

			pSqlUnit6->Exec(&nRowCount);
		}
		break;		
	}
	NTL_PRINT(PRINT_APP, "ExecuteQuery Done");

	return NTL_SUCCESS;
}


int CTableInventory::ExecuteResult()
{
	NTL_PRINT(PRINT_APP, "ExecuteResult Done");
	CQueryServer* app = (CQueryServer*)NtlSfxGetApp();
	app->g_exitEvent.Notify();
	return NTL_SUCCESS;
}