#include "stdafx.h"
#include "TableWarFog.h"


CTableWarFog::CTableWarFog()
{
}


CTableWarFog::~CTableWarFog()
{
}

int CTableWarFog::ExecuteQuery(CNtlDatabaseConnection * pConnection)
{
	SQLLEN nRowCount = 0;

	switch (eSqlID)
	{
		case SP_WarFogSelect:
		{
			FIND_SQLUNIT(SP_WarFogSelect, pConnection, pSqlUnit);
			if (NULL == pSqlUnit)
			{
				return NTL_FAIL;
			}

			pSqlUnit->m_charId = this->charID;

			if (pSqlUnit->Store())
			{
				if (pSqlUnit->Fetch())
				{
					this->charID = pSqlUnit->m_charId;
					memcpy(this->m_achWarFogFlag, pSqlUnit->m_achWarFogFlag, NTL_MAX_SIZE_WAR_FOG);
				}
			}
		}
		break;
		case SP_WarFogInsert:
		{
			FIND_SQLUNIT(SP_WarFogInsert, pConnection, pSqlUnit5);
			if (NULL == pSqlUnit5)
			{
				return NTL_FAIL;
			}

			pSqlUnit5->Exec(&nRowCount);
		}
		break;
		case SP_WarFogUpdate:
		{
			FIND_SQLUNIT(SP_WarFogUpdate, pConnection, pSqlUnit4);
			if (NULL == pSqlUnit4)
			{
				return NTL_FAIL;
			}

			pSqlUnit4->Exec(&nRowCount);
		}
		break;
	}

	NTL_PRINT(PRINT_APP, "ExecuteQuery Done");

	return NTL_SUCCESS;
}


int CTableWarFog::ExecuteResult()
{
	NTL_PRINT(PRINT_APP, "ExecuteResult Done");
	CQueryServer* app = (CQueryServer*)NtlSfxGetApp();
	app->g_exitEvent.Notify();
	return NTL_SUCCESS;
}