#include "stdafx.h"
#include "QueryServer.h"

//For Multiple Server - Luiz45
const std::string CQueryServer::GetConfigFileEnabledMultipleServers()
{
	return EnableMultipleServers.GetString();
}
const DWORD CQueryServer::GetConfigFileMaxServers()
{
	return MAX_NUMOF_QUERY_SERVER;
}
int	CQueryServer::OnInitApp()
{
	m_nMaxSessionCount = MAX_NUMOF_QUERY_SESSION;

	m_pSessionFactory = new CQuerySessionFactory;
	if (NULL == m_pSessionFactory)
	{
		return NTL_ERR_SYS_MEMORY_ALLOC_FAIL;
	}
	m_pAuthServerSession = NULL;
	m_pMasterServerSession = NULL;
	m_pCharServerSession = NULL;
	m_pCommunityServerSession = NULL;	
	m_pGameServerSession = NULL;
	m_pQuerySession = (CQuerySession*)m_pSessionFactory->CreateSession(SESSION_QUERY_SERVER);
	return NTL_SUCCESS;
}

int	CQueryServer::OnCreate()
{
	int rc = NTL_SUCCESS;

	rc = m_clientAcceptor.Create(m_config.strQueryServerIP.c_str(), m_config.wQueryServerPort, SESSION_QUERY_SERVER,
		MAX_NUMOF_QUERY_GAME_CLIENT, 5, 15, MAX_NUMOF_QUERY_GAME_CLIENT);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_clientAcceptor, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_masterServerConnector.Create(m_config.strMasterServerIP.c_str(), m_config.wMasterServerPort, SESSION_MASTER_SERVER);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_masterServerConnector,true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_authServerConnector.Create(m_config.strAuthServerIP.c_str(), m_config.wAuthServerPort, SESSION_AUTH_SERVER);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_authServerConnector, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_charServerConnector.Create(m_config.strCharServerIP.c_str(), m_config.wCharServerPort, SESSION_CHAR_SERVER);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_charServerConnector, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_communityServerConnector.Create(m_config.strCommunityServerIP.c_str(), m_config.wCommunityServerPort, SESSION_COMMUNITY_SERVER);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_communityServerConnector, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_gameServerConnector.Create(m_config.strGameServerIP.c_str(), m_config.wGameServerPort, SESSION_GAME_SERVER);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_gameServerConnector, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	return NTL_SUCCESS;

}

void	CQueryServer::OnDestroy()
{
	SAFE_DELETE(m_pSessionFactory);
}

int	CQueryServer::OnCommandArgument(int argc, _TCHAR* argv[])
{
	return NTL_SUCCESS;
}

int	CQueryServer::OnConfiguration(const char * lpszConfigFile)
{
	CNtlIniFile file;

	int rc = file.Create(lpszConfigFile);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}
	//For multiple servers - Luiz45
	if (!file.Read("ServerOptions", "EnableMultipleServers", EnableMultipleServers))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	/*if (!file.Read("ServerOptions", "MaxServerAllowed", MAX_NUMOF_QUERY_SERVER))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}*/
	if (!file.Read("AuthServer", "Address", m_config.strAuthServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("AuthServer", "Port", m_config.wAuthServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("CharServer", "Address", m_config.strCharServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("CharServer", "Port", m_config.wCharServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("MasterServer", "Address", m_config.strMasterServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("MasterServer", "Port", m_config.wMasterServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("CommunityServer", "Address", m_config.strCommunityServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("CommunityServer", "Port", m_config.wCommunityServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("OperatingServer", "Address", m_config.strOperatingServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("OperatingServer", "Port", m_config.wOperatingServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("GameServer", "Address", m_config.strGameServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("GameServer", "Port", m_config.wGameServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("QueryServer", "Address", m_config.strQueryServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("QueryServer", "Port", m_config.wQueryServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("QueryServer", "DBDSN", m_config.strDatabaseDSN))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("QueryServer", "DBUser", m_config.strDatabaseUsername))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("QueryServer", "DBPwd", m_config.strDatabasePassword))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("QueryServer", "DBName", m_config.strDatabaseName))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	return NTL_SUCCESS;
}

int	CQueryServer::OnAppStart()
{
	return NTL_SUCCESS;
}

void	CQueryServer::Run()
{
	DWORD dwTickCur, dwTickOld = ::GetTickCount();

	while (IsRunnable())
	{
		dwTickCur = ::GetTickCount();
		if (dwTickCur - dwTickOld >= 100000)
		{
			//	NTL_PRINT(PRINT_APP, "Auth Server Run()");
			DoAliveTime();
			dwTickOld = dwTickCur;
			if (!m_pAuthServerSession)
				this->m_pQuerySession->OnAccept();
			if (!m_pMasterServerSession)
				this->m_pQuerySession->OnAccept();
			if (!m_pCharServerSession)
				this->m_pQuerySession->OnAccept();
			if (!m_pCommunityServerSession)
				this->m_pQuerySession->OnAccept();
			if (!m_pGameServerSession)
				this->m_pQuerySession->OnAccept();
		}
		Sleep(2);
		DoAliveTime();
	}
}

bool	CQueryServer::Add(HSESSION hSession)
{
	if (Find(hSession))
		return false;

	m_sessionList.push_back(hSession);

	return true;
}

void	CQueryServer::Remove(HSESSION hSession)
{
	for (SESSIONIT it = m_sessionList.begin(); it != m_sessionList.end(); it++)
	{
		if (hSession == *it)
		{
			m_sessionList.erase(it);
			break;
		}
	}
}

bool	CQueryServer::Find(HSESSION hSession)
{
	for (SESSIONIT it = m_sessionList.begin(); it != m_sessionList.end(); it++)
	{
		if (hSession == *it)
		{
			return true;
		}
	}

	return false;
}

void	CQueryServer::ConvertDBInventoryToSummary(int* charId, CSqlUnit_SP_InventorySelect* pSqlUnit, sITEM_SUMMARY* sm)
{
	pSqlUnit->m_charID = (DWORD)charId;
	do{
		sm[pSqlUnit->m_byPosition].aOptionTblidx[0] = pSqlUnit->m_OptionTblidx1;
		sm[pSqlUnit->m_byPosition].aOptionTblidx[1] = pSqlUnit->m_OptionTblidx2;
		sm[pSqlUnit->m_byPosition].byBattleAttribute = pSqlUnit->m_byBattleAttribute;
		sm[pSqlUnit->m_byPosition].byGrade = pSqlUnit->m_byGrade;
		sm[pSqlUnit->m_byPosition].byRank = pSqlUnit->m_byRank;
		sm[pSqlUnit->m_byPosition].tblidx = pSqlUnit->m_tblidx;
	} while (pSqlUnit->Fetch() != false);
}

void	CQueryServer::ConvertStructInvTODB(int* charID, CSqlUnit_SP_InventoryCreate* pSqlUnit, sITEM_DATA* sm)
{
	for (int i = 0; i < EQUIP_SLOT_TYPE_COUNT; i++)
	{
		if ((sm[i].itemId != INVALID_TBLIDX) && (sm[i].itemId !=0)&&
			(sm[i].byPlace != 255) && (sm[i].byStackcount < 2) && 
			(sm[i].byRank < 2) && (sm[i].bNeedToIdentify == 0))
		{
			pSqlUnit->m_charID = (DWORD)charID;
			pSqlUnit->m_bNeedToIdentify = sm[i].bNeedToIdentify;
			pSqlUnit->m_byGrade = sm[i].byGrade;
			pSqlUnit->m_byPlace = sm[i].byPlace;
			pSqlUnit->m_byPosition = sm[i].byPosition;
			pSqlUnit->m_byBattleAttribute = sm[i].byBattleAttribute;
			pSqlUnit->m_byRank = sm[i].byRank;
			pSqlUnit->m_byRestrictType = sm[i].byRestrictType;
			pSqlUnit->m_byStackCount = sm[i].byStackcount;
			pSqlUnit->m_byCurrentDurability = sm[i].byCurrentDurability;
			pSqlUnit->m_byDurationType = sm[i].byDurationType;
			pSqlUnit->m_OptionTblidx1 = sm[i].aOptionTblidx[0];
			pSqlUnit->m_OptionTblidx2 = sm[i].aOptionTblidx[1];
			pSqlUnit->m_tblidx = sm[i].itemId;
			pSqlUnit->Exec();
		}
	}
}
void	CQueryServer::CreateChar(int pos, 
								 sPC_SUMMARY* create,
								 int				charID,
								 WCHAR*			awchName,
								 BYTE			byRace,
								 BYTE			byClass,
								 BYTE			byGender,
								 BYTE			byFace,
								 BYTE			byHair,
								 BYTE			byHairColor,
								 BYTE			bySkinColor,
								 TBLIDX			worldTblidx,
								 WORLDID			worldId,
								 float			fPositionX,
								 float			fPositionY,
								 float			fPositionZ,
								 sITEM_SUMMARY*	sItem,
								 DWORD			dwMapInfoIndex
								)
{
	sPC_SUMMARY tmp;
	ZeroMemory(&tmp, sizeof(sPC_SUMMARY));
	//Default values	
	tmp.charId = (++charID);
	wcscpy_s(tmp.awchName, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1, awchName);
	tmp.bIsAdult = false;
	tmp.bNeedNameChange = false;
	tmp.bTutorialFlag = false;
	tmp.dwMoney = 10000;
	tmp.dwMoneyBank = 10000;
	tmp.byLevel = 1;
	tmp.sMarking.byCode = INVALID_BYTE;
	tmp.sDogi.guildId = INVALID_GUILDID;
	tmp.sDogi.byDojoColor = INVALID_BYTE;
	tmp.sDogi.byDojoColor = INVALID_BYTE;
	tmp.sDogi.byType = INVALID_BYTE;
	tmp.byClass = byClass;
	tmp.byFace = byFace;
	tmp.byGender = byGender;
	tmp.byHair = byHair;
	tmp.byHairColor = byHairColor;
	tmp.byRace = byRace;
	tmp.bySkinColor = bySkinColor;
	tmp.dwMapInfoIndex = dwMapInfoIndex;
	tmp.worldId = worldId;
	tmp.worldTblidx = worldTblidx;
	tmp.fPositionX = fPositionX;
	tmp.fPositionY = fPositionY;
	tmp.fPositionZ = fPositionZ;
	memcpy(tmp.sItem, sItem, sizeof(sITEM_SUMMARY)* EQUIP_SLOT_TYPE_COUNT);
	create[pos] = tmp;
}

void	CQueryServer::ConvertStructTODB(sPC_SUMMARY* from, 
										CSqlUnit_SP_CharacterCreate* to,
										BYTE byBlood,
										BYTE byBindType,
										float fDirX,
										float fDirY, 
										float fDirZ, 
										float fBindLocX,
										float fBindLocY,
										float fBindLocZ, 
										float fBindDirX,
										float fBindDirY,
										float fBindDirZ, 
										DWORD dwBindObjectTblidx,
										DWORD dwBindWorldID, 
										DWORD dwInitEP,
										DWORD dwInitLP,
										DWORD dwInitRP)
{
	size_t   i;
	wcstombs_s(&i, to->m_awchName, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1, from->awchName, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1);
	to->m_byClass = from->byClass;
	to->m_byFace = from->byFace;
	to->m_byGender = from->byGender;
	to->m_byHair = from->byHair;
	to->m_byHairColor = from->byHairColor;
	to->m_byLevel = from->byLevel;
	to->m_byRace = from->byRace;
	to->m_bySkinColor = from->bySkinColor;
	to->m_byBlood = byBlood;
	to->m_byBindType = byBindType;
	to->m_byMarking = from->sMarking.byCode;
	to->m_bIsAdult = from->bIsAdult;
	to->m_worldTblidx = from->worldTblidx;
	to->m_worldId = from->worldId;	
	to->m_bTutorialFlag = from->bTutorialFlag;
	to->m_bNeedNameChange = from->bNeedNameChange;		
	to->m_fPositionX = from->fPositionX;
	to->m_fPositionY = from->fPositionY;
	to->m_fPositionZ = from->fPositionZ;
	to->m_fDirX = fDirX;
	to->m_fDirY = fDirY;
	to->m_fDirZ = fDirZ;
	to->m_fBindPositionX = fBindLocX;
	to->m_fBindPositionY = fBindLocY;
	to->m_fBindPositionZ = fBindLocZ;
	to->m_fBindDirX = fBindDirX;
	to->m_fBindDirY = fBindDirY;
	to->m_fBindDirZ = fBindDirZ;
	to->m_dwMapInfoIndex = from->dwMapInfoIndex;
	to->m_dwMoney = from->dwMoney;
	to->m_dwMoneyBank = from->dwMoneyBank;
	to->m_dwCharID = from->charId;	
	to->m_dwBindObjectTblidx = dwBindObjectTblidx;
	to->m_dwBindWorldID = dwBindWorldID;
	to->m_dwEP = dwInitEP;
	to->m_dwLP = dwInitLP;
	to->m_dwRP = dwInitRP;
	to->m_dwEXP = 0;
	to->m_dwReputationPoint = 0;
	to->m_dwSpPoint = 0;
	to->m_dwMudosaPoint = 0;	
}

void	CQueryServer::ConvertStructTODB(CSqlUnit_SP_CharacterSelect* from, sPC_SUMMARY* to)
{
	size_t   i;
	mbstowcs_s(&i, to->awchName, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1, from->m_awchName, _TRUNCATE);
	to->bIsAdult = from->m_bIsAdult;
	to->bNeedNameChange = from->m_bNeedNameChange;
	to->bTutorialFlag = from->m_bTutorialFlag;
	to->byClass = from->m_byClass;
	to->byFace = from->m_byFace;
	to->byGender = from->m_byGender;
	to->byHair = from->m_byHair;
	to->byHairColor = from->m_byHairColor;
	to->byLevel = from->m_byLevel;
	to->byRace = from->m_byRace;
	to->bySkinColor = from->m_bySkinColor;
	to->dwMapInfoIndex = from->m_dwMapInfoIndex;
	to->dwMoney = from->m_dwMoney;
	to->dwMoneyBank = from->m_dwMoneyBank;
	to->charId = from->m_dwCharID;
	to->fPositionX = from->m_fPositionX;
	to->fPositionY = from->m_fPositionY;
	to->fPositionZ = from->m_fPositionZ;
	to->worldId = from->m_worldId;
	to->worldTblidx = from->m_worldTblidx;
}

void	CQueryServer::ConvertDBToStruct(sPC_SUMMARY* create,
	int				charID,
	WCHAR*			awchName,
	BYTE			byRace,
	BYTE			byClass,
	bool			bIsAdult,
	BYTE			byGender,
	BYTE			byFace,
	BYTE			byHair,
	BYTE			byHairColor,
	BYTE			bySkinColor,
	BYTE			byLevel,
	TBLIDX			worldTblidx,
	WORLDID			worldId,
	float			fPositionX,
	float			fPositionY,
	float			fPositionZ,
	DWORD			dwMoney,
	DWORD			dwMoneyBank,
	DWORD			dwMapInfoIndex,
	bool			bTutorialFlag,
	bool			bNeedNameChange)
{
	wcscpy_s(create->awchName, NTL_MAX_SIZE_CHAR_NAME_UNICODE+1, awchName);
	create->charId = ++charID;
	create->bIsAdult = bIsAdult;
	create->bNeedNameChange = bNeedNameChange;
	create->bTutorialFlag = bTutorialFlag;
	create->byClass = byClass;
	create->byFace = byFace;
	create->byGender = byGender;
	create->byHair = byHair;
	create->byHairColor = byHairColor;
	create->byLevel = byLevel;
	create->byRace = byRace;
	create->bySkinColor = bySkinColor;
	create->fPositionX = fPositionX;
	create->fPositionY = fPositionY;
	create->fPositionZ = fPositionZ;
	create->dwMapInfoIndex = dwMapInfoIndex;
	create->dwMoney = dwMoney;
	create->dwMoneyBank = dwMoneyBank;
	create->worldId = worldId;
	create->worldTblidx = worldTblidx;
}
void	CQueryServer::ConvertInventoryTMPToInventory(sITEM_SUMMARY* tmp, sITEM_SUMMARY* pcSummary)
{
	for (int i = 0; i < EQUIP_SLOT_TYPE_COUNT; i++)
	{
		pcSummary[i].aOptionTblidx[0] = tmp[i].aOptionTblidx[0];
		pcSummary[i].aOptionTblidx[1] = tmp[i].aOptionTblidx[1];
		pcSummary[i].byBattleAttribute = tmp[i].byBattleAttribute;
		pcSummary[i].byGrade = tmp[i].byGrade;
		pcSummary[i].byRank = ITEM_RANK_NORMAL;
		pcSummary[i].tblidx = tmp[i].tblidx;
	}
}
int		CQueryServer::UpdateChar(sPC_SUMMARY* update,
									int				charID,
									WCHAR*			awchName,
									BYTE			byRace,
									BYTE			byClass,
									BYTE			byGender,
									BYTE			byFace,
									BYTE			byHair,
									BYTE			byHairColor,
									BYTE			bySkinColor,
									TBLIDX			worldTblidx,
									WORLDID			worldId,
									float			fPositionX,
									float			fPositionY,
									float			fPositionZ,
									sITEM_SUMMARY*	sItem,
									DWORD			dwMapInfoIndex)
{
	int i;
	for (i = 0; i < NTL_MAX_COUNT_USER_CHAR_SLOT; i++)
	{
		if ((update[i].charId == INVALID_CHARACTERID) || (update[i].charId == 3435973836) || ((update[i].charId == 0)))
		{
			CreateChar(i, 
					   update,
					   charID,
					   awchName,
					   byRace,
					   byClass,
					   byGender,
					   byFace,
					   byHair,
					   byHairColor,
					   bySkinColor,
					   worldTblidx,
					   worldId,
					   fPositionX,
					   fPositionY,
					   fPositionZ,
					   sItem,
					   dwMapInfoIndex);
			break;
		}
	}
	return i;
}

void	CQueryServer::SetMasterSession(CMasterSession * pServerSession)
{
	CNtlAutoMutex mutex(&m_masterServerMutex);
	mutex.Lock();

	if (NULL == pServerSession)
	{
		m_pMasterServerSession->Release();
		m_pMasterServerSession = pServerSession;
	}
	else
	{
		pServerSession->Acquire();
		m_pMasterServerSession = pServerSession;
	}
}

CMasterSession * CQueryServer::AcquireMasterSession()
{
	CNtlAutoMutex mutex(&m_masterServerMutex);
	mutex.Lock();

	if (NULL == m_pMasterServerSession) return NULL;

	m_pMasterServerSession->Acquire();
	return m_pMasterServerSession;
}

CMasterSession * CQueryServer::GetMasterSession()
{
	return m_pMasterServerSession;
}

void	CQueryServer::SetAuthSession(CAuthSession * pServerSession)
{
	CNtlAutoMutex mutex(&m_authServerMutex);
	mutex.Lock();

	if (NULL == pServerSession)
	{
		m_pAuthServerSession->Release();
		m_pAuthServerSession = pServerSession;
	}
	else
	{
		pServerSession->Acquire();
		m_pAuthServerSession = pServerSession;
	}
}

CAuthSession* CQueryServer::AcquireAuthSession()
{
	CNtlAutoMutex mutex(&m_authServerMutex);
	mutex.Lock();

	if (NULL == m_pAuthServerSession) return NULL;

	m_pAuthServerSession->Acquire();
	return m_pAuthServerSession;
}

CAuthSession* CQueryServer::GetAuthSession()
{
	return m_pAuthServerSession;
}

void	CQueryServer::SetCharSession(CCharSession * pServerSession)
{
	CNtlAutoMutex mutex(&m_charServerMutex);
	mutex.Lock();

	if (NULL == pServerSession)
	{
		m_pCharServerSession->Release();
		m_pCharServerSession = pServerSession;
	}
	else
	{
		pServerSession->Acquire();
		m_pCharServerSession = pServerSession;
	}
}

CCharSession* CQueryServer::AcquireCharSession()
{
	CNtlAutoMutex mutex(&m_charServerMutex);
	mutex.Lock();

	if (NULL == m_pCharServerSession) return NULL;

	m_pCharServerSession->Acquire();
	return m_pCharServerSession;
}

CCharSession* CQueryServer::GetCharSession()
{
	return m_pCharServerSession;
}

void	CQueryServer::SetCommunitySession(CCommunitySession * pServerSession)
{
	CNtlAutoMutex mutex(&m_communityServerMutex);
	mutex.Lock();

	if (NULL == pServerSession)
	{
		m_pCommunityServerSession->Release();
		m_pCommunityServerSession = pServerSession;
	}
	else
	{
		pServerSession->Acquire();
		m_pCommunityServerSession = pServerSession;
	}
}

CCommunitySession* CQueryServer::AcquireCommunitySession()
{
	CNtlAutoMutex mutex(&m_communityServerMutex);
	mutex.Lock();

	if (NULL == m_pCommunityServerSession) return NULL;

	m_pCommunityServerSession->Acquire();
	return m_pCommunityServerSession;
}

CCommunitySession* CQueryServer::GetCommunitySession()
{
	return m_pCommunityServerSession;
}

void	CQueryServer::SetGameSession(CGameSession * pServerSession)
{
	CNtlAutoMutex mutex(&m_gameServerMutex);
	mutex.Lock();

	if (NULL == pServerSession)
	{
		m_pGameServerSession->Release();
		m_pGameServerSession = pServerSession;
	}
	else
	{
		pServerSession->Acquire();
		m_pGameServerSession = pServerSession;
	}
}

CGameSession* CQueryServer::AcquireGameSession()
{
	CNtlAutoMutex mutex(&m_gameServerMutex);
	mutex.Lock();

	if (NULL == m_pGameServerSession) return NULL;

	m_pGameServerSession->Acquire();
	return m_pGameServerSession;
}

CGameSession* CQueryServer::GetGameSession()
{
	return m_pGameServerSession;
}

void CQueryServer::DoAliveTime()
{
	for (int i = 0; i < this->GetNetwork()->GetSessionList()->GetMaxCount(); i++)
	{
		CNtlSession* first = this->GetNetwork()->GetSessionList()->Find(i);
		if (first)
		{
			if (first->GetSessionType() != SESSION_QUERY_SERVER)
			{
				PACKETDATA res;
				res.wOpCode = SYS_ALIVE;
				CNtlPacket packet((BYTE*)&res, sizeof(res));
				first->PushPacket(&packet);
			}
		}
	}
}