#pragma once
#include "QueryServer.h"
#include "NtlQuery.h"
class CTableInventory :	public CNtlQuery
{
	SQL_ID  eSqlID;
	DWORD ivtID;
	DWORD charID;
	DWORD itemId;
	DWORD tblidx;
	DWORD	m_nUseEndTime;
	DWORD	m_nUseStartTime;
	BYTE  byPlace;
	BYTE  byPosition;
	BYTE  byStackCount;
	BYTE  byRank;
	BYTE  byCurrentDurability;
	int	  bNeedToIdentify;
	BYTE  byGrade;
	BYTE  byBattleAttribute;
	BYTE  byRestrictType;
	BYTE  OptionTblidx1;
	BYTE  OptionTblidx2;
	BYTE  byDurationType;
public:
	CTableInventory();
	~CTableInventory();
	int ExecuteQuery(CNtlDatabaseConnection * pConnection);
	int ExecuteResult();
	void    SetSqlID(SQL_ID ID_SQL) { this->eSqlID = ID_SQL; }
	void	SetCharID(DWORD charId) { this->charID = charId; }
};

