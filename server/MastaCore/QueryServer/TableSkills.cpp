#include "stdafx.h"
#include "TableSkills.h"


CTableSkills::CTableSkills()
{
}


CTableSkills::~CTableSkills()
{
}

int CTableSkills::ExecuteQuery(CNtlDatabaseConnection * pConnection)
{
	SQLLEN nRowCount = 0;

	switch (eSqlID)
	{
	case SP_SkillsInsert:
	{
		FIND_SQLUNIT(SP_SkillsInsert, pConnection, pSqlUnit3);
		if (NULL == pSqlUnit3)
		{
			return NTL_FAIL;
		}

		pSqlUnit3->Exec(&nRowCount);
	}
	break;
	case SP_SkillsDelete:
	{
		FIND_SQLUNIT(SP_SkillsDelete, pConnection, pSqlUnit4);
		if (NULL == pSqlUnit4)
		{
			return NTL_FAIL;
		}

		pSqlUnit4->Exec(&nRowCount);
	}
	break;
	case SP_SkillsSelect:
	{
		FIND_SQLUNIT(SP_SkillsSelect, pConnection, pSqlUnit5);
		if (NULL == pSqlUnit5)
		{
			return NTL_FAIL;
		}

		pSqlUnit5->Exec(&nRowCount);
	}
	break;
	case SP_SkillsSelectAll:
	{
		FIND_SQLUNIT(SP_SkillsSelectAll, pConnection, pSqlUnit);
		if (NULL == pSqlUnit)
		{
			return NTL_FAIL;
		}
		pSqlUnit->m_charID = this->charID;
		if (pSqlUnit->Store())
		{
			if (pSqlUnit->Fetch())
			{
				this->charID = pSqlUnit->m_charID;
			}
			else
			{
				pSqlUnit->m_charID = 0;
				//pSqlUnit->m_itemId = 0;
			}
		}
	}
	break;
	case SP_SkillsMaxSlotByChar:
	{
		FIND_SQLUNIT(SP_SkillsMaxSlotByChar, pConnection, pSqlUnit);
		if (NULL == pSqlUnit)
		{
			return NTL_FAIL;
		}
		pSqlUnit->m_charID = this->charID;
		if (pSqlUnit->Store())
		{
			if (pSqlUnit->Fetch())
			{
				this->charID = pSqlUnit->m_charID;
			}
			else
			{
				pSqlUnit->m_charID = 0;
				//pSqlUnit->m_itemId = 0;
			}
		}
	}
	break;
	case SP_SkillsUpdate:
	{
		FIND_SQLUNIT(SP_SkillsUpdate, pConnection, pSqlUnit);
		if (NULL == pSqlUnit)
		{
			return NTL_FAIL;
		}
		pSqlUnit->m_charID = this->charID;
		if (pSqlUnit->Store())
		{
			if (pSqlUnit->Fetch())
			{
				this->charID = pSqlUnit->m_charID;
			}
			else
			{
				pSqlUnit->m_charID = 0;
				//pSqlUnit->m_itemId = 0;
			}
		}
	}
	break;
	}
	NTL_PRINT(PRINT_APP, "ExecuteQuery Done");

	return NTL_SUCCESS;
}


int CTableSkills::ExecuteResult()
{
	NTL_PRINT(PRINT_APP, "ExecuteResult Done");
	CQueryServer* app = (CQueryServer*)NtlSfxGetApp();
	app->g_exitEvent.Notify();
	return NTL_SUCCESS;
}