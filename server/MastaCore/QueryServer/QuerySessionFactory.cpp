#include "stdafx.h"
#include "QuerySessionFactory.h"


CQuerySessionFactory::CQuerySessionFactory()
{
}


CQuerySessionFactory::~CQuerySessionFactory()
{
}

CNtlSession * CQuerySessionFactory::CreateSession(SESSIONTYPE sessionType)
{
	CNtlSession * pSession = NULL;
	switch (sessionType)
	{
		case SESSION_QUERY_SERVER:
		{
			pSession = new CQuerySession(sessionType);
		}
		break;
		case SESSION_AUTH_SERVER:
		{
			pSession = new CNtlSession(sessionType);
		}
		break;
		case SESSION_MASTER_SERVER:
		{
			pSession = new CNtlSession(sessionType);
		}
		break;
		case SESSION_CHAR_SERVER:
		{
			pSession = new CNtlSession(sessionType);
		}
		break;
		case SESSION_COMMUNITY_SERVER:
		{
			pSession = new CNtlSession(sessionType);
		}
		break;
		case SESSION_GAME_SERVER:
		{
			pSession = new CNtlSession(sessionType);
		}
		break;
	default:
		break;
	}

	return pSession;
}



void CAuthSessionFactory::DestroySession(CNtlSession * pSession)
{
	switch (pSession->GetSessionType())
	{
	case SESSION_MASTER_SERVER:
	case SESSION_QUERY_SERVER:
	case SESSION_AUTH_SERVER:
	case SESSION_COMMUNITY_SERVER:
	case SESSION_CHAR_SERVER:
	case SESSION_GAME_SERVER:
	{
		SAFE_DELETE(pSession);
	}
	break;
	default:
		break;
	}
}