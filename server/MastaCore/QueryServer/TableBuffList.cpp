#include "stdafx.h"
#include "TableBuffList.h"


CTableBuffList::CTableBuffList()
{
}


CTableBuffList::~CTableBuffList()
{
}

int CTableBuffList::ExecuteQuery(CNtlDatabaseConnection * pConnection)
{
	SQLLEN nRowCount = 0;

	switch (eSqlID)
	{
		case SP_BuffListSelect:
		{
			FIND_SQLUNIT(SP_BuffListSelect, pConnection, pSqlUnit);
			if (NULL == pSqlUnit)
			{
				return NTL_FAIL;
			}

			pSqlUnit->m_charID = this->charID;

			if (pSqlUnit->Store())
			{
				if (pSqlUnit->Fetch())
				{
					this->charID = pSqlUnit->m_charID;
				}
			}
		}
		break;
		case SP_BuffListCreate:
		{
			FIND_SQLUNIT(SP_BuffListCreate, pConnection, pSqlUnit3);
			if (NULL == pSqlUnit3)
			{
				return NTL_FAIL;
			}

			pSqlUnit3->Exec(&nRowCount);
		}
		break;
		case SP_BuffListDelete:
		{
			FIND_SQLUNIT(SP_BuffListDelete, pConnection, pSqlUnit5);
			if (NULL == pSqlUnit5)
			{
				return NTL_FAIL;
			}

			pSqlUnit5->Exec(&nRowCount);
		}
		break;
		case SP_BuffListUpdate:
		{
			FIND_SQLUNIT(SP_BuffListUpdate, pConnection, pSqlUnit4);
			if (NULL == pSqlUnit4)
			{
				return NTL_FAIL;
			}

			pSqlUnit4->Exec(&nRowCount);
		}
		break;
	}

	NTL_PRINT(PRINT_APP, "ExecuteQuery Done");

	return NTL_SUCCESS;
}


int CTableBuffList::ExecuteResult()
{
	NTL_PRINT(PRINT_APP, "ExecuteResult Done");
	CQueryServer* app = (CQueryServer*)NtlSfxGetApp();
	app->g_exitEvent.Notify();
	return NTL_SUCCESS;
}