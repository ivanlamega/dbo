#pragma once
#include "QueryServer.h"
#include "NtlSessionFactory.h"

#ifndef CQUERYSESSION_FACTORY_H
#define CQUERYSESSION_FACTORY_H

class CQuerySessionFactory : public CNtlSessionFactory
{
public:
	CQuerySessionFactory();
	~CQuerySessionFactory();
	CNtlSession * CreateSession(SESSIONTYPE sessionType);
	void DestroySession(CNtlSession * pSession);
};
#endif

