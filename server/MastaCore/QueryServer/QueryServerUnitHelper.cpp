#include "stdafx.h"
#include "QueryServer.h"
#include "QueryServerUnitHelper.h"


CQueryServerUnitHelper::CQueryServerUnitHelper(SQLUNITID maxSqlUnitID) :CNtlSqlUnitHelper(maxSqlUnitID)
{
}


CQueryServerUnitHelper::~CQueryServerUnitHelper()
{
}
BOOL	CQueryServerUnitHelper::PreCreateSqlUnit(CNtlDatabaseConnection * pConnection)
{
	//List Avaible Query for this application
	//Table Accounts
	if (FALSE == MAKE_SQLUNIT(SP_AccountSelect, pConnection))
	{
		return FALSE;
	}
	//Table Characters
	if (FALSE == MAKE_SQLUNIT(SP_CharacterCreate, pConnection))
	{
		return FALSE;
	}
	if (FALSE == MAKE_SQLUNIT(SP_CharacterUpdate, pConnection))
	{
		return FALSE;
	}
	if (FALSE == MAKE_SQLUNIT(SP_CharacterSelect, pConnection))
	{
		return FALSE;
	}
	if (FALSE == MAKE_SQLUNIT(SP_CharacterSelectByID, pConnection))
	{
		return FALSE;
	}
	if (FALSE == MAKE_SQLUNIT(SP_CharacterOnline, pConnection))
	{
		return FALSE;
	}
	if (FALSE == MAKE_SQLUNIT(SP_CharacterMaxID, pConnection))
	{
		return FALSE;
	}
	//Table HTB List
	if (FALSE == MAKE_SQLUNIT(SP_HTBSkillSelect, pConnection))
	{
		return FALSE;
	}
	if (FALSE == MAKE_SQLUNIT(SP_HTBSkillInsert, pConnection))
	{
		return FALSE;
	}	
	if (FALSE == MAKE_SQLUNIT(SP_HTBSkillUpdate, pConnection))
	{
		return FALSE;
	}
	//Table Inventory
	if (FALSE == MAKE_SQLUNIT(SP_InventorySelect, pConnection))
	{
		return FALSE;
	}
	if (FALSE == MAKE_SQLUNIT(SP_InventoryCreate, pConnection))
	{
		return FALSE;
	}
	if (FALSE == MAKE_SQLUNIT(SP_InventoryInsertItem, pConnection))
	{
		return FALSE;
	}
	if (FALSE == MAKE_SQLUNIT(SP_InventoryDeleteItem, pConnection))
	{
		return FALSE;
	}
	if (FALSE == MAKE_SQLUNIT(SP_InventoryItemMove, pConnection))
	{
		return FALSE;
	}
	//Table QuickSlot
	if (FALSE == MAKE_SQLUNIT(SP_QuickSlotSelect, pConnection))
	{
		return FALSE;
	}
	if (FALSE == MAKE_SQLUNIT(SP_QuickSlotCreate, pConnection))
	{
		return FALSE;
	}
	if (FALSE == MAKE_SQLUNIT(SP_QuickSlotUpdate, pConnection))
	{
		return FALSE;
	}
	if (FALSE == MAKE_SQLUNIT(SP_QuickSlotDelete, pConnection))
	{
		return FALSE;
	}
	//Table Skills
	if (FALSE == MAKE_SQLUNIT(SP_SkillsDelete, pConnection))
	{
		return FALSE;
	}
	if (FALSE == MAKE_SQLUNIT(SP_SkillsInsert, pConnection))
	{
		return FALSE;
	}
	if (FALSE == MAKE_SQLUNIT(SP_SkillsSelect, pConnection))
	{
		return FALSE;
	}
	if (FALSE == MAKE_SQLUNIT(SP_SkillsMaxSlotByChar, pConnection))
	{
		return FALSE;
	}
	if (FALSE == MAKE_SQLUNIT(SP_SkillsSelectAll, pConnection))
	{
		return FALSE;
	}
	if (FALSE == MAKE_SQLUNIT(SP_SkillsUpdate, pConnection))
	{
		return FALSE;
	}
	//Table BuffList
	if (FALSE == MAKE_SQLUNIT(SP_BuffListCreate, pConnection))
	{
		return FALSE;
	}
	if (FALSE == MAKE_SQLUNIT(SP_BuffListDelete, pConnection))
	{
		return FALSE;
	}
	if (FALSE == MAKE_SQLUNIT(SP_BuffListSelect, pConnection))
	{
		return FALSE;
	}
	if (FALSE == MAKE_SQLUNIT(SP_BuffListUpdate, pConnection))
	{
		return FALSE;
	}
	//Table WarFog
	if (FALSE == MAKE_SQLUNIT(SP_WarFogInsert, pConnection))
	{
		return FALSE;
	}
	if (FALSE == MAKE_SQLUNIT(SP_WarFogSelect, pConnection))
	{
		return FALSE;
	}
	if (FALSE == MAKE_SQLUNIT(SP_WarFogUpdate, pConnection))
	{
		return FALSE;
	}
	return TRUE;
}