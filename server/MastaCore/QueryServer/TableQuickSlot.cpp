#include "stdafx.h"
#include "TableQuickSlot.h"


CTableQuickSlot::CTableQuickSlot()
{
}


CTableQuickSlot::~CTableQuickSlot()
{
}

int CTableQuickSlot::ExecuteQuery(CNtlDatabaseConnection * pConnection)
{
	SQLLEN nRowCount = 0;

	switch (eSqlID)
	{
		case SP_QuickSlotCreate:
		{
			FIND_SQLUNIT(SP_QuickSlotCreate, pConnection, pSqlUnit3);
			if (NULL == pSqlUnit3)
			{
				return NTL_FAIL;
			}

			pSqlUnit3->Exec(&nRowCount);
		}
		break;
		case SP_QuickSlotUpdate:
		{
			FIND_SQLUNIT(SP_QuickSlotUpdate, pConnection, pSqlUnit4);
			if (NULL == pSqlUnit4)
			{
				return NTL_FAIL;
			}

			pSqlUnit4->Exec(&nRowCount);
		}
		break;
		case SP_QuickSlotDelete:
		{
			FIND_SQLUNIT(SP_QuickSlotDelete, pConnection, pSqlUnit5);
			if (NULL == pSqlUnit5)
			{
				return NTL_FAIL;
			}

			pSqlUnit5->Exec(&nRowCount);
		}
		break;
		case SP_QuickSlotSelect:
		{
			FIND_SQLUNIT(SP_QuickSlotSelect, pConnection, pSqlUnit);
			if (NULL == pSqlUnit)
			{
				return NTL_FAIL;
			}
			pSqlUnit->m_charID = this->charID;
			if (pSqlUnit->Store())
			{
				if (pSqlUnit->Fetch())
				{
					this->charID = pSqlUnit->m_charID;
				}
				else
				{
					pSqlUnit->m_charID = 0;
					//pSqlUnit->m_itemId = 0;
				}
			}
		}
		break;
	}
	NTL_PRINT(PRINT_APP, "ExecuteQuery Done");

	return NTL_SUCCESS;
}


int CTableQuickSlot::ExecuteResult()
{
	NTL_PRINT(PRINT_APP, "ExecuteResult Done");
	CQueryServer* app = (CQueryServer*)NtlSfxGetApp();
	app->g_exitEvent.Notify();
	return NTL_SUCCESS;
}