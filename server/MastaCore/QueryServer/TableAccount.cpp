#include "stdafx.h"
#include "TableAccount.h"


CTableAccount::CTableAccount()
{
	ZeroMemory(m_szUserID, MAX_SIZE_USER_ID + 1);
	ZeroMemory(m_szUserPW, MAX_SIZE_USER_PW + 1);
}


CTableAccount::~CTableAccount()
{	
}

int CTableAccount::ExecuteQuery(CNtlDatabaseConnection * pConnection)
{
	SQLLEN nRowCount = 0;

	switch (eSqlID)
	{
		case SP_AccountSelect:
		{
			FIND_SQLUNIT(SP_AccountSelect, pConnection, pSqlUnit);
			if (NULL == pSqlUnit)
			{
				return NTL_FAIL;
			}

			strncpy_s(pSqlUnit->m_szUserID, GetUsername(), MAX_SIZE_USER_ID + 1);
			strncpy_s(pSqlUnit->m_szUserPW, GetPassword(), MAX_SIZE_USER_PW + 1);

			if (pSqlUnit->Store())
			{
				if (pSqlUnit->Fetch())
				{
					SetAccountID(pSqlUnit->m_dwAccountID);
					SetUsername(pSqlUnit->m_szUserID);
					SetPassword(pSqlUnit->m_szUserPW);
					SetLastFarmID(pSqlUnit->m_dwLastFarmID);
				}
			}
		}
		break;
	}

	NTL_PRINT(PRINT_APP, "ExecuteQuery Done");

	return NTL_SUCCESS;
}


int CTableAccount::ExecuteResult()
{
	NTL_PRINT(PRINT_APP, "ExecuteResult Done");
	CQueryServer* app = (CQueryServer*)NtlSfxGetApp();
	app->g_exitEvent.Notify();
	return NTL_SUCCESS;
}