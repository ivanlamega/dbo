#pragma once
#include "QueryServer.h"
#include "NtlQuery.h"
class CTableQuickSlot :	public CNtlQuery
{
private:
	SQL_ID  eSqlID;
	DWORD charID;
	BYTE		m_bySlot;
	DWORD		m_dwItemTblidx;
	DWORD		m_dwSkillTblidx;
	BYTE		m_byType;
public:
	CTableQuickSlot();
	~CTableQuickSlot();
	int ExecuteQuery(CNtlDatabaseConnection * pConnection);
	int ExecuteResult();
	void    SetSqlID(SQL_ID ID_SQL) { this->eSqlID = ID_SQL; }
	void	SetCharID(DWORD charId) { this->charID = charId; }
};

