#pragma once
#include "QueryServer.h"
#include "NtlSession.h"
#include "NtlPacketEncoder_RandKey.h"
///////////////////////////////////////RESPONSE PACKETS///////////////////////////
//Query Server -> Auth Server
#include "NtlPacketQA.h"
//Query Server -> Master Server
#include "NtlPacketQM.h"
//Query Server -> Char Server
#include "NtlPacketQC.h"
//Query server -> Community Server
#include "NtlPacketQT.h"
//Query Server -> Game Server
#include "NtlPacketQG.h"
//Query server -> Operating Server
#include "NtlPacketQP.h"
/////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////REQUEST PACKETS///////////////////////////
//Auth Server -> QueryServer
#include "NtlPacketAQ.h"
//Master Server -> Query Server
#include "NtlPacketMQ.h"
//Char Server -> Query Server
#include "NtlPacketCQ.h"
//Community Server -> Query Server
#include "NtlPacketTQ.h"
//Game server -> Query Server
#include "NtlPacketGQ.h"
//Operating server -> Query Server
#include "NtlPacketPQ.h"
/////////////////////////////////////////////////////////////////////////////////
#include "NtlPacketSYS.h"
#include "NtlPacketUtil.h"

class CQuerySession :	public CNtlSession
{
public:
	CQuerySession(bool bAliveCheck = true, bool bOpcodeCheck = true);
	~CQuerySession();
	int							OnAccept();
	int							OnConnect();
	void						OnClose();
	int							OnDispatch(CNtlPacket * pPacket);
	// Packet functions
	//Query session
	void						SendQueryCharInfo(ACCOUNTID accID, CHARACTERID id, sPC_DATA* plr,sVECTOR3& vBindLoc, sVECTOR3& vBindDir, BYTE& byBindType);
	BYTE						SendQueryLoadInventory(ACCOUNTID accId, CHARACTERID id, sITEM_DATA* itDat);
	BYTE						SendQueryQuickSlot(ACCOUNTID accId, CHARACTERID id, sQUICK_SLOT_DATA* qS);
	BYTE						SendQuerySkill(ACCOUNTID accId, CHARACTERID id, sSKILL_DATA* sk);
	BYTE						SendQueryHTBSkill(ACCOUNTID accId, CHARACTERID id, sHTB_SKILL_DATA* ht);
	BYTE						SendQueryBuffSkill(ACCOUNTID accId, CHARACTERID id, sBUFF_DATA* bfDt);
	void						SendQueryWarFogInfo(CHARACTERID id, sCHAR_WAR_FOG_FLAG* warFog);
	void						SendQueryLoadBank(ACCOUNTID accId, CHARACTERID id, DWORD* dwZenny, DWORD* dwBankZenny);	
	BYTE						SendQueryAddSkill(CHARACTERID charId, BYTE byRpBonus, DWORD dwSkill, DWORD nEXP, DWORD nRemainSec);
	//AUTH Server
	void						SendAuthLoginReq(CNtlPacket* pPacket);
	//Char Server
	void						SendCharNotifyServerReq(CNtlPacket* pPacket);
	void						SendCharLoadReq(CNtlPacket* pPacket);
	void						SendCharAddReq(CNtlPacket* pPacket);
	void						SendCharConnectCheck(CNtlPacket* pPacket);
	void						SendCharConnectCheckCancel(CNtlPacket* pPacket);
	void						SendCharDisconnect(CNtlPacket* pPacket);
	//Game Server
	void						SendGameLoadPcDataReq(CNtlPacket* pPacket);
	void						SendGameLoadPcItems(CNtlPacket* pPacket);
	void						SendGameLoadPcSkills(CNtlPacket* pPacket);
	void						SendGameLoadBuff(CNtlPacket* pPacket);
	void						SendGameLoadWarFog(CNtlPacket* pPacket);
	void						SendGameLoadPcHTB(CNtlPacket* pPacket);
	void						SendGameLoadPcQuickSlot(CNtlPacket* pPacket);
	void						SendGameLoadPcGMT(CNtlPacket* pPacket);//<--- This will tell to game server send the packets
	//void						SendGameMailNotify(ACCOUNTID accID, CHARACTERID charID);
	void						SendGameSkillLearn(CNtlPacket* pPacket);
	// End Packet functions
private:
	CNtlPacketEncoder_RandKey	m_packetEncoder;
};