// QueryMain.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "NtlSfx.h"
#include "QueryServer.h"
#include "NtlFile.h"



int _tmain(int argc, _TCHAR* argv[])
{
	NtlSetPrintFlag(PRINT_APP);

	CQueryServer app;
	CNtlFileStream traceFileStream;

	// LOG FILE
	std::cout << "Loading Server.ini... \n";
	int rc = traceFileStream.Create("QueryServerLog");
	rc = app.Create(argc, argv, ".\\Server.ini");
	NtlSetPrintStream(traceFileStream.GetFilePtr());
	NtlSetPrintFlag(PRINT_APP | PRINT_SYSTEM);

	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "log file CreateFile error %d(%s)", rc, NtlGetErrorMessage(rc));
		return rc;
	}
	std::cout << "Loaded!\n";

	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "Server Application Create Fail %d(%s)", rc, NtlGetErrorMessage(rc));
		return rc;
	}
	app.Start();
	std::cout << "QUERY SERVER RUNNING... \n";
	std::cout << "DATABASE MANAGER INITIATING... \n";
	HDATABASE hDB = INVALID_HDATABASE;
	CQueryServerUnitHelper sqlUnitHelper(MAX_SQL_ID);

	rc = app.GetDatabaseManager()->Create(&sqlUnitHelper, 10);
	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "DatabaseManager Create Error %d(%s)", rc, NtlGetErrorMessage(rc));
		return -1;
	}

	rc = app.GetDatabaseManager()->Open(app.GetQueryServerConfig()->strDatabaseDSN.c_str(), app.GetQueryServerConfig()->strDatabaseUsername.c_str(), app.GetQueryServerConfig()->strDatabasePassword.c_str(), &hDB, 100, true);
	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "DatabaseManager Open Error %d(%s)", rc, NtlGetErrorMessage(rc));
		return -1;
	}
	app.GetDatabaseManager()->Start();
	std::cout << "DATABASE MANAGER RUNNING... \n";
	app.SetHDatabase(hDB);
	app.WaitForTerminate();

	return 0;
}

