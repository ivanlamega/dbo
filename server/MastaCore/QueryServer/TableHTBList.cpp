#include "stdafx.h"
#include "TableHTBList.h"


CTableHTBList::CTableHTBList()
{
}


CTableHTBList::~CTableHTBList()
{
}

int CTableHTBList::ExecuteQuery(CNtlDatabaseConnection * pConnection)
{
	SQLLEN nRowCount = 0;

	switch (eSqlID)
	{
	case SP_HTBSkillInsert:
	{
		FIND_SQLUNIT(SP_HTBSkillInsert, pConnection, pSqlUnit3);
		if (NULL == pSqlUnit3)
		{
			return NTL_FAIL;
		}

		pSqlUnit3->Exec(&nRowCount);
	}
	break;
	case SP_HTBSkillSelect:
	{
		FIND_SQLUNIT(SP_HTBSkillSelect, pConnection, pSqlUnit4);
		if (NULL == pSqlUnit4)
		{
			return NTL_FAIL;
		}

		pSqlUnit4->Exec(&nRowCount);
	}
	break;
	case SP_HTBSkillUpdate:
	{
		FIND_SQLUNIT(SP_HTBSkillUpdate, pConnection, pSqlUnit5);
		if (NULL == pSqlUnit5)
		{
			return NTL_FAIL;
		}

		pSqlUnit5->Exec(&nRowCount);
	}
	break;	
	}
	NTL_PRINT(PRINT_APP, "ExecuteQuery Done");

	return NTL_SUCCESS;
}


int CTableHTBList::ExecuteResult()
{
	NTL_PRINT(PRINT_APP, "ExecuteResult Done");
	CQueryServer* app = (CQueryServer*)NtlSfxGetApp();
	app->g_exitEvent.Notify();
	return NTL_SUCCESS;
}