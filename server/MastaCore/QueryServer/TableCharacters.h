#pragma once
#include "QueryServer.h"
#include "NtlQuery.h"
class CTableCharacters :	public CNtlQuery
{
private:
	//From DB
	int								m_bIsOnline;
	char							m_awchName[NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1];
	BYTE							m_byRace;
	BYTE							m_byClass;
	BYTE							m_byGender;
	BYTE							m_byFace;
	BYTE							m_byHair;
	BYTE							m_byHairColor;
	BYTE							m_bySkinColor;
	BYTE							m_byLevel;
	BYTE							m_byBindType;
	BYTE							m_byBlood;
	BYTE							m_byMarking;//eMARKING_TYPE
	int								m_bIsAdult;
	int								m_worldTblidx;
	int								m_worldId;
	int								m_bTutorialFlag;
	int								m_bNeedNameChange;
	float							m_fPositionX;
	float							m_fPositionY;
	float							m_fPositionZ;
	float							m_fDirX;
	float							m_fDirY;
	float							m_fDirZ;
	float							m_fBindPositionX;
	float							m_fBindPositionY;
	float							m_fBindPositionZ;
	float							m_fBindDirX;
	float							m_fBindDirY;
	float							m_fBindDirZ;
	DWORD							m_dwAccountID;
	DWORD							m_dwServerID;
	DWORD							m_dwCharID;
	DWORD							m_dwBindObjectTblidx;
	DWORD							m_dwBindWorldID;
	DWORD							m_dwMoney;
	DWORD							m_dwMoneyBank;
	DWORD							m_dwMapInfoIndex;
	DWORD							m_dwLP;
	DWORD							m_dwEP;
	DWORD							m_dwRP;
	DWORD							m_dwEXP;
	DWORD							m_dwReputationPoint;
	DWORD							m_dwMudosaPoint;
	DWORD							m_dwSpPoint;
	int								m_iMaxCharID;
	SQL_ID  eSqlID;
	sPC_SUMMARY					m_asCharList[NTL_MAX_COUNT_USER_CHAR_SLOT];
public:
	CTableCharacters();
	~CTableCharacters();
	int ExecuteQuery(CNtlDatabaseConnection * pConnection);
	int ExecuteResult();
	void    SetSqlID(SQL_ID ID_SQL) { this->eSqlID = ID_SQL; }
	void	SetCharID(DWORD charId) { this->m_dwCharID = charId; }
	void	SetAccountID(DWORD accID){ this->m_dwAccountID = accID; }
	void	SetServerID(DWORD serverID){ this->m_dwServerID = serverID; }	
	void	SetIsOnline(bool bIsOnline){ this->m_bIsOnline = bIsOnline; }
	void	SetMaxCharID(int maxId){ this->m_iMaxCharID = maxId; }
};

