#include "stdafx.h"
#include "QueryServer.h"
#include "QuerySession.h"


CQuerySession::CQuerySession(bool bAliveCheck, bool bOpcodeCheck) :CNtlSession(SESSION_QUERY_SERVER)
{	
	SetControlFlag(CONTROL_FLAG_USE_SEND_QUEUE);

	if (bAliveCheck)
	{
		SetControlFlag(CONTROL_FLAG_CHECK_ALIVE);
	}
	if (bOpcodeCheck)
	{
		SetControlFlag(CONTROL_FLAG_CHECK_OPCODE);
	}

	SetPacketEncoder(&m_packetEncoder);
}

//-----------------------------------------------------------------------------------
//		클라이언트 소멸
//-----------------------------------------------------------------------------------
CQuerySession::~CQuerySession()
{
	NTL_PRINT(PRINT_APP, "CQuerySession Destructor Called");

	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	app->Remove(this->GetHandle());
}

int CQuerySession::OnAccept()
{
	NTL_PRINT(PRINT_APP, "%s", __FUNCTION__);
	CQueryServer* app = (CQueryServer*)NtlSfxGetApp();
	for (int i = 0; i < app->GetNetwork()->GetSessionList()->GetMaxCount(); i++)
	{
		CNtlSession* first = app->GetNetwork()->GetSessionList()->Find(i);
		if (first)
		{
			if (first->GetSessionType() == SESSION_AUTH_SERVER)
			{
				app->SetAuthSession((CAuthSession*)first);
			}
			if (first->GetSessionType() == SESSION_MASTER_SERVER)
			{
				app->SetMasterSession((CMasterSession*)first);
			}
			if (first->GetSessionType() == SESSION_CHAR_SERVER)
			{
				app->SetCharSession((CCharSession*)first);
			}
			if (first->GetSessionType() == SESSION_COMMUNITY_SERVER)
			{
				app->SetCommunitySession((CCommunitySession*)first);
			}
			if (first->GetSessionType() == SESSION_GAME_SERVER)
			{
				app->SetGameSession((CGameSession*)first);
			}
		}
	}
	if ((app->GetAuthSession() != NULL) && (!app->IsAuthServerConnected()))
	{
		CNtlPacket packetA(sizeof(sQA_HEARTBEAT));
		sQA_HEARTBEAT* msgA = (sQA_HEARTBEAT *)packetA.GetPacketData();
		msgA->wOpCode = QA_HEARTBEAT;
		packetA.SetPacketLen(sizeof(sQA_HEARTBEAT));
		app->GetAuthSession()->PushPacket(&packetA);
		app->SetAuthServerConnected(true);
	}
	if ((app->GetMasterSession() != NULL) && (!app->IsMasterServerConnected()))
	{
		CNtlPacket packetM(sizeof(sQM_HEARTBEAT));
		sQM_HEARTBEAT* msgM = (sQM_HEARTBEAT*)packetM.GetPacketData();
		msgM->wOpCode = QM_HEARTBEAT;
		packetM.SetPacketLen(sizeof(sQM_HEARTBEAT));
		app->GetMasterSession()->PushPacket(&packetM);
		app->SetMasterServerConnected(true);
	}
	if ((app->GetCharSession() != NULL) && (!app->IsCharServerConnected()))
	{
		CNtlPacket packetC(sizeof(sQC_HEARTBEAT));
		sQC_HEARTBEAT* msgC = (sQC_HEARTBEAT*)packetC.GetPacketData();
		msgC->wOpCode = QC_HEARTBEAT;
		packetC.SetPacketLen(sizeof(sQC_HEARTBEAT));
		app->GetCharSession()->PushPacket(&packetC);
		app->SetCharServerConnected(true);
	}
	if ((app->GetCommunitySession() != NULL) && (!app->IsCommunityServerConnected()))
	{
		CNtlPacket packetT(sizeof(sQT_HEARTBEAT));
		sQT_HEARTBEAT* msgT = (sQT_HEARTBEAT*)packetT.GetPacketData();
		msgT->wOpCode = QT_HEARTBEAT;
		packetT.SetPacketLen(sizeof(sQT_HEARTBEAT));
		app->GetCommunitySession()->PushPacket(&packetT);
		app->SetCommunityServerConnected(true);
	}
	if ((app->GetGameSession() != NULL) && (!app->IsGameServerConnected()))
	{
		CNtlPacket packetG(sizeof(sQG_HEARTBEAT));
		sQG_HEARTBEAT* msgG = (sQG_HEARTBEAT*)packetG.GetPacketData();
		msgG->wOpCode = QG_HEARTBEAT;
		packetG.SetPacketLen(sizeof(sQT_HEARTBEAT));
		app->GetGameSession()->PushPacket(&packetG);
		app->SetGameServerConnected(true);
	}
	return NTL_SUCCESS;
}


void CQuerySession::OnClose()
{
	NTL_PRINT(PRINT_APP, "%s", __FUNCTION__);
}

int CQuerySession::OnConnect()
{
	NTL_PRINT(PRINT_APP, "%s", __FUNCTION__);
	CQueryServer* app = (CQueryServer*)NtlSfxGetApp();
	do{
		this->OnAccept();
		Sleep(5000);
	} while ((!app->GetAuthSession()) ||
			 (!app->GetCharSession()) ||
			 (!app->GetMasterSession())||
			 (!app->GetGameSession()) ||
			 (!app->GetCommunitySession()));
	return 0;
}

int CQuerySession::OnDispatch(CNtlPacket * pPacket)
{
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	if ((!app->GetAuthSession()) ||
		(!app->GetCharSession()) ||
		(!app->GetMasterSession())||
		(!app->GetGameSession()) ||
		(!app->GetCommunitySession()))
		this->OnConnect();
	sNTLPACKETHEADER * pHeader = (sNTLPACKETHEADER *)pPacket->GetPacketData();
	switch (pHeader->wOpCode)
	{
		//Auth Server Handling
		case AQ_HEARTBEAT:
		{
			if (app->GetAuthSession() != NULL)
				app->GetAuthSession()->ResetAliveTime();
		}
		break;
		case AQ_LOGIN_REQ:
		{
			CQuerySession::SendAuthLoginReq(pPacket);
		}
		break;
		//Master Server Handling
		case MQ_HEARTBEAT:
		{
			if (app->GetMasterSession() != NULL)
				app->GetMasterSession()->ResetAliveTime();
		}
		break;	
		//Char Server Handling
		case CQ_NOTIFY_SERVER_BEGIN:
		{
			CQuerySession::SendCharNotifyServerReq(pPacket);
		}
		break;
		case CQ_CHARACTER_LOAD_REQ:
		{
			CQuerySession::SendCharLoadReq(pPacket);
		}
		break;
		case CQ_CHARACTER_ADD_REQ:
		{
			CQuerySession::SendCharAddReq(pPacket);
		}
		break;
		case CQ_CONNECT_WAIT_CHECK_REQ:
		{
			CQuerySession::SendCharConnectCheck(pPacket);
		}
		break;
		case CQ_CONNECT_WAIT_CANCEL_REQ:
		{
			CQuerySession::SendCharConnectCheckCancel(pPacket);
		}
		break;
		case CQ_CHARACTER_EXIT:
		{
			CQuerySession::SendCharDisconnect(pPacket);
		}
		break;
		//Community Handling
		case TQ_HEARTBEAT:
		{
			if (app->GetCommunitySession() != NULL)
				app->GetCommunitySession()->ResetAliveTime();
		}
		break;
		//Game Handling
		case GQ_NOTIFY_SERVER_BEGIN:
		{
			if (app->GetGameSession() != NULL)
				app->GetGameSession()->ResetAliveTime();
		}
		break;
		case GQ_SKILL_ADD_REQ:
		{
			CQuerySession::SendGameSkillLearn(pPacket);
		}
		break;
		case GQ_PC_DATA_LOAD_REQ:
		{
			CQuerySession::SendGameLoadPcDataReq(pPacket);
		}
		break;
		case GQ_PC_ITEM_LOAD_REQ:
		{
			CQuerySession::SendGameLoadPcItems(pPacket);
		}
			break;
		case GQ_PC_SKILL_LOAD_REQ:
		{
			CQuerySession::SendGameLoadPcSkills(pPacket);
		}
			break;
		case GQ_PC_BUFF_LOAD_REQ:
		{
			CQuerySession::SendGameLoadBuff(pPacket);
		}
			break;
		case GQ_PC_HTB_LOAD_REQ:
		{
			CQuerySession::SendGameLoadPcHTB(pPacket);
		}
			break;
		case GQ_PC_QUEST_ITEM_LOAD_REQ:
		{
			//CQuerySession::SendGameLoadPcDataReq(pPacket);
		}
			break;
		case GQ_PC_QUEST_COMPLETE_LOAD_REQ:
		{
			//CQuerySession::SendGameLoadPcDataReq(pPacket);
		}
			break;
		case GQ_PC_QUEST_PROGRESS_LOAD_REQ:
		{
			//CQuerySession::SendGameLoadPcDataReq(pPacket);
		}
			break;
		case GQ_PC_QUICK_SLOT_LOAD_REQ:
		{
			CQuerySession::SendGameLoadPcQuickSlot(pPacket);
		}
			break;
		case GQ_PC_SHORTCUT_LOAD_REQ:
		{
			//CQuerySession::SendGameLoadPcDataReq(pPacket);
		}
			break;
		case GQ_PC_ITEM_RECIPE_LOAD_REQ:
		{
			//CQuerySession::SendGameLoadPcDataReq(pPacket);
		}
			break;
		case GQ_PC_GMT_LOAD_REQ:
		{
			CQuerySession::SendGameLoadPcGMT(pPacket);
		}
			break;
		case GQ_PC_WAR_FOG_REQ:
		{
			CQuerySession::SendGameLoadWarFog(pPacket);
		}
		break;
		default:
		{
			if (pHeader->wOpCode != SYS_ALIVE)
			{
				NTL_PRINT(PRINT_APP, "The following packet was not handled: %s", NtlGetPacketName(pHeader->wOpCode));
			}
			return CNtlSession::OnDispatch(pPacket);
		}
	}

	return NTL_SUCCESS;
}
////////////////////////////QUERY SERVER HANDLING
BYTE    CQuerySession::SendQueryAddSkill(CHARACTERID charId,BYTE byRpBonus,DWORD dwSkill,DWORD nEXP,DWORD nRemainSec)
{
	int rc = 0;
	BYTE pos = 0;
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	CTableSkills* query = new CTableSkills;
	query->SetCharID(charId);
	query->SetSqlID(SP_SkillsMaxSlotByChar);
	rc = app->GetDatabaseManager()->Query(app->GetHDatabase(), query);
	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "Query Fail %d(%s)", rc, NtlGetErrorMessage(rc));
	}
	else
		app->g_exitEvent.Wait();
	FIND_SQLUNIT(SP_SkillsMaxSlotByChar, app->GetDatabaseManager()->GetDatabaseConnection(app->GetHDatabase()), pSqlUnit2);
	pos = pSqlUnit2->m_bySlot;

	FIND_SQLUNIT(SP_SkillsInsert, app->GetDatabaseManager()->GetDatabaseConnection(app->GetHDatabase()), pSqlUnit);
	if (pSqlUnit)
	{
		pSqlUnit->m_charID = charId;
		pSqlUnit->m_bIsRpBonusAuto = false;
		pSqlUnit->m_byRpBonusType = byRpBonus;
		pSqlUnit->m_dwSkillTblidx = dwSkill;
		pSqlUnit->m_nExp = nEXP;
		pSqlUnit->m_nRemainSec = nRemainSec;
		pSqlUnit->m_bySlot = pos;
		pSqlUnit->Exec();
		pSqlUnit->Close();
		query = NULL;
	}
	return pos;
}
void	CQuerySession::SendQueryCharInfo(ACCOUNTID accID, CHARACTERID id, sPC_DATA* plr, sVECTOR3& vBindLoc, sVECTOR3& vBindDir, BYTE& byBindType)
{
	int rc = 0;
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	CTableCharacters* query = new CTableCharacters();
	query->SetAccountID(accID);
	query->SetCharID(id);
	query->SetSqlID(SP_CharacterSelectByID);
	rc = app->GetDatabaseManager()->Query(app->GetHDatabase(), query);
	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "Query Fail %d(%s)", rc, NtlGetErrorMessage(rc));
	}
	else
		app->g_exitEvent.Wait();
	FIND_SQLUNIT(SP_CharacterSelectByID, app->GetDatabaseManager()->GetDatabaseConnection(app->GetHDatabase()), pSqlUnit);
	if (pSqlUnit)
	{
		pSqlUnit->m_dwCharID = id;
		pSqlUnit->m_dwAccountID = accID;
		size_t convertedChars = 0;
		mbstowcs_s(&convertedChars, plr->awchName, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1, pSqlUnit->m_awchName, _TRUNCATE);
		//memcpy(plr->awchName, pSqlUnit->m_awchName, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1);
		plr->bIsAdult = pSqlUnit->m_bIsAdult;
		plr->byClass = pSqlUnit->m_byClass;
		plr->byFace = pSqlUnit->m_byFace;
		plr->byGender = pSqlUnit->m_byGender;
		plr->byHair = pSqlUnit->m_byHair;
		plr->byHairColor = pSqlUnit->m_byHairColor;
		plr->byLevel = pSqlUnit->m_byLevel;
		plr->bySkinColor = pSqlUnit->m_bySkinColor;
		plr->byBlood = pSqlUnit->m_byBlood;
		plr->byBindType = pSqlUnit->m_byBindType;
		plr->byRace = pSqlUnit->m_byRace;
		plr->charId = pSqlUnit->m_dwCharID;
		plr->fPositionX = pSqlUnit->m_fPositionX;
		plr->fPositionY = pSqlUnit->m_fPositionY;
		plr->fPositionZ = pSqlUnit->m_fPositionZ;
		plr->fDirX = pSqlUnit->m_fDirX;
		plr->fDirY = pSqlUnit->m_fDirY;
		plr->fDirZ = pSqlUnit->m_fDirZ;
		plr->vBindLoc.x = pSqlUnit->m_fBindPositionX;
		plr->vBindLoc.y = pSqlUnit->m_fBindPositionY;
		plr->vBindLoc.z = pSqlUnit->m_fBindPositionZ;
		plr->vBindDir.x = pSqlUnit->m_fBindDirX;
		plr->vBindDir.y = pSqlUnit->m_fBindDirY;
		plr->vBindDir.z = pSqlUnit->m_fBindDirZ;
		plr->bChangeClass = pSqlUnit->m_bChangeClass;
		plr->dwEXP = pSqlUnit->m_dwEXP;
		plr->dwEP = pSqlUnit->m_dwEP;
		plr->dwLP = pSqlUnit->m_dwLP;
		plr->dwEP = pSqlUnit->m_dwEP;
		plr->dwRP = pSqlUnit->m_dwRP;
		plr->dwMudosaPoint = pSqlUnit->m_dwMudosaPoint;
		plr->dwSpPoint = pSqlUnit->m_dwSpPoint;
		plr->dwTutorialHint = pSqlUnit->m_dwTutorialHint;
		plr->bindObjectTblidx = pSqlUnit->m_dwBindObjectTblidx;
		plr->bindWorldId = pSqlUnit->m_dwBindWorldID;
		plr->worldId = pSqlUnit->m_worldId;
		plr->worldTblidx = pSqlUnit->m_worldTblidx;	
		plr->GMAccountID = pSqlUnit->m_GMAcc;
		plr->dwMoney = pSqlUnit->m_dwMoney;
		plr->dwMoneyBank = pSqlUnit->m_dwMoneyBank;
		plr->sLocalize.pcDataCJKor.netP = pSqlUnit->m_dwNetP;
		vBindDir.x = pSqlUnit->m_fBindDirX;
		vBindDir.y = pSqlUnit->m_fBindDirY;
		vBindDir.z = pSqlUnit->m_fBindDirZ;
		vBindLoc.x = pSqlUnit->m_fBindPositionX;
		vBindLoc.y = pSqlUnit->m_fBindPositionY;
		vBindLoc.z = pSqlUnit->m_fBindPositionZ;
		byBindType = pSqlUnit->m_byBindType;

		pSqlUnit->Close();
		query = NULL;
	}
}
//Max item:
BYTE	CQuerySession::SendQueryLoadInventory(ACCOUNTID accId, CHARACTERID id, sITEM_DATA* itDat)
{
	int rc = 0;
	BYTE pos = 0;
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	CTableInventory* query = new CTableInventory();	
	query->SetCharID(id);
	query->SetSqlID(SP_InventorySelect);
	rc = app->GetDatabaseManager()->Query(app->GetHDatabase(), query);
	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "Query Fail %d(%s)", rc, NtlGetErrorMessage(rc));
	}
	else
		app->g_exitEvent.Wait();

	FIND_SQLUNIT(SP_InventorySelect, app->GetDatabaseManager()->GetDatabaseConnection(app->GetHDatabase()), pSqlUnit);
	if (pSqlUnit)
	{
		pSqlUnit->m_charID = id;
		
		do{			
			itDat[pos].aOptionTblidx[0] = pSqlUnit->m_OptionTblidx1;
			itDat[pos].aOptionTblidx[1] = pSqlUnit->m_OptionTblidx2;
			itDat[pos].bNeedToIdentify = pSqlUnit->m_bNeedToIdentify;
			itDat[pos].byBattleAttribute = pSqlUnit->m_byBattleAttribute;
			itDat[pos].byCurrentDurability = pSqlUnit->m_byCurrentDurability;
			itDat[pos].byDurationType = pSqlUnit->m_byDurationType;
			itDat[pos].byGrade = pSqlUnit->m_byGrade;
			itDat[pos].byPlace = pSqlUnit->m_byPlace;
			itDat[pos].byPosition = pSqlUnit->m_byPosition;
			itDat[pos].byRank = pSqlUnit->m_byRank;
			itDat[pos].byRestrictType = pSqlUnit->m_byRestrictType;
			itDat[pos].byStackcount = pSqlUnit->m_byRestrictType;
			itDat[pos].charId = pSqlUnit->m_charID;
			itDat[pos].itemNo = pSqlUnit->m_tblidx;
			itDat[pos].itemId = INVALID_HOBJECT;
			itDat[pos].nUseEndTime = pSqlUnit->m_nUseEndTime;
			itDat[pos].nUseStartTime = pSqlUnit->m_nUseStartTime;
			pos++;
		} while (pSqlUnit->Fetch() != false);
		pSqlUnit->Close();
		query = NULL;
	}
	return pos;
}
//Max Quick Slot
BYTE	CQuerySession::SendQueryQuickSlot(ACCOUNTID accId, CHARACTERID id, sQUICK_SLOT_DATA* qS)
{
	int rc = 0;
	BYTE pos = 0;
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	CTableQuickSlot* query = new CTableQuickSlot();	
	query->SetCharID(id);
	query->SetSqlID(SP_QuickSlotSelect);
	rc = app->GetDatabaseManager()->Query(app->GetHDatabase(), query);
	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "Query Fail %d(%s)", rc, NtlGetErrorMessage(rc));
	}
	else
		app->g_exitEvent.Wait();
	FIND_SQLUNIT(SP_QuickSlotSelect, app->GetDatabaseManager()->GetDatabaseConnection(app->GetHDatabase()), pSqlUnit);
	if (pSqlUnit)
	{
		pSqlUnit->m_charID = id;		
		do{
			qS[pos].bySlot = pSqlUnit->m_bySlot;
			qS[pos].byType = pSqlUnit->m_byType;
			qS[pos].itemID = pSqlUnit->m_dwItemTblidx;
			qS[pos].tblidx = pSqlUnit->m_dwSkillTblidx;
			qS[pos].hItem = INVALID_HOBJECT;
			pos++;
		} while (pSqlUnit->Fetch() != false);
		pSqlUnit->Close();
		query = NULL;
	}
	return pos;
}
BYTE	CQuerySession::SendQuerySkill(ACCOUNTID accId, CHARACTERID id, sSKILL_DATA* sk)
{
	int rc = 0;
	BYTE pos = 0;
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	CTableSkills* query = new CTableSkills();
	query->SetCharID(id);
	query->SetSqlID(SP_SkillsSelectAll);
	rc = app->GetDatabaseManager()->Query(app->GetHDatabase(), query);
	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "Query Fail %d(%s)", rc, NtlGetErrorMessage(rc));
	}
	else
		app->g_exitEvent.Wait();
	FIND_SQLUNIT(SP_SkillsSelectAll, app->GetDatabaseManager()->GetDatabaseConnection(app->GetHDatabase()), pSqlUnit);
	if (pSqlUnit)
	{
		pSqlUnit->m_charID = id;
		do{
			sk[pos].bIsRpBonusAuto = pSqlUnit->m_bIsRpBonusAuto;
			sk[pos].byRpBonusType = pSqlUnit->m_byRpBonusType;
			sk[pos].nExp = pSqlUnit->m_nExp;
			sk[pos].nRemainSec = pSqlUnit->m_nRemainSec;
			sk[pos].bySlot = pSqlUnit->m_bySlot;
			sk[pos].skillId = pSqlUnit->m_dwSkillTblidx;
			pos++;
		} while (pSqlUnit->Fetch() != false);
		pSqlUnit->Close();
		query = NULL;
	}
	return pos;
}
BYTE	CQuerySession::SendQueryHTBSkill(ACCOUNTID accId, CHARACTERID id, sHTB_SKILL_DATA* ht)
{
	int rc = 0;
	BYTE pos = 0;
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	CTableHTBList* query = new CTableHTBList();
	query->SetCharID(id);
	query->SetSqlID(SP_HTBSkillSelect);
	rc = app->GetDatabaseManager()->Query(app->GetHDatabase(), query);
	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "Query Fail %d(%s)", rc, NtlGetErrorMessage(rc));
	}
	else
		app->g_exitEvent.Wait();
	FIND_SQLUNIT(SP_HTBSkillSelect, app->GetDatabaseManager()->GetDatabaseConnection(app->GetHDatabase()), pSqlUnit);
	if (pSqlUnit)
	{
		pSqlUnit->m_charId = id;
		if (pSqlUnit->m_skillId != 0 )
		{
			do{
				ht[pos].nSkillTime = pSqlUnit->m_dwTimeRemaining;
				ht[pos].skillId = pSqlUnit->m_skillId;
				pos++;
			} while (pSqlUnit->Fetch() != false);
		}
		pSqlUnit->Close();
		query = NULL;
	}
	return pos;
}
BYTE	CQuerySession::SendQueryBuffSkill(ACCOUNTID accId, CHARACTERID id, sBUFF_DATA* bfDt)
{
	int rc = 0;
	BYTE pos = 0;
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	CTableBuffList* query = new CTableBuffList();
	query->SetCharID(id);
	query->SetSqlID(SP_BuffListSelect);
	rc = app->GetDatabaseManager()->Query(app->GetHDatabase(), query);
	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "Query Fail %d(%s)", rc, NtlGetErrorMessage(rc));
	}
	else
		app->g_exitEvent.Wait();
	FIND_SQLUNIT(SP_BuffListSelect, app->GetDatabaseManager()->GetDatabaseConnection(app->GetHDatabase()), pSqlUnit);
	if (pSqlUnit)
	{
		pSqlUnit->m_charID = id;
		if (pSqlUnit->m_dwTblidx != 0)
		{
			do{
				bfDt[pos].byBuffGroup = pSqlUnit->m_byBuffGroup;
				bfDt[pos].bySourceType = pSqlUnit->m_bySourceType;
				bfDt[pos].dwInitialDuration = pSqlUnit->m_dwInitialDuration;
				bfDt[pos].dwTimeRemaining = pSqlUnit->m_dwTimeRemaining;
				bfDt[pos].fEffectValue1 = pSqlUnit->m_afEffectValue0;
				bfDt[pos].fEffectValue2 = pSqlUnit->m_afEffectValue1;
				bfDt[pos].sourceTblidx = pSqlUnit->m_dwTblidx;
				pos++;
			} while (pSqlUnit->Fetch() != false);
		}
		pSqlUnit->Close();
		query = NULL;
	}
	return pos;
}
void	CQuerySession::SendQueryLoadBank(ACCOUNTID accId, CHARACTERID id, DWORD* dwZenny, DWORD* dwBankZenny)
{
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
}

void	CQuerySession::SendQueryWarFogInfo(CHARACTERID id, sCHAR_WAR_FOG_FLAG* fl)
{
	int rc = 0;
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	CTableWarFog* query = new CTableWarFog();
	query->SetCharID(id);
	query->SetSqlID(SP_WarFogSelect);
	rc = app->GetDatabaseManager()->Query(app->GetHDatabase(), query);
	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "Query Fail %d(%s)", rc, NtlGetErrorMessage(rc));
	}
	else
		app->g_exitEvent.Wait();
	FIND_SQLUNIT(SP_WarFogSelect, app->GetDatabaseManager()->GetDatabaseConnection(app->GetHDatabase()), pSqlUnit);
	if (pSqlUnit)
	{
		pSqlUnit->m_charId = id;
		memcpy(fl->achWarFogFlag, pSqlUnit->m_achWarFogFlag, NTL_MAX_SIZE_WAR_FOG);
		pSqlUnit->Close();
		query = NULL;
	}
}

/////AUTH SERVER HANDLING
void CQuerySession::SendAuthLoginReq(CNtlPacket* pPacket)
{
	int rc = NTL_SUCCESS;
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	sAQ_LOGIN_REQ* req = (sAQ_LOGIN_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sQA_LOGIN_RES));
	sQA_LOGIN_RES* res = (sQA_LOGIN_RES*)packet.GetPacketData();

	CTableAccount* query = new CTableAccount();
	query->SetUsername((CHAR*)req->awchUserId);
	query->SetPassword((CHAR*)req->awchPasswd);
	query->SetSqlID(SP_AccountSelect);
	rc = app->GetDatabaseManager()->Query(app->GetHDatabase(), query);
	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "Query Fail %d(%s)", rc, NtlGetErrorMessage(rc));
		res->wResultCode = AUTH_DB_FAIL;
	}
	else
		app->g_exitEvent.Wait();

	//Get Results
	FIND_SQLUNIT(SP_AccountSelect, app->GetDatabaseManager()->GetDatabaseConnection(app->GetHDatabase()), pSqlUnit);
	if (NULL == pSqlUnit)
	{
		res->wResultCode = AUTH_DB_FAIL;
	}
	else
	{
		res->accountId = pSqlUnit->m_dwAccountID;
		if (res->accountId)
		{
			size_t convertedChars = 0;
			WCHAR tmpPass[NTL_MAX_SIZE_USERPW_UNICODE + 1];
			mbstowcs_s(&convertedChars, tmpPass, NTL_MAX_SIZE_USERPW_UNICODE + 1, pSqlUnit->m_szUserPW, _TRUNCATE);
			//wcsmp will compare a string with other string, if they have some diff, he will return other number > 0
			if (wcscmp(req->awchPasswd, tmpPass) == NTL_SUCCESS)
			{
				convertedChars = 0;
				mbstowcs_s(&convertedChars, res->awchUserId, NTL_MAX_SIZE_USERID_UNICODE + 1, pSqlUnit->m_szUserID, _TRUNCATE);
				res->lastServerFarmId = pSqlUnit->m_dwLastFarmID;
				res->wResultCode = AUTH_SUCCESS;
			}
			else
			{
				res->wResultCode = AUTH_WRONG_PASSWORD;
			}
		}
		else
			res->wResultCode = AUTH_USER_NOT_FOUND;

		pSqlUnit->Close();
	}	
	res->wOpCode = QA_LOGIN_RES;
	packet.SetPacketLen(sizeof(sQA_LOGIN_RES));	
	g_pApp->Send(app->GetAuthSession()->GetHandle(), &packet);	
}
/////CHAR SERVER HANDLING
void CQuerySession::SendCharNotifyServerReq(CNtlPacket* pPacket)
{
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	sCQ_NOTIFY_SERVER_BEGIN* req = (sCQ_NOTIFY_SERVER_BEGIN*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sQC_NOTIFY_SERVER_BEGIN_ACK));
	sQC_NOTIFY_SERVER_BEGIN_ACK* res = (sQC_NOTIFY_SERVER_BEGIN_ACK*)packet.GetPacketData();
	res->serverFarmId = req->byServerIndex;
	res->wOpCode = QC_NOTIFY_SERVER_BEGIN_ACK;
	packet.SetPacketLen(sizeof(sQC_NOTIFY_SERVER_BEGIN_ACK));
	g_pApp->Send(app->GetCharSession()->GetHandle(), &packet);
}
void CQuerySession::SendCharLoadReq(CNtlPacket* pPacket)
{
	int rc = NTL_SUCCESS;
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	sCQ_CHARACTER_LOAD_REQ* req = (sCQ_CHARACTER_LOAD_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sQC_CHARACTER_LOAD_RES));
	sQC_CHARACTER_LOAD_RES* res = (sQC_CHARACTER_LOAD_RES*)packet.GetPacketData();

	CTableCharacters* query = new CTableCharacters();
	query->SetAccountID(req->accountId);
	query->SetServerID(req->serverFarmId);
	query->SetSqlID(SP_CharacterSelect);	
	rc = app->GetDatabaseManager()->Query(app->GetHDatabase(), query);
	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "Query Fail %d(%s)", rc, NtlGetErrorMessage(rc));
		res->wResultCode = CHARACTER_DB_QUERY_FAIL;
	}
	else
		app->g_exitEvent.Wait();
	query = NULL;
	//Get Results
	FIND_SQLUNIT(SP_CharacterSelect, app->GetDatabaseManager()->GetDatabaseConnection(app->GetHDatabase()), pSqlUnit);
	if (NULL == pSqlUnit)
	{
		res->wResultCode = CHARACTER_DB_QUERY_FAIL;
	}
	else
	{
		res->accountId = req->accountId;
		res->serverFarmId = req->serverFarmId;

		pSqlUnit->m_dwAccountID = res->accountId;
		pSqlUnit->m_dwServerID = res->serverFarmId;
		sPC_SUMMARY m_asPcSummary[NTL_MAX_COUNT_USER_CHAR_SLOT];//WorkAround
		sDELETE_WAIT_CHARACTER_INFO m_asDelData;
		m_asDelData.charId = INVALID_CHARACTERID;		
		int pos = 0;
		do{
			app->ConvertStructTODB(pSqlUnit, &m_asPcSummary[pos]);
			CTableInventory* inventory = new CTableInventory();
			inventory->SetCharID(m_asPcSummary[pos].charId);
			inventory->SetSqlID(SP_InventorySelect);
			rc = app->GetDatabaseManager()->Query(app->GetHDatabase(), inventory);
			if (NTL_SUCCESS != rc)
			{
				NTL_PRINT(PRINT_APP, "Query Fail %d(%s)", rc, NtlGetErrorMessage(rc));
				res->wResultCode = CHARACTER_DB_QUERY_FAIL;
			}
			else
				app->g_exitEvent.Wait();
			FIND_SQLUNIT(SP_InventorySelect, app->GetDatabaseManager()->GetDatabaseConnection(app->GetHDatabase()), pSqlUnit2);
			if (NULL == pSqlUnit2)
			{
				res->wResultCode = CHARACTER_DB_QUERY_FAIL;
			}
			else
			{					
				app->ConvertDBInventoryToSummary((int*)m_asPcSummary[pos].charId, pSqlUnit2, m_asPcSummary[pos].sItem);
				pSqlUnit2->Close();
			}			
			inventory = NULL;
			pos++;
		} while (pSqlUnit->Fetch() != false);
		if (res->accountId != 0)
		{
			int i, p;
			BYTE charCount = 0;
			BYTE charDelete = 0;
			for (i = 0; i < NTL_MAX_COUNT_USER_CHAR_SLOT; i++)
			{
				if ((m_asPcSummary[i].charId != INVALID_CHARACTERID) && ((m_asPcSummary[i].charId != 0) && (m_asPcSummary[i].charId != 3435973836)))
				{
					memcpy(res->sPcData[i].awchName, m_asPcSummary[i].awchName, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1);
					res->sPcData[i].bIsAdult = m_asPcSummary[i].bIsAdult;
					res->sPcData[i].bNeedNameChange = m_asPcSummary[i].bNeedNameChange;
					res->sPcData[i].bTutorialFlag = m_asPcSummary[i].bTutorialFlag;
					res->sPcData[i].byClass = m_asPcSummary[i].byClass;
					res->sPcData[i].byFace = m_asPcSummary[i].byFace;
					res->sPcData[i].byGender = m_asPcSummary[i].byGender;
					res->sPcData[i].byHair = m_asPcSummary[i].byHair;
					res->sPcData[i].byHairColor = m_asPcSummary[i].byHairColor;
					res->sPcData[i].byLevel = m_asPcSummary[i].byLevel;
					res->sPcData[i].byRace = m_asPcSummary[i].byRace;
					res->sPcData[i].bySkinColor = m_asPcSummary[i].bySkinColor;
					res->sPcData[i].worldId = m_asPcSummary[i].worldId;					
					res->sPcData[i].worldTblidx = m_asPcSummary[i].worldTblidx;
					res->sPcData[i].dwMoney = m_asPcSummary[i].dwMoney;
					res->sPcData[i].dwMoneyBank = m_asPcSummary[i].dwMoneyBank;
					res->sPcData[i].dwMapInfoIndex = m_asPcSummary[i].dwMapInfoIndex;
					res->sPcData[i].charId = m_asPcSummary[i].charId;
					for (int p = 0; p < EQUIP_SLOT_TYPE_COUNT; p++)
					{
						res->sPcData[i].sItem[p].aOptionTblidx[0] = m_asPcSummary[i].sItem[p].aOptionTblidx[0];
						res->sPcData[i].sItem[p].aOptionTblidx[1] = m_asPcSummary[i].sItem[p].aOptionTblidx[1];
						res->sPcData[i].sItem[p].byBattleAttribute = m_asPcSummary[i].sItem[p].byBattleAttribute;
						res->sPcData[i].sItem[p].byGrade = m_asPcSummary[i].sItem[p].byGrade;
						res->sPcData[i].sItem[p].byRank = m_asPcSummary[i].sItem[p].byRank;
						res->sPcData[i].sItem[p].tblidx = m_asPcSummary[i].sItem[p].tblidx;
					}
					charCount++;
				}
			}
			for (p = 0; p < NTL_MAX_COUNT_USER_CHAR_SLOT; p++)
			{
				if ((m_asDelData.charId != INVALID_CHARACTERID) && ((m_asDelData.charId != 0)))
				{
					res->sDelData[p] = m_asDelData;
					charDelete++;
				}
			}
			res->byCharCount = charCount;
			res->byDelCount = charDelete;
			res->wResultCode = CHARACTER_SUCCESS;
		}
		else
		{
			res->wResultCode = CHARACTER_FAIL;
		}
		pSqlUnit->Close();
	}
	res->wOpCode = QC_CHARACTER_LOAD_RES;
	packet.SetPacketLen(sizeof(sQC_CHARACTER_LOAD_RES));
	g_pApp->Send(app->GetCharSession()->GetHandle(), &packet);
}

void CQuerySession::SendCharAddReq(CNtlPacket* pPacket)
{
	int rc = NTL_SUCCESS;
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	sCQ_CHARACTER_ADD_REQ* req = (sCQ_CHARACTER_ADD_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sQC_CHARACTER_ADD_RES));
	sQC_CHARACTER_ADD_RES* res = (sQC_CHARACTER_ADD_RES*)packet.GetPacketData();
	//Retrieve the others characters from account
	CTableCharacters* query = new CTableCharacters();
	query->SetAccountID(req->accountId);
	query->SetServerID(req->serverFarmId);
	query->SetSqlID(SP_CharacterSelect);
	rc = app->GetDatabaseManager()->Query(app->GetHDatabase(), query);
	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "Query Fail %d(%s)", rc, NtlGetErrorMessage(rc));
		res->wResultCode = CHARACTER_DB_QUERY_FAIL;
	}
	else 
		app->g_exitEvent.Wait();
	query = NULL;
	query = new CTableCharacters;
	//Get Results
	FIND_SQLUNIT(SP_CharacterSelect, app->GetDatabaseManager()->GetDatabaseConnection(app->GetHDatabase()), pSqlUnit);
	if (NULL == pSqlUnit)
	{
		res->wResultCode = CHARACTER_DB_QUERY_FAIL;
	}
	else
	{
		query->SetSqlID(SP_CharacterMaxID);
		rc = app->GetDatabaseManager()->Query(app->GetHDatabase(), query);
		if (NTL_SUCCESS != rc)
		{
			NTL_PRINT(PRINT_APP, "Query Fail %d(%s)", rc, NtlGetErrorMessage(rc));
			res->wResultCode = CHARACTER_DB_QUERY_FAIL;
		}
		else
			app->g_exitEvent.Wait();
		query = NULL;
		FIND_SQLUNIT(SP_CharacterMaxID, app->GetDatabaseManager()->GetDatabaseConnection(app->GetHDatabase()), pSqlUnit5);
		sITEM_SUMMARY tmpItem[EQUIP_SLOT_TYPE_COUNT];
		for (int i = 0; i < NTL_MAX_NEWBIE_ITEM; i++)
		{
			if ((req->sItem[i].itemId != INVALID_TBLIDX) && (req->sItem[i].byPlace != 255) && 
				(req->sItem[i].byStackcount < 2) && (req->sItem[i].byRank < 2) &&
				(req->sItem[i].bNeedToIdentify == 0))
			{
				tmpItem[i].aOptionTblidx[0] = req->sItem[i].aOptionTblidx[0];
				tmpItem[i].aOptionTblidx[1] = req->sItem[i].aOptionTblidx[1];
				tmpItem[i].byBattleAttribute = req->sItem[i].byBattleAttribute;
				tmpItem[i].byGrade = req->sItem[i].byGrade;
				tmpItem[i].byRank = ITEM_RANK_NORMAL;
				tmpItem[i].tblidx = req->sItem[i].itemId;
			}
		}		
		//If he finds the account the we are going to use the avaible objects from database
		if (pSqlUnit->m_dwAccountID != 0)
		{			
			sPC_SUMMARY m_asPcSummary[NTL_MAX_COUNT_USER_CHAR_SLOT];//WorkAround	
			int pos = 0;
			do{
				app->ConvertStructTODB(pSqlUnit, &m_asPcSummary[pos]);
				pos++;
			} while (pSqlUnit->Fetch() != false);
			int positionCreated = app->UpdateChar(m_asPcSummary,
							pSqlUnit5->m_MaxCharID,
							req->awchCharName,
							req->byRace,
							req->byClass,
							req->byGender,
							req->byFace,
							req->byHair,
							req->byHairColor,
							req->bySkinColor,
							req->worldTblix,
							req->worldID,
							req->vSpawn_Loc.x,
							req->vSpawn_Loc.y,
							req->vSpawn_Loc.z,
							tmpItem,
							req->mapNameTblidx);			
			FIND_SQLUNIT(SP_CharacterCreate, app->GetDatabaseManager()->GetDatabaseConnection(app->GetHDatabase()), pSqlUnit3);
			if (NULL == pSqlUnit3)
			{
				res->wResultCode = CHARACTER_DB_QUERY_FAIL;
			}
			else
			{
				pSqlUnit3->m_dwAccountID = req->accountId;
				pSqlUnit3->m_dwServerID = req->serverFarmId;
				app->ConvertStructTODB(&m_asPcSummary[pos], 
									   pSqlUnit3,
									   req->byBlood,
									   req->byBindType,
									   req->vSpawn_Dir.x,
									   req->vSpawn_Dir.y,
									   req->vSpawn_Dir.z,
									   req->vBind_Loc.x,
									   req->vBind_Loc.y,
									   req->vBind_Loc.z,
									   req->vBind_Dir.x,
									   req->vBind_Dir.y,
									   req->vBind_Dir.z,
									   req->bindWorldId,
									   req->bindWorldId,
									   100,
									   100,
									   100);
				SQLLEN nRowCount = 0;
				if (pSqlUnit3->Exec(&nRowCount))
				{
					FIND_SQLUNIT(SP_InventoryCreate, app->GetDatabaseManager()->GetDatabaseConnection(app->GetHDatabase()), pSqlUnit9);
					if (NULL == pSqlUnit9)
					{
						res->wResultCode = CHARACTER_DB_QUERY_FAIL;
					}
					else
					{
						app->ConvertStructInvTODB((int*)m_asPcSummary[pos].charId, pSqlUnit9, req->sItem);
						
					}
					FIND_SQLUNIT(SP_SkillsInsert, app->GetDatabaseManager()->GetDatabaseConnection(app->GetHDatabase()), pSqlUnit10);
					if (NULL == pSqlUnit10)
					{
						res->wResultCode = CHARACTER_DB_QUERY_FAIL;
					}
					else
					{
						for (int i = 0; i < NTL_MAX_NEWBIE_SKILL; i++)
						{
							if (req->aSkill[i] != INVALID_TBLIDX)
							{
								pSqlUnit10->m_dwSkillTblidx = req->aSkill[i];
								pSqlUnit10->m_charID = m_asPcSummary[pos].charId;
								pSqlUnit10->m_bySlot = i;
								pSqlUnit10->m_nExp = 0;
								pSqlUnit10->m_nRemainSec = 0;
								pSqlUnit10->m_bIsRpBonusAuto = false;
								pSqlUnit10->m_byRpBonusType = 0;//data needs be updated when send in avatar info
								pSqlUnit10->Exec();
							}
						}
					}
					if (res->wResultCode != CHARACTER_DB_QUERY_FAIL)
					{
						res->sPcDataSummary = m_asPcSummary[pos];
						res->wResultCode = CHARACTER_SUCCESS;
					}
				}
				else
				{
					res->wResultCode = CHARACTER_DB_QUERY_FAIL;
				}
				pSqlUnit3->Close();
			}
		}
		//Else, then no problem, we create to use it later
		else
		{
			sPC_SUMMARY tmp;
			app->ConvertDBToStruct(&tmp,
								pSqlUnit5->m_MaxCharID,
								req->awchCharName,
								req->byRace,
								req->byClass,
								false,
								req->byGender,
								req->byFace,
								req->byHair,
								req->byHairColor,
								req->bySkinColor,
								1,
								req->worldTblix,
								req->worldID,
								req->vSpawn_Loc.x,
								req->vSpawn_Loc.y,
								req->vSpawn_Loc.z,
								10000,
								500,
								req->mapNameTblidx,
								false,
								false);
			app->ConvertInventoryTMPToInventory(tmpItem, tmp.sItem);
			FIND_SQLUNIT(SP_CharacterCreate, app->GetDatabaseManager()->GetDatabaseConnection(app->GetHDatabase()), pSqlUnit2);
			if (NULL == pSqlUnit2)
			{
				res->wResultCode = CHARACTER_DB_QUERY_FAIL;
			}
			else
			{
				pSqlUnit2->m_dwAccountID = req->accountId;
				pSqlUnit2->m_dwServerID = req->serverFarmId;
				app->ConvertStructTODB(&tmp, 
									   pSqlUnit2,
									   req->byBlood,
									   req->byBindType,
									   req->vSpawn_Dir.x,
									   req->vSpawn_Dir.y,
									   req->vSpawn_Dir.z,
									   req->vBind_Loc.x,
									   req->vBind_Loc.y,
									   req->vBind_Loc.z,
									   req->vBind_Dir.x,
									   req->vBind_Dir.y,
									   req->vBind_Dir.z,
									   req->bindWorldId,
									   req->bindWorldId,
									   100,
									   100,
									   100);
				SQLLEN nRowCount = 0;
				if (pSqlUnit2->Exec(&nRowCount))
				{			
					FIND_SQLUNIT(SP_InventoryCreate, app->GetDatabaseManager()->GetDatabaseConnection(app->GetHDatabase()), pSqlUnit9);
					if (NULL == pSqlUnit9)
					{
						res->wResultCode = CHARACTER_DB_QUERY_FAIL;
					}
					else
					{
						app->ConvertStructInvTODB((int*)tmp.charId, pSqlUnit9, req->sItem);
					}
					FIND_SQLUNIT(SP_SkillsInsert, app->GetDatabaseManager()->GetDatabaseConnection(app->GetHDatabase()), pSqlUnit10);
					if (NULL == pSqlUnit10)
					{
						res->wResultCode = CHARACTER_DB_QUERY_FAIL;
					}
					else
					{
						for (int i = 0; i < NTL_MAX_NEWBIE_SKILL; i++)
						{
							if (req->aSkill[i] != INVALID_TBLIDX)
							{
								pSqlUnit10->m_dwSkillTblidx =  req->aSkill[i];
								pSqlUnit10->m_bySlot = i;
								pSqlUnit10->m_nExp = 0;
								pSqlUnit10->m_nRemainSec = 0;
								pSqlUnit10->m_bIsRpBonusAuto = false;
								pSqlUnit10->m_byRpBonusType = 0;//data needs be updated when send in avatar info
								pSqlUnit10->m_charID = tmp.charId;
								pSqlUnit10->Exec();
							}
						}
					}
					if (res->wResultCode != CHARACTER_DB_QUERY_FAIL)
					{
						res->sPcDataSummary = tmp;
						res->wResultCode = CHARACTER_SUCCESS;
					}
				}
				else
				{
					res->wResultCode = CHARACTER_DB_QUERY_FAIL;
				}
				pSqlUnit2->Close();
			}			
		}
		pSqlUnit->Close();
		pSqlUnit5->Close();		 
	}	
	res->accountId = req->accountId;
	res->wOpCode = QC_CHARACTER_ADD_RES;
	packet.SetPacketLen(sizeof(sQC_CHARACTER_ADD_RES));
	g_pApp->Send(app->GetCharSession()->GetHandle(), &packet);
}

void CQuerySession::SendCharConnectCheck(CNtlPacket* pPacket)
{
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	sCQ_CONNECT_WAIT_CHECK_REQ* req = (sCQ_CONNECT_WAIT_CHECK_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sQC_CONNECT_WAIT_CHECK_RES));
	sQC_CONNECT_WAIT_CHECK_RES* res = (sQC_CONNECT_WAIT_CHECK_RES*)packet.GetPacketData();
	res->accountId = req->accountId;
	res->byChannelIdx = req->byChannelIdx;
	res->wOpCode = QC_CONNECT_WAIT_CHECK_RES;
	res->wResultCode = CHARACTER_SUCCESS;
	packet.SetPacketLen(sizeof(sQC_CONNECT_WAIT_CHECK_RES));
	g_pApp->Send(app->GetCharSession()->GetHandle(), &packet);
}
void CQuerySession::SendCharConnectCheckCancel(CNtlPacket* pPacket)
{
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	sCQ_CONNECT_WAIT_CANCEL_REQ* req = (sCQ_CONNECT_WAIT_CANCEL_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sQC_CONNECT_WAIT_CANCEL_RES));
	sQC_CONNECT_WAIT_CANCEL_RES* res = (sQC_CONNECT_WAIT_CANCEL_RES*)packet.GetPacketData();
	res->accountId = req->accountId;
	res->wOpCode = QC_CONNECT_WAIT_CANCEL_RES;
	res->wResultCode = CHARACTER_SUCCESS;
	packet.SetPacketLen(sizeof(sQC_CONNECT_WAIT_CANCEL_RES));
	g_pApp->Send(app->GetCharSession()->GetHandle(), &packet);
}
void CQuerySession::SendCharDisconnect(CNtlPacket* pPacket)
{
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	sCQ_CHARACTER_EXIT* req = (sCQ_CHARACTER_EXIT*)pPacket->GetPacketData();
	CTableCharacters* query = new CTableCharacters();
	query->SetAccountID(req->accountId);
	query->SetIsOnline(req->bIsGameMove);
	query->SetServerID(req->serverFarmId);
	query->SetSqlID(SP_CharacterOnline);
	int rc = app->GetDatabaseManager()->Query(app->GetHDatabase(), query);
	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "Query Fail %d(%s)", rc, NtlGetErrorMessage(rc));
	}
	else
		app->g_exitEvent.Wait();
	query = NULL;
	if (!req->bIsGameMove)
	{
		CNtlPacket packetA(sizeof(sQA_CHARACTER_EXIT));		
		sQA_CHARACTER_EXIT* auth = (sQA_CHARACTER_EXIT*)packetA.GetPacketData();
		auth->accountId = req->accountId;
		auth->wOpCode = QA_CHARACTER_EXIT;
		packetA.SetPacketLen(sizeof(sQA_CHARACTER_EXIT));
		Sleep(2500);//Just to get the time for client back to other screen(auth screen);
		app->GetAuthSession()->PushPacket(&packetA);		
	}
	else
	{
		CNtlPacket packetG(sizeof(sQG_NOTIFY_CAN_LOAD_CHAR));
		sQG_NOTIFY_CAN_LOAD_CHAR* game = (sQG_NOTIFY_CAN_LOAD_CHAR*)packetG.GetPacketData();
		game->charID = req->charId;
		game->accountID = req->charId;
		game->wOpCode = QG_NOTIFY_CAN_LOAD_CHAR;
		packetG.SetPacketLen(sizeof(sQG_NOTIFY_CAN_LOAD_CHAR));
		app->GetGameSession()->PushPacket(&packetG);
	}
}
/////////////////////////////////GAME SERVER HANDLING
void CQuerySession::SendGameLoadPcDataReq(CNtlPacket* pPacket)
{
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	sGQ_PC_DATA_LOAD_REQ* req = (sGQ_PC_DATA_LOAD_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sQG_PC_DATA_LOAD_RES));
	sQG_PC_DATA_LOAD_RES* res = (sQG_PC_DATA_LOAD_RES*)packet.GetPacketData();
	res->accountId = req->accountId;
	res->charId = req->charId;
	sVECTOR3 vBindLoc;
	sVECTOR3 vBindDir;
	BYTE byBindType;
	SendQueryCharInfo(req->accountId, req->charId, &res->sPcData, vBindLoc, vBindDir, byBindType);
	res->fBindDirX = vBindDir.x;
	res->fBindDirY = vBindDir.y;
	res->fBindDirZ = vBindDir.z;
	res->fBindLocX = vBindLoc.x;
	res->fBindLocY = vBindLoc.y;
	res->fBindLocZ = vBindLoc.z;
	res->byBindType = byBindType;
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = QG_PC_DATA_LOAD_RES;	
	packet.AdjustPacketLen(sizeof(sNTLPACKETHEADER) + sizeof(sPC_DATA));
	g_pApp->Send(app->GetGameSession()->GetHandle(), &packet);	
}

void CQuerySession::SendGameLoadPcItems(CNtlPacket* pPacket)
{
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	sGQ_PC_ITEM_LOAD_REQ* req = (sGQ_PC_ITEM_LOAD_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sQG_PC_ITEM_LOAD_RES));
	sQG_PC_ITEM_LOAD_RES* res = (sQG_PC_ITEM_LOAD_RES*)packet.GetPacketData();
	res->accountId = req->accountId;
	res->charId = req->charId;
	res->byItemCount = SendQueryLoadInventory(req->accountId, req->charId, res->asItemData);
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = QG_PC_ITEM_LOAD_RES;
	packet.AdjustPacketLen(sizeof(sNTLPACKETHEADER)*(res->byItemCount * sizeof(sITEM_DATA)));
	packet.IsValidPacket();
	g_pApp->Send(app->GetGameSession()->GetHandle(), &packet);
}

void CQuerySession::SendGameLoadPcSkills(CNtlPacket* pPacket)
{
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	sGQ_PC_SKILL_LOAD_REQ* req = (sGQ_PC_SKILL_LOAD_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sQG_PC_SKILL_LOAD_RES));
	sQG_PC_SKILL_LOAD_RES* res = (sQG_PC_SKILL_LOAD_RES*)packet.GetPacketData();
	res->accountId = req->accountId;
	res->charId = req->charId;
	res->bySkillCount = SendQuerySkill(req->accountId, req->charId, res->asSkill);
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = QG_PC_SKILL_LOAD_RES;
	packet.AdjustPacketLen(sizeof(sNTLPACKETHEADER) + (res->bySkillCount * sizeof(sSKILL_DATA)));
	packet.IsValidPacket();
	g_pApp->Send(app->GetGameSession()->GetHandle(), &packet);
}

void CQuerySession::SendGameLoadPcHTB(CNtlPacket* pPacket)
{
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	sGQ_PC_HTB_LOAD_REQ* req = (sGQ_PC_HTB_LOAD_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sQG_PC_HTB_LOAD_RES));
	sQG_PC_HTB_LOAD_RES* res = (sQG_PC_HTB_LOAD_RES*)packet.GetPacketData();
	res->accountId = req->accountId;
	res->charId = req->charId;
	res->byHTBSkillCount = SendQueryHTBSkill(req->accountId, req->charId, res->asHTBSkill);
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = QG_PC_HTB_LOAD_RES;
	//packet.AdjustPacketLen(sizeof(sNTLPACKETHEADER) + (res->byHTBSkillCount * sizeof(sHTB_SKILL_DATA)));
	packet.SetPacketLen(sizeof(sQG_PC_HTB_LOAD_RES));
	packet.IsValidPacket();
	g_pApp->Send(app->GetGameSession()->GetHandle(), &packet);
}

void CQuerySession::SendGameLoadPcQuickSlot(CNtlPacket* pPacket)
{
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	sGQ_PC_QUICK_SLOT_LOAD_REQ* req = (sGQ_PC_QUICK_SLOT_LOAD_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sQG_PC_QUICK_SLOT_LOAD_RES));
	sQG_PC_QUICK_SLOT_LOAD_RES* res = (sQG_PC_QUICK_SLOT_LOAD_RES*)packet.GetPacketData();
	res->accountId = req->accountId;
	res->charId = req->charId;
	res->byQuickSlotCount = SendQueryQuickSlot(req->accountId, req->charId, res->asQuickSlotData);
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = QG_PC_QUICK_SLOT_LOAD_RES;
	packet.AdjustPacketLen(sizeof(sNTLPACKETHEADER) + (res->byQuickSlotCount * sizeof(sQUICK_SLOT_DATA)));
	packet.IsValidPacket();
	g_pApp->Send(app->GetGameSession()->GetHandle(), &packet);
}

void CQuerySession::SendGameLoadPcGMT(CNtlPacket* pPacket)
{
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	sGQ_PC_GMT_LOAD_REQ* req = (sGQ_PC_GMT_LOAD_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sQG_PC_GMT_LOAD_RES));
	sQG_PC_GMT_LOAD_RES* res = (sQG_PC_GMT_LOAD_RES*)packet.GetPacketData();
	res->accountId = req->accountId;
	res->charId = req->charId;
	res->sGMT_Current.dwSetTime = 0;
	res->sGMT_Next.dwSetTime = 0;
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = QG_PC_GMT_LOAD_RES;	
	packet.SetPacketLen(sizeof(sQG_PC_GMT_LOAD_RES));
	g_pApp->Send(app->GetGameSession()->GetHandle(), &packet);
}

void CQuerySession::SendGameLoadBuff(CNtlPacket* pPacket)
{
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	sGQ_PC_BUFF_LOAD_REQ* req = (sGQ_PC_BUFF_LOAD_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sQG_PC_BUFF_LOAD_RES));
	sQG_PC_BUFF_LOAD_RES* res = (sQG_PC_BUFF_LOAD_RES*)packet.GetPacketData();
	res->accountId = req->accountId;
	res->charId = req->charId;
	res->byBuffCount = SendQueryBuffSkill(req->accountId, req->charId, res->asBuff);
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = QG_PC_BUFF_LOAD_RES;
	packet.SetPacketLen(sizeof(sQG_PC_BUFF_LOAD_RES));
	g_pApp->Send(app->GetGameSession()->GetHandle(), &packet);
}

void CQuerySession::SendGameLoadWarFog(CNtlPacket* pPacket)
{
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	sGQ_PC_WAR_FOG_REQ* req = (sGQ_PC_WAR_FOG_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sQG_PC_WAR_FOG_RES));
	sQG_PC_WAR_FOG_RES* res = (sQG_PC_WAR_FOG_RES*)packet.GetPacketData();
	res->accountId = req->accountId;
	res->charId = req->charId;
	SendQueryWarFogInfo(req->charId, &res->sWarFogInfo);
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = QG_PC_WAR_FOG_RES;
	packet.SetPacketLen(sizeof(sQG_PC_WAR_FOG_RES));
	g_pApp->Send(app->GetGameSession()->GetHandle(), &packet);
}

void CQuerySession::SendGameSkillLearn(CNtlPacket* pPacket)
{
	CQueryServer * app = (CQueryServer*)NtlSfxGetApp();
	sGQ_SKILL_ADD_REQ* req = (sGQ_SKILL_ADD_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sQG_SKILL_ADD_RES));
	sQG_SKILL_ADD_RES* res = (sQG_SKILL_ADD_RES*)packet.GetPacketData();
	res->charId = req->charId;
	res->bySlot = SendQueryAddSkill(req->charId, req->byRpBonusType, req->skillId, req->dwNExp, req->dwRemainSec);;
	res->dwSP = req->dwSP;
	res->dwZenny = req->dwZenny;
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = QG_SKILL_ADD_RES;
	res->skillId = req->skillId;
	packet.SetPacketLen(sizeof(sQG_SKILL_ADD_RES));
	g_pApp->Send(app->GetGameSession()->GetHandle(), &packet);
}