#pragma once
#include "QueryServer.h"
#include "NtlQuery.h"
class CTableSkills : public CNtlQuery
{
private:
	SQL_ID  eSqlID;
	DWORD charID;
	BYTE		m_bySlot;
	DWORD		m_dwSkillTblidx;
	DWORD		m_nRemainSec;
	DWORD		m_nExp;
	BYTE		m_byRpBonusType;
	int			m_bIsRpBonusAuto;
public:
	CTableSkills();
	~CTableSkills();
	int ExecuteQuery(CNtlDatabaseConnection * pConnection);
	int ExecuteResult();
	void    SetSqlID(SQL_ID ID_SQL) { this->eSqlID = ID_SQL; }
	void	SetCharID(DWORD charId) { this->charID = charId; }
};

