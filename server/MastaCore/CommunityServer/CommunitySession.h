#pragma once

#include "CommunityServer.h"
#include "NtlSession.h"
#include "NtlPacketEncoder_RandKey.h"
///////////////////////////////////////RESPONSE PACKETS///////////////////////////
//Community -> Game Server
#include "NtlPacketTG.h"
//Community -> Log Server
#include "NtlPacketTL.h"
//Community -> Master Server
#include "NtlPacketTM.h"
//Community -> System Tool Server
#include "NtlPacketTP.h"
//Community -> Query Server
#include "NtlPacketTQ.h"
//Community -> Client(EXE)
#include "NtlPacketTU.h"
/////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////REQUEST PACKETS///////////////////////////
//Game server -> Community Server
#include "NtlPacketGT.h"
//Master Server -> Community Server
#include "NtlPacketMT.h"
//System Tool -> Community Server
#include "NtlPacketPT.h"
//Query Server -> Community Server
#include "NtlPacketQT.h"
//Client(EXE) -> Community Server
#include "NtlPacketUT.h"
/////////////////////////////////////////////////////////////////////////////////
#include "NtlPacketUtil.h"

#ifndef CCOMMUNITYSESSION_H
#define CCOMMUNITYSESSION_H

class CCommunitySession :	public CNtlSession
{	
private:
	CNtlPacketEncoder_RandKey	m_packetEncoder;
public:
	CCommunitySession(bool bAliveCheck = true, bool bOpcodeCheck = true);
	~CCommunitySession();
	int							OnAccept();
	void						OnClose();
	int							OnConnect();
	int							OnDispatch(CNtlPacket * pPacket);
	// Packet functions
	void						SendClientChatEnterReq(CNtlPacket* pPacket);
	void						SendClientChatSayReq(CNtlPacket* pPacket);
	void						SendClientChatShoutReq(CNtlPacket* pPacket);
	void						SendClientChatWhisperReq(CNtlPacket* pPacket);
	void						SendClientChatPartyReq(CNtlPacket* pPacket);
	void						SendClientWelcomeServer(CNtlPacket* pPacket);
	//Game Server
	void						SendGameChatEnter(CNtlPacket* pPacket);
	void						SendGameAuthKey(CNtlPacket* pPacket);
	// End Packet functions
};

#endif