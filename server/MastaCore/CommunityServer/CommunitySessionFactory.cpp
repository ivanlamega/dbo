#include "stdafx.h"
#include "CommunitySessionFactory.h"


CCommunitySessionFactory::CCommunitySessionFactory()
{
}


CCommunitySessionFactory::~CCommunitySessionFactory()
{
}

CNtlSession * CCommunitySessionFactory::CreateSession(SESSIONTYPE sessionType)
{
	CNtlSession * pSession = NULL;
	switch (sessionType)
	{
	case SESSION_CLIENT:
	case SESSION_COMMUNITY_SERVER:
	{
		pSession = new CCommunitySession(sessionType);
	}
	break;
	case SESSION_MASTER_SERVER:
	{
		pSession = new CNtlSession(sessionType);
	}
	break;
	case SESSION_QUERY_SERVER:
	{
		pSession = new CNtlSession(sessionType);
	}
	break;
	case SESSION_GAME_SERVER:
	{
		pSession = new CNtlSession(sessionType);
	}
	break;

	default:
		break;
	}

	return pSession;
}



void CCommunitySessionFactory::DestroySession(CNtlSession * pSession)
{
	switch (pSession->GetSessionType())
	{
	case SESSION_CLIENT:
	case SESSION_MASTER_SERVER:
	case SESSION_QUERY_SERVER:
	case SESSION_COMMUNITY_SERVER:
	case SESSION_GAME_SERVER:
	{
		SAFE_DELETE(pSession);
	}
	break;

	default:
		break;
	}
}