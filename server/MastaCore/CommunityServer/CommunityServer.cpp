#include "stdafx.h"
#include "CommunityServer.h"

//For Multiple Server - Luiz45
const std::string CCommunityServer::GetConfigFileEnabledMultipleServers()
{
	return EnableMultipleServers.GetString();
}
const DWORD CCommunityServer::GetConfigFileMaxServers()
{
	return MAX_NUMOF_COMMUNITY_SERVER;
}
int	CCommunityServer::OnInitApp()
{
	m_nMaxSessionCount = MAX_NUMOF_COMMUNITY_SESSION;

	m_pSessionFactory = new CCommunitySessionFactory;
	if (NULL == m_pSessionFactory)
	{
		return NTL_ERR_SYS_MEMORY_ALLOC_FAIL;
	}
	m_pMasterServerSession = NULL;
	m_pQueryServerSession = NULL;
	m_pGameServerSession = NULL;
	m_pServerSession = (CCommunitySession*)m_pSessionFactory->CreateSession(SESSION_COMMUNITY_SERVER);
	return NTL_SUCCESS;
}

int	CCommunityServer::OnCreate()
{
	int rc = NTL_SUCCESS;
	rc = m_clientAcceptor.Create(m_config.strCommunityServerIP.c_str(), m_config.wCommunityServerPort, SESSION_COMMUNITY_SERVER,
		MAX_NUMOF_COMMUNITY_GAME_CLIENT, 5, 15, MAX_NUMOF_COMMUNITY_GAME_CLIENT);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_clientAcceptor, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_masterServerConnector.Create(m_config.strMasterServerIP.c_str(), m_config.wMasterServerPort, SESSION_MASTER_SERVER);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_masterServerConnector, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_queryServerConnector.Create(m_config.strQueryServerIP.c_str(), m_config.wQueryServerPort, SESSION_QUERY_SERVER);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_queryServerConnector, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_gameServerConnector.Create(m_config.strGameServerIP.c_str(), m_config.wGameServerPort, SESSION_GAME_SERVER);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_gameServerConnector, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	return NTL_SUCCESS;

}

void	CCommunityServer::OnDestroy()
{
	SAFE_DELETE(m_pSessionFactory);
}

int	CCommunityServer::OnCommandArgument(int argc, _TCHAR* argv[])
{
	return NTL_SUCCESS;
}

int	CCommunityServer::OnConfiguration(const char * lpszConfigFile)
{
	CNtlIniFile file;

	int rc = file.Create(lpszConfigFile);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}
	//For multiple servers - Luiz45
	if (!file.Read("ServerOptions", "EnableMultipleServers", EnableMultipleServers))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	CNtlString sTmpCountServer;
	if (!file.Read("ServerOptions", "MaxServerAllowed", sTmpCountServer))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (GetConfigFileEnabledMultipleServers() == "true")
	{
		int maxServer = atoi(sTmpCountServer.c_str());
		int i = 0;
		for (i = 0; i < maxServer; i++)
		{
			if (!file.Read("CommunityServer" + i, "Address", ServersConfig[i][0]))
			{
				return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
			}
			if (!file.Read("CommunityServer" + i, "Port", ServersConfig[i][1]))
			{
				return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
			}
		}
	}
	else
	{
		if (!file.Read("CommunityServer", "Address", m_config.strCommunityServerIP))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("CommunityServer", "Port", m_config.wCommunityServerPort))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
	}
	if (!file.Read("MasterServer", "Address", m_config.strMasterServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("MasterServer", "Port", m_config.wMasterServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("OperatingServer", "Address", m_config.strOperatingServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("OperatingServer", "Port", m_config.wOperatingServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("QueryServer", "Address", m_config.strQueryServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("QueryServer", "Port", m_config.wQueryServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("GameServer", "Address", m_config.strGameServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("GameServer", "Port", m_config.wGameServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	return NTL_SUCCESS;
}

int	CCommunityServer::OnAppStart()
{
	return NTL_SUCCESS;
}

void	CCommunityServer::Run()
{
	DWORD dwTickCur, dwTickOld = ::GetTickCount();

	while (IsRunnable())
	{
		dwTickCur = ::GetTickCount();
		if (dwTickCur - dwTickOld >= 100000)
		{
			//	NTL_PRINT(PRINT_APP, "Auth Server Run()");
			DoAliveTime();
			dwTickOld = dwTickCur;
			if (!m_pMasterServerSession)
				this->m_pServerSession->OnAccept();
			if (!m_pQueryServerSession)
				this->m_pServerSession->OnAccept();
			if (!m_pGameServerSession)
				this->m_pServerSession->OnAccept();
		}
		Sleep(2);
		DoAliveTime();
	}
}

bool	CCommunityServer::Add(ACCOUNTID accID, HSESSION hSession)
{
	if (Find(accID))
		return false;

	m_sessionList.insert(std::pair<ACCOUNTID, HSESSION>(accID, hSession));

	return true;
}

void	CCommunityServer::RemoveClientSession(ACCOUNTID accID)
{
	for (CHAR_CLIENT_IT it = m_sessionList.begin(); it != m_sessionList.end(); it++)
	{
		if (accID == it->first)
		{
			m_sessionList.erase(it->first);
			break;
		}
	}
}

void	CCommunityServer::Remove(HSESSION session)
{
	for (CHAR_CLIENT_IT it = m_sessionList.begin(); it != m_sessionList.end(); it++)
	{
		if (session == it->second)
		{
			m_sessionList.erase(it);
			break;
		}
	}
}

bool	CCommunityServer::Find(ACCOUNTID accID)
{
	for (CHAR_CLIENT_IT it = m_sessionList.begin(); it != m_sessionList.end(); it++)
	{
		if (accID == it->first)
		{
			return true;
		}
	}

	return false;
}

HSESSION CCommunityServer::GetHSession(ACCOUNTID accID)
{
	for (CHAR_CLIENT_IT it = m_sessionList.begin(); it != m_sessionList.end(); it++)
	{
		if (accID == it->first)
		{
			return it->second;
		}
	}
	return INVALID_HSESSION;
}


bool	CCommunityServer::AddPlayer(HSESSION hSession,sCHAR_GAME_INFO pCharInfo)
{
	if (FindPlayer(hSession))
		return false;

	m_playersList.insert(std::pair<HSESSION, sCHAR_GAME_INFO>(hSession,pCharInfo));
	return true;
}

void	CCommunityServer::SetPlayer(sGT_USER_ENTER_GAME* req)
{
	sCHAR_GAME_INFO* m_sCharInfo = GetPlayerByAccID(req->accountId);
	m_sCharInfo->charId = req->charId;
	m_sCharInfo->guildId = req->guildId;
	m_sCharInfo->byClass = req->byClass;
	m_sCharInfo->byLevel = req->byLevel;
	m_sCharInfo->byRace = req->byRace;
	m_sCharInfo->dwPunishBitFlag = req->dwPunishBitFlag;
	m_sCharInfo->dwReputation = req->dwReputation;
	m_sCharInfo->worldId = req->worldId;
	m_sCharInfo->gmGroupId = req->gmGroupId;
	m_sCharInfo->mapNameTblidx = req->mapNameTblidx;
	m_sCharInfo->vCurrentPosition = req->vCurrentPosition;	
	wcscpy_s(m_sCharInfo->awchName, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1, req->awchName);
	wcscpy_s(m_sCharInfo->awchUserID, NTL_MAX_SIZE_USERID + 1, req->awchUserID);	
}

sCHAR_GAME_INFO	CCommunityServer::GetPlayer(HSESSION hSession)
{
	for (PLAYERS_IT it = m_playersList.begin(); it != m_playersList.end(); it++)
	{
		if (hSession == it->first)
		{
			return it->second;
		}
	}	
}

sCHAR_GAME_INFO	CCommunityServer::GetPlayerByCharID(CHARACTERID charID)
{
	for (PLAYERS_IT it = m_playersList.begin(); it != m_playersList.end(); it++)
	{
		if (charID == it->second.charId)
		{
			return it->second;
		}
	}
}

sCHAR_GAME_INFO*	CCommunityServer::GetPlayerByAccID(ACCOUNTID charID)
{
	for (PLAYERS_IT it = m_playersList.begin(); it != m_playersList.end(); it++)
	{
		if (charID == it->second.accountId)
		{
			return &it->second;
		}
	}
}

void	CCommunityServer::RemovePlayer(HSESSION hSession)
{
	for (PLAYERS_IT it = m_playersList.begin(); it != m_playersList.end(); it++)
	{
		if (hSession == it->first)
		{
			m_playersList.erase(hSession);
			break;
		}
	}
}

bool	CCommunityServer::FindPlayer(HSESSION hSession)
{
	for (PLAYERS_IT it = m_playersList.begin(); it != m_playersList.end(); it++)
	{
		if (hSession == it->first)
		{
			return true;
		}
	}

	return false;
}
bool	CCommunityServer::FindPlayer(WCHAR* charName)
{
	for (PLAYERS_IT it = m_playersList.begin(); it != m_playersList.end(); it++)
	{
		if (wcscmp(charName,it->second.awchName) == 0)
		{
			return true;
		}
	}

	return false;
}
HSESSION	CCommunityServer::GetPlayerSession(WCHAR* charName)
{
	for (PLAYERS_IT it = m_playersList.begin(); it != m_playersList.end(); it++)
	{
		if (wcscmp(charName, it->second.awchName) == 0)
		{
			return it->first;
		}
	}
	return INVALID_HSESSION;
}

HSESSION	CCommunityServer::GetPlayerSession(CHARACTERID charID)
{
	for (PLAYERS_IT it = m_playersList.begin(); it != m_playersList.end(); it++)
	{
		if (it->second.charId == charID)
		{
			return it->first;
		}
	}
	return INVALID_HSESSION;
}

bool	CCommunityServer::AddPlayerToParty(PARTYID ptID, CHARACTERID charId)
{
	if (FindPlayerInParty(ptID, charId))
		return FALSE;

	int freePosition = GetFreePositionParty(ptID);
	PARTY_MEMBERS pt;
	pt.insert(std::pair<int, CHARACTERID>(freePosition, charId));
	m_partyList.insert(std::pair<PARTYID, PARTY_MEMBERS>(ptID,pt));
	return true;
}

int    CCommunityServer::GetFreePositionParty(PARTYID ptID)
{
	bool bFound = false;
	int iPosition = 0;
	for (PARTY_IT it = m_partyList.begin(); it != m_partyList.end(); it++)
	{
		if (bFound)
			break;
		if (it->first == ptID)
		{
			for (int i = 0; i < NTL_MAX_MEMBER_IN_PARTY; i++)
			{
				PARTY_MEMBER_IT pt = it->second.find(i);
				if (pt->second == INVALID_CHARACTERID)
				{
					iPosition = i;
					bFound = true;
				}
			}
		}
	}
	return iPosition;
}

void	CCommunityServer::RemovePlayerFromParty(PARTYID ptID, CHARACTERID charId)
{
	if (FindPlayerInParty(ptID,charId))
	{
		PARTY_IT it = m_partyList.find(ptID);
		for (int i = 0; i < NTL_MAX_MEMBER_IN_PARTY; i++)
		{
			if (it->first == ptID)
			{
				PARTY_MEMBER_IT pt = it->second.find(i);
				if (pt->second == charId)
				{
					it->second.erase(pt);				
					break;
				}
			}
		}
	}
}

bool	CCommunityServer::FindPlayerInParty(PARTYID ptID, CHARACTERID charID)
{
	bool bFound = false;
	for (PARTY_IT it = m_partyList.begin(); it != m_partyList.end(); it++)
	{
		if (bFound)
			break;
		if (it->first == ptID)
		{
			for (int i = 0; i < NTL_MAX_MEMBER_IN_PARTY; i++)
			{
				PARTY_MEMBER_IT pt = it->second.find(i);
				if (pt->second == charID)
				{
					bFound = true;
				}
			}
		}
	}
	return bFound;
}

void	CCommunityServer::SetGameSession(CGameSession * pServerSession)
{
	CNtlAutoMutex mutex(&m_gameServerMutex);
	mutex.Lock();

	if (NULL == pServerSession)
	{
		m_pGameServerSession->Release();
		m_pGameServerSession = pServerSession;
	}
	else
	{
		pServerSession->Acquire();
		m_pGameServerSession = pServerSession;
	}
}

CGameSession* CCommunityServer::AcquireGameSession()
{
	CNtlAutoMutex mutex(&m_gameServerMutex);
	mutex.Lock();

	if (NULL == m_pGameServerSession) return NULL;

	m_pGameServerSession->Acquire();
	return m_pGameServerSession;
}

CGameSession* CCommunityServer::GetGameSession()
{
	return m_pGameServerSession;
}

void	CCommunityServer::SetMasterSession(CMasterSession * pServerSession)
{
	CNtlAutoMutex mutex(&m_masterServerMutex);
	mutex.Lock();

	if (NULL == pServerSession)
	{
		m_pMasterServerSession->Release();
		m_pMasterServerSession = pServerSession;
	}
	else
	{
		pServerSession->Acquire();
		m_pMasterServerSession = pServerSession;
	}
}

CMasterSession * CCommunityServer::AcquireMasterSession()
{
	CNtlAutoMutex mutex(&m_masterServerMutex);
	mutex.Lock();

	if (NULL == m_pMasterServerSession) return NULL;

	m_pMasterServerSession->Acquire();
	return m_pMasterServerSession;
}

CMasterSession * CCommunityServer::GetMasterSession()
{
	return m_pMasterServerSession;
}
void	CCommunityServer::SetQuerySession(CQuerySession * pServerSession)
{
	CNtlAutoMutex mutex(&m_queryServerMutex);
	mutex.Lock();

	if (NULL == pServerSession)
	{
		m_pQueryServerSession->Release();
		m_pQueryServerSession = pServerSession;
	}
	else
	{
		pServerSession->Acquire();
		m_pQueryServerSession = pServerSession;
	}
}

CQuerySession * CCommunityServer::AcquireQuerySession()
{
	CNtlAutoMutex mutex(&m_queryServerMutex);
	mutex.Lock();

	if (NULL == m_pQueryServerSession) return NULL;

	m_pQueryServerSession->Acquire();
	return m_pQueryServerSession;
}
CQuerySession * CCommunityServer::GetQuerySession()
{
	return m_pQueryServerSession;
}
void	CCommunityServer::SetClientSession(CCommunitySession* pSession)
{
	this->m_pClientSession = pSession;
}
CCommunitySession* CCommunityServer::GetClientSession()
{
	return this->m_pClientSession;
}
void	CCommunityServer::SendAllConnectedClients(CNtlPacket* pPacket, HSESSION exceptSession)
{
	for (COMMUNITY_CLIENT_IT it = m_sessionList.begin(); it != m_sessionList.end(); it++)
	{
		HSESSION session = it->second;
		if (session != INVALID_HSESSION)
		{
			if (session != exceptSession)
				Send(session, pPacket);
		}
	}
}
PARTYID CCommunityServer::GetPartyIDFromChar(CHARACTERID charID)
{
	PARTYID id = INVALID_PARTYID;
	bool bFound = false;
	for (PARTY_IT it = m_partyList.begin(); it != m_partyList.end(); it++)
	{
		if (bFound)
			break;
		if (it->first != INVALID_PARTYID)
		{
			for (int i = 0; i < NTL_MAX_MEMBER_IN_PARTY; i++)
			{
				PARTY_MEMBER_IT pt = it->second.find(i);
				if (pt->second == charID)
				{
					bFound = true;
					id = it->first;
					break;
				}
			}
		}
	}
	return id;
}
void	CCommunityServer::SendAllMembersParty(CNtlPacket* pPacket, CHARACTERID currentCharID)
{
	PARTYID id = GetPartyIDFromChar(currentCharID);
	if (id != INVALID_PARTYID)
	{
		for (PARTY_IT it = m_partyList.begin(); it != m_partyList.end(); it++)
		{
			if (it->first == id)
			{
				for (int i = 0; i < NTL_MAX_MEMBER_IN_PARTY; i++)
				{
					PARTY_MEMBER_IT pt = it->second.find(i);
					if (pt->second != INVALID_CHARACTERID)
					{
						this->Send(GetPlayerSession(pt->second), pPacket);
					}
				}
			}
		}		
	}
}
void CCommunityServer::DoAliveTime()
{
	for (int i = 0; i < this->GetNetwork()->GetSessionList()->GetMaxCount(); i++)
	{
		CNtlSession* first = this->GetNetwork()->GetSessionList()->Find(i);
		if (first)
		{
			if (first->GetSessionType() != SESSION_COMMUNITY_SERVER)
			{
				PACKETDATA res;
				res.wOpCode = SYS_ALIVE;
				CNtlPacket packet((BYTE*)&res, sizeof(res));
				first->PushPacket(&packet);
			}
		}
	}
}

HOBJECT	CCommunityServer::AcquireSerialId(void)
{
	if (m_uiSerialId++)
	{
		if (m_uiSerialId == 0xffffffff)
			m_uiSerialId = 0;
	}

	return m_uiSerialId;
}