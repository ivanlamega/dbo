#pragma once
#include "CommunityServer.h"
#include "NtlSessionFactory.h"

#ifndef CCOMUNNITYSESSION_FACTORY_H
#define CCOMUNNITYSESSION_FACTORY_H

class CCommunitySessionFactory : public CNtlSessionFactory
{
public:
	CCommunitySessionFactory();
	~CCommunitySessionFactory();
	CNtlSession * CreateSession(SESSIONTYPE sessionType);
	void DestroySession(CNtlSession * pSession);
};
#endif

