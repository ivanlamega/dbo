#include "stdafx.h"
#include "CommunityServer.h"
#include "CommunitySession.h"


CCommunitySession::CCommunitySession(bool bAliveCheck, bool bOpcodeCheck) :CNtlSession(SESSION_COMMUNITY_SERVER)
{
	SetControlFlag(CONTROL_FLAG_USE_SEND_QUEUE);

	if (bAliveCheck)
	{
		SetControlFlag(CONTROL_FLAG_CHECK_ALIVE);
	}
	if (bOpcodeCheck)
	{
		SetControlFlag(CONTROL_FLAG_CHECK_OPCODE);
	}

	SetPacketEncoder(&m_packetEncoder);
}

//-----------------------------------------------------------------------------------
//		클라이언트 소멸
//-----------------------------------------------------------------------------------
CCommunitySession::~CCommunitySession()
{
	NTL_PRINT(PRINT_APP, "CCommunitySession Destructor Called");

	CCommunityServer * app = (CCommunityServer*)NtlSfxGetApp();
	app->Remove(this->GetHandle());
}

int CCommunitySession::OnAccept()
{
	NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
	CCommunityServer * app = (CCommunityServer*)NtlSfxGetApp();
	for (int i = 0; i < app->GetNetwork()->GetSessionList()->GetMaxCount(); i++)
	{
		CNtlSession* first = app->GetNetwork()->GetSessionList()->Find(i);
		if (first)
		{
			if (first->GetSessionType() == SESSION_QUERY_SERVER)
			{
				app->SetQuerySession((CQuerySession*)first);
			}
			if (first->GetSessionType() == SESSION_MASTER_SERVER)
			{
				app->SetMasterSession((CMasterSession*)first);
			}
			if (first->GetSessionType() == SESSION_GAME_SERVER)
			{
				app->SetGameSession((CGameSession*)first);
			}
		}
	}
	if ((app->GetQuerySession() != NULL) && (!app->IsQueryServerConnected()))
	{
		app->GetQuerySession()->OnConnect();
		CNtlPacket packetA(sizeof(sTQ_HEARTBEAT));
		sTQ_HEARTBEAT* msgA = (sTQ_HEARTBEAT *)packetA.GetPacketData();
		msgA->wOpCode = TQ_HEARTBEAT;
		packetA.SetPacketLen(sizeof(sTQ_HEARTBEAT));
		app->GetQuerySession()->PushPacket(&packetA);
		app->SetQueryServerConnected(true);
	}
	if ((app->GetMasterSession() != NULL) && (!app->IsMasterServerConnected()))
	{
		app->GetMasterSession()->OnConnect();
		CNtlPacket packetM(sizeof(sTM_HEARTBEAT));
		sTM_HEARTBEAT* msgM = (sTM_HEARTBEAT*)packetM.GetPacketData();
		msgM->wOpCode = TM_HEARTBEAT;
		packetM.SetPacketLen(sizeof(sTM_HEARTBEAT));
		app->GetMasterSession()->PushPacket(&packetM);
		app->SetMasterServerConnected(true);
	}
	if ((app->GetGameSession() != NULL) && (!app->IsGameServerConnected()))
	{
		CNtlPacket packetG(sizeof(sTG_HEARTBEAT));
		sTG_HEARTBEAT* msgG = (sTG_HEARTBEAT*)packetG.GetPacketData();
		msgG->wOpCode = TG_HEARTBEAT;
		packetG.SetPacketLen(sizeof(sTG_HEARTBEAT));
		app->GetGameSession()->PushPacket(&packetG);
		app->SetGameServerConnected(true);
	}
	return NTL_SUCCESS;
}


void CCommunitySession::OnClose()
{
	NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
}

int CCommunitySession::OnConnect()
{
	NTL_PRINT(PRINT_APP, "%s", __FUNCTION__);
	CCommunityServer* app = (CCommunityServer*)NtlSfxGetApp();
	do{
		this->OnAccept();
		Sleep(5000);
	} while ((!app->GetQuerySession()) ||
			 (!app->GetGameSession()) ||
		(!app->GetMasterSession()));
	return 0;
}

int CCommunitySession::OnDispatch(CNtlPacket * pPacket)
{
	CCommunityServer * app = (CCommunityServer*)NtlSfxGetApp();
	if ((!app->GetQuerySession()) ||
		(!app->GetGameSession()) ||
		(!app->GetMasterSession()))
		this->OnConnect();

	sNTLPACKETHEADER * pHeader = (sNTLPACKETHEADER *)pPacket->GetPacketData();
	if (pHeader->wOpCode != SYS_ALIVE)
	 NTL_PRINT(PRINT_APP, "The following packet was not handled: %s", NtlGetPacketName(pHeader->wOpCode));
	switch (pHeader->wOpCode)
	{
		//Query Server
		case QT_HEARTBEAT:
		{
			if (app->GetQuerySession() != NULL)
				app->GetQuerySession()->ResetAliveTime();
		}
		break;
		//Master Server
		case MT_HEARTBEAT:
		{
			if (app->GetMasterSession() != NULL)
				app->GetMasterSession()->ResetAliveTime();
		}
		break;
		//Game Server		
		case GT_HEARTBEAT:
		{
			if (app->GetGameSession() != NULL)
				app->GetGameSession()->ResetAliveTime();
		}
		break;
		case GT_GAME_INFO:
		{
			
		}
		break;
		case GT_USER_AUTH_KEY_CREATED_NFY:
		{
			CCommunitySession::SendGameAuthKey(pPacket);
		}
		break;
		case GT_USER_ENTER_GAME:
		{
			CCommunitySession::SendGameChatEnter(pPacket);
		}
		break;
		case GT_PARTY_CREATED:
		{
		}
		break;
		case GT_CHAR_READY_FOR_COMMUNITY_SERVER_NFY:
		{
			//TODO: send a notify to all connected clients or at last friend contact that person is online
		}
		break;
		case GT_PARTY_DISBANDED:
		{
		}
		break;
		case GT_PARTY_MEMBER_JOINED:
		{
		}
		break;
		case GT_PARTY_MEMBER_LEFT:
		{
		}
		break;
		case GT_PARTY_LEADER_CHANGED:
		{
		}
		break;
		//Client(EXE)
		case UT_ENTER_CHAT:
		{
			app->SetClientSession(this);
			CCommunitySession::SendClientChatEnterReq(pPacket);			
		}
		break;
		case UT_CHAT_MESSAGE_SAY:
		{
			CCommunitySession::SendClientChatSayReq(pPacket);
		}
		break;
		case UT_CHAT_MESSAGE_SHOUT:
		{
			CCommunitySession::SendClientChatShoutReq(pPacket);
		}
		break;
		case UT_CHAT_MESSAGE_WHISPER:
		{
			CCommunitySession::SendClientChatWhisperReq(pPacket);
		}
		break;
		case UT_CHAT_MESSAGE_PARTY:
		{

		}
		break;
		default:
		{
			if (pHeader->wOpCode != SYS_ALIVE)
			{
				NTL_PRINT(PRINT_APP, "The following packet was not handled: %s", NtlGetPacketName(pHeader->wOpCode));
			}
			return CNtlSession::OnDispatch(pPacket);
		}
	}
	
	return NTL_SUCCESS;
}
void	CCommunitySession::SendClientChatEnterReq(CNtlPacket* pPacket)
{
	CCommunityServer * app = (CCommunityServer*)NtlSfxGetApp();
	sUT_ENTER_CHAT* req = (sUT_ENTER_CHAT*)pPacket->GetPacketData();	
	CNtlPacket packet(sizeof(sTU_ENTER_CHAT_RES));
	sTU_ENTER_CHAT_RES* res = (sTU_ENTER_CHAT_RES*)packet.GetPacketData();
	res->wResultCode = CHAT_SUCCESS;
	res->wOpCode = TU_ENTER_CHAT_RES;
	packet.SetPacketLen(sizeof(sTU_ENTER_CHAT_RES));
	app->Send(app->GetHSession(req->accountId), &packet);
	sCHAR_GAME_INFO sTemp;
	sTemp.accountId = req->accountId;
	sTemp.hHandle = app->AcquireSerialId();
	app->AddPlayer(this->GetHandle(), sTemp);
	app->Add(req->accountId, this->GetHandle());
}
void	CCommunitySession::SendClientChatSayReq(CNtlPacket* pPacket)
{
	CCommunityServer * app = (CCommunityServer*)NtlSfxGetApp();
	sUT_CHAT_MESSAGE_SAY* req = (sUT_CHAT_MESSAGE_SAY*)pPacket->GetPacketData();

	CNtlPacket packet(sizeof(sTU_CHAT_MESSAGE_SAY));
	sTU_CHAT_MESSAGE_SAY* res = (sTU_CHAT_MESSAGE_SAY*)packet.GetPacketData();

	res->wOpCode = TU_CHAT_MESSAGE_SAY;

	wcscpy_s(res->awchMessage, NTL_MAX_LENGTH_OF_CHAT_MESSAGE_UNICODE+1, req->awchMessage);
	res->wMessageLengthInUnicode = req->wMessageLengthInUnicode;
	res->hSubject = app->GetPlayer(this->GetHandle()).hHandle;
	wcscpy_s(res->awchSenderCharName, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1, app->GetPlayer(this->GetHandle()).awchName);
	
	packet.SetPacketLen(sizeof(sTU_CHAT_MESSAGE_SAY));		
	app->SendAllConnectedClients(&packet);
}
void	CCommunitySession::SendClientChatShoutReq(CNtlPacket* pPacket)
{
	CCommunityServer * app = (CCommunityServer*)NtlSfxGetApp();
	sUT_CHAT_MESSAGE_SHOUT* req = (sUT_CHAT_MESSAGE_SHOUT*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sTU_CHAT_MESSAGE_SHOUT));
	sTU_CHAT_MESSAGE_SHOUT* res = (sTU_CHAT_MESSAGE_SHOUT*)packet.GetPacketData();
	wcscpy_s(res->awchMessage, NTL_MAX_LENGTH_OF_CHAT_MESSAGE_UNICODE+1, req->awchMessage);
	wcscpy_s(res->awchSenderCharName, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1, app->GetPlayer(this->GetHandle()).awchName);
	res->wMessageLengthInUnicode = req->wMessageLengthInUnicode;
	res->hSubject = app->GetPlayer(this->GetHandle()).hHandle;//This serial is different from game server
	res->wOpCode = TU_CHAT_MESSAGE_SHOUT;
	packet.SetPacketLen(sizeof(sTU_CHAT_MESSAGE_SHOUT));
	app->SendAllConnectedClients(&packet);
}
void    CCommunitySession::SendClientChatPartyReq(CNtlPacket* pPacket)
{
	CCommunityServer * app = (CCommunityServer*)NtlSfxGetApp();
	sUT_CHAT_MESSAGE_PARTY* req = (sUT_CHAT_MESSAGE_PARTY*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sTU_CHAT_MESSAGE_PARTY));
	sTU_CHAT_MESSAGE_PARTY* res = (sTU_CHAT_MESSAGE_PARTY*)packet.GetPacketData();
	wcscpy_s(res->awchSenderCharName, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1, app->GetPlayer(this->GetHandle()).awchName);
	wcscpy_s(res->awchMessage, NTL_MAX_LENGTH_OF_CHAT_MESSAGE_UNICODE + 1, req->awchMessage);
	res->wMessageLengthInUnicode = req->wMessageLengthInUnicode;
	res->byChattingType = req->byChattingType;
	res->hSubject = app->GetPlayer(this->GetHandle()).hHandle;
	res->wOpCode = TU_CHAT_MESSAGE_PARTY;
	packet.SetPacketLen(sizeof(sTU_CHAT_MESSAGE_PARTY));
	app->SendAllMembersParty(&packet,app->GetPlayer(this->GetHandle()).charId);
}

void	CCommunitySession::SendClientChatWhisperReq(CNtlPacket* pPacket)
{
	CCommunityServer * app = (CCommunityServer*)NtlSfxGetApp();
	sUT_CHAT_MESSAGE_WHISPER* req = (sUT_CHAT_MESSAGE_WHISPER*)pPacket->GetPacketData();
	if (app->FindPlayer(req->awchReceiverCharName))
	{
		CNtlPacket packet(sizeof(sTU_CHAT_MESSAGE_WHISPER));
		sTU_CHAT_MESSAGE_WHISPER* res = (sTU_CHAT_MESSAGE_WHISPER*)packet.GetPacketData();
		wcscpy_s(res->awchMessage, NTL_MAX_LENGTH_OF_CHAT_MESSAGE_UNICODE+1, req->awchMessage);
		wcscpy_s(res->awchSenderCharName, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1, app->GetPlayer(this->GetHandle()).awchName);
		res->wMessageLengthInUnicode = req->wMessageLengthInUnicode;
		res->wOpCode = TU_CHAT_MESSAGE_WHISPER;
		packet.SetPacketLen(sizeof(sTU_CHAT_MESSAGE_WHISPER));
		app->Send(app->GetPlayerSession(req->awchReceiverCharName), &packet);
		app->Send(app->GetPlayerSession(res->awchSenderCharName), &packet);
	}
	else
	{
		CNtlPacket packet(sizeof(sTU_CHAT_MESSAGE_WHISPER_FAILED_NFY));
		sTU_CHAT_MESSAGE_WHISPER_FAILED_NFY* res = (sTU_CHAT_MESSAGE_WHISPER_FAILED_NFY*)packet.GetPacketData();
		wcscpy_s(res->awchReceiverCharName, NTL_MAX_SIZE_CHAR_NAME_UNICODE+1, req->awchReceiverCharName);
		res->wOpCode = TU_CHAT_MESSAGE_WHISPER_FAILED_NFY;
		app->Send(app->GetPlayerSession(app->GetPlayer(this->GetHandle()).awchName), &packet);
	}
	
}
///////////////Game Server Handling//////////////////
void	CCommunitySession::SendGameChatEnter(CNtlPacket* pPacket)
{
	CCommunityServer * app = (CCommunityServer*)NtlSfxGetApp();
	sGT_USER_ENTER_GAME* req = (sGT_USER_ENTER_GAME*)pPacket->GetPacketData();
	app->SetPlayer(req);
	CNtlPacket packet(sizeof(sTG_USER_ENTER_GAME_ACK));
	sTG_USER_ENTER_GAME_ACK* res = (sTG_USER_ENTER_GAME_ACK*)packet.GetPacketData();
	res->accountId = app->GetPlayerByAccID(req->accountId)->accountId;
	//res->charId = req->charId;
	res->wOpCode = TG_USER_ENTER_GAME_ACK;
	res->wResultCode = CHAT_SUCCESS;
	packet.SetPacketLen(sizeof(sTG_USER_ENTER_GAME_ACK));
	app->GetGameSession()->PushPacket(&packet);
}

void	CCommunitySession::SendGameAuthKey(CNtlPacket* pPacket)
{
	CCommunityServer * app = (CCommunityServer*)NtlSfxGetApp();
	sGT_USER_AUTH_KEY_CREATED_NFY* req = (sGT_USER_AUTH_KEY_CREATED_NFY*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sTG_USER_AUTH_KEY_CREATED_ACK));
	sTG_USER_AUTH_KEY_CREATED_ACK* res = (sTG_USER_AUTH_KEY_CREATED_ACK*)packet.GetPacketData();
	res->accountId = req->accountId;
	memcpy(res->abyAuthKey, req->abyAuthKey, NTL_MAX_SIZE_AUTH_KEY);
	//res->charId = req->charId;
	res->wResultCode = CHAT_SUCCESS;
	res->wOpCode = TG_USER_AUTH_KEY_CREATED_ACK;
	packet.SetPacketLen(sizeof(sTG_USER_AUTH_KEY_CREATED_ACK));
	app->GetGameSession()->PushPacket(&packet);
}

void	CCommunitySession::SendClientWelcomeServer(CNtlPacket* pPacket)
{
	CCommunityServer * app = (CCommunityServer*)NtlSfxGetApp();
	sGT_CHAR_READY_FOR_COMMUNITY_SERVER_NFY* req = (sGT_CHAR_READY_FOR_COMMUNITY_SERVER_NFY*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sTU_CHAT_MESSAGE_SHOUT));
	sTU_CHAT_MESSAGE_SHOUT* res = (sTU_CHAT_MESSAGE_SHOUT*)packet.GetPacketData();
	wcscpy_s(res->awchMessage, NTL_MAX_LENGTH_OF_CHAT_MESSAGE_UNICODE + 1, L"Welcome Back My Brother");
	wcscpy_s(res->awchSenderCharName, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1, L"SysAdmin");
	res->wMessageLengthInUnicode = sizeof(res->awchMessage) + 1;
	res->hSubject = app->GetPlayer(req->charId).hHandle;
	res->wOpCode = TU_CHAT_MESSAGE_SHOUT;
	packet.SetPacketLen(sizeof(sTU_CHAT_MESSAGE_SHOUT));
	app->Send(app->GetPlayerSession(req->charId), &packet);
}