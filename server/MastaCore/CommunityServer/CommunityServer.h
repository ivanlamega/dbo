#pragma once
#include "NtlSfx.h"
#include "CommunitySession.h"
#include "CommunitySessionFactory.h"

#ifndef CCOMMUNITY_SERVER_H
#define CCOMMUNITY_SERVER_H

class CMasterSession;
class CQuerySession;
class CGameSession;

#include "MasterSession.h"
#include "QuerySession.h"
#include "GameSession.h"

#include "NtlPacketGT.h"

class CCommunityServer :	public CNtlServerApp
{
	//Members
public:
	CNtlString				ServersConfig[99][2];//For Config of multiple server 0->{Ip,Port} - Luiz45
private:
	DWORD					m_uiSerialId;
	///CommunityServer Ptr Refresh
	CCommunitySession*			m_pServerSession;
	//Client Session
	CCommunitySession*			m_pClientSession;
	//Master Server Connection
	CNtlConnector				m_masterServerConnector;
	CNtlMutex					m_masterServerMutex;
	CMasterSession*				m_pMasterServerSession;
	//Game Server Connection
	CNtlConnector				m_gameServerConnector;
	CNtlMutex					m_gameServerMutex;
	CGameSession*				m_pGameServerSession;
	//Operating Server Connection
	CNtlConnector				m_operatingServerConnector;
	CNtlMutex					m_operatingServerMutex;
	//CServerSession *			m_pOperatingServerSession;
	//Query Server Connection
	CNtlConnector				m_queryServerConnector;
	CNtlMutex					m_queryServerMutex;
	CQuerySession*			    m_pQueryServerSession;

	CNtlAcceptor			m_clientAcceptor;
	CNtlLog  				m_log;
	sSERVERCONFIG			m_config;
	CNtlString				EnableMultipleServers;//Added for enabling multiple server - Luiz45

	COMMUNITY_CLIENT_SESSION	m_sessionList;

	PLAYERS_LIST			m_playersList;
	PARTY_LIST				m_partyList;
	GUILD_LIST				m_guildList;
	//Methods
	//Connected
	bool					m_bMasterServerConnected;
	bool					m_bQueryServerConnected;
	bool					m_bGameServerConnected;
	void					DoAliveTime();
public:	
	//For Multiple Server - Luiz45
	const std::string		GetConfigFileEnabledMultipleServers();
	const DWORD				GetConfigFileMaxServers();
	int						OnInitApp();
	int						OnCreate();
	void					OnDestroy();
	int						OnCommandArgument(int argc, _TCHAR* argv[]);
	int						OnConfiguration(const char * lpszConfigFile);
	int						OnAppStart();
	int						GetSessionCount();
	void					Run();
	bool					Add(ACCOUNTID accID, HSESSION hSession);
	void					RemoveClientSession(ACCOUNTID accID);
	void					Remove(HSESSION session);
	bool					Find(ACCOUNTID accID);
	HSESSION				GetHSession(ACCOUNTID accID);
	//Add Player in Chat
	void					SetPlayer(sGT_USER_ENTER_GAME* req);
	bool					AddPlayer(HSESSION hSession,sCHAR_GAME_INFO pCharInfo);
	sCHAR_GAME_INFO			GetPlayer(HSESSION hSession);
	sCHAR_GAME_INFO			GetPlayerByCharID(CHARACTERID charId);
	sCHAR_GAME_INFO*		GetPlayerByAccID(ACCOUNTID accId);
	void					RemovePlayer(HSESSION hSession);
	bool					FindPlayer(HSESSION hSession);
	bool					FindPlayer(WCHAR* charName);
	//Add Player in Party
	bool					AddPlayerToParty(PARTYID ptID,CHARACTERID charId);
	void					RemovePlayerFromParty(PARTYID ptID, CHARACTERID charId);
	bool					FindPlayerInParty(PARTYID ptID,CHARACTERID charID);
	int						GetFreePositionParty(PARTYID ptID);
	PARTYID					GetPartyIDFromChar(CHARACTERID charID);
	//////////////////////////
	HSESSION				GetPlayerSession(WCHAR* charName);
	HSESSION				GetPlayerSession(CHARACTERID charID);
	void					SetMasterSession(CMasterSession* pServerSession);
	CMasterSession*			AcquireMasterSession();
	CMasterSession*			GetMasterSession();
	void					SetQuerySession(CQuerySession* pServerSession);
	CQuerySession*			AcquireQuerySession();
	CQuerySession*			GetQuerySession();
	void					SetGameSession(CGameSession* pServerSession);
	CGameSession*			AcquireGameSession();
	CGameSession*			GetGameSession();
	void					SetClientSession(CCommunitySession* pClientSession);
	CCommunitySession*		GetClientSession();
	void					SetMasterServerConnected(bool c){ m_bMasterServerConnected = c; }
	void					SetQueryServerConnected(bool c){ m_bQueryServerConnected = c; }
	void					SetGameServerConnected(bool c){ m_bGameServerConnected = c; }
	bool					IsMasterServerConnected(){ return m_bMasterServerConnected; }
	bool					IsQueryServerConnected(){ return m_bQueryServerConnected; }
	bool					IsGameServerConnected(){ return m_bGameServerConnected; }
	void					SendAllConnectedClients(CNtlPacket* pPacket, HSESSION exceptSession = INVALID_HSESSION);
	void					SendAllMembersParty(CNtlPacket* pPacket, CHARACTERID currentCharID);
	HOBJECT					AcquireSerialId(void);
};

#endif