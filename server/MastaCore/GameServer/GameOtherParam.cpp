#include "stdafx.h"
#include "GameOtherParam.h"
#include "GameServer.h"


CGameOtherParam::CGameOtherParam()
{
}


CGameOtherParam::~CGameOtherParam()
{
}


void CGameOtherParam::SetBindPosition(float x, float y, float z)
{
	this->vBindLoc.x = x;
	this->vBindLoc.y = y;
	this->vBindLoc.z = z;
}
void CGameOtherParam::SetBindDir(float x, float y, float z)
{
	this->vBindDir.x = x;
	this->vBindDir.y = y;
	this->vBindDir.z = z;
}

sVECTOR3 CGameOtherParam::GetBindPosition()
{
	return vBindLoc;
}
sVECTOR3 CGameOtherParam::GetBindDir()
{
	return vBindDir;
}

void CGameOtherParam::SetBindObjectTblidx(TBLIDX wd)
{
	this->bindObjectTblidx = wd;
}

void CGameOtherParam::SetBindWorldID(WORLDID wdId)
{
	this->bindWorldId = wdId;
}

TBLIDX CGameOtherParam::GetBindObjectTblidx()
{
	return this->bindObjectTblidx;
}

WORLDID CGameOtherParam::GetBindWorldID()
{
	return this->bindWorldId;
}

void CGameOtherParam::SetBindType(BYTE byType)
{
	this->byBindType = byType;
}

BYTE CGameOtherParam::GetBindType()
{
	return this->byBindType;
}