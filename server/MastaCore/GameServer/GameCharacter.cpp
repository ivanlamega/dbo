#include "stdafx.h"
#include "GameServer.h"
#include "GameCharacter.h"
#include "PCTable.h"
#include "NtlSharedType.h"


CGameCharacter::CGameCharacter(ACCOUNTID accID, CHARACTERID charID)
{
	this->m_accountID = accID;
	this->m_charID = charID;
	this->standup = true;
	this->byRpBallCounter = 0;
	this->m_SerialHandle = ((CGameServer*)NtlSfxGetApp())->AcquireSerialId();
	this->bTutorial = true;
}


CGameCharacter::~CGameCharacter()
{
}

void CGameCharacter::CreateAvatarInfo(sPC_DATA sPCData,
										sITEM_DATA* sItem,
										sSKILL_DATA* sSkill,
										sHTB_SKILL_DATA* sHtbSkill,
										sBUFF_DATA* sBuff,
										sQUEST_PROGRESS_DATA* sProgress,
										sQUEST_INVENTORY_DATA* sInventoryQuest,
										sQUICK_SLOT_DATA* sQuickSlot,
										sPUNISH_DATA* sPunish,
										sSHORTCUT_DATA* sShortcut,
										sRECIPE_DATA* sRecipe,
										BYTE byItemCount,
										BYTE bySkillCount,
										BYTE byBuffCount,
										BYTE byHTBSkillCount,
										BYTE byQuestProgressCount,
										BYTE byQuestInventoryCount,
										BYTE byQuickSlotCount,
										BYTE byPunishCount,
										BYTE byShortcutCount,
										BYTE byRecipeCount)
{
	this->m_SkillContainer.CreateQuickSlot(sQuickSlot, byQuickSlotCount,this->GetInventory());	
}

void CGameCharacter::CreatePcProfile(sPC_DATA sPC)
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CPCTable* pcTable = app->GetTableContainer()->GetPcTable();
	sPC_TBLDAT* pPcTbldat = reinterpret_cast<sPC_TBLDAT*>(pcTable->GetPcTbldat(sPC.byRace,sPC.byClass,sPC.byGender));

	this->byRace = sPC.byRace;
	this->byClass = sPC.byClass;
	this->byGender = sPC.byGender;
	memcpy(this->m_sPcProfile.awchName, sPC.awchName, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1);
	this->m_sPcProfile.bChangeClass = sPC.bChangeClass;
	this->m_sPcProfile.bindObjectTblidx = sPC.bindObjectTblidx;
	this->m_sPcProfile.bindWorldId = sPC.bindWorldId;
	this->m_sPcProfile.bIsAdult = sPC.bIsAdult;
	this->m_sPcProfile.bIsGameMaster = ((sPC.GMAccountID != 0) && (sPC.GMAccountID != INVALID_ACCOUNTID) ? true : false);
	this->m_sPcProfile.byBindType = sPC.byBindType;
	this->m_sPcProfile.byLevel = sPC.byLevel;
	this->m_sPcProfile.charId = sPC.charId;
	this->m_sPcProfile.dwCurExp = sPC.dwEXP;
	this->m_sPcProfile.dwMudosaPoint = sPC.dwMudosaPoint;
	this->m_sPcProfile.dwReputation = sPC.dwReputation;
	this->m_sPcProfile.dwSpPoint = sPC.dwSpPoint;
	this->m_sPcProfile.dwZenny = sPC.dwMoney;
	this->m_sPcProfile.guildId = sPC.guildId;
	this->m_sPcProfile.sMarking.byCode = sPC.sMarking.byCode;
	this->m_sPcProfile.sPcShape.byFace = sPC.byFace;
	this->m_sPcProfile.sPcShape.byHair = sPC.byHair;
	this->m_sPcProfile.sPcShape.byHairColor = sPC.byHairColor;
	this->m_sPcProfile.sPcShape.bySkinColor = sPC.bySkinColor;
	this->m_sPcProfile.dwTutorialHint = sPC.dwTutorialHint;
	this->m_sPcProfile.sLocalize.pcProfileCJKor.netP = sPC.sLocalize.pcDataCJKor.netP;
	//this->m_sPcProfile.sLocalize.type = sPC.sLocalize.type;
	//memcpy(&this->m_sPcProfile.sLocalize, &sPC.sLocalize, sizeof(sPC_PROFILE_LOCALIZE));
	this->m_sPcProfile.tblidx = pPcTbldat->tblidx;	
	LoadAttributes(&this->m_sPcProfile.avatarAttribute,pPcTbldat);
	SetPosition(sPC.fPositionX, sPC.fPositionY, sPC.fPositionZ);
	SetDir(sPC.fDirX, sPC.fDirY, sPC.fDirZ);
	//Set default char state
	memset(&this->m_sCharState, 0xFFFFFFF, sizeof(sCHARSTATE));
	this->m_sCharState.sCharStateBase.bFightMode = false;
	this->m_sCharState.sCharStateBase.aspectState.sAspectStateBase.byAspectStateId = 255;
	this->m_sCharState.sCharStateBase.byStateID = CHARSTATE_SPAWNING;
	this->m_sCharState.sCharStateBase.vCurDir.x = this->vCurDir.x;
	this->m_sCharState.sCharStateBase.vCurDir.y = this->vCurDir.y;
	this->m_sCharState.sCharStateBase.vCurDir.z = this->vCurDir.z;
	this->m_sCharState.sCharStateBase.vCurLoc.x = this->vCurLoc.x;
	this->m_sCharState.sCharStateBase.vCurLoc.y = this->vCurLoc.y;
	this->m_sCharState.sCharStateBase.vCurLoc.z = this->vCurLoc.z;
	this->m_sCharState.sCharStateBase.dwConditionFlag = 0;
	this->m_sCharState.sCharStateBase.aspectState.sAspectStateDetail.sGreatNamek.bySourceGrade = 0;
	this->m_sCharState.sCharStateBase.aspectState.sAspectStateDetail.sKaioken.bySourceGrade = 0;
	this->m_sCharState.sCharStateBase.aspectState.sAspectStateDetail.sPureMajin.bySourceGrade = 0;
	this->m_sCharState.sCharStateBase.aspectState.sAspectStateDetail.sSuperSaiyan.bySourceGrade = 0;
	this->m_sCharState.sCharStateBase.aspectState.sAspectStateDetail.sVehicle.idVehicleTblidx = 0;
	this->currentWorld = sPC.worldTblidx;
	this->currentWorldID = sPC.worldId;
	this->m_Warehouse.Create(&this->m_sPcProfile.dwZenny, sPC.dwMoneyBank);
}

void CGameCharacter::LoadAttributes(sAVATAR_ATTRIBUTE* sAttr, sPC_TBLDAT* pPcTbl)
{
	sAttr->wBaseMaxEP = pPcTbl->wBasic_EP;
	sAttr->wBaseMaxLP = pPcTbl->wBasic_LP;
	sAttr->wBaseMaxRP = pPcTbl->wBasic_RP;
	sAttr->byBaseStr = pPcTbl->byStr;
	sAttr->byBaseFoc = pPcTbl->byFoc;
	sAttr->byBaseSol = pPcTbl->bySol;
	sAttr->byBaseDex = pPcTbl->byDex;
	sAttr->byBaseCon = pPcTbl->byCon;
	sAttr->byBaseEng = pPcTbl->byEng;
	sAttr->fBaseAttackRange = pPcTbl->fAttack_Range;
	sAttr->wBaseAttackRate = pPcTbl->wAttack_Rate;
	sAttr->wBaseAttackSpeedRate = pPcTbl->wAttack_Speed_Rate;
	sAttr->wBaseBlockRate = pPcTbl->wBlock_Rate;
	sAttr->wBaseCurseSuccessRate = pPcTbl->wCurse_Success_Rate;
	sAttr->wBaseCurseToleranceRate = pPcTbl->wCurse_Tolerance_Rate;
	sAttr->wBaseDodgeRate = pPcTbl->wDodge_Rate;
	sAttr->wBasePhysicalOffence = (pPcTbl->byLevel_Up_Physical_Offence * this->m_sPcProfile.byLevel);
	sAttr->wBasePhysicalDefence = (pPcTbl->byLevel_Up_Physical_Defence * this->m_sPcProfile.byLevel);
	sAttr->wBaseEnergyOffence = (pPcTbl->byLevel_Up_Energy_Offence * this->m_sPcProfile.byLevel);
	sAttr->wBaseEnergyDefence = (pPcTbl->byLevel_Up_Energy_Defence * this->m_sPcProfile.byLevel);

	sAttr->wBaseMaxEP += (pPcTbl->byLevel_Up_EP * this->m_sPcProfile.byLevel);
	sAttr->wBaseMaxLP += (pPcTbl->byLevel_Up_LP * this->m_sPcProfile.byLevel);
	sAttr->wBaseMaxRP += (pPcTbl->byLevel_Up_RP * this->m_sPcProfile.byLevel);
	sAttr->byBaseStr += (BYTE)(pPcTbl->fLevel_Up_Str * this->m_sPcProfile.byLevel);
	sAttr->byBaseDex += (BYTE)(pPcTbl->fLevel_Up_Dex * this->m_sPcProfile.byLevel);
	sAttr->byBaseFoc += (BYTE)(pPcTbl->fLevel_Up_Foc * this->m_sPcProfile.byLevel);
	sAttr->byBaseEng += (BYTE)(pPcTbl->fLevel_Up_Eng * this->m_sPcProfile.byLevel);
	sAttr->byBaseCon += (BYTE)(pPcTbl->fLevel_Up_Con * this->m_sPcProfile.byLevel);
	sAttr->byBaseSol += (BYTE)(pPcTbl->fLevel_Up_Sol * this->m_sPcProfile.byLevel);

	sAttr->wBaseMaxLP += (WORD)(((BYTE)sAttr->byBaseCon * this->m_sPcProfile.byLevel) * 4.7);
	sAttr->wBaseMaxEP += (WORD)(((BYTE)sAttr->byBaseEng * this->m_sPcProfile.byLevel) * 4.7);
	sAttr->fLastRunSpeed = pPcTbl->fChild_Run_Speed;
	sAttr->wLastMaxEP = sAttr->wBaseMaxEP;
	sAttr->wLastMaxLP = sAttr->wBaseMaxLP;
	sAttr->wLastMaxRP = sAttr->wBaseMaxRP;
	sAttr->byLastStr = sAttr->byBaseStr;
	sAttr->byLastDex = sAttr->byBaseDex;
	sAttr->byLastFoc = sAttr->byBaseFoc;
	sAttr->byLastEng = sAttr->byBaseEng;
	sAttr->byLastCon = sAttr->byBaseCon;
	sAttr->byLastSol = sAttr->byBaseSol;
	sAttr->wLastPhysicalOffence = sAttr->wBasePhysicalOffence;
	sAttr->wLastPhysicalDefence = sAttr->wBasePhysicalDefence;
	sAttr->wLastEnergyOffence = sAttr->wBaseEnergyOffence;
	sAttr->wLastEnergyDefence = sAttr->wBaseEnergyDefence;
	/////////////////TODO: SEE HOW IT FILL THAT
	sAttr->wBaseLpRegen = 1;
	sAttr->wLastLpRegen = 1;
	sAttr->wBaseLpSitdownRegen = 1;
	sAttr->wLastLpSitdownRegen = 1;
	sAttr->wBaseLpBattleRegen = 1;
	sAttr->wLastLpBattleRegen = 1;

	sAttr->wBaseEpRegen = 1;
	sAttr->wLastEpRegen = 1;
	sAttr->wBaseEpSitdownRegen = 1;
	sAttr->wLastEpSitdownRegen = 1;
	sAttr->wBaseEpBattleRegen = 1;
	sAttr->wLastEpBattleRegen = 1;

	sAttr->wBaseRpRegen = 1;
	sAttr->wLastRpRegen = 1;
	sAttr->wLastRpDimimutionRate = 1;
	sAttr->wBaseCurseSuccessRate = 1;
	sAttr->wLastCurseSuccessRate = 1;
	sAttr->wBaseCurseToleranceRate = 1;
	sAttr->wLastCurseToleranceRate = 1;
	sAttr->fCastingTimeChangePercent = 1.0f;
	sAttr->fCoolTimeChangePercent = 1.0f;
	sAttr->fKeepTimeChangePercent = 1.0f;
	sAttr->fDotValueChangePercent = 1.0f;
	sAttr->fDotTimeChangeAbsolute = 1.0f;
	sAttr->fRequiredEpChangePercent = 1.0f;

	sAttr->fHonestOffence = 1.0f;
	sAttr->fHonestDefence = 1.0f;
	sAttr->fStrangeOffence = 1.0f;
	sAttr->fStrangeDefence = 1.0f;
	sAttr->fWildOffence = 1.0f;
	sAttr->fWildDefence = 1.0f;
	sAttr->fEleganceOffence = 1.0f;
	sAttr->fEleganceDefence = 1.0f;
	sAttr->fFunnyOffence = 1.0f;
	sAttr->fFunnyDefence = 1.0f;

	sAttr->wParalyzeToleranceRate = 1;
	sAttr->wTerrorToleranceRate = 1;
	sAttr->wConfuseToleranceRate = 1;
	sAttr->wStoneToleranceRate = 1;
	sAttr->wCandyToleranceRate = 1;

	sAttr->fParalyzeKeepTimeDown = 1.0f;
	sAttr->fTerrorKeepTimeDown = 1.0f;
	sAttr->fConfuseKeepTimeDown = 1.0f;
	sAttr->fStoneKeepTimeDown = 1.0f;
	sAttr->fCandyKeepTimeDown = 1.0f;
	sAttr->fBleedingKeepTimeDown = 1.0f;
	sAttr->fPoisonKeepTimeDown = 1.0f;
	sAttr->fStomachacheKeepTimeDown = 1.0f;

	sAttr->fCriticalBlockSuccessRate = 1.0f;

	sAttr->wGuardRate = 1;

	sAttr->fSkillDamageBlockModeSuccessRate = 1.0f;
	sAttr->fCurseBlockModeSuccessRate = 1.0f;
	sAttr->fKnockdownBlockModeSuccessRate = 1.0f;
	sAttr->fHtbBlockModeSuccessRate = 1.0f;

	sAttr->fSitDownLpRegenBonusRate = 1.0f;
	sAttr->fSitDownEpRegenBonusRate = 1.0f;

	sAttr->fPhysicalCriticalDamageBonusRate = 1.0f;
	sAttr->fEnergyCriticalDamageBonusRate = 1.0f;

	sAttr->fItemUpgradeBonusRate = 1.0f;
	sAttr->fItemUpgradeBreakBonusRate = 1.0f;
	///////////////////////////////////////////////////////////////
	this->m_sAttributeLink = CNtlAvatar::ConvertAttributeToAttributeLink(sAttr);
	this->m_sPcProfile.wCurEP = sAttr->wLastMaxEP;
	this->m_sPcProfile.wCurLP = sAttr->wLastMaxLP;
	this->m_sPcProfile.wCurRP = sAttr->wLastMaxRP;
}

void CGameCharacter::CreateInventory(sITEM_DATA* itDt, BYTE byCount)
{
	this->m_Inventory.Create(itDt, byCount);
}

void CGameCharacter::CreateSkills(sSKILL_DATA* sk, BYTE byCount)
{
	this->m_SkillContainer.Create(sk, byCount);
}

void CGameCharacter::CreateHTB(sHTB_SKILL_DATA* pHtbData, BYTE byHtbCount)
{
	this->m_SkillContainer.CreateHTB(pHtbData, byHtbCount);
}

void CGameCharacter::CreateBuff(sBUFF_DATA* buffDt, BYTE byBuffCount)
{
	this->m_SkillContainer.CreateBuff(buffDt, byBuffCount);
}

void CGameCharacter::CreateHoiPoiMix(sRECIPE_DATA* recipe, BYTE byCount)
{
	this->m_HoipoiMix.Create(recipe, byCount);
}

//Dont need do checks here, only in gamesession
void CGameCharacter::IncreaseRP()
{
	this->m_sPcProfile.wCurRP += (this->m_sPcProfile.avatarAttribute.wBaseRpRegen + this->m_sPcProfile.avatarAttribute.wLastRpRegen);
	if (this->m_sPcProfile.wCurRP > this->m_sPcProfile.avatarAttribute.wLastMaxRP)
		this->m_sPcProfile.wCurRP = this->m_sPcProfile.avatarAttribute.wLastMaxRP;
}

bool CGameCharacter::IsCharCanCharge()
{
	if (this->m_sPcProfile.wCurRP <= this->m_sPcProfile.avatarAttribute.wLastMaxRP)
		return true;
	return false;
}

void CGameCharacter::SetWarFog(DWORD idx)
{
	if (idx > NTL_MAX_COUNT_WAR_FOG)
		return;

	if (!m_warFog.achWarFogFlag)
		return;

	DWORD uiArrayPos = idx / 8;
	BYTE byCurBit = (BYTE)(idx % 8);

	m_warFog.achWarFogFlag[uiArrayPos] |= 0x01ui8 << byCurBit;
}

void CGameCharacter::SendCharInfo()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_AVATAR_CHAR_INFO));
	sGU_AVATAR_CHAR_INFO* res = (sGU_AVATAR_CHAR_INFO*)packet.GetPacketData();
	res->handle = this->GetHObject();
	memcpy(&res->sPcProfile, &this->m_sPcProfile, sizeof(sPC_PROFILE));
	memcpy(&res->sCharState, &this->m_sCharState, sizeof(sCHARSTATE));
	res->wCharStateSize = sizeof(sCHARSTATE_BASE);
	res->wOpCode = GU_AVATAR_CHAR_INFO;
	packet.SetPacketLen(sizeof(sGU_AVATAR_CHAR_INFO));	
	app->Send(app->GetHSession(this->m_charID), &packet);
}

void CGameCharacter::SendItemInfo()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_AVATAR_ITEM_INFO));
	sGU_AVATAR_ITEM_INFO* res = (sGU_AVATAR_ITEM_INFO*)packet.GetPacketData();	
	res->byBeginCount = 0;
	res->byItemCount = this->m_Inventory.GetInvenCount();
	for (int i = 0; i < res->byItemCount; i++)
	{
		memcpy(res->aItemProfile[i].awchMaker, this->GetInventory()->GetItemInventory()[i].awchMaker, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1);
		res->aItemProfile[i].aOptionTblidx[0] = this->GetInventory()->GetItemInventory()[i].aOptionTblidx[0];
		res->aItemProfile[i].aOptionTblidx[1] = this->GetInventory()->GetItemInventory()[i].aOptionTblidx[1];
		res->aItemProfile[i].bNeedToIdentify = this->GetInventory()->GetItemInventory()[i].bNeedToIdentify;
		res->aItemProfile[i].byBattleAttribute = this->GetInventory()->GetItemInventory()[i].byBattleAttribute;
		res->aItemProfile[i].byCurDur = this->GetInventory()->GetItemInventory()[i].byCurDur;
		res->aItemProfile[i].byDurationType = this->GetInventory()->GetItemInventory()[i].byDurationType;
		res->aItemProfile[i].byGrade = this->GetInventory()->GetItemInventory()[i].byGrade;
		res->aItemProfile[i].byPlace = this->GetInventory()->GetItemInventory()[i].byPlace;
		res->aItemProfile[i].byPos = this->GetInventory()->GetItemInventory()[i].byPos;
		res->aItemProfile[i].byRank = this->GetInventory()->GetItemInventory()[i].byRank;
		res->aItemProfile[i].byRestrictType = this->GetInventory()->GetItemInventory()[i].byRestrictType;
		res->aItemProfile[i].byStackcount = this->GetInventory()->GetItemInventory()[i].byStackcount;
		res->aItemProfile[i].handle = this->GetInventory()->GetItemInventory()[i].handle;
		res->aItemProfile[i].nUseEndTime = this->GetInventory()->GetItemInventory()[i].nUseEndTime;
		res->aItemProfile[i].nUseStartTime = this->GetInventory()->GetItemInventory()[i].nUseStartTime;
		res->aItemProfile[i].tblidx = this->GetInventory()->GetItemInventory()[i].tblidx;
	}
	res->wOpCode = GU_AVATAR_ITEM_INFO;
	packet.AdjustPacketLen(sizeof(sNTLPACKETHEADER) + (2 * sizeof(BYTE)) + (res->byItemCount * sizeof(sITEM_PROFILE)));
	app->Send(app->GetHSession(this->m_charID), &packet);
}

void CGameCharacter::SendSkillInfo()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_AVATAR_SKILL_INFO));
	sGU_AVATAR_SKILL_INFO* res = (sGU_AVATAR_SKILL_INFO*)packet.GetPacketData();
	res->bySkillCount = this->GetSkillContainer()->GetSkillCount();
	for (int i = 0; i < res->bySkillCount; i++)
	{
		res->aSkillInfo[i].bIsRpBonusAuto = this->GetSkillContainer()->GetSkillInfo()[i].bIsRpBonusAuto;
		res->aSkillInfo[i].byRpBonusType = this->GetSkillContainer()->GetSkillInfo()[i].byRpBonusType;
		res->aSkillInfo[i].bySlotId = this->GetSkillContainer()->GetSkillInfo()[i].bySlotId;
		res->aSkillInfo[i].dwTimeRemaining = this->GetSkillContainer()->GetSkillInfo()[i].dwTimeRemaining;
		res->aSkillInfo[i].nExp = this->GetSkillContainer()->GetSkillInfo()[i].nExp;
		res->aSkillInfo[i].tblidx = this->GetSkillContainer()->GetSkillInfo()[i].tblidx;
	}
	res->wOpCode = GU_AVATAR_SKILL_INFO;
	packet.AdjustPacketLen(sizeof(sNTLPACKETHEADER) + (2 * sizeof(BYTE)) + (res->bySkillCount * sizeof(sSKILL_INFO)));
	app->Send(app->GetHSession(this->m_charID), &packet);
}

void CGameCharacter::SendHTBSkillInfo()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_AVATAR_HTB_INFO));
	sGU_AVATAR_HTB_INFO* res = (sGU_AVATAR_HTB_INFO*)packet.GetPacketData();
	res->byHTBSkillCount = this->GetSkillContainer()->GetHTBSkillCount();
	for (int i = 0; i < res->byHTBSkillCount; i++)
	{
		res->aHTBSkillnfo[i].bySlotId = this->GetSkillContainer()->GetHTBSkillInfo()[i].bySlotId;
		res->aHTBSkillnfo[i].dwTimeRemaining = this->GetSkillContainer()->GetHTBSkillInfo()[i].dwTimeRemaining;
		res->aHTBSkillnfo[i].skillId = this->GetSkillContainer()->GetHTBSkillInfo()[i].skillId;
	}
	res->wOpCode = GU_AVATAR_HTB_INFO;
	packet.SetPacketLen(sizeof(sGU_AVATAR_HTB_INFO));	
	app->Send(app->GetHSession(this->m_charID), &packet);
}

void CGameCharacter::SendBuffInfo()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_AVATAR_BUFF_INFO));
	sGU_AVATAR_BUFF_INFO* res = (sGU_AVATAR_BUFF_INFO*)packet.GetPacketData();
	res->byBuffCount = this->GetSkillContainer()->GetBuffCount();
	memcpy(res->aBuffInfo, this->GetSkillContainer()->GetBuffInfo(), sizeof(sBUFF_INFO) * (NTL_MAX_BLESS_BUFF_CHARACTER_HAS + NTL_MAX_CURSE_BUFF_CHARACTER_HAS));
	res->wOpCode = GU_AVATAR_BUFF_INFO;
	packet.SetPacketLen(sizeof(sGU_AVATAR_BUFF_INFO));
	app->Send(app->GetHSession(this->m_charID), &packet);
}

void CGameCharacter::SendQuickSlotInfo()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_QUICK_SLOT_INFO));
	sGU_QUICK_SLOT_INFO* res = (sGU_QUICK_SLOT_INFO*)packet.GetPacketData();
	res->byQuickSlotCount = this->GetSkillContainer()->GetQuickSlotCount();
	for (int i = 0; i < res->byQuickSlotCount; i++)
	{
		res->asQuickSlotData[i].bySlot = this->GetSkillContainer()->GetQuickSlotData()[i].bySlot;
		res->asQuickSlotData[i].byType = this->GetSkillContainer()->GetQuickSlotData()[i].byType;
		res->asQuickSlotData[i].hItem = this->GetSkillContainer()->GetQuickSlotData()[i].hItem;
		res->asQuickSlotData[i].tblidx = this->GetSkillContainer()->GetQuickSlotData()[i].tblidx;
	}
	res->wOpCode = GU_QUICK_SLOT_INFO;
	packet.AdjustPacketLen(sizeof(sNTLPACKETHEADER) + (2 * sizeof(BYTE)) + (res->byQuickSlotCount * (sizeof(sQUICK_SLOT_DATA))));
	app->Send(app->GetHSession(this->m_charID), &packet);
}

void CGameCharacter::SendRecipeInfo()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_HOIPOIMIX_ITEM_RECIPE_INFO));
	sGU_HOIPOIMIX_ITEM_RECIPE_INFO* res = (sGU_HOIPOIMIX_ITEM_RECIPE_INFO*)packet.GetPacketData();
	res->byCount = this->GetHoipoiMix()->GetRecipeCount();
	for (int i = 0; i < res->byCount; i++)
	{
		if (this->GetHoipoiMix()->GetRecipeData(i) != NULL)
		{
			res->asRecipeData[i].byRecipeType = this->GetHoipoiMix()->GetRecipeData(i)->byRecipeType;
			res->asRecipeData[i].recipeTblidx = this->GetHoipoiMix()->GetRecipeData(i)->recipeTblidx;
		}
	}
	res->wOpCode = GU_HOIPOIMIX_ITEM_RECIPE_INFO;
	packet.SetPacketLen(sizeof(sGU_HOIPOIMIX_ITEM_RECIPE_INFO));
	app->Send(app->GetHSession(this->m_charID), &packet);
}

void CGameCharacter::SendWorldInfo(TBLIDX world, WORLDID id, eGAMERULE_TYPE ruleType)
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_AVATAR_WORLD_INFO));
	sGU_AVATAR_WORLD_INFO* res = (sGU_AVATAR_WORLD_INFO*)packet.GetPacketData();
	res->vCurLoc = this->vCurLoc;
	res->vCurDir = this->vCurDir;
	//Load from db in the future
	res->byDojoCount = 0;
	res->sDojoData;
	res->worldInfo.hTriggerObjectOffset = HTRIGGER_OBJECT_WORLD;
	if (IsPlayerInTutorial())
		res->worldInfo.sRuleInfo.byRuleType = GAMERULE_TUTORIAL;
	else
		res->worldInfo.sRuleInfo.byRuleType = ruleType;
	if (ruleType == GAMERULE_TIMEQUEST)
	{
		res->worldInfo.sRuleInfo.sTimeQuestRuleInfo;
	}
	if ((world == 0) && (id == 0))
	{
		res->worldInfo.tblidx = this->currentWorld;
		res->worldInfo.worldID = this->currentWorldID;
	}
	else
	{
		res->worldInfo.tblidx = world;
		res->worldInfo.worldID = id;
	}	
	res->wOpCode = GU_AVATAR_WORLD_INFO;
	packet.SetPacketLen(sizeof(sGU_AVATAR_WORLD_INFO));
	app->Send(app->GetHSession(this->m_charID), &packet);
}

//This will be util when we are going to use the Dragon Ball
void CGameCharacter::SendWorldZoneInfo(bool IsDark, ZONEID zd,bool IsSendAll)
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_AVATAR_ZONE_INFO));
	sGU_AVATAR_ZONE_INFO* res = (sGU_AVATAR_ZONE_INFO*)packet.GetPacketData();
	res->zoneInfo.bIsDark = IsDark;
	res->zoneInfo.zoneId = zd;
	res->wOpCode = GU_AVATAR_ZONE_INFO;
	packet.SetPacketLen(sizeof(sGU_AVATAR_ZONE_INFO));
	if (IsSendAll)
		app->SendAll(&packet);
	else
		app->Send(app->GetHSession(this->m_charID), &packet);
}

void CGameCharacter::SendCharInfoEnd()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_AVATAR_INFO_END));
	sGU_AVATAR_INFO_END* res = (sGU_AVATAR_INFO_END*)packet.GetPacketData();
	res->wOpCode = GU_AVATAR_INFO_END;
	packet.SetPacketLen(sizeof(sGU_AVATAR_INFO_END));
	app->Send(app->GetHSession(this->m_charID), &packet);
}

void CGameCharacter::SendCharStateBase(bool bFightMode, eCHARSTATE state, eCHARCONDITION c, DWORD stateTime)
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_UPDATE_CHAR_STATE));
	sGU_UPDATE_CHAR_STATE* res = (sGU_UPDATE_CHAR_STATE*)packet.GetPacketData();
	res->handle = this->GetHObject();	
	res->sCharState.sCharStateBase.bFightMode = bFightMode;
	res->sCharState.sCharStateBase.byStateID = state;
	res->sCharState.sCharStateBase.vCurDir.x = this->vCurDir.x;
	res->sCharState.sCharStateBase.vCurDir.y = this->vCurDir.y;
	res->sCharState.sCharStateBase.vCurDir.z = this->vCurDir.z;
	res->sCharState.sCharStateBase.vCurLoc.x = this->vCurLoc.x;
	res->sCharState.sCharStateBase.vCurLoc.y = this->vCurLoc.y;
	res->sCharState.sCharStateBase.vCurLoc.z = this->vCurLoc.z;
	res->sCharState.sCharStateBase.dwConditionFlag = c;
	res->sCharState.sCharStateBase.dwStateTime = stateTime;	
	res->wOpCode = GU_UPDATE_CHAR_STATE;
	packet.SetPacketLen(sizeof(sGU_UPDATE_CHAR_STATE));
	app->SendAll(&packet);
}

void CGameCharacter::SendCharStateDetailed(sCHARSTATE_BASE sBase, sCHARSTATE_DETAIL sDetail)
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_UPDATE_CHAR_STATE));
	sGU_UPDATE_CHAR_STATE* res = (sGU_UPDATE_CHAR_STATE*)packet.GetPacketData();
	res->handle = this->GetHObject();
	res->sCharState.sCharStateBase = sBase;
	res->sCharState.sCharStateDetail = sDetail;	
	res->wOpCode = GU_UPDATE_CHAR_STATE;
	packet.SetPacketLen(sizeof(sGU_UPDATE_CHAR_STATE));
	app->SendAll(&packet);
}
void CGameCharacter::SetPosition(float x, float y, float z)
{
	this->vCurLoc.x = x;
	this->vCurLoc.y = y;
	this->vCurLoc.z = z;
}
void CGameCharacter::SetDir(float x, float y, float z)
{
	this->vCurDir.x = x;
	this->vCurDir.y = y;
	this->vCurDir.z = z;
}

void CGameCharacter::SendWarFogInfo()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_WAR_FOG_INFO));
	sGU_WAR_FOG_INFO* res = (sGU_WAR_FOG_INFO*)packet.GetPacketData();
	memcpy(res->abyWarFogInfo, this->m_warFog.achWarFogFlag, NTL_MAX_SIZE_WAR_FOG);
	res->wOpCode = GU_WAR_FOG_INFO;
	packet.SetPacketLen(sizeof(sGU_WAR_FOG_INFO));
	app->Send(app->GetHSession(this->m_charID), &packet);
}

void CGameCharacter::SendCreatePlayer()
{	
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	//Just to not create every time a new handle.
	//PS: if this thing creates a new Handle, then you must delete a handle in community server and resend all data again
	if (GetHObject() == INVALID_TBLIDX)
		this->m_SerialHandle = app->AcquireSerialId();
	CNtlPacket packet(sizeof(sGU_OBJECT_CREATE));
	sGU_OBJECT_CREATE* res = (sGU_OBJECT_CREATE*)packet.GetPacketData();
	res->handle = GetHObject();
	res->sObjectInfo.objType = OBJTYPE_PC;
	memcpy(res->sObjectInfo.pcBrief.awchName, this->m_sPcProfile.awchName, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1);
	res->sObjectInfo.pcBrief.bIsAdult = this->m_sPcProfile.bIsAdult;
	res->sObjectInfo.pcBrief.byLevel = this->m_sPcProfile.byLevel;
	res->sObjectInfo.pcBrief.charId = GetCharID();
	res->sObjectInfo.pcBrief.fSpeed = this->m_sPcProfile.avatarAttribute.fLastRunSpeed;//or 7.0f
	res->sObjectInfo.pcBrief.tblidx = this->m_sPcProfile.tblidx;
	res->sObjectInfo.pcBrief.wCurEP = this->m_sPcProfile.wCurEP;
	res->sObjectInfo.pcBrief.wCurLP = this->m_sPcProfile.wCurLP;
	res->sObjectInfo.pcBrief.wMaxEP = this->m_sPcProfile.avatarAttribute.wLastMaxEP;
	res->sObjectInfo.pcBrief.wMaxLP = this->m_sPcProfile.avatarAttribute.wLastMaxLP;
	res->sObjectInfo.pcBrief.wAttackSpeedRate = this->m_sPcProfile.avatarAttribute.wLastAttackSpeedRate;
	res->sObjectInfo.pcBrief.sPcShape.byFace = this->m_sPcProfile.sPcShape.byFace;
	res->sObjectInfo.pcBrief.sPcShape.byHair = this->m_sPcProfile.sPcShape.byHair;
	res->sObjectInfo.pcBrief.sPcShape.byHairColor = this->m_sPcProfile.sPcShape.byHairColor;
	res->sObjectInfo.pcBrief.sPcShape.bySkinColor = this->m_sPcProfile.sPcShape.bySkinColor;
	res->sObjectInfo.pcBrief.sMarking.byCode = this->m_sPcProfile.sMarking.byCode;
	//TODO: Add Guild Here like a GetGuild()->GetDogi(),GetGuild()->GetMark(),GetGuild()->GetName() something like that
	//res->sObjectInfo.pcBrief.sDogi;
	//res->sObjectInfo.pcBrief.sMark;
	//res->sObjectInfo.pcBrief.wszGuildName	
	memset(res->sObjectInfo.pcBrief.sItemBrief, 0xFFFFFFFFF, sizeof(sITEM_BRIEF)*EQUIP_SLOT_TYPE_COUNT);
	for (int i = 0; i < this->GetInventory()->GetEquippedCount(); i++)
	{
		if ((this->GetInventory()->GetItemEquipped()[i].tblidx != INVALID_TBLIDX)&&
			(this->GetInventory()->GetItemEquipped()[i].tblidx != 0))
		{
			res->sObjectInfo.pcBrief.sItemBrief[i].aOptionTblidx[0] = this->GetInventory()->GetItemEquipped()[i].aOptionTblidx[0];
			res->sObjectInfo.pcBrief.sItemBrief[i].aOptionTblidx[1] = this->GetInventory()->GetItemEquipped()[i].aOptionTblidx[1];
			res->sObjectInfo.pcBrief.sItemBrief[i].byBattleAttribute = this->GetInventory()->GetItemEquipped()[i].byBattleAttribute;
			res->sObjectInfo.pcBrief.sItemBrief[i].byGrade = this->GetInventory()->GetItemEquipped()[i].byGrade;
			res->sObjectInfo.pcBrief.sItemBrief[i].byRank = this->GetInventory()->GetItemEquipped()[i].byRank;
			res->sObjectInfo.pcBrief.sItemBrief[i].tblidx = this->GetInventory()->GetItemEquipped()[i].tblidx;
		}
		else
		{
			res->sObjectInfo.pcBrief.sItemBrief[i].tblidx = INVALID_TBLIDX;
		}
	}
	memcpy(&res->sObjectInfo.pcState, &this->m_sCharState, sizeof(sCHARSTATE));
	res->wOpCode = GU_OBJECT_CREATE;
	packet.SetPacketLen(sizeof(sGU_OBJECT_CREATE));
	app->SendAll(&packet,app->GetHSession(GetCharID()));//Tell to everybody that you spawned ow yeah
}


void CGameCharacter::SendCommunityKeyReq(BYTE* abyKey)
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_AUTH_KEY_FOR_COMMUNITY_SERVER_RES));
	sGU_AUTH_KEY_FOR_COMMUNITY_SERVER_RES* res = (sGU_AUTH_KEY_FOR_COMMUNITY_SERVER_RES*)packet.GetPacketData();
	memcpy(res->abyAuthKey, abyKey, NTL_MAX_SIZE_AUTH_KEY);
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = GU_AUTH_KEY_FOR_COMMUNITY_SERVER_RES;
	packet.SetPacketLen(sizeof(sGU_AUTH_KEY_FOR_COMMUNITY_SERVER_RES));
	app->SendAll(&packet);
	memcpy(this->abyAuthKey, abyKey, NTL_MAX_SIZE_AUTH_KEY);
}

void CGameCharacter::SendCharMove(sVECTOR3* Dir, sVECTOR3* Pos, BYTE byMoveDir)
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_CHAR_MOVE));
	sGU_CHAR_MOVE* res = (sGU_CHAR_MOVE*)packet.GetPacketData();
	res->handle = GetHObject();
	res->vCurDir.x = Dir->x;
	res->vCurDir.y = 0.0f;
	res->vCurDir.z = Dir->z;
	res->vCurLoc.x = Pos->x;
	res->vCurLoc.y = Pos->y;
	res->vCurLoc.z = Pos->z;
	res->byMoveDirection = byMoveDir;
	res->byMoveFlag = byMoveDir;
	res->wOpCode = GU_CHAR_MOVE;
	packet.SetPacketLen(sizeof(sGU_CHAR_MOVE));
	app->SendAll(&packet);
}

void CGameCharacter::SendCharMoveSync(sVECTOR3* Dir, sVECTOR3* Pos)
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_CHAR_MOVE_SYNC));
	sGU_CHAR_MOVE_SYNC* res = (sGU_CHAR_MOVE_SYNC*)packet.GetPacketData();
	res->handle = GetHObject();
	res->vCurDir.x = Dir->x;
	res->vCurDir.y = Dir->y;
	res->vCurDir.z = Dir->z;
	res->vCurLoc.x = Pos->x;
	res->vCurLoc.y = Pos->y;
	res->vCurLoc.z = Pos->z;
	res->dwTimeStamp = 0;
	res->wOpCode = GU_CHAR_MOVE_SYNC;
	packet.SetPacketLen(sizeof(sGU_CHAR_MOVE_SYNC));
	app->SendAll(&packet);
}

void CGameCharacter::SendCharChangeDirFloating(sVECTOR3* vect3, BYTE byMoveDir)
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_CHAR_CHANGE_DIRECTION_ON_FLOATING));
	sGU_CHAR_CHANGE_DIRECTION_ON_FLOATING* res = (sGU_CHAR_CHANGE_DIRECTION_ON_FLOATING*)packet.GetPacketData();
	res->hSubject = GetHObject();
	res->vCurDir.x = vect3->x;
	res->vCurDir.y = vect3->y;
	res->vCurDir.z = -vect3->z;
	res->byMoveDirection = byMoveDir;
	res->wOpCode = GU_CHAR_CHANGE_DIRECTION_ON_FLOATING;
	packet.SetPacketLen(sizeof(sGU_CHAR_CHANGE_DIRECTION_ON_FLOATING));
	app->SendAll(&packet);
}

void CGameCharacter::SendCharChangeHeading(sVECTOR3* dir, sVECTOR3* pos)
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_CHAR_CHANGE_HEADING));
	sGU_CHAR_CHANGE_HEADING* res = (sGU_CHAR_CHANGE_HEADING*)packet.GetPacketData();
	res->handle = GetHObject();
	res->vNewHeading.x = dir->x;
	res->vNewHeading.y = dir->y;
	res->vNewHeading.z = dir->z;
	res->vNewLoc.x = pos->x;
	res->vNewLoc.y = pos->y;
	res->vNewLoc.z = pos->z;
	res->wOpCode = GU_CHAR_CHANGE_HEADING;
	packet.SetPacketLen(sizeof(sGU_CHAR_CHANGE_HEADING));
	app->SendAll(&packet);
}

void CGameCharacter::SendCharJump(sVECTOR3* Dir, BYTE byMoveDir)
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_CHAR_JUMP));
	sGU_CHAR_JUMP* res = (sGU_CHAR_JUMP*)packet.GetPacketData();
	res->handle = GetHObject();
	res->byMoveDirection = byMoveDir;
	res->vCurrentHeading.x = GetPosition().x;
	res->vCurrentHeading.y = GetPosition().y;
	res->vCurrentHeading.z = GetPosition().z;
	res->vJumpDir.x = Dir->x;
	res->vJumpDir.y = Dir->y;
	res->vJumpDir.z = Dir->z;
	res->wOpCode = GU_CHAR_JUMP;
	packet.SetPacketLen(sizeof(sGU_CHAR_JUMP));
	app->SendAll(&packet);
}

void CGameCharacter::SendCharJumpEnd()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_CHAR_JUMP_END));
	sGU_CHAR_JUMP_END* res = (sGU_CHAR_JUMP_END*)packet.GetPacketData();
	res->handle = GetHObject();
	res->wOpCode = GU_CHAR_JUMP_END;
	packet.SetPacketLen(sizeof(sGU_CHAR_JUMP_END));
	app->SendAll(&packet);
}

//This will be only used if the player clicks with mouse
void CGameCharacter::SendCharDestMove(sVECTOR3 vDestLoc, DWORD dwTimeStamp)
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_CHAR_DEST_MOVE));
	sGU_CHAR_DEST_MOVE* res = (sGU_CHAR_DEST_MOVE*)packet.GetPacketData();	
	res->handle = GetHObject();
	res->vCurLoc.x = this->vCurLoc.x;
	res->vCurLoc.y = this->vCurLoc.y;
	res->vCurLoc.z = this->vCurLoc.z;	
	res->avDestLoc[0].x = vDestLoc.x;
	res->avDestLoc[0].y = vDestLoc.y;
	res->avDestLoc[0].z = vDestLoc.z;
	res->byMoveFlag = NTL_MOVE_MOUSE_MOVEMENT;
	res->dwTimeStamp = dwTimeStamp;
	res->bHaveSecondDestLoc = false;
	res->byDestLocCount = 1;
	res->wOpCode = GU_CHAR_DEST_MOVE;
	packet.SetPacketLen(sizeof(sGU_CHAR_DEST_MOVE));
	app->SendAll(&packet);
}

void CGameCharacter::SendCharDestroyObj()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_OBJECT_DESTROY));
	sGU_OBJECT_DESTROY* res = (sGU_OBJECT_DESTROY*)packet.GetPacketData();
	res->handle = GetHObject();
	res->wOpCode = GU_OBJECT_DESTROY;
	packet.SetPacketLen(sizeof(sGU_OBJECT_DESTROY));
	app->SendAll(&packet);
}

void CGameCharacter::SendCharFollowMove(float fDistance, BYTE byMovementDir, BYTE byMovementReason, DWORD dwTimeStamp, HOBJECT hTarget)
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_CHAR_FOLLOW_MOVE));
	sGU_CHAR_FOLLOW_MOVE* res = (sGU_CHAR_FOLLOW_MOVE*)packet.GetPacketData();
	res->byMoveFlag = byMovementDir;
	res->byMovementReason = byMovementReason;
	res->dwTimeStamp = dwTimeStamp;
	res->fDistance = fDistance;
	res->hTarget = hTarget;
	res->handle = GetHObject();
	res->wOpCode = GU_CHAR_FOLLOW_MOVE;
	packet.SetPacketLen(sizeof(sGU_CHAR_FOLLOW_MOVE));
	app->SendAll(&packet);
}

void CGameCharacter::SendCharToggSitDown()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_CHAR_SITDOWN));
	sGU_CHAR_SITDOWN* res = (sGU_CHAR_SITDOWN*)packet.GetPacketData();
	res->handle = GetHObject();
	res->wOpCode = GU_CHAR_SITDOWN;
	packet.SetPacketLen(sizeof(sGU_CHAR_SITDOWN));
	app->SendAll(&packet);
}

void CGameCharacter::SendCharToggStandUp()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_CHAR_STANDUP));
	sGU_CHAR_STANDUP* res = (sGU_CHAR_STANDUP*)packet.GetPacketData();
	res->handle = GetHObject();
	res->wOpCode = GU_CHAR_STANDUP;
	packet.SetPacketLen(sizeof(sGU_CHAR_STANDUP));
	app->SendAll(&packet);
}

void CGameCharacter::SendCharRevivalPopoStone()
{
	SendCharDestroyObj();
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	sWORLD_TBLDAT* pWorldData = (sWORLD_TBLDAT*)app->GetTableContainer()->GetWorldTable()->FindData(GetOtherParam()->GetBindWorldID());
	CNtlPacket packet(sizeof(sGU_AVATAR_WORLD_INFO));
	sGU_AVATAR_WORLD_INFO* res = (sGU_AVATAR_WORLD_INFO*)packet.GetPacketData();
	res->worldInfo.hTriggerObjectOffset = HTRIGGER_OBJECT_WORLD;
	res->worldInfo.tblidx = pWorldData->tblidx;
	res->worldInfo.worldID = pWorldData->tblidx;
	res->worldInfo.sRuleInfo.byRuleType = pWorldData->byWorldRuleType;
	res->vCurLoc.x = GetOtherParam()->GetBindPosition().x;
	res->vCurLoc.y = GetOtherParam()->GetBindPosition().y;
	res->vCurLoc.z = GetOtherParam()->GetBindPosition().z;
	res->vCurDir.x = GetOtherParam()->GetBindDir().x;
	res->vCurDir.y = GetOtherParam()->GetBindDir().y;
	res->vCurDir.z = GetOtherParam()->GetBindDir().z;
	res->wOpCode = GU_AVATAR_WORLD_INFO;
	packet.SetPacketLen(sizeof(sGU_AVATAR_WORLD_INFO));
	app->Send(app->GetHSession(GetCharID()), &packet);
	app->RetrievePlayers();
}

void CGameCharacter::SendCharRevivalCurrentLoc()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	sWORLD_TBLDAT* pWorldData = (sWORLD_TBLDAT*)app->GetTableContainer()->GetWorldTable()->FindData(this->m_sPcProfile.bindWorldId);
	CNtlPacket packet(sizeof(sGU_CHAR_TELEPORT_RES));
	sGU_CHAR_TELEPORT_RES* res = (sGU_CHAR_TELEPORT_RES*)packet.GetPacketData();
	res->bIsToMoveAnotherServer = false;
	res->sWorldInfo.hTriggerObjectOffset = HTRIGGER_OBJECT_WORLD;
	res->sWorldInfo.tblidx = pWorldData->tblidx;
	res->sWorldInfo.worldID = pWorldData->tblidx;
	res->sWorldInfo.sRuleInfo.byRuleType = pWorldData->byWorldRuleType;
	res->vNewLoc.x = GetPosition().x;
	res->vNewLoc.x = GetPosition().y;
	res->vNewLoc.x = GetPosition().z;
	res->vNewDir.x = GetDir().x;
	res->vNewDir.y = GetDir().y;
	res->vNewDir.z = GetDir().z;
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = GU_CHAR_TELEPORT_RES;
	packet.SetPacketLen(sizeof(sGU_CHAR_TELEPORT_RES));
	app->Send(app->GetHSession(GetCharID()), &packet);
}

void CGameCharacter::SendCharCharging(bool bHitDelay)
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_UPDATE_CHAR_RP));
	sGU_UPDATE_CHAR_RP* res = (sGU_UPDATE_CHAR_RP*)packet.GetPacketData();
	res->bHitDelay = bHitDelay;
	res->handle = GetHObject();
	res->wCurRP = this->m_sPcProfile.wCurRP;
	res->wMaxRP = this->m_sPcProfile.avatarAttribute.wLastMaxRP;
	res->wOpCode = GU_UPDATE_CHAR_EP;
	packet.SetPacketLen(sizeof(sGU_UPDATE_CHAR_EP));
	app->SendAll(&packet);
}

void CGameCharacter::SendCharChargingCancel()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_CHAR_CHARGE_CANCELED_NFY));
	sGU_CHAR_CHARGE_CANCELED_NFY* res = (sGU_CHAR_CHARGE_CANCELED_NFY*)packet.GetPacketData();
	res->hSubject = GetHObject();
	res->wOpCode = GU_CHAR_CHARGE_CANCELED_NFY;
	packet.SetPacketLen(sizeof(sGU_CHAR_CHARGE_CANCELED_NFY));
	app->SendAll(&packet);
}

void CGameCharacter::SendCharUpdateRp()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_UPDATE_CHAR_RP_BALL));
	sGU_UPDATE_CHAR_RP_BALL* res = (sGU_UPDATE_CHAR_RP_BALL*)packet.GetPacketData();
	if (IsCharging())
		res->bDropByTime = false;
	else
		res->bDropByTime = true;
	res->handle = GetHObject();
	res->byCurRPBall = this->byRpBallCounter;
	res->wOpCode = GU_UPDATE_CHAR_RP_BALL;
	packet.SetPacketLen(sizeof(sGU_UPDATE_CHAR_RP_BALL));
	app->SendAll(&packet);
}

void CGameCharacter::SendCharUpdateRpBallMax()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_UPDATE_CHAR_RP_BALL_MAX));
	sGU_UPDATE_CHAR_RP_BALL_MAX* res = (sGU_UPDATE_CHAR_RP_BALL_MAX*)packet.GetPacketData();
	res->byMaxRPBall = this->byRpBallCounter;
	res->handle = GetHObject();
	res->wOpCode = GU_UPDATE_CHAR_RP_BALL_MAX;
	packet.SetPacketLen(sizeof(sGU_UPDATE_CHAR_RP_BALL_MAX));
	app->SendAll(&packet);
}

void CGameCharacter::SendItemMove(BYTE byPlaceSource, BYTE byPlaceDestination, BYTE byPositionSource, BYTE byPositionDestination)
{
	WORD wResultCode = GAME_SUCCESS;
	HOBJECT oldItem = GetInventory()->GetHItemByPosPlace(byPlaceSource, byPositionSource);
	if (!GetInventory()->IsCanPlayerMoveItem(byPlaceSource, byPlaceDestination, byPositionSource, byPositionDestination))
	{
		wResultCode = GetInventory()->GetError();
	}		
	if (GetInventory()->IsItemWasStocked())
	{
		SendItemStack(GetInventory()->GetStockedCount(),GetInventory()->GetStockedItem());
		SendItemDelete(byPlaceSource, byPositionSource, oldItem);
	}
	CNtlPacket packet(sizeof(sGU_ITEM_MOVE_RES));
	sGU_ITEM_MOVE_RES* res = (sGU_ITEM_MOVE_RES*)packet.GetPacketData();
	res->byDestPlace = byPlaceDestination;
	res->byDestPos = byPositionDestination;
	res->bySrcPlace = byPlaceSource;
	res->bySrcPos = byPositionSource;
	/*
		Rules:
		-For switching items, hDestItem and hSrcItem must be have some HOBJECT
		-For just remove and put into a empty slot hDestItem must be Invalid(INVALID_HOBJECT) and hSrcItem must be filled
	*/
	res->hDestItem = GetInventory()->GetHItemByPosPlace(byPlaceSource, byPositionSource);
	res->hSrcItem = GetInventory()->GetHItemByPosPlace(byPlaceDestination, byPositionDestination);
	res->wResultCode = wResultCode;
	res->wOpCode = GU_ITEM_MOVE_RES;
	packet.SetPacketLen(sizeof(sGU_ITEM_MOVE_RES));
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	app->Send(app->GetHSession(GetCharID()), &packet);
}

void CGameCharacter::SendItemStack(BYTE byStack,HOBJECT hItem, bool IsNew)
{
	CNtlPacket packet(sizeof(sGU_ITEM_STACK_UPDATE));
	sGU_ITEM_STACK_UPDATE* res = (sGU_ITEM_STACK_UPDATE*)packet.GetPacketData();
	res->bIsNew = IsNew;
	res->byStack = byStack;
	res->hItemHandle = hItem;
	res->wOpCode = GU_ITEM_STACK_UPDATE;
	packet.SetPacketLen(sizeof(sGU_ITEM_STACK_UPDATE));
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	app->Send(app->GetHSession(GetCharID()), &packet);
}

void CGameCharacter::SendItemDelete(BYTE byPlaceSource, BYTE byPositionSource, HOBJECT hItemObj)
{
	CNtlPacket packet(sizeof(sGU_ITEM_DELETE));
	sGU_ITEM_DELETE* res = (sGU_ITEM_DELETE*)packet.GetPacketData();
	res->bySrcPlace = byPlaceSource;
	res->bySrcPos = byPositionSource;
	res->hSrcItem = hItemObj;
	res->wOpCode = GU_ITEM_DELETE;
	packet.SetPacketLen(sizeof(sGU_ITEM_DELETE));
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	app->Send(app->GetHSession(GetCharID()), &packet);
}

void CGameCharacter::SendItemDeleteRes(BYTE byPlace, BYTE byPos)
{
	CNtlPacket packet(sizeof(sGU_ITEM_DELETE_RES));
	sGU_ITEM_DELETE_RES* res = (sGU_ITEM_DELETE_RES*)packet.GetPacketData();
	res->byPlace = byPlace;
	res->byPos = byPos;
	res->wResultCode = GetInventory()->DeleteItem(byPlace,byPos);
	res->wOpCode = GU_ITEM_DELETE_RES;	
	packet.SetPacketLen(sizeof(sGU_ITEM_DELETE_RES));
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	app->Send(app->GetHSession(GetCharID()), &packet);
}

void CGameCharacter::SendItemPickUp(HOBJECT hItemHandle, BYTE byAvatarType)
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_ITEM_PICK_RES));
	sGU_ITEM_PICK_RES* res = (sGU_ITEM_PICK_RES*)packet.GetPacketData();
	res->bByPartyHunting = false;//TODO: needs do a GetParty()->IsInParty(); and check the item division
	res->itemTblidx = app->FindTblidxOnDropList(hItemHandle);	
	res->wResultCode = GetInventory()->AddItem(hItemHandle, app->FindItemOnDropList(hItemHandle));
	res->wOpCode = GU_ITEM_PICK_RES;
	packet.SetPacketLen(sizeof(sGU_ITEM_PICK_RES));	
	app->Send(app->GetHSession(GetCharID()), &packet);
	app->RemoveItemDrop(hItemHandle);
}

void CGameCharacter::SendNpcSpawn()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGN_NPC_SPAWN_REQ));
	sGN_NPC_SPAWN_REQ* reqs = (sGN_NPC_SPAWN_REQ*)packet.GetPacketData();
	reqs->vPlayerPos = GetPosition();
	reqs->vPlayerDir = GetDir();
	reqs->worldTblidx = GetCurrentWorld();
	reqs->hPlayerHandle = GetHObject();
	reqs->wOpCode = GN_NPC_SPAWN_REQ;
	packet.SetPacketLen(sizeof(sGN_NPC_SPAWN_REQ));
	app->GetNpcSession()->PushPacket(&packet);
}

void CGameCharacter::SendMobSpawn()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGN_MOB_SPAWN_REQ));
	sGN_MOB_SPAWN_REQ* reqs = (sGN_MOB_SPAWN_REQ*)packet.GetPacketData();
	reqs->vPlayerPos = GetPosition();
	reqs->vPlayerDir = GetDir();
	reqs->worldTblidx = GetCurrentWorld();
	reqs->hPlayerHandle = GetHObject();
	reqs->wOpCode = GN_MOB_SPAWN_REQ;
	packet.SetPacketLen(sizeof(sGN_MOB_SPAWN_REQ));
	app->GetNpcSession()->PushPacket(&packet);
}

void CGameCharacter::SendNpcCheck()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGN_NPC_CHECK_REQ));
	sGN_NPC_CHECK_REQ* reqs = (sGN_NPC_CHECK_REQ*)packet.GetPacketData();
	reqs->vPlayerPos = GetPosition();
	reqs->vPlayerDir = GetDir();
	reqs->worldTblidx = GetCurrentWorld();
	reqs->hPlayerHandle = GetHObject();
	reqs->wOpCode = GN_NPC_CHECK_REQ;
	packet.SetPacketLen(sizeof(sGN_NPC_CHECK_REQ));
	app->GetNpcSession()->PushPacket(&packet);
}

void CGameCharacter::SendMobCheck()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGN_MOB_CHECK_REQ));
	sGN_MOB_CHECK_REQ* reqs = (sGN_MOB_CHECK_REQ*)packet.GetPacketData();
	reqs->vPlayerPos = GetPosition();
	reqs->vPlayerDir = GetDir();
	reqs->worldTblidx = GetCurrentWorld();
	reqs->hPlayerHandle = GetHObject();
	reqs->wOpCode = GN_MOB_CHECK_REQ;
	packet.SetPacketLen(sizeof(sGN_MOB_CHECK_REQ));
	app->GetNpcSession()->PushPacket(&packet);
}

void CGameCharacter::SendBankLoad(HOBJECT hWarehouse)
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_BANK_LOAD_RES));
	sGU_BANK_LOAD_RES* res = (sGU_BANK_LOAD_RES*)packet.GetPacketData();
	res->handle = hWarehouse;
	res->wOpCode = GU_BANK_LOAD_RES;
	res->wResultCode = GAME_SUCCESS;
	packet.SetPacketLen(sizeof(sGU_BANK_LOAD_RES));
	app->Send(app->GetHSession(GetCharID()), &packet);
}

void CGameCharacter::SendBankStart(HOBJECT hWarehouse)
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_BANK_START_RES));
	sGU_BANK_START_RES* res = (sGU_BANK_START_RES*)packet.GetPacketData();
	res->handle = hWarehouse;
	res->wOpCode = GU_BANK_START_RES;
	res->wResultCode = GAME_SUCCESS;
	packet.SetPacketLen(sizeof(sGU_BANK_START_RES));
	app->Send(app->GetHSession(GetCharID()), &packet);
}

void CGameCharacter::SendBankZennyInfo()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_BANK_ZENNY_INFO));
	sGU_BANK_ZENNY_INFO* res = (sGU_BANK_ZENNY_INFO*)packet.GetPacketData();
	res->dwZenny = GetWarehouse()->GetZennyBank();
	res->wOpCode = GU_BANK_ZENNY_INFO;
	packet.SetPacketLen(sizeof(sGU_BANK_ZENNY_INFO));
	app->Send(app->GetHSession(GetCharID()), &packet);
}

void CGameCharacter::SendBankZennyRes(bool IsSave, DWORD amount, HOBJECT hWarehouse)
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_BANK_ZENNY_RES));
	sGU_BANK_ZENNY_RES* res = (sGU_BANK_ZENNY_RES*)packet.GetPacketData();
	res->handle = hWarehouse;
	res->bIsSave = IsSave;
	res->dwZenny = amount;
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = GU_BANK_ZENNY_RES;
	packet.SetPacketLen(sizeof(sGU_BANK_ZENNY_RES));
	app->Send(app->GetHSession(GetCharID()), &packet);
	//If is for deposit
	if (IsSave)
	{
		GetWarehouse()->AddBankMoney(amount);
		GetWarehouse()->SubMoney(amount);
	}
	else
	{
		//Else for withdraw
		GetWarehouse()->SubBankMoney(amount);
		GetWarehouse()->AddMoney(amount);
	}
}

void CGameCharacter::SendBankItemInfo()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_BANK_ITEM_INFO));
	sGU_BANK_ITEM_INFO* res = (sGU_BANK_ITEM_INFO*)packet.GetPacketData();
	res->byBeginCount = 0;
	res->byItemCount = GetWarehouse()->GetItemCount();
	res->wOpCode = GU_BANK_ITEM_INFO;
	if (GetWarehouse()->GetItemCount()>0)
	{
		for (int i = 0; i < GetWarehouse()->GetItemCount();i++)
		{
			res->aBankProfile[i].handle = app->AcquireSerialId();
		}
		packet.AdjustPacketLen(sizeof(sNTLPACKETHEADER) + (2 * sizeof(BYTE)) + (res->byItemCount * sizeof(sITEM_PROFILE)));
		app->Send(app->GetHSession(GetCharID()), &packet);
	}
}

void CGameCharacter::SendBankEnd()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_BANK_END_RES));
	sGU_BANK_END_RES* res = (sGU_BANK_END_RES*)packet.GetPacketData();
	res->wOpCode = GU_BANK_END_RES;
	res->wResultCode = GAME_SUCCESS;
	packet.SetPacketLen(sizeof(sGU_BANK_END_RES));
	app->Send(app->GetHSession(GetCharID()), &packet);
}

void CGameCharacter::SendNetPyNotification()
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_NETMARBLEMEMBERIP_NFY));
	sGU_NETMARBLEMEMBERIP_NFY* res = (sGU_NETMARBLEMEMBERIP_NFY*)packet.GetPacketData();
	res->wOpCode = GU_NETMARBLEMEMBERIP_NFY;
	packet.SetPacketLen(sizeof(sGU_NETMARBLEMEMBERIP_NFY));
	app->Send(app->GetHSession(GetCharID()), &packet);
}

void CGameCharacter::SwitchBindToLocal()
{
	this->m_sCharState.sCharStateBase.vCurDir = GetOtherParam()->GetBindDir();
	this->m_sCharState.sCharStateBase.vCurLoc = GetOtherParam()->GetBindPosition();
	this->currentWorld = GetOtherParam()->GetBindWorldID();
	this->currentWorldID = GetOtherParam()->GetBindWorldID();
}

void CGameCharacter::SendItemCreate(sITEM_PROFILE* pItem)
{
	CGameServer* app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_ITEM_CREATE));
	sGU_ITEM_CREATE* res = (sGU_ITEM_CREATE*)packet.GetPacketData();
	res->bIsNew = true;
	res->handle = pItem->handle;
	res->sItemData.aOptionTblidx[0] = pItem->aOptionTblidx[0];
	res->sItemData.aOptionTblidx[1] = pItem->aOptionTblidx[1];
	res->sItemData.bNeedToIdentify = pItem->bNeedToIdentify;
	res->sItemData.byBattleAttribute = pItem->byBattleAttribute;
	res->sItemData.byCurrentDurability = pItem->byCurDur;
	res->sItemData.byDurationType = pItem->byDurationType;
	res->sItemData.byGrade = pItem->byGrade;	
	res->sItemData.byPlace = pItem->byPlace;
	res->sItemData.byPosition = pItem->byPos;
	res->sItemData.byRank = pItem->byRank;
	res->sItemData.byRestrictType = pItem->byRestrictType;
	res->sItemData.byStackcount = pItem->byStackcount;
	res->sItemData.charId = GetCharID();
	res->sItemData.itemNo = pItem->tblidx;//TBLIDX
	res->sItemData.itemId = pItem->handle;//HOBJECT	
	res->wOpCode = GU_OBJECT_CREATE;
	packet.SetPacketLen(sizeof(sGU_ITEM_CREATE));
	app->Send(app->GetHSession(GetCharID()), &packet);
}