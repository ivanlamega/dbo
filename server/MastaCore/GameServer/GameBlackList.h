#pragma once
#include "NtlFriend.h"
class CGameBlackList
{
private:
	sFRIEND_INFO		m_sFriendInfo[NTL_MAX_COUNT_FRIEND * 2];
	sFRIEND_FULL_DATA   m_sAllFriends[NTL_MAX_COUNT_FRIEND * 2];
public:
	CGameBlackList();
	~CGameBlackList();
};

