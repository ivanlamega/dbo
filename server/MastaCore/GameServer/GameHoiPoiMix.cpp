#include "stdafx.h"
#include "GameHoiPoiMix.h"


CGameHoiPoiMix::CGameHoiPoiMix()
{
	byRecipeCount = 0;
}


CGameHoiPoiMix::~CGameHoiPoiMix()
{
}

void CGameHoiPoiMix::Create(sRECIPE_DATA* sRecipeData, BYTE byRecipeCount)
{
	for (int i = 0; i <= byRecipeCount;i++)
	{
		this->m_mapRecipeData.insert(std::pair<UINT32, sRECIPE_DATA*>(i,sRecipeData));
	}	
	this->byRecipeCount = byRecipeCount;
}