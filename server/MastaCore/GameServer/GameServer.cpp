#include "stdafx.h"
#include "GameServer.h"

//For Multiple Server - Luiz45
const std::string CGameServer::GetConfigFileEnabledMultipleServers()
{
	return EnableMultipleServers.GetString();
}
const DWORD CGameServer::GetConfigFileMaxServers()
{
	return MAX_NUMOF_GAME_SERVER;
}
int	CGameServer::OnInitApp()
{
	m_nMaxSessionCount = MAX_NUMOF_GAME_SESSION;

	m_pSessionFactory = new CGameSessionFactory;
	if (NULL == m_pSessionFactory)
	{
		return NTL_ERR_SYS_MEMORY_ALLOC_FAIL;
	}

	m_pCommunityServerSession = NULL;
	m_pNpcServerSession = NULL;
	m_pQueryServerSession = NULL;
	m_pMasterServerSession = NULL;
	m_pServerSession = (CGameSession*)m_pSessionFactory->CreateSession(SESSION_GAME_SERVER);

	return NTL_SUCCESS;
}

int	CGameServer::OnCreate()
{
	int rc = NTL_SUCCESS;
	rc = m_clientAcceptor.Create(m_config.strGameServerIP.c_str(), m_config.wGameServerPort, SESSION_GAME_SERVER,
		MAX_NUMOF_GAME_GAME_CLIENT, 10, 15, MAX_NUMOF_GAME_GAME_CLIENT);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_clientAcceptor, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_masterServerConnector.Create(m_config.strMasterServerIP.c_str(), m_config.wMasterServerPort, SESSION_MASTER_SERVER);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_masterServerConnector, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_queryServerConnector.Create(m_config.strQueryServerIP.c_str(), m_config.wQueryServerPort, SESSION_QUERY_SERVER);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_queryServerConnector, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}
	
	rc = m_communityServerConnector.Create(m_config.strCommunityServerIP.c_str(), m_config.wCommunityServerPort, SESSION_COMMUNITY_SERVER);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_communityServerConnector, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_npcServerConnector.Create(m_config.strNpcServerIP.c_str(), m_config.wNpcServerPort, SESSION_NPC_SERVER,5000);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_npcServerConnector, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}
	return NTL_SUCCESS;

}

void CGameServer::LoadQuests()
{
	this->g_pQuestWrapper = new CQuestWrapper;
	this->g_pQuestWrapper->Create();
}

void	CGameServer::OnDestroy()
{
	SAFE_DELETE(m_pSessionFactory);
}

int	CGameServer::OnCommandArgument(int argc, _TCHAR* argv[])
{
	return NTL_SUCCESS;
}

int	CGameServer::OnConfiguration(const char * lpszConfigFile)
{
	CNtlIniFile file;

	int rc = file.Create(lpszConfigFile);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}
	//For multiple servers - Luiz45
	if (!file.Read("ServerOptions", "EnableMultipleServers", EnableMultipleServers))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	CNtlString sTmpCountServer;
	if (!file.Read("ServerOptions", "MaxServerAllowed", sTmpCountServer))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (GetConfigFileEnabledMultipleServers() == "true")
	{
		int maxServer = atoi(sTmpCountServer.c_str());
		int i = 0;
		for (i = 0; i < maxServer; i++)
		{
			if (!file.Read("GameServer" + i, "Address", ServersConfig[i][0]))
			{
				return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
			}
			if (!file.Read("GameServer" + i, "Port", ServersConfig[i][1]))
			{
				return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
			}
		}
	}
	else
	{
		if (!file.Read("GameServer", "Address", m_config.strGameServerIP))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("GameServer", "Port", m_config.wGameServerPort))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
	}
	if (!file.Read("MasterServer", "Address", m_config.strMasterServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("MasterServer", "Port", m_config.wMasterServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("OperatingServer", "Address", m_config.strOperatingServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("OperatingServer", "Port", m_config.wOperatingServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("CommunityServer", "Address", m_config.strCommunityServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("CommunityServer", "Port", m_config.wCommunityServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("QueryServer", "Address", m_config.strQueryServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("QueryServer", "Port", m_config.wQueryServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("NPCServer", "Address", m_config.strNpcServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("NPCServer", "Port", m_config.wNpcServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	return NTL_SUCCESS;
}

int	CGameServer::OnAppStart()
{
	return NTL_SUCCESS;
}

void	CGameServer::Run()
{
	DWORD dwTickCur, dwTickOld = ::GetTickCount();

	while (IsRunnable())
	{
		dwTickCur = ::GetTickCount();
		if (dwTickCur - dwTickOld >= 10000)
		{			
			//	NTL_PRINT(PRINT_APP, "Auth Server Run()");
			DoAliveTime();
			dwTickOld = dwTickCur;
			if (!m_pMasterServerSession)
				this->m_pServerSession->OnAccept();
			if (!m_pQueryServerSession)
				this->m_pServerSession->OnAccept();
			if (!m_pCommunityServerSession)
				this->m_pServerSession->OnAccept();
			if (!m_pNpcServerSession)
				this->m_pServerSession->OnAccept();
		}
		Sleep(2);
		DoAliveTime();
	}
}

bool	CGameServer::Add(CHARACTERID id,HSESSION hSession)
{
	if (Find(id,hSession))
		return false;

	m_sessionList.insert(std::pair<CHARACTERID, HSESSION>(id,hSession));

	return true;
}

void	CGameServer::Remove(HSESSION hSession)
{
	for (GAME_CLIENT_IT it = m_sessionList.begin(); it != m_sessionList.end(); it++)
	{
		if (hSession == it->second)
		{
			m_sessionList.erase(it);
			break;
		}
	}
}

bool	CGameServer::Find(CHARACTERID id,HSESSION hSession)
{
	for (GAME_CLIENT_IT it = m_sessionList.begin(); it != m_sessionList.end(); it++)
	{
		if (id)
		{
			if (id == it->first)
				return true;
		}
		else
		{
			if (hSession == it->second)
			{
				return true;
			}
		}
	}

	return false;
}

int		CGameServer::CreateTableContainer()
{
	CNtlBitFlagManager flagManager;
	if (false == flagManager.Create(CTableContainer::TABLE_COUNT))
	{
		return false;
	}

	CTableFileNameList fileNameList;
	if (false == fileNameList.Create())
	{
		return false;
	}


	flagManager.Set(CTableContainer::TABLE_WORLD);
	flagManager.Set(CTableContainer::TABLE_PC);
	flagManager.Set(CTableContainer::TABLE_MOB);
	flagManager.Set(CTableContainer::TABLE_NPC);
	flagManager.Set(CTableContainer::TABLE_ITEM);
	flagManager.Set(CTableContainer::TABLE_ITEM_OPTION);
	flagManager.Set(CTableContainer::TABLE_SKILL);
	flagManager.Set(CTableContainer::TABLE_SYSTEM_EFFECT);
	flagManager.Set(CTableContainer::TABLE_NEWBIE);
	flagManager.Set(CTableContainer::TABLE_MERCHANT);
	flagManager.Set(CTableContainer::TABLE_HTB_SET);
	flagManager.Set(CTableContainer::TABLE_USE_ITEM);
	flagManager.Set(CTableContainer::TABLE_SET_ITEM);
	flagManager.Set(CTableContainer::TABLE_CHARM);
	flagManager.Set(CTableContainer::TABLE_ACTION);
	flagManager.Set(CTableContainer::TABLE_CHAT_COMMAND);
	flagManager.Set(CTableContainer::TABLE_QUEST_ITEM);
	flagManager.Set(CTableContainer::TABLE_QUEST_TEXT_DATA);
	flagManager.Set(CTableContainer::TABLE_TEXT_ALL);
	flagManager.Set(CTableContainer::TABLE_OBJECT);
	flagManager.Set(CTableContainer::TABLE_WORLD_MAP);
	flagManager.Set(CTableContainer::TABLE_LAND_MARK);
	flagManager.Set(CTableContainer::TABLE_HELP);
	flagManager.Set(CTableContainer::TABLE_GUIDE_HINT);
	flagManager.Set(CTableContainer::TABLE_DRAGONBALL);
	flagManager.Set(CTableContainer::TABLE_DRAGONBALL_REWARD);
	flagManager.Set(CTableContainer::TABLE_TIMEQUEST);
	flagManager.Set(CTableContainer::TABLE_BUDOKAI);
	flagManager.Set(CTableContainer::TABLE_RANKBATTLE);
	flagManager.Set(CTableContainer::TABLE_DIRECTION_LINK);
	flagManager.Set(CTableContainer::TABLE_CHATTING_FILTER);
	flagManager.Set(CTableContainer::TABLE_PORTAL);
	flagManager.Set(CTableContainer::TABLE_SPEECH);
	flagManager.Set(CTableContainer::TABLE_SCRIPT_LINK);
	flagManager.Set(CTableContainer::TABLE_QUEST_NARRATION);
	flagManager.Set(CTableContainer::TABLE_VEHICLE);
	flagManager.Set(CTableContainer::TABLE_DUNGEON);
	flagManager.Set(CTableContainer::TABLE_MOB_MOVE_PATTERN);
	flagManager.Set(CTableContainer::TABLE_DYNAMIC_OBJECT);
	flagManager.Set(CTableContainer::TABLE_ITEM_RECIPE);
	flagManager.Set(CTableContainer::TABLE_ITEM_UPGRADE);
	flagManager.Set(CTableContainer::TABLE_MIX_MACHINE);
	flagManager.Set(CTableContainer::TABLE_DOJO);
	flagManager.Set(CTableContainer::TABLE_QUEST_REWARD);
	flagManager.Set(CTableContainer::TABLE_WORLD_ZONE);
	flagManager.Set(CTableContainer::TABLE_NPC_SPAWN);
	flagManager.Set(CTableContainer::TABLE_FORMULA);
	flagManager.Set(CTableContainer::TABLE_GAME_MANIA_TIME);

	fileNameList.SetFileName(CTableContainer::TABLE_WORLD, "Table_World_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_PC, "Table_PC_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_MOB, "Table_MOB_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_NPC, "Table_NPC_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_ITEM, "Table_Item_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_ITEM_OPTION, "Table_Item_Option_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_SKILL, "Table_Skill_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_SYSTEM_EFFECT, "Table_System_Effect_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_NEWBIE, "Table_Newbie_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_MERCHANT, "Table_Merchant_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_HTB_SET, "Table_HTB_Set_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_USE_ITEM, "Table_Use_Item_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_SET_ITEM, "Table_Set_Item_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_CHARM, "Table_Charm_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_ACTION, "Table_Action_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_CHAT_COMMAND, "Table_Chat_Command_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_QUEST_ITEM, "Table_Quest_Item_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_QUEST_TEXT_DATA, "Table_Quest_Text_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_TEXT_ALL, "Table_Text_All_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_OBJECT, "Table_Object");
	fileNameList.SetFileName(CTableContainer::TABLE_WORLD_MAP, "Table_Worldmap_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_LAND_MARK, "Table_Landmark_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_HELP, "Table_Help_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_GUIDE_HINT, "Table_Guide_Hint_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_DRAGONBALL, "Table_Dragon_Ball_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_DRAGONBALL_REWARD, "Table_DB_Reward_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_TIMEQUEST, "Table_TMQ_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_BUDOKAI, "Table_Tenkaichibudokai_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_RANKBATTLE, "Table_RankBattle_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_DIRECTION_LINK, "Table_Direction_Link_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_CHATTING_FILTER, "Table_Chatting_Filter_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_PORTAL, "Table_Portal_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_SPEECH, "Table_NPC_Speech_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_SCRIPT_LINK, "Table_Script_Link_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_QUEST_NARRATION, "Table_Quest_Narration_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_VEHICLE, "Table_Vehicle_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_DUNGEON, "Table_Dungeon_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_MOB_MOVE_PATTERN, "Table_Mob_Move_Pattern_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_DYNAMIC_OBJECT, "Table_Dynamic_Object_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_ITEM_RECIPE, "Table_Item_Recipe_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_ITEM_UPGRADE, "Table_Item_Upgrade_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_MIX_MACHINE, "Table_Item_Mix_Machine_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_DOJO, "Table_Dojo_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_QUEST_REWARD, "Table_Quest_Reward_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_WORLD_ZONE, "Table_World_Zone_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_FORMULA, "TD_Formula");
	fileNameList.SetFileName(CTableContainer::TABLE_GAME_MANIA_TIME, "Table_GameManiaTime_Data");

	g_pTableContainer = new CTableContainer;

	std::string str;
	CTable::eLOADING_METHOD eLoadMethod = (CTable::eLOADING_METHOD)1;
	str = "data";

	bool bResult = FALSE;
	bResult = g_pTableContainer->Create(flagManager, (char*)str.c_str(), &fileNameList, eLoadMethod, GetACP(), NULL);

	if (bResult)
	{
		CWorldTable* worldTable = g_pTableContainer->GetWorldTable();
		for (CTable::TABLEIT wt = worldTable->Begin(); wt != worldTable->End();wt++)
		{
			sWORLD_TBLDAT* worldTbldat = reinterpret_cast<sWORLD_TBLDAT*>(worldTable->FindData(wt->first));
			if (NULL != worldTbldat)
			{
				CObjectTable* obj = g_pTableContainer->GetObjectTable(worldTbldat->tblidx);
				for (CTable::TABLEIT it = obj->Begin(); it != obj->End(); it++)
				{
					sOBJECT_TBLDAT* objTbl = reinterpret_cast<sOBJECT_TBLDAT*>(obj->FindData(it->first));
					if (NULL != objTbl)
					{
						m_gameObject.insert(std::pair<HOBJECT, sOBJECT_TBLDAT*>((HTRIGGER_OBJECT_WORLD+objTbl->dwSequence), objTbl));
					}
				}
				m_worldList.insert(std::pair<WORLDID, sWORLD_TBLDAT*>(worldTbldat->tblidx,worldTbldat));
			}
		}
	}
	return bResult;
}
void	CGameServer::SetMasterSession(CMasterSession * pServerSession)
{
	CNtlAutoMutex mutex(&m_masterServerMutex);
	mutex.Lock();

	if (NULL == pServerSession)
	{
		m_pMasterServerSession->Release();
		m_pMasterServerSession = pServerSession;
	}
	else
	{
		pServerSession->Acquire();
		m_pMasterServerSession = pServerSession;
	}
}

CMasterSession * CGameServer::AcquireMasterSession()
{
	CNtlAutoMutex mutex(&m_masterServerMutex);
	mutex.Lock();

	if (NULL == m_pMasterServerSession) return NULL;

	m_pMasterServerSession->Acquire();
	return m_pMasterServerSession;
}

CMasterSession * CGameServer::GetMasterSession()
{
	return m_pMasterServerSession;
}

void	CGameServer::SetCommunitySession(CCommunitySession * pServerSession)
{
	CNtlAutoMutex mutex(&m_communityServerMutex);
	mutex.Lock();

	if (NULL == pServerSession)
	{
		m_pCommunityServerSession->Release();
		m_pCommunityServerSession = pServerSession;
	}
	else
	{
		pServerSession->Acquire();
		m_pCommunityServerSession = pServerSession;
	}
}

CCommunitySession * CGameServer::AcquireCommunitySession()
{
	CNtlAutoMutex mutex(&m_communityServerMutex);
	mutex.Lock();

	if (NULL == m_pCommunityServerSession) return NULL;

	m_pCommunityServerSession->Acquire();
	return m_pCommunityServerSession;
}

CCommunitySession * CGameServer::GetCommunitySession()
{
	return m_pCommunityServerSession;
}

void	CGameServer::SetNpcSession(CNpcSession * pServerSession)
{
	CNtlAutoMutex mutex(&m_npcServerMutex);
	mutex.Lock();

	if (NULL == pServerSession)
	{
		m_pNpcServerSession->Release();
		m_pNpcServerSession = pServerSession;
	}
	else
	{
		pServerSession->Acquire();
		m_pNpcServerSession = pServerSession;
	}
}

CNpcSession * CGameServer::AcquireNpcSession()
{
	CNtlAutoMutex mutex(&m_npcServerMutex);
	mutex.Lock();

	if (NULL == m_pNpcServerSession) return NULL;

	m_pNpcServerSession->Acquire();
	return m_pNpcServerSession;
}

CNpcSession * CGameServer::GetNpcSession()
{
	return m_pNpcServerSession;
}

void	CGameServer::SetQuerySession(CQuerySession * pServerSession)
{
	CNtlAutoMutex mutex(&m_queryServerMutex);
	mutex.Lock();

	if (NULL == pServerSession)
	{
		m_pQueryServerSession->Release();
		m_pQueryServerSession = pServerSession;
	}
	else
	{
		pServerSession->Acquire();
		m_pQueryServerSession = pServerSession;
	}
}

CQuerySession * CGameServer::AcquireQuerySession()
{
	CNtlAutoMutex mutex(&m_queryServerMutex);
	mutex.Lock();

	if (NULL == m_pQueryServerSession) return NULL;

	m_pQueryServerSession->Acquire();
	return m_pQueryServerSession;
}
CQuerySession * CGameServer::GetQuerySession()
{
	return m_pQueryServerSession;
}
void	CGameServer::SetClientSession(CGameSession* pSession)
{
	this->m_pClientSession = pSession;
}
CGameSession* CGameServer::GetClientSession()
{
	return this->m_pClientSession;
}

bool CGameServer::AddPlayer(CHARACTERID charID, CGameCharacter* plr)
{
	if (FindPlayer(charID))
		return false;
	
	m_playerList.insert(std::pair<CHARACTERID, CGameCharacter*>(charID, plr));
	return true;
}
void CGameServer::RemovePlayer(CHARACTERID charID, CGameCharacter* plr)
{
	if (FindPlayer(charID))
	{
		m_playerList.erase(charID);
	}
}
bool CGameServer::FindPlayer(CHARACTERID charID)
{
	for (PLAYER_IT it = m_playerList.begin(); it != m_playerList.end(); it++)
	{
		if (charID == it->first)
			return true;
	}
	return false;
}
CGameCharacter* CGameServer::GetPlayerByCharID(CHARACTERID charID)
{
	for (PLAYER_IT it = m_playerList.begin(); it != m_playerList.end(); it++)
	{
		if (charID == it->first)
			return it->second;
	}
	return NULL;
}
CGameCharacter*	CGameServer::GetPlayerByHandle(HOBJECT hHandle)
{
	for (PLAYER_IT it = m_playerList.begin(); it != m_playerList.end(); it++)
	{
		if (hHandle == it->second->GetHObject())
		{
			return it->second;
		}
	}
	return NULL;
}
CGameCharacter*	CGameServer::GetPlayerByCharName(WCHAR* charName)
{
	for (PLAYER_IT it = m_playerList.begin(); it != m_playerList.end(); it++)
	{
		if (wcscmp(it->second->GetCharName(), charName) == NTL_SUCCESS)
		{
			return it->second;
		}
	}
	return NULL;
}

HOBJECT	CGameServer::AcquireSerialId(void)
{
	if (m_uiSerialId++)
	{
		if (m_uiSerialId == 0xffffffff)
			m_uiSerialId = 0;
	}

	return m_uiSerialId;
}

void CGameServer::SendAll(CNtlPacket* packet, HSESSION except)
{
	for (PLAYER_IT it = m_playerList.begin(); it != m_playerList.end(); it++)
	{
		if (FindPlayer(it->first))
		{
			if (this->GetHSession(it->second->GetCharID()) != except)
				this->Send(this->GetHSession(it->second->GetCharID()), packet);
		}
	}
}

sOBJECT_TBLDAT*	CGameServer::GetObjectTbl(HOBJECT hObject)
{
	for (OBJECT_IT it = m_gameObject.begin(); it != m_gameObject.end(); it++)
	{
		if (hObject == it->first)
			return it->second;
	}
	return NULL;
}
sWORLD_TBLDAT*	CGameServer::GetWorld(WORLDID WorldId)
{
	for (WORLD_IT it = m_worldList.begin(); it != m_worldList.end(); it++)
	{
		if (WorldId == it->first)
			return it->second;
	}
	return NULL;
}
HSESSION CGameServer::GetHSession(CHARACTERID charID)
{
	for (GAME_CLIENT_IT it = m_sessionList.begin(); it != m_sessionList.end(); it++)
	{
		if (charID == it->first)
		{
			return it->second;
		}
	}
	return INVALID_HSESSION;
}

void CGameServer::DoAliveTime()
{
	for (int i = 0; i < this->GetNetwork()->GetSessionList()->GetMaxCount(); i++)
	{
		CNtlSession* first = this->GetNetwork()->GetSessionList()->Find(i);
		if (first)
		{
			if (first->GetSessionType() != SESSION_GAME_SERVER)
			{
				PACKETDATA res;
				res.wOpCode = SYS_ALIVE;
				CNtlPacket packet((BYTE*)&res, sizeof(res));
				first->PushPacket(&packet);
			}
		}
	}
}

TBLIDX CGameServer::FindTblidxOnDropList(HOBJECT hItem)
{
	for (DROP_LIST_IT it = m_dropList.begin(); it != m_dropList.end(); it++)
	{
		if (it->first == hItem)
			return it->second->tblidx;
	}
	return INVALID_TBLIDX;
}

sITEM_PROFILE* CGameServer::FindItemOnDropList(HOBJECT hItem)
{
	for (DROP_LIST_IT it = m_dropList.begin(); it != m_dropList.end(); it++)
	{
		if (it->first == hItem)
			return it->second;
	}
	return NULL;
}

void CGameServer::AddItemDrop(HOBJECT hItem, sITEM_PROFILE* itemProf)
{
	m_dropList.insert(std::pair<HOBJECT,sITEM_PROFILE*>(hItem,itemProf));
}

void CGameServer::RemoveItemDrop(HOBJECT hItem)
{
	DROP_LIST_IT it = m_dropList.find(hItem);
	m_dropList.erase(it);
}

void CGameServer::RetrievePlayers()
{
	for (PLAYER_IT it = m_playerList.begin(); it != m_playerList.end(); it++)
	{
		if (it->first)
		{
			it->second->SendCreatePlayer();
		}			
	}
}

void CGameServer::RefreshSerial(HOBJECT serial)
{
	m_uiSerialId = serial;
}