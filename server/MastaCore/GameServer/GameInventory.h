#pragma once
#include "NtlItem.h"
#include "ItemTable.h"

class CGameInventory
{
private:
	BYTE			byInvenCount;
	bool			bItemStocked;
	BYTE			byStockedCount;
	BYTE			byEquipedCount;
	HOBJECT			hItemStocked;
	WORD			errorCode;
	sITEM_PROFILE	m_sItemInventory[NTL_MAX_COUNT_USER_HAVE_INVEN_ITEM];
	sITEM_BRIEF		m_sItemEquipped[NTL_MAX_EQUIP_ITEM_SLOT];
	bool			IsItemCanBeStockable(sITEM_TBLDAT* pItemTbldat);
	TBLIDX			GetItemByPlacePos(BYTE byPlace, BYTE byPos);
	void			SwitchItems(BYTE byPlaceSource, BYTE byPlaceDestination, BYTE byPositionSource, BYTE byPositionDestination);
	void			StockItem(BYTE byPlaceSource, BYTE byPlaceDestination, BYTE byPositionSource, BYTE byPositionDestination);
	BYTE            CheckAvailableBag();
	BYTE			CheckAvailableSlot(int iBagToCheck);
	BYTE			CheckTotalFreeSlotOnBag(int iBagToCheck);
	void			CreateSlotsInBag();
public:
	CGameInventory();
	~CGameInventory();
	void	Create(sITEM_DATA* itDat, BYTE byCount);
	sITEM_PROFILE* GetItemInventory();
	sITEM_BRIEF*   GetItemEquipped();
	BYTE		   GetInvenCount();
	HOBJECT		   GetHObjectFromTblidx(TBLIDX idx);
	bool		   IsCanPlayerMoveItem(BYTE byPlaceSource, BYTE byPlaceDestination, BYTE byPositionSource, BYTE byPositionDestination);
	bool		   IsItemWasStocked();
	WORD		   GetError();
	HOBJECT		   GetHItemByPosPlace(BYTE byPlace, BYTE byPos);
	BYTE		   GetStockedCount();
	HOBJECT		   GetStockedItem();	
	WORD		   DeleteItem(BYTE byPlace, BYTE byPos);
	WORD		   AddItem(HOBJECT hItem, sITEM_PROFILE* sItemProfile);
	BYTE		   GetEquippedCount();
	bool		   IsPlayerHasAnySlotForShop(BYTE byItemsOnShop);
};

inline sITEM_PROFILE* CGameInventory::GetItemInventory()
{
	return m_sItemInventory;
}
inline sITEM_BRIEF* CGameInventory::GetItemEquipped()
{
	return m_sItemEquipped;
}
inline BYTE CGameInventory::GetInvenCount()
{
	return byInvenCount;
}
inline BYTE CGameInventory::GetEquippedCount()
{
	return byEquipedCount;
}

inline WORD CGameInventory::GetError()
{
	return errorCode;
}
inline bool CGameInventory::IsItemWasStocked()
{
	return bItemStocked;
}
inline BYTE CGameInventory::GetStockedCount()
{
	BYTE count = byStockedCount;
	byStockedCount = INVALID_BYTE;
	return count;
}
inline HOBJECT CGameInventory::GetStockedItem()
{
	HOBJECT stock = hItemStocked;
	hItemStocked = INVALID_HOBJECT;
	return stock;
}