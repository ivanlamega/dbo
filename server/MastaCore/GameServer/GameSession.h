#pragma once
#include "GameServer.h"
#include "NtlSession.h"
#include "NtlPacketEncoder_RandKey.h"
///////////////////////////////////////RESPONSE PACKETS///////////////////////////
//Game Server -> Master Server
#include "NtlPacketGM.h"
//Game Server -> NPC Server
#include "NtlPacketGN.h"
//Game Server -> Operating Server
#include "NtlPacketGP.h"
//Game Server -> Query server
#include "NtlPacketGQ.h"
//Game Server -> Community Server
#include "NtlPacketGT.h"
//Game Server -> Client(EXE)
#include "NtlPacketGU.h"
/////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////REQUEST PACKETS///////////////////////////
//Master Server -> Game Server
#include "NtlPacketMG.h"
//NPC Server -> Game Server
#include "NtlPacketNG.h"
//Operating server -> Game Server
#include "NtlPacketPG.h"
//Query Server -> Game Server
#include "NtlPacketQG.h"
//Community Server -> Game Server
#include "NtlPacketTG.h"
//Cliente(EXE) -> Game Server
#include "NtlPacketUG.h"
/////////////////////////////////////////////////////////////////////////////////
#include "NtlPacketUtil.h"

#ifndef CGAMESESSION_H
#define CGAMESESSION_H

class CGameSession :	public CNtlSession
{
private:
	CHARACTERID		charId;
	ACCOUNTID		accountId;
public:
	CGameSession(bool bAliveCheck = true, bool bOpcodeCheck = true);
	~CGameSession();
	int							OnAccept();
	void						OnClose();
	int							OnConnect();
	int							OnDispatch(CNtlPacket * pPacket);
	// Packet functions
	void						SendGameEnterRes(CNtlPacket* pPacket);
	void						SendGameWorldEnter(CNtlPacket* pPacket);
	void						SendGameWorldComplete();
	void						SendGameCharReady(CNtlPacket* pPacket);
	void						SendGameCharReadySpawn(CNtlPacket* pPacket);
	void						SendGameCharReadyForCommunity(CNtlPacket* pPacket);
	void						SendGameCommunityKeyReq(CNtlPacket* pPacket);
	//Char Behavior - Movement
	void						SendGameCharMove(CNtlPacket* pPacket);
	void						SendGameCharMoveSync(CNtlPacket* pPacket);
	void						SendGameCharChangeDirFloating(CNtlPacket* pPacket);
	void						SendGameCharChangeHeading(CNtlPacket* pPacket);
	void						SendGameCharJump(CNtlPacket* pPacket);
	void						SendGameCharJumpEnd(CNtlPacket* pPacket);
	void						SendGameCharDestMove(CNtlPacket* pPacket);
	void						SendGameCharFalling(CNtlPacket* pPacket);
	void						SendGameCharFollowMove(CNtlPacket* pPacket);
	void						SendGameCharToggSitDown(CNtlPacket* pPacket);
	void						SendGameCharToggRunning(CNtlPacket* pPacket);
	void						SendGameCharDashKeyboard(CNtlPacket* pPacket);
	void						SendGameCharDashMouse(CNtlPacket* pPacket);	
	//Char Actions
	void						SendGameCharTargetFace(CNtlPacket* pPacket);
	void						SendGameCharTargetInfo(CNtlPacket* pPacket);
	void						SendGameCharTargetSelect(CNtlPacket* pPacket);
	void						SendGameCharAttackBegin(CNtlPacket* pPacket);
	void						SendGameCharAttackEnd(CNtlPacket* pPacket);
	void						SendGameCharCharge(CNtlPacket* pPacket);
	void						SendGameCharBlockMode(CNtlPacket* pPacket);
	void						SendGameCharBindReq(CNtlPacket* pPacket);
	void						SendGameCharKnockdownRelease(CNtlPacket* pPacket);
	void						SendGameCharRevivalReq(CNtlPacket* pPacket);
	//Char Inventory
	void						SendGameCharItemMove(CNtlPacket* pPacket);
	void						SendGameCharItemDelete(CNtlPacket* pPacket);
	void						SendGameCharItemPickUp(CNtlPacket* pPacket);
	//NetPy shop
	void						SendGameNetPyStart(CNtlPacket* pPacket);
	void						SendGameNetPyEnd(CNtlPacket* pPacket);
	void						SendGameDurationItemBuy(CNtlPacket* pPacket);
	//Char Skills 
	void						SendGameCharSkillReq(CNtlPacket* pPacket);
	void						SendGameCharSkillLearn(CNtlPacket* pPacket);
	void						SendGameCharSkillUpgrade(CNtlPacket* pPacket);
	//Char Bank/Warehouse
	void						SendGameCharBankLoad(CNtlPacket* pPacket);
	void						SendGameCharBankStart(CNtlPacket* pPacket);
	void						SendGameCharBankZennyInfo(CNtlPacket* pPacket);
	void						SendGameCharBankEnd(CNtlPacket* pPacket);
	//Char Misc
	void						SendGameCharServerChangeReq(CNtlPacket* pPacket);
	void						SendGameCharChanneChangeReq(CNtlPacket* pPacket);
	void						SendGameCharExitReq(CNtlPacket* pPacket);
	void						SendGameCharGameExitReq(CNtlPacket* pPacket);
	void						SendGameCharLeaveReq(CNtlPacket* pPacket);
	void						SendGameCharAwayReq(CNtlPacket* pPacket);
	void						SendGameCharDirectPlayAck(CNtlPacket* pPacket);
	void						SendGameCharDirectPlayCancel(CNtlPacket* pPacket);
	void						SendGameCharTeleportReq(CNtlPacket* pPacket);
	void						SendGameServerCommand(CNtlPacket* pPacket);
	//Party
	void						SendGameCharPartyInvite(CNtlPacket* pPacket);
	void						SendGameCharPartyInviteCharId(CNtlPacket* pPacket);
	void						SendGameCharPartyInviteCharName(CNtlPacket* pPacket);
	//Shop
	void						SendGameShopBuy(CNtlPacket* pPacket);
	void						SendGameShopStart(CNtlPacket* pPacket);
	void						SendGameShopEnd(CNtlPacket* pPacket);
	//NPC Function
	void						SendNpcCreate(CNtlPacket* pPacket);
	void						SendMobCreate(CNtlPacket* pPacket);
	void						SendNpcUnspawn(CNtlPacket* pPacket);
	void						SendMobUnspawn(CNtlPacket* pPacket);
	void						SendGameNpcSpawn(CNtlPacket* pPacket);
	void						SendGameMobSpawn(CNtlPacket* pPacket);
	void						SendGameShopResult(CNtlPacket* pPacket);
	//Community Function
	void						SendCommunityAuthOk(CNtlPacket* pPacket);
	//Query Functions
	void						SendQueryLoadPcProfile(CNtlPacket* pPacket);
	void						SendQueryPcItemReq(CNtlPacket* pPacket);
	void						SendQueryPcSkillReq(CNtlPacket* pPacket);
	void						SendQueryPcBuffReq(CNtlPacket* pPacket);
	void						SendQueryPcWarfog(CNtlPacket* pPacket);
	void						SendQueryPcHtbReq(CNtlPacket* pPacket);
	void						SendQueryPcQuickSlot(CNtlPacket* pPacket);
	//void						SendQueryPcQuestItemReq();
	//void						SendQueryPcQuestCompleteReq();
	//void						SendQueryPcQuestProgressReq();
	//void						SendQueryPcShortCutReq();
	//void						SendQueryPcRecipeReq();
	void						SendQueryPcGmtReq(CNtlPacket* pPacket);
	void						SendQueryCharSkillLearn(CNtlPacket* pPacket);
	void						SendQueryCharSkillUpgrade(CNtlPacket* pPacket);

	// End Packet functions
private:
	CNtlPacketEncoder_RandKey	m_packetEncoder;
};

#endif