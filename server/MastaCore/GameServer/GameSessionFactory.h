#pragma once
#include "GameServer.h"
#include "NtlSessionFactory.h"

#ifndef CGAMESSIONSESSION_FACTORY_H
#define CGAMESSIONSESSION_FACTORY_H

class CGameSessionFactory : public CNtlSessionFactory
{
public:
	CGameSessionFactory();
	~CGameSessionFactory();
	CNtlSession * CreateSession(SESSIONTYPE sessionType);
	void DestroySession(CNtlSession * pSession);
};
#endif

