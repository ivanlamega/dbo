#pragma once
#include "NtlGuild.h"
class CGameGuild
{
private:
	sDBO_GUILD_DATA			m_sGuildData;
	sDBO_GUILD_MEMBER_INFO	m_sGuildMembersInfo[DBO_MAX_MEMBER_IN_GUILD];
public:
	CGameGuild();
	~CGameGuild();
};

