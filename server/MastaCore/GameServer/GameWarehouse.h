#pragma once
#include "NtlItem.h"
class CGameWarehouse
{
private:
	sITEM_PROFILE	m_sItemGeneralWarehouse[NTL_MAX_COUNT_USER_HAVE_NORMAL_BANK_ITEM];
	sITEM_PROFILE	m_sItemPublicWarehouse[NTL_MAX_COUNT_USER_HAVE_PUBLIC_BANK_ITEM];
	BYTE			byItemCount;
	DWORD*			dwCurrentMoney;
	DWORD			dwBankMoney;
public:
	CGameWarehouse();
	~CGameWarehouse();
	void	Create(DWORD* dwMoney, DWORD dwBankMoney);
	void	AddMoney(DWORD dwMoney);
	void	SubMoney(DWORD dwMoney);
	void	AddBankMoney(DWORD dwMoney);
	void	SubBankMoney(DWORD dwMoney);
	bool	IsGreaterEqualsThan(DWORD dwMoney);
	bool	IsLessEqualsThan(DWORD dwMoney);
	DWORD	GetZennyBank();
	BYTE	GetItemCount();
};
inline bool CGameWarehouse::IsGreaterEqualsThan(DWORD dwMoney)
{
	return (DWORD)this->dwCurrentMoney >= dwMoney;
}
inline bool CGameWarehouse::IsLessEqualsThan(DWORD dwMoney)
{
	return (DWORD)this->dwCurrentMoney <= dwMoney;
}
inline void CGameWarehouse::AddMoney(DWORD dwMoney)
{
	this->dwCurrentMoney += dwMoney;
}
inline void CGameWarehouse::SubMoney(DWORD dwMoney)
{
	this->dwCurrentMoney -= dwMoney;
}
inline void CGameWarehouse::AddBankMoney(DWORD dwMoney)
{
	this->dwBankMoney += dwMoney;
}
inline void CGameWarehouse::SubBankMoney(DWORD dwMoney)
{
	this->dwBankMoney -= dwMoney;
}
inline BYTE CGameWarehouse::GetItemCount()
{
	return this->byItemCount;
}
inline DWORD CGameWarehouse::GetZennyBank()
{
	return this->dwBankMoney;
}