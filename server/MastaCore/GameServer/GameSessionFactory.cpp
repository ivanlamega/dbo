#include "stdafx.h"
#include "GameSessionFactory.h"


CGameSessionFactory::CGameSessionFactory()
{
}


CGameSessionFactory::~CGameSessionFactory()
{
}

CNtlSession * CGameSessionFactory::CreateSession(SESSIONTYPE sessionType)
{
	CNtlSession * pSession = NULL;
	switch (sessionType)
	{
	case SESSION_CLIENT:
	case SESSION_GAME_SERVER:
	{
		pSession = new CGameSession(sessionType);
	}
	break;
	case SESSION_MASTER_SERVER:
	{
		pSession = new CNtlSession(sessionType);
	}
	break;
	case SESSION_QUERY_SERVER:
	{
		pSession = new CNtlSession(sessionType);
	}
	break;
	case SESSION_COMMUNITY_SERVER:
	{
		pSession = new CNtlSession(sessionType);
	}
	break;
	case SESSION_NPC_SERVER:
	{
		pSession = new CNtlSession(sessionType);
	}
	break;
	default:
		break;
	}

	return pSession;
}



void CGameSessionFactory::DestroySession(CNtlSession * pSession)
{
	switch (pSession->GetSessionType())
	{
	case SESSION_CLIENT:
	case SESSION_COMMUNITY_SERVER:
	case SESSION_MASTER_SERVER:
	case SESSION_QUERY_SERVER:
	case SESSION_GAME_SERVER:		
	case SESSION_NPC_SERVER:
	{
		SAFE_DELETE(pSession);
	}
	break;

	default:
		break;
	}
}