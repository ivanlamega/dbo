#pragma once
#include "NtlItem.h"
class CGamePrivateShop
{
private:
	sPRIVATESHOP_SHOP_DATA	m_PrivateShopData;
	sPRIVATESHOP_ITEM_DATA	m_aPrivateShopItemData[NTL_MAX_PRIVATESHOP_INVENTORY];
	ePRIVATESHOP_STATE		m_ePrivateShopState;
public:
	CGamePrivateShop();
	~CGamePrivateShop();
};

