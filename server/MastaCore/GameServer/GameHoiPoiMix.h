#pragma once
#include "NtlItem.h"
#include <map>
class CGameHoiPoiMix
{
public:
	typedef std::map< UINT32, sRECIPE_DATA* > MAPDEF_RECIPEDATA;
	typedef std::map< UINT32, sRECIPE_DATA* >::iterator MAPDEF_RECIPEDATA_IT;
	CGameHoiPoiMix();
	~CGameHoiPoiMix();
	void	Create(sRECIPE_DATA* sRecipeData, BYTE byRecipeCount);
	BYTE	GetRecipeCount();
	sRECIPE_DATA*	GetRecipeData(UINT32 n);
private:
	BYTE				byRecipeCount;
	MAPDEF_RECIPEDATA	m_mapRecipeData;

	sHOIPOIMIX_DATA		m_sHoipoiMixData;
};

inline BYTE CGameHoiPoiMix::GetRecipeCount()
{
	return byRecipeCount;
}

inline sRECIPE_DATA* CGameHoiPoiMix::GetRecipeData(UINT32 n)
{
	for (MAPDEF_RECIPEDATA_IT it = m_mapRecipeData.begin(); it != m_mapRecipeData.end(); it++)
	{
		if (it->first == n)
			return it->second;
	}
	return NULL;
}