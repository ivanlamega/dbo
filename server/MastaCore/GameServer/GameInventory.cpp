#include "stdafx.h"
#include "GameInventory.h"
#include "GameServer.h"
#include "ItemTable.h"

CGameInventory::CGameInventory()
{
	byInvenCount = 0;
	byEquipedCount = 0;
}


CGameInventory::~CGameInventory()
{
}

void CGameInventory::Create(sITEM_DATA* itDat, BYTE byCount)
{
	for (int i = 0; i < byCount; i++)
	{
		if ((itDat[i].itemNo == INVALID_TBLIDX) || (itDat[i].byPlace == 255) || (itDat[i].itemNo == 0))
			continue;
		if (itDat[i].byPlace == CONTAINER_TYPE_EQUIP)
		{
			this->m_sItemEquipped[i].aOptionTblidx[0] = itDat[i].aOptionTblidx[0];
			this->m_sItemEquipped[i].aOptionTblidx[1] = itDat[i].aOptionTblidx[1];
			this->m_sItemEquipped[i].byBattleAttribute = itDat[i].byBattleAttribute;
			this->m_sItemEquipped[i].byGrade = itDat[i].byGrade;
			this->m_sItemEquipped[i].byRank = itDat[i].byRank;
			this->m_sItemEquipped[i].tblidx = itDat[i].itemNo;
			byEquipedCount++;
		}
		memcpy(this->m_sItemInventory[i].awchMaker, itDat[i].awchMaker, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1);
		this->m_sItemInventory[i].aOptionTblidx[0] = itDat[i].aOptionTblidx[0];
		this->m_sItemInventory[i].aOptionTblidx[1] = itDat[i].aOptionTblidx[1];
		this->m_sItemInventory[i].bNeedToIdentify = itDat[i].bNeedToIdentify;
		this->m_sItemInventory[i].byBattleAttribute = itDat[i].byBattleAttribute;
		this->m_sItemInventory[i].byCurDur = itDat[i].byCurrentDurability;
		this->m_sItemInventory[i].byDurationType = itDat[i].byDurationType;
		this->m_sItemInventory[i].byGrade = itDat[i].byGrade;
		this->m_sItemInventory[i].byPlace = itDat[i].byPlace;
		this->m_sItemInventory[i].byPos = itDat[i].byPosition;
		this->m_sItemInventory[i].byRank = itDat[i].byRank;
		this->m_sItemInventory[i].byRestrictType = itDat[i].byRestrictType;
		this->m_sItemInventory[i].byStackcount = itDat[i].byStackcount;
		this->m_sItemInventory[i].handle = ((CGameServer*)NtlSfxGetApp())->AcquireSerialId();
		this->m_sItemInventory[i].nUseEndTime = itDat[i].nUseEndTime;
		this->m_sItemInventory[i].nUseStartTime = itDat[i].nUseStartTime;
		this->m_sItemInventory[i].tblidx = itDat[i].itemNo;
		this->byInvenCount++;
	}
	CreateSlotsInBag();
}

HOBJECT CGameInventory::GetHObjectFromTblidx(TBLIDX idx)
{
	for (int i = 0; i < byInvenCount; i++)
	{
		if (this->m_sItemInventory[i].tblidx == idx)
			return this->m_sItemInventory[i].handle;
	}
	return INVALID_HOBJECT;
}

bool CGameInventory::IsCanPlayerMoveItem(BYTE byPlaceSource, BYTE byPlaceDestination, BYTE byPositionSource, BYTE byPositionDestination)
{
	for (int i = 0; i < NTL_MAX_COUNT_USER_HAVE_INVEN_ITEM; i++)
	{
		if ((this->m_sItemInventory[i].byPlace == byPlaceDestination) && (this->m_sItemInventory[i].byPos == byPositionDestination))
		{
			if ((this->m_sItemInventory[i].handle == INVALID_HOBJECT) || (this->m_sItemInventory[i].handle == 0))
			{
				SwitchItems(byPlaceSource, byPlaceDestination, byPositionSource, byPositionDestination);
				return TRUE;
			}
			else
			{
				sITEM_TBLDAT* pItemOld = reinterpret_cast<sITEM_TBLDAT*>(((CGameServer*)NtlSfxGetApp())->GetTableContainer()->GetItemTable()->FindData(GetItemByPlacePos(byPlaceDestination, byPositionDestination)));
				if (pItemOld)
				{
					//If is can amount
					if (IsItemCanBeStockable(pItemOld))
					{
						StockItem(byPlaceSource, byPlaceDestination, byPositionSource, byPositionDestination);
						return TRUE;
					}
					else
					{
						SwitchItems(byPlaceSource, byPlaceDestination, byPositionSource, byPositionDestination);
						return TRUE;
					}
				}
			}
			
		}		
	}
	return FALSE;
}

HOBJECT	CGameInventory::GetHItemByPosPlace(BYTE byPlace, BYTE byPos)
{
	for (int i = 0; i < NTL_MAX_COUNT_USER_HAVE_INVEN_ITEM; i++)
	{
		if ((this->m_sItemInventory[i].byPlace == byPlace) && (this->m_sItemInventory[i].byPos == byPos))
			return this->m_sItemInventory[i].handle;
	}
	return INVALID_HOBJECT;
}

TBLIDX	CGameInventory::GetItemByPlacePos(BYTE byPlace, BYTE byPos)
{
	for (int i = 0; i < NTL_MAX_COUNT_USER_HAVE_INVEN_ITEM; i++)
	{
		if ((this->m_sItemInventory[i].byPlace == byPlace) && (this->m_sItemInventory[i].byPos == byPos))
			return this->m_sItemInventory[i].tblidx;
	}
	return INVALID_TBLIDX;
}

bool CGameInventory::IsItemCanBeStockable(sITEM_TBLDAT* pItemTbldat)
{
	if (pItemTbldat->byMax_Stack > 1)
		return true;
	return false;
}
void CGameInventory::SwitchItems(BYTE byPlaceSource, BYTE byPlaceDestination, BYTE byPositionSource, BYTE byPositionDestination)
{
	for (int i = 0; i < NTL_MAX_COUNT_USER_HAVE_INVEN_ITEM; i++)
	{
		if ((this->m_sItemInventory[i].byPlace == byPlaceDestination) && (this->m_sItemInventory[i].byPos == byPositionDestination))
		{
			for (int p = 0; p < NTL_MAX_COUNT_USER_HAVE_INVEN_ITEM; p++)
			{
				if ((this->m_sItemInventory[p].byPlace == byPlaceSource) && (this->m_sItemInventory[p].byPos == byPositionSource))
				{
					this->m_sItemInventory[p].byPlace = byPlaceDestination;
					this->m_sItemInventory[p].byPos = byPositionDestination;
					this->m_sItemInventory[i].byPlace = byPlaceSource;
					this->m_sItemInventory[i].byPos = byPositionSource;					
				}
			}
		}
	}
}
void CGameInventory::StockItem(BYTE byPlaceSource, BYTE byPlaceDestination, BYTE byPositionSource, BYTE byPositionDestination)
{
	for (int i = 0; i < NTL_MAX_COUNT_USER_HAVE_INVEN_ITEM; i++)
	{
		if ((this->m_sItemInventory[i].byPlace == byPlaceDestination) && (this->m_sItemInventory[i].byPos == byPositionDestination))
		{
			for (int p = 0; p < NTL_MAX_COUNT_USER_HAVE_INVEN_ITEM; p++)
			{
				if ((this->m_sItemInventory[p].byPlace == byPlaceSource) && (this->m_sItemInventory[p].byPos == byPositionSource))
				{
					this->m_sItemInventory[p].byPlace = 255;
					this->m_sItemInventory[p].byPos = 255;
					this->m_sItemInventory[p].handle = INVALID_HOBJECT;
					this->m_sItemInventory[i].byStackcount += this->m_sItemInventory[p].byStackcount;
					this->bItemStocked = true;
					this->byStockedCount = this->m_sItemInventory[i].byStackcount;
					this->hItemStocked = this->m_sItemInventory[i].handle;
					break;
				}
			}
		}
	}
}

WORD CGameInventory::DeleteItem(BYTE byPlace, BYTE byPos)
{
	WORD wResult;
	for (int i = 0; i < NTL_MAX_COUNT_USER_HAVE_INVEN_ITEM; i++)
	{
		if ((this->m_sItemInventory[i].byPlace == byPlace) && (this->m_sItemInventory[i].byPos == byPos))
		{
			wResult = GAME_SUCCESS;
			memcpy(&this->m_sItemInventory[i], (new sITEM_PROFILE), sizeof(sITEM_PROFILE));
			break;
		}
		wResult = GAME_ITEM_NOT_FOUND;
	}
	return wResult;
}

BYTE CGameInventory::CheckAvailableBag()
{
	BYTE byAvaibleBag = CONTAINER_TYPE_NONE;
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	for (int i = 0; i < NTL_MAX_COUNT_USER_HAVE_INVEN_ITEM; i++)
	{
		if (this->GetItemInventory()[i].byPlace == CONTAINER_TYPE_BAGSLOT)
		{
			sITEM_TBLDAT* pItemTbl = reinterpret_cast<sITEM_TBLDAT*>(app->GetTableContainer()->GetItemTable()->FindData(this->GetItemInventory()[i].tblidx));
			//Main Bag
			if (this->GetItemInventory()[i].byPos == (CONTAINER_TYPE_BAG1 - 1))
			{
				if (this->CheckAvailableSlot(CONTAINER_TYPE_BAG1) >= pItemTbl->byBag_Size)
					continue;
				else
				{
					byAvaibleBag = CONTAINER_TYPE_BAG1;
					break;
				}
			}
			//Secondary Bag
			else if (this->GetItemInventory()[i].byPos == (CONTAINER_TYPE_BAG2 - 1))
			{
				if (this->CheckAvailableSlot(CONTAINER_TYPE_BAG2) >= pItemTbl->byBag_Size)
					continue;
				else
				{
					byAvaibleBag = CONTAINER_TYPE_BAG2;
					break;
				}
			}
			//Third Bag
			else if (this->GetItemInventory()[i].byPos == (CONTAINER_TYPE_BAG3 - 1))
			{
				if (this->CheckAvailableSlot(CONTAINER_TYPE_BAG3) >= pItemTbl->byBag_Size)
					continue;
				else
				{
					byAvaibleBag = CONTAINER_TYPE_BAG3;
					break;
				}				
			}
			//Fourth Bag
			else if (this->GetItemInventory()[i].byPos == (CONTAINER_TYPE_BAG4 - 1))
			{
				if (this->CheckAvailableSlot(CONTAINER_TYPE_BAG4) >= pItemTbl->byBag_Size)
					continue;
				else
				{
					byAvaibleBag = CONTAINER_TYPE_BAG4;
					break;
				}				
			}
			//Five Bag
			else if (this->GetItemInventory()[i].byPos == (CONTAINER_TYPE_BAG5 - 1))
			{
				if (this->CheckAvailableSlot(CONTAINER_TYPE_BAG5) >= pItemTbl->byBag_Size)
					continue;
				else
				{
					byAvaibleBag = CONTAINER_TYPE_BAG5;
					break;
				}					
			}
		}
	}

	return byAvaibleBag;
}
/*
This method receive the Bag container to check if have any slot available
he will check the slots stored in our struct sItemBrief...
*/
BYTE CGameInventory::CheckAvailableSlot(int iBagToCheck)
{
	BYTE byAvailableSlot = INVALID_SLOTID;
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	for (int i = 0; i < NTL_MAX_COUNT_USER_HAVE_INVEN_ITEM; i++)
	{
		if (this->GetItemInventory()[i].byPlace == (iBagToCheck -1))
		{
			sITEM_TBLDAT* pItemTbl = reinterpret_cast<sITEM_TBLDAT*>(app->GetTableContainer()->GetItemTable()->FindData(this->GetItemInventory()[i].tblidx));
			for (int p = 0; p < pItemTbl->byBag_Size; p++)
			{
				if (this->GetItemInventory()[p].byPlace == (iBagToCheck - 1) &&
					(this->GetItemInventory()[p].byPos == p) &&
					((this->GetItemInventory()[p].handle == INVALID_HOBJECT) || (this->GetItemInventory()[p].handle == 0)))
				{
					byAvailableSlot = p;
					break;
				}
			}
			if (byAvailableSlot != INVALID_SLOTID)
				break;
		}
	}
	return byAvailableSlot;
}

WORD CGameInventory::AddItem(HOBJECT hItem, sITEM_PROFILE* itemTbl)
{
	WORD wResult = GAME_SUCCESS;
	BYTE byBag = CheckAvailableBag();
	BYTE bySlot = CheckAvailableSlot(byBag);
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	if (byBag != CONTAINER_TYPE_NONE)
	{
		if (bySlot != INVALID_SLOTID)
		{
			for (int i = 0; i < NTL_MAX_COUNT_USER_HAVE_INVEN_ITEM; i++)
			{
				if (((this->m_sItemInventory[i].handle == INVALID_HOBJECT) &&
					(this->m_sItemInventory[i].tblidx == INVALID_TBLIDX)) ||
					((this->m_sItemInventory[i].handle == 0) &&
					(this->m_sItemInventory[i].tblidx == 0)))
				{			
					wResult = GAME_SUCCESS;
					itemTbl->byPlace = byBag;
					itemTbl->byPos = bySlot;
					this->m_sItemInventory[i].aOptionTblidx[0] = itemTbl->aOptionTblidx[0];
					this->m_sItemInventory[i].aOptionTblidx[1] = itemTbl->aOptionTblidx[1];
					this->m_sItemInventory[i].bNeedToIdentify = itemTbl->bNeedToIdentify;
					this->m_sItemInventory[i].byBattleAttribute = itemTbl->byBattleAttribute;
					this->m_sItemInventory[i].byCurDur = itemTbl->byCurDur;
					this->m_sItemInventory[i].byDurationType = itemTbl->byDurationType;
					this->m_sItemInventory[i].byGrade = itemTbl->byGrade;
					this->m_sItemInventory[i].byPlace = itemTbl->byPlace;
					this->m_sItemInventory[i].byPos = itemTbl->byPos;
					this->m_sItemInventory[i].byRank = itemTbl->byRank;
					this->m_sItemInventory[i].byRestrictType = itemTbl->byRestrictType;
					this->m_sItemInventory[i].byStackcount = itemTbl->byStackcount;
					this->m_sItemInventory[i].handle = hItem;
					this->m_sItemInventory[i].nUseEndTime = itemTbl->nUseEndTime;
					this->m_sItemInventory[i].nUseStartTime = itemTbl->nUseStartTime;
					this->m_sItemInventory[i].tblidx = itemTbl->tblidx;
					this->byInvenCount++;
					break;
				}
				wResult = GAME_ITEM_CANNOT_BUY_NOW;
			}
		}
		else
		{
			wResult = GAME_ITEM_INVEN_FULL;
		}
	}
	else
	{
		wResult = GAME_ITEM_BAG_IS_NOT_EMPTY;
	}
	return wResult;
}

void CGameInventory::CreateSlotsInBag()
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	BYTE byPlace = CONTAINER_TYPE_BAG1;
	for (int i = 0; i < byInvenCount; i++)
	{
		sITEM_TBLDAT* pItemTbl = reinterpret_cast<sITEM_TBLDAT*>(app->GetTableContainer()->GetItemTable()->FindData(this->GetItemInventory()[i].tblidx));
		if ((pItemTbl) && (pItemTbl->byItem_Type == ITEM_TYPE_BAG))
		{
			for (int p = 0; p < NTL_MAX_COUNT_USER_HAVE_INVEN_ITEM;p++)
			{
				if ((this->m_sItemInventory[p].byPos == 0) && (this->m_sItemInventory[p].byPlace == 0) && (this->m_sItemInventory[p].handle == 0))
				{
					for (int z = 0; z < pItemTbl->byBag_Size; z++)
					{				
						this->m_sItemInventory[p].handle = INVALID_HOBJECT;
						this->m_sItemInventory[p].byPlace = byPlace;
						this->m_sItemInventory[p].byPos = z;
						p++;
					}
				}
			}
			byPlace++;
		}
	}
}

BYTE CGameInventory::CheckTotalFreeSlotOnBag(int iBagCheck)
{
	BYTE byAvailableSlot = INVALID_SLOTID;
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	for (int i = 0; i < NTL_MAX_COUNT_USER_HAVE_INVEN_ITEM; i++)
	{
		if (this->GetItemInventory()[i].byPlace == (iBagCheck - 1))
		{
			sITEM_TBLDAT* pItemTbl = reinterpret_cast<sITEM_TBLDAT*>(app->GetTableContainer()->GetItemTable()->FindData(this->GetItemInventory()[i].tblidx));
			for (int p = 0; p < pItemTbl->byBag_Size; p++)
			{
				if (this->GetItemInventory()[p].byPlace == (iBagCheck - 1) &&
					(this->GetItemInventory()[p].byPos == p) &&
					((this->GetItemInventory()[p].handle == INVALID_HOBJECT) || (this->GetItemInventory()[p].handle == 0)))
				{
					byAvailableSlot++;
				}
			}
			if (byAvailableSlot != INVALID_SLOTID)
				break;
		}
	}
	return byAvailableSlot;
}

bool CGameInventory::IsPlayerHasAnySlotForShop(BYTE byItemShop)
{
	BYTE byBag = CheckAvailableBag();
	BYTE bySlot = CheckAvailableSlot(byBag);
	if (byBag != CONTAINER_TYPE_NONE)
	{
		if (CheckTotalFreeSlotOnBag(byBag) >= byItemShop)
			return true;
		else
			return false;
	}
	else
	{
		return false;
	}	
}