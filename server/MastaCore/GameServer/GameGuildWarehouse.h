#pragma once
#include "NtlItem.h"

struct sITEM_TBLDAT;

struct sGuildWarehouseSlot
{
	HOBJECT				hHandle;
	sITEM_TBLDAT*		pITEM_TBLDAT;
	BYTE				byServerPlace;
	BYTE				byPos;
	BYTE				byStackcount;
	BYTE				byRank;
	BYTE				byCurDur;
	bool				bNeedToIdentify;
	BYTE				byGrade;
	BYTE				byBattleAttribute;
	TBLIDX				aOptionTblidx[NTL_MAX_OPTION_IN_ITEM];
};
class CGameGuildWarehouse
{
private:
	sGuildWarehouseSlot	m_sWarehouseSlot[NTL_MAX_GUILD_BANK_COUNT][NTL_MAX_GUILD_ITEM_SLOT];

public:
	CGameGuildWarehouse();
	~CGameGuildWarehouse();
};

