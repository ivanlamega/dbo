#include "stdafx.h"
#include "GameServer.h"
#include "GameSession.h"


CGameSession::CGameSession(bool bAliveCheck, bool bOpcodeCheck) :CNtlSession(SESSION_GAME_SERVER)
{
	SetControlFlag(CONTROL_FLAG_USE_SEND_QUEUE);

	if (bAliveCheck)
	{
		SetControlFlag(CONTROL_FLAG_CHECK_ALIVE);
	}
	if (bOpcodeCheck)
	{
		SetControlFlag(CONTROL_FLAG_CHECK_OPCODE);
	}

	SetPacketEncoder(&m_packetEncoder);
}

//-----------------------------------------------------------------------------------
//		클라이언트 소멸
//-----------------------------------------------------------------------------------
CGameSession::~CGameSession()
{
	NTL_PRINT(PRINT_APP, "CGameSession Destructor Called");

	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	app->Remove(this->GetHandle());
}

int CGameSession::OnAccept()
{
	NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	for (int i = 0; i < app->GetNetwork()->GetSessionList()->GetMaxCount(); i++)
	{
		CNtlSession* first = app->GetNetwork()->GetSessionList()->Find(i);
		if (first)
		{
			if (first->GetSessionType() == SESSION_QUERY_SERVER)
			{
				app->SetQuerySession((CQuerySession*)first);
			}
			if (first->GetSessionType() == SESSION_NPC_SERVER)
			{
				app->SetNpcSession((CNpcSession*)first);
			}
			if (first->GetSessionType() == SESSION_MASTER_SERVER)
			{
				app->SetMasterSession((CMasterSession*)first);
			}
			if (first->GetSessionType() == SESSION_COMMUNITY_SERVER)
			{
				app->SetCommunitySession((CCommunitySession*)first);
			}
		}
	}
	if ((app->AcquireQuerySession() != NULL) && (!app->IsQueryServerConnected()))
	{
		CNtlPacket packetQ(sizeof(sGQ_NOTIFY_SERVER_BEGIN));
		sGQ_NOTIFY_SERVER_BEGIN* msgQ = (sGQ_NOTIFY_SERVER_BEGIN *)packetQ.GetPacketData();
		msgQ->wOpCode = GQ_NOTIFY_SERVER_BEGIN;
		packetQ.SetPacketLen(sizeof(sGQ_NOTIFY_SERVER_BEGIN));
		app->AcquireQuerySession()->PushPacket(&packetQ);
		app->SetQueryServerConnected(true);
	}
	if ((app->GetMasterSession() != NULL) && (!app->IsMasterServerConnected()))
	{
		CNtlPacket packetM(sizeof(sGM_HEARTBEAT));
		sGM_HEARTBEAT* msgM = (sGM_HEARTBEAT*)packetM.GetPacketData();
		msgM->wOpCode = GM_HEARTBEAT;
		packetM.SetPacketLen(sizeof(sGM_HEARTBEAT));
		app->GetMasterSession()->PushPacket(&packetM);
		app->SetMasterServerConnected(true);
	}
	if ((app->GetCommunitySession() != NULL) && (!app->IsCommunityServerConnected()))
	{
		CNtlPacket packetT(sizeof(sGT_HEARTBEAT));
		sGT_HEARTBEAT* msgT = (sGT_HEARTBEAT*)packetT.GetPacketData();
		msgT->wOpCode = GT_HEARTBEAT;
		packetT.SetPacketLen(sizeof(sGT_HEARTBEAT));
		app->GetCommunitySession()->PushPacket(&packetT);
		app->SetCommunityServerConnected(true);
	}
	if ((app->GetNpcSession() != NULL) && (!app->IsNpcServerConnected()))
	{
		CNtlPacket packetNpc(sizeof(sGN_GAME_DATA_RES));
		sGN_GAME_DATA_RES* msgNpc = (sGN_GAME_DATA_RES*)packetNpc.GetPacketData();
		msgNpc->wOpCode = GN_GAME_DATA_RES;
		packetNpc.SetPacketLen(sizeof(sGN_GAME_DATA_RES));
		app->GetNpcSession()->PushPacket(&packetNpc);
		app->SetNpcServerConnected(true);
	}
	return NTL_SUCCESS;
}

int CGameSession::OnConnect()
{
	NTL_PRINT(PRINT_APP, "%s", __FUNCTION__);
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	do{
		this->OnAccept();
		Sleep(5000);
	} while ((!app->AcquireQuerySession()) ||
		(!app->GetCommunitySession()) ||
		(!app->AcquireNpcSession()) ||
		(!app->GetMasterSession()));
	return NTL_SUCCESS;
}

void CGameSession::OnClose()
{
	NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
}

int CGameSession::OnDispatch(CNtlPacket * pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	if ((!app->AcquireQuerySession()) ||
		(!app->GetCommunitySession()) ||
		(!app->GetNpcSession()) ||
		(!app->GetMasterSession()))
		this->OnConnect();

	sNTLPACKETHEADER * pHeader = (sNTLPACKETHEADER *)pPacket->GetPacketData();
	switch (pHeader->wOpCode)
	{
		//Community
		case TG_HEARTBEAT:
		{
			if (app->GetCommunitySession() != NULL)
				app->GetCommunitySession()->ResetAliveTime();
		}
		break;
		case TG_USER_AUTH_KEY_CREATED_ACK:
		{
			CGameSession::SendCommunityAuthOk(pPacket);
		}
		break;
		case TG_USER_ENTER_GAME_ACK:
		{
			//Do nothing
		}
		break;
		//Master
		case MG_HEARTBEAT:
		{
			if (app->GetMasterSession() != NULL)
				app->GetMasterSession()->ResetAliveTime();
		}
		break;
		//Query
		case QG_HEARTBEAT:
		{
			if (app->AcquireQuerySession() != NULL)
				app->AcquireQuerySession()->ResetAliveTime();
		}
		break;
		case QG_PC_DATA_LOAD_RES:
		{
			CGameSession::SendQueryLoadPcProfile(pPacket);
		}
		break;
		case QG_PC_SKILL_LOAD_RES:
		{
			CGameSession::SendQueryPcSkillReq(pPacket);
		}
		break;
		case QG_PC_ITEM_LOAD_RES:
		{
			CGameSession::SendQueryPcItemReq(pPacket);
		}
		break;
		case QG_PC_HTB_LOAD_RES:
		{
			CGameSession::SendQueryPcHtbReq(pPacket);
		}
		break;
		case QG_PC_BUFF_LOAD_RES:
		{
			CGameSession::SendQueryPcBuffReq(pPacket);
		}
		break;
		case QG_PC_QUICK_SLOT_LOAD_RES:
		{
			CGameSession::SendQueryPcQuickSlot(pPacket);
		}
		break;
		case QG_PC_WAR_FOG_RES:
		{
			CGameSession::SendQueryPcWarfog(pPacket);
		}
		break;
		case QG_PC_GMT_LOAD_RES:
		{
			CGameSession::SendQueryPcGmtReq(pPacket);
		}
		break;
		case QG_NOTIFY_CAN_LOAD_CHAR:
		{
			//TODO:			
		}
		break;
		case QG_SKILL_ADD_RES:
		{
			CGameSession::SendQueryCharSkillLearn(pPacket);
		}
		break;
		case QG_SKILL_UPGRADE_RES:
		{

		}
		break;
		//Operating
		case PG_HEARTBEAT:
		{

		}
		break;
		//NPC
		case NG_REFRESH_SERIAL:
		{
			sNG_REFRESH_SERIAL* req = (sNG_REFRESH_SERIAL*)pPacket->GetPacketData();
			app->RefreshSerial(req->hLastSerial);
		}
		break;
		case NG_GAME_DATA_REQ:
		case NG_HEARTBEAT:
		{
			if (app->GetNpcSession() != NULL)
				app->GetNpcSession()->ResetAliveTime();
		}
		break;
		case NG_NPC_SPAWN_RES:
		{
			CGameSession::SendNpcCreate(pPacket);
		}
		break;
		case NG_MOB_SPAWN_RES:
		{
			CGameSession::SendMobCreate(pPacket);
		}
		break;
		case NG_NPC_UNSPAWN_RES:
		{
			CGameSession::SendNpcUnspawn(pPacket);
		}
		break;
		case NG_MOB_UNSPAWN_RES:
		{
			CGameSession::SendMobUnspawn(pPacket);
		}
		break;
		case NG_ADMIN_MOB_SPAWN:
		{
			CGameSession::SendGameMobSpawn(pPacket);
		}
		break;
		case NG_ADMIN_NPC_SPAWN:
		{
			CGameSession::SendGameNpcSpawn(pPacket);
		}
		break;
		case NG_SHOP_BUY:
		{
			CGameSession::SendGameShopResult(pPacket);
		}
		break;
		//Client (EXE)
		case UG_GAME_ENTER_REQ:
		{
			CGameSession::SendGameEnterRes(pPacket);	
		}
		break;
		case UG_ENTER_WORLD:
		{
			CGameSession::SendGameWorldEnter(pPacket);
			CGameSession::SendGameWorldComplete();
		}
		break;
		case UG_CHAR_READY_TO_SPAWN:
		{
			CGameSession::SendGameCharReadySpawn(pPacket);
		}
		break;
		case UG_CHAR_READY:
		{
			CGameSession::SendGameCharReady(pPacket);
		}
		break;
		case UG_CHAR_READY_FOR_COMMUNITY_SERVER_NFY:
		{
			CGameSession::SendGameCharReadyForCommunity(pPacket);
		}
		break;
		case UG_AUTH_KEY_FOR_COMMUNITY_SERVER_REQ:
		{
			CGameSession::SendGameCommunityKeyReq(pPacket);
			
		}
		break;
		case UG_TUTORIAL_HINT_UPDATE_REQ:
		{
			//TODO: do some valid thing here
		}
		break;
		case UG_CHAR_MOVE:
		{
			CGameSession::SendGameCharMove(pPacket);
		}
		break;
		case UG_CHAR_MOVE_SYNC:
		{
			CGameSession::SendGameCharMoveSync(pPacket);
		}
		break;
		case UG_CHAR_CHANGE_DIRECTION_ON_FLOATING:
		{
			CGameSession::SendGameCharChangeDirFloating(pPacket);
		}
		break;
		case UG_CHAR_CHANGE_HEADING:
		{
			CGameSession::SendGameCharChangeHeading(pPacket);
		}
		break;
		case UG_CHAR_JUMP:
		{
			CGameSession::SendGameCharJump(pPacket);
		}
		break;
		case UG_CHAR_JUMP_END:
		{
			CGameSession::SendGameCharJumpEnd(pPacket);
		}
		break;
		case UG_CHAR_DEST_MOVE:
		{
			CGameSession::SendGameCharDestMove(pPacket);
		}
		break;
		case UG_CHAR_FALLING:
		{
			CGameSession::SendGameCharFalling(pPacket);
		}
		break;
		case UG_CHAR_FOLLOW_MOVE:
		{
			CGameSession::SendGameCharFollowMove(pPacket);
		}
		break;
		case UG_CHAR_DASH_KEYBOARD:
		{
			CGameSession::SendGameCharDashKeyboard(pPacket);
		}
		break;
		case UG_CHAR_DASH_MOUSE:
		{
			CGameSession::SendGameCharDashMouse(pPacket);
		}
		break;
		case UG_CHAR_TOGG_SITDOWN:
		{
			CGameSession::SendGameCharToggSitDown(pPacket);
		}
		break;
		case UG_CHAR_TOGG_RUNNING:
		{
			CGameSession::SendGameCharToggRunning(pPacket);
		}
		break;
		case UG_CHAR_TARGET_FACING:
		{
			CGameSession::SendGameCharTargetFace(pPacket);
		}
		break;
		case UG_CHAR_TARGET_INFO:
		{
			CGameSession::SendGameCharTargetInfo(pPacket);
		}
		break;
		case UG_CHAR_TARGET_SELECT:
		{
			CGameSession::SendGameCharTargetSelect(pPacket);
		}
		break;
		case UG_CHAR_ATTACK_BEGIN:
		{
			CGameSession::SendGameCharAttackBegin(pPacket);
		}
		break;
		case UG_CHAR_ATTACK_END:
		{
			CGameSession::SendGameCharAttackEnd(pPacket);
		}
		break;
		case UG_CHAR_CHARGE:
		{
			CGameSession::SendGameCharCharge(pPacket);
		}
		break;
		case UG_CHAR_BLOCK_MODE:
		{
			CGameSession::SendGameCharBlockMode(pPacket);
		}
		break;
		case UG_CHAR_TELEPORT_REQ:
		{
			CGameSession::SendGameCharTeleportReq(pPacket);
		}
		break;
		case UG_CHAR_BIND_REQ:
		{
			CGameSession::SendGameCharBindReq(pPacket);
		}
		break;
		case UG_CHAR_REVIVAL_REQ:
		{
			CGameSession::SendGameCharRevivalReq(pPacket);
		}
		break;
		case UG_CHAR_SERVER_CHANGE_REQ:
		{
			CGameSession::SendGameCharServerChangeReq(pPacket);
		}
		break;
		case UG_CHAR_CHANNEL_CHANGE_REQ:
		{
			CGameSession::SendGameCharChanneChangeReq(pPacket);
		}
		break;
		case UG_CHAR_EXIT_REQ:
		{
			CGameSession::SendGameCharExitReq(pPacket);
		}
		break;
		case UG_SERVER_COMMAND:
		{
			CGameSession::SendGameServerCommand(pPacket);
		}
		break;
		case UG_GAME_LEAVE_REQ:
		{
			CGameSession::SendGameCharLeaveReq(pPacket);
		}
		break;
		case UG_GAME_EXIT_REQ:
		{
			CGameSession::SendGameCharGameExitReq(pPacket);
		}
		break;
		case UG_CHAR_AWAY_REQ:
		{
			CGameSession::SendGameCharAwayReq(pPacket);
		}
		break;
		case UG_CHAR_TOGG_FIGHTING:
		{

		}
		break;
		case UG_CHAR_KEY_UPDATE_REQ:
		{
			
		}
		break;
		case UG_PARTY_INVITE_REQ:
		{
			CGameSession::SendGameCharPartyInvite(pPacket);
		}
		break;
		case UG_PARTY_INVITE_CHARID_REQ:
		{
			CGameSession::SendGameCharPartyInviteCharId(pPacket);
		}
		break;
		case UG_PARTY_INVITE_CHAR_NAME_REQ:
		{
			CGameSession::SendGameCharPartyInviteCharName(pPacket);
		}
		break;
		case UG_CHAR_DIRECT_PLAY_ACK:
		{
			CGameSession::SendGameCharDirectPlayAck(pPacket);
		}
		break;
		case UG_TUTORIAL_PLAY_QUIT_REQ:
		{
			CGameSession::SendGameCharDirectPlayCancel(pPacket);
		}
		break;
		case UG_CHAR_KNOCKDOWN_RELEASE_NFY:
		{
			CGameSession::SendGameCharKnockdownRelease(pPacket);
		}
		break;
		case UG_CHAR_SKILL_REQ:
		{
			CGameSession::SendGameCharSkillReq(pPacket);
		}
		break;
		case UG_SKILL_TARGET_LIST:
		{
			NTL_PRINT(PRINT_APP, "UG SKILL TARGET LIST NEED BE IMPLEMENTED\n");
		}
		break;
		case UG_SHOP_NETPYITEM_START_REQ:
		{
			CGameSession::SendGameNetPyStart(pPacket);
		}
		break;
		case UG_SHOP_NETPYITEM_END_REQ:
		{
			CGameSession::SendGameNetPyEnd(pPacket);
		}
		break;
		case UG_DURATION_ITEM_BUY_REQ:
		{
			CGameSession::SendGameDurationItemBuy(pPacket);
		}
		break;
		case UG_SKILL_LEARN_REQ:
		{
			CGameSession::SendGameCharSkillLearn(pPacket);
		}
		break;
		case UG_SKILL_UPGRADE_REQ:
		{
			CGameSession::SendGameCharSkillUpgrade(pPacket);
		}
		break;
		case UG_SKILL_RP_BONUS_SETTING_REQ:
		{
		}
		break;
		case UG_HTB_START_REQ:
		{
		}
		break;
		case UG_HTB_LEARN_REQ:
		{
		}
		break;
		case UG_HTB_FORWARD_REQ:
		{
		}
		break;
		case UG_HTB_RP_BALL_USE_REQ:
		{
		}
		break;
		case UG_BANK_LOAD_REQ:
		{
			CGameSession::SendGameCharBankLoad(pPacket);
		}
		break;
		case UG_BANK_END_REQ:
		{
			CGameSession::SendGameCharBankEnd(pPacket);
		}
		break;
		case UG_BANK_START_REQ:
		{
			CGameSession::SendGameCharBankStart(pPacket);
		}
		break;
		case UG_BANK_ZENNY_REQ:
		{
			CGameSession::SendGameCharBankZennyInfo(pPacket);
		}
		break;
		case UG_BUFF_DROP_REQ:
		{
		}
		break;
		case UG_ITEM_MOVE_REQ:
		{
			CGameSession::SendGameCharItemMove(pPacket);
		}
		break;
		case UG_ITEM_MOVE_STACK_REQ:
		{
		}
		break;
		case UG_ITEM_DELETE_REQ:
		{
			CGameSession::SendGameCharItemDelete(pPacket);
		}
		break;
		case UG_ITEM_EQUIP_REPAIR_REQ:
		{
		}
		break;
		case UG_ITEM_PICK_REQ:
		{
			CGameSession::SendGameCharItemPickUp(pPacket);
		}
		break;
		case UG_ITEM_REPAIR_REQ:
		{
		}
		break;
		case UG_ITEM_USE_REQ:
		{
		}
		break;
		case UG_ITEM_UPGRADE_REQ:
		{
		}
		break;
		case UG_ITEM_IDENTIFY_REQ:
		{
		}
		break;
		case UG_SHOP_BUY_REQ:
		{
			CGameSession::SendGameShopBuy(pPacket);
		}
		break;
		case UG_SHOP_END_REQ:
		{
			CGameSession::SendGameShopEnd(pPacket);
		}
		break;
		case UG_SHOP_START_REQ:
		{
			CGameSession::SendGameShopStart(pPacket);
		}
		break;
		default:
		{
			if (pHeader->wOpCode != SYS_ALIVE)
			{
				NTL_PRINT(PRINT_APP, "The following packet was not handled: %s", NtlGetPacketName(pHeader->wOpCode));
			}
			return CNtlSession::OnDispatch(pPacket);
		}
	}

	return NTL_SUCCESS;
}

void CGameSession::SendGameEnterRes(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	sUG_GAME_ENTER_REQ* req = (sUG_GAME_ENTER_REQ*)pPacket->GetPacketData();	
	app->Add(req->charId, this->GetHandle());
	this->charId = req->charId;	
	this->accountId = req->accountId;

	CNtlPacket packet(sizeof(sGU_GAME_ENTER_RES));
	sGU_GAME_ENTER_RES* res = (sGU_GAME_ENTER_RES*)packet.GetPacketData();
	strcpy_s(res->achCommunityServerIP, sizeof(res->achCommunityServerIP), app->m_config.strCommunityServerIP.c_str());
	res->wCommunityServerPort = app->m_config.wCommunityServerPort;
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = GU_GAME_ENTER_RES;
	packet.SetPacketLen(sizeof(sGU_GAME_ENTER_RES));
	app->Send(this->GetHandle() , &packet);
	CGameCharacter* plr = app->GetPlayerByCharID(req->charId);
	plr->SetPlayerInTutorial(req->bTutorialMode);
	plr->SendCharInfo();
	plr->SendItemInfo();
	plr->SendSkillInfo();
	plr->SendBuffInfo();	
	plr->SendHTBSkillInfo();
	plr->SendQuickSlotInfo();
	plr->SendWarFogInfo();
	//plr->SendKeyInfo();//ShortCut
	plr->SendCharInfoEnd();
}

void CGameSession::SendGameWorldEnter(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	sUG_ENTER_WORLD* req = (sUG_ENTER_WORLD*)pPacket->GetPacketData();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	plr->SendWorldInfo();
	plr->SendCreatePlayer();		
	plr->SendNetPyNotification();
}

void CGameSession::SendGameWorldComplete()
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGU_ENTER_WORLD_COMPLETE));
	sGU_ENTER_WORLD_COMPLETE* res = (sGU_ENTER_WORLD_COMPLETE*)packet.GetPacketData();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	res->wOpCode = GU_ENTER_WORLD_COMPLETE;
	packet.SetPacketLen(sizeof(sGU_ENTER_WORLD_COMPLETE));
	app->Send(app->GetHSession(plr->GetCharID()), &packet);
}
void CGameSession::SendGameCharMove(CNtlPacket* pPacket)
{
	sUG_CHAR_MOVE* req = (sUG_CHAR_MOVE*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);	
	sVECTOR3 dir;
	dir.x = req->vCurDir.x;
	dir.y = 0.0f;
	dir.z = req->vCurDir.z;
	plr->SendCharMove(&dir,&req->vCurLoc,req->byMoveDirection);
	plr->SetPosition(req->vCurLoc.x, req->vCurLoc.y, req->vCurLoc.z);
	plr->SetDir(req->vCurDir.x, 0.0f, req->vCurDir.z);
}

void CGameSession::SendGameCharMoveSync(CNtlPacket* pPacket)
{
	sUG_CHAR_MOVE_SYNC* req = (sUG_CHAR_MOVE_SYNC*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	plr->SendCharMoveSync(&req->vCurDir,&req->vCurLoc);
	plr->SetPosition(req->vCurLoc.x, req->vCurLoc.y, req->vCurLoc.z);
	plr->SetDir(req->vCurDir.x, req->vCurDir.y, req->vCurDir.z);
	plr->SendNpcCheck();
	plr->SendMobCheck();
}

void CGameSession::SendGameCharReady(CNtlPacket* pPacket)
{
	sUG_CHAR_READY* req = (sUG_CHAR_READY*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);	

	CNtlPacket packet2(sizeof(sGT_USER_ENTER_GAME));
	sGT_USER_ENTER_GAME* res2 = (sGT_USER_ENTER_GAME*)packet2.GetPacketData();
	res2->accountId = plr->GetAccountID();
	wcscpy_s(res2->awchName, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1, plr->GetCharName());
	res2->byClass = plr->GetClass();
	res2->byLevel = plr->GetLevel();
	res2->byRace = plr->GetRace();
	res2->charId = plr->GetCharID();
	res2->hHandle = plr->GetHObject();
	res2->worldId = plr->GetCurrentWorld();
	res2->mapNameTblidx = plr->GetMapNameTblidx();
	res2->vCurrentPosition = plr->GetPosition();
	res2->wOpCode = GT_USER_ENTER_GAME;
	res2->guildId = 0;//plr->GetGuild()->GetGuildID();
	res2->gmGroupId = 0;//???
	res2->dwPunishBitFlag = 0;// plr->GetPunish()->GetPunishBitFlag();
	res2->dwReputation = plr->GetReputation();
	packet2.SetPacketLen(sizeof(sGT_USER_ENTER_GAME));
	app->GetCommunitySession()->PushPacket(&packet2);

	plr->SendNpcSpawn();
	plr->SendMobSpawn();
}

void CGameSession::SendGameCharReadyForCommunity(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CNtlPacket packet(sizeof(sGT_CHAR_READY_FOR_COMMUNITY_SERVER_NFY));
	sGT_CHAR_READY_FOR_COMMUNITY_SERVER_NFY* res = (sGT_CHAR_READY_FOR_COMMUNITY_SERVER_NFY*)packet.GetPacketData();
	res->charId = this->charId;
	res->wOpCode = GT_CHAR_READY_FOR_COMMUNITY_SERVER_NFY;
	packet.SetPacketLen(sizeof(sGT_CHAR_READY_FOR_COMMUNITY_SERVER_NFY));
	app->GetCommunitySession()->PushPacket(&packet);
}

void CGameSession::SendGameCharReadySpawn(CNtlPacket* pPacket)
{
	sUG_CHAR_READY_TO_SPAWN* req = (sUG_CHAR_READY_TO_SPAWN*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);	
}

void CGameSession::SendGameCommunityKeyReq(CNtlPacket* pPacket)
{
	sUG_AUTH_KEY_FOR_COMMUNITY_SERVER_REQ* req = (sUG_AUTH_KEY_FOR_COMMUNITY_SERVER_REQ*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	BYTE abyAuthKey[NTL_MAX_SIZE_AUTH_KEY];
	strcpy_s((char*)abyAuthKey, NTL_MAX_SIZE_AUTH_KEY, "ChatCon");
	plr->SendCommunityKeyReq(abyAuthKey);

	CNtlPacket packet(sizeof(sGT_USER_AUTH_KEY_CREATED_NFY));
	sGT_USER_AUTH_KEY_CREATED_NFY* res = (sGT_USER_AUTH_KEY_CREATED_NFY*)packet.GetPacketData();
	res->accountId = plr->GetAccountID();
	memcpy(res->abyAuthKey, abyAuthKey, NTL_MAX_SIZE_AUTH_KEY);
	res->charId = plr->GetCharID();
	res->wOpCode = GT_USER_AUTH_KEY_CREATED_NFY;
	packet.SetPacketLen(sizeof(sGT_USER_AUTH_KEY_CREATED_NFY));
	app->GetCommunitySession()->PushPacket(&packet);
}

void CGameSession::SendGameCharChangeDirFloating(CNtlPacket* pPacket)
{
	sUG_CHAR_CHANGE_DIRECTION_ON_FLOATING* req = (sUG_CHAR_CHANGE_DIRECTION_ON_FLOATING*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	plr->SendCharChangeDirFloating(&req->vCurDir,req->byMoveDirection);

	plr->SetDir(req->vCurDir.x, req->vCurDir.y, req->vCurDir.z);
}

void CGameSession::SendGameCharChangeHeading(CNtlPacket* pPacket)
{
	sUG_CHAR_CHANGE_HEADING* req = (sUG_CHAR_CHANGE_HEADING*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	plr->SendCharChangeHeading(&req->vCurrentHeading,&req->vCurrentPosition);
	plr->SetDir(req->vCurrentHeading.x, req->vCurrentHeading.y, req->vCurrentHeading.z);
	plr->SetPosition(req->vCurrentPosition.x, req->vCurrentPosition.y, req->vCurrentPosition.z);
}

void CGameSession::SendGameCharJump(CNtlPacket* pPacket)
{
	sUG_CHAR_JUMP* req = (sUG_CHAR_JUMP*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	plr->SendCharJump(&req->vCurrentHeading, NTL_MOVE_STATUS_JUMP);
	plr->SetDir(req->vCurrentHeading.x, req->vCurrentHeading.y, req->vCurrentHeading.z);	
}

void CGameSession::SendGameCharJumpEnd(CNtlPacket* pPacket)
{
	sUG_CHAR_JUMP_END* req = (sUG_CHAR_JUMP_END*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	plr->SendCharJumpEnd();
}

void CGameSession::SendGameCharDestMove(CNtlPacket* pPacket)
{
	sUG_CHAR_DEST_MOVE* req = (sUG_CHAR_DEST_MOVE*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	plr->SetPosition(req->vCurLoc.x, req->vCurLoc.y, req->vCurLoc.z);
	plr->SendCharDestMove(req->vDestLoc, req->dwTimeStamp);
}

void CGameSession::SendGameCharFalling(CNtlPacket* pPacket)
{
	sUG_CHAR_FALLING* req = (sUG_CHAR_FALLING*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	plr->SetDir(req->vCurDir.x, 0.0f, req->vCurDir.z);
	plr->SetPosition(req->vCurLoc.x, req->vCurLoc.y, req->vCurLoc.z);
}

void CGameSession::SendGameNetPyStart(CNtlPacket* pPacket)
{
	sUG_SHOP_NETPYITEM_START_REQ* req = (sUG_SHOP_NETPYITEM_START_REQ*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	CNtlPacket packet(sizeof(sGU_SHOP_NETPYITEM_START_RES));
	sGU_SHOP_NETPYITEM_START_RES* res = (sGU_SHOP_NETPYITEM_START_RES*)packet.GetPacketData();
	res->byType = 0;//eNETPYSHOP_EVENT_START
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = GU_SHOP_NETPYITEM_START_RES;
	packet.SetPacketLen(sizeof(sGU_SHOP_NETPYITEM_START_RES));
	app->Send(app->GetHSession(plr->GetCharID()), &packet);
}

void CGameSession::SendGameNetPyEnd(CNtlPacket* pPacket)
{
	sUG_SHOP_NETPYITEM_END_REQ* req = (sUG_SHOP_NETPYITEM_END_REQ*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	CNtlPacket packet(sizeof(sGU_SHOP_NETPYITEM_END_RES));
	sGU_SHOP_NETPYITEM_END_RES* res = (sGU_SHOP_NETPYITEM_END_RES*)packet.GetPacketData();
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = GU_SHOP_NETPYITEM_END_RES;
	packet.SetPacketLen(sizeof(sGU_SHOP_NETPYITEM_END_RES));
	app->Send(app->GetHSession(plr->GetCharID()), &packet);
}

void CGameSession::SendGameDurationItemBuy(CNtlPacket* pPacket)
{
	sUG_DURATION_ITEM_BUY_REQ* req = (sUG_DURATION_ITEM_BUY_REQ*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	CNtlPacket packet(sizeof(sGU_DURATION_ITEM_BUY_RES));
	sGU_DURATION_ITEM_BUY_RES* res = (sGU_DURATION_ITEM_BUY_RES*)packet.GetPacketData();
	res->byPos = req->byPos;
	res->merchantTblidx = req->merchantTblidx;
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = GU_DURATION_ITEM_BUY_RES;
	packet.SetPacketLen(sizeof(sGU_DURATION_ITEM_BUY_RES));
	app->Send(app->GetHSession(plr->GetCharID()), &packet);
}

void CGameSession::SendGameCharFollowMove(CNtlPacket* pPacket)
{
	sUG_CHAR_FOLLOW_MOVE* req = (sUG_CHAR_FOLLOW_MOVE*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	CGameCharacter* plr2 = app->GetPlayerByHandle(req->hTarget);
	//TODO:Add Condition for mob in other words first to NPC server and after NPC throwback here
	//The hTarget can be a Player(Check in Game Server) or a Mob/Npc(Check in NPC Server)
	//you need check through byAvatarType
	// if == 0 then Player else is a NPC or Mob
	float fDistance = 0.0f;
	if (NULL != plr2)
		fDistance = NtlGetDistance(plr->GetPosition().x, plr->GetPosition().z, plr2->GetPosition().x, plr2->GetPosition().z);	
	plr->SendCharFollowMove(fDistance, NTL_MOVE_FOLLOW_MOVEMENT, req->byMovementReason, req->dwTimeStamp, req->hTarget);	
}

void CGameSession::SendGameCharToggSitDown(CNtlPacket* pPacket)
{
	sUG_CHAR_TOGG_SITDOWN* req = (sUG_CHAR_TOGG_SITDOWN*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);	
	if (plr->IsStandup())
	{
		plr->SetStandUp(req->bSitDown);//This will be false on player
		plr->SendCharToggSitDown();
	}
	else
	{
		plr->SetStandUp(req->bSitDown);//This will be true on player
		plr->SendCharToggStandUp();
	}
}

void CGameSession::SendGameCharToggRunning(CNtlPacket* pPacket)
{
	sUG_CHAR_TOGG_RUNNING* req = (sUG_CHAR_TOGG_RUNNING*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
}

void CGameSession::SendGameCharDashKeyboard(CNtlPacket* pPacket)
{
	sUG_CHAR_DASH_KEYBOARD* req = (sUG_CHAR_DASH_KEYBOARD*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	plr->SetDir(req->vCurDir.x, 0.0f, req->vCurDir.z);
	plr->SetPosition(req->vCurLoc.x, req->vCurLoc.y, req->vCurLoc.z);
}

void CGameSession::SendGameCharDashMouse(CNtlPacket* pPacket)
{
	sUG_CHAR_DASH_MOUSE* req = (sUG_CHAR_DASH_MOUSE*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	plr->SetPosition(req->vDestLoc.x, req->vDestLoc.y, req->vDestLoc.z);
}

void CGameSession::SendGameCharTargetFace(CNtlPacket* pPacket)
{
	sUG_CHAR_TARGET_FACING* req = (sUG_CHAR_TARGET_FACING*)pPacket->GetPacketData();	
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
}

void CGameSession::SendGameCharTargetInfo(CNtlPacket* pPacket)
{
	sUG_CHAR_TARGET_INFO* req = (sUG_CHAR_TARGET_INFO*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);	
}

void CGameSession::SendGameCharTargetSelect(CNtlPacket* pPacket)
{
	sUG_CHAR_TARGET_SELECT* req = (sUG_CHAR_TARGET_SELECT*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
}

void CGameSession::SendGameCharAttackBegin(CNtlPacket* pPacket)
{
	sUG_CHAR_ATTACK_BEGIN* req = (sUG_CHAR_ATTACK_BEGIN*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);	
}

void CGameSession::SendGameCharAttackEnd(CNtlPacket* pPacket)
{
	sUG_CHAR_ATTACK_END* req = (sUG_CHAR_ATTACK_END*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);	
}

void CGameSession::SendGameCharCharge(CNtlPacket* pPacket)
{
	sUG_CHAR_CHARGE* req = (sUG_CHAR_CHARGE*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	plr->SetCharging(req->bCharge);
	if (plr->IsCharging())
	{
		while (plr->IsCharging())
		{ 
			if (plr->IsCharCanCharge())
			{
				plr->IncreaseRP();
				plr->SendCharCharging();
			}
			else
			{
				plr->SetCharging(false);
				plr->SendCharChargingCancel();
			}				
		}		
	}
	else
	{
		plr->SendCharChargingCancel();
	}
}

void CGameSession::SendGameCharBlockMode(CNtlPacket* pPacket)
{
	sUG_CHAR_BLOCK_MODE* req = (sUG_CHAR_BLOCK_MODE*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	if (!plr->IsBlocking())
	{
		CNtlPacket packet(sizeof(sGU_CHAR_BLOCK_MODE_COOL_TIME_NFY));
		sGU_CHAR_BLOCK_MODE_COOL_TIME_NFY* res = (sGU_CHAR_BLOCK_MODE_COOL_TIME_NFY*)packet.GetPacketData();
		res->dwCoolTimeRemaining = 12000;//TODO:Reduce 2000 each level of skill
		res->wOpCode = GU_CHAR_BLOCK_MODE_COOL_TIME_NFY;
		packet.SetPacketLen(sizeof(sGU_CHAR_BLOCK_MODE_COOL_TIME_NFY));
		app->Send(app->GetHSession(this->charId), &packet);
	}	
}

void CGameSession::SendGameCharBindReq(CNtlPacket* pPacket)
{
	sUG_CHAR_BIND_REQ* req = (sUG_CHAR_BIND_REQ*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	sOBJECT_TBLDAT* pOBJECT_TBLDAT = reinterpret_cast<sOBJECT_TBLDAT*>(app->GetTableContainer()->GetObjectTable(plr->GetCurrentWorld())->FindData(req->bindObjectTblidx));
	
	CNtlPacket packet(sizeof(sGU_CHAR_BIND_RES));
	sGU_CHAR_BIND_RES* res = (sGU_CHAR_BIND_RES*)packet.GetPacketData();	
	res->wOpCode = GU_CHAR_BIND_RES;
	if (BIT_FLAG_TEST(pOBJECT_TBLDAT->wFunction, eDBO_TRIGGER_OBJECT_FUNC_BIND))
	{
		res->bindWorldId = plr->GetCurrentWorld();
		res->bindObjectTblidx = req->bindObjectTblidx;
		res->byBindType = DBO_BIND_TYPE_POPO_STONE;
		res->wResultCode = GAME_SUCCESS;
		plr->GetOtherParam()->SetBindType(DBO_BIND_TYPE_POPO_STONE);
		plr->GetOtherParam()->SetBindWorldID(res->bindWorldId);
		plr->GetOtherParam()->SetBindObjectTblidx(req->bindObjectTblidx);
		plr->GetOtherParam()->SetBindPosition(plr->GetPosition().x, plr->GetPosition().y, plr->GetPosition().z);
		plr->GetOtherParam()->SetBindDir(plr->GetDir().x, plr->GetDir().y, plr->GetDir().z);
	}
	else
	{
		res->wResultCode = GAME_FAIL;
	}
	packet.SetPacketLen(sizeof(sGU_CHAR_BIND_RES));
	app->Send(app->GetHSession(this->charId), &packet);
}

void CGameSession::SendGameCharSkillReq(CNtlPacket* pPacket)
{
	sUG_CHAR_SKILL_REQ* req = (sUG_CHAR_SKILL_REQ*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	CNtlPacket packet(sizeof(sGU_CHAR_SKILL_RES));
	sGU_CHAR_SKILL_RES* res = (sGU_CHAR_SKILL_RES*)packet.GetPacketData();
	res->wOpCode = GU_CHAR_SKILL_RES;
	res->wResultCode = GAME_SUCCESS;
	packet.SetPacketLen(sizeof(sGU_CHAR_SKILL_RES));
	app->Send(app->GetHSession(this->charId), &packet);//Just reply the client
	//TODO: do something with that!
	if (req->byApplyTargetCount > 1)
	{
		for (int i = 0; i < req->byApplyTargetCount; i++)
		{ 
			CGameCharacter* dumPlayer = app->GetPlayerByHandle(req->ahApplyTarget[i]);
			if (NULL != dumPlayer)
			{
				//What we gonna do with each player in our req?
			}
			else
			{
				//Dispatch to NPC Server????
			}
		}
	}
	else
	{
		CGameCharacter* dumPlayer = app->GetPlayerByHandle(req->hTarget);
		if (NULL != dumPlayer)
		{
			//What we gonna do with our player target?
		}
		else
		{
			//Dispatch to NPC Server?????
		}
	}
}

void CGameSession::SendGameCharKnockdownRelease(CNtlPacket* pPacket)
{
	sUG_CHAR_KNOCKDOWN_RELEASE_NFY* req = (sUG_CHAR_KNOCKDOWN_RELEASE_NFY*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
}

void CGameSession::SendGameCharRevivalReq(CNtlPacket* pPacket)
{
	sUG_CHAR_REVIVAL_REQ* req = (sUG_CHAR_REVIVAL_REQ*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	CNtlPacket packet(sizeof(sGU_CHAR_REVIVAL_RES));
	sGU_CHAR_REVIVAL_RES* res = (sGU_CHAR_REVIVAL_RES*)packet.GetPacketData();
	res->wOpCode = GU_CHAR_REVIVAL_RES;
	res->wResultCode = GAME_SUCCESS;
	packet.SetPacketLen(sizeof(sGU_CHAR_REVIVAL_RES));
	app->Send(app->GetHSession(this->charId), &packet);
	/*
	Info: this cases, after reading the client code, seens he only send the second case
	when your character is on TLQ mode, if is in any mode else he send the first case
	*/
	switch (req->byRevivalRequestType)
	{
		case DBO_REVIVAL_REQUEST_TYPE_TELEPORT_SOMEWHERE:
		{
			plr->SendCharRevivalPopoStone();
		}
		break;
		case DBO_REVIVAL_REQUEST_TYPE_CURRENT_POSITION:
		{
			plr->SendCharRevivalCurrentLoc();
		}
		break;
		default:
			NTL_PRINT(PRINT_APP, "Revival Not Implemented yet");
		break;
	}
}

//Not implemented yet
void CGameSession::SendGameCharServerChangeReq(CNtlPacket* pPacket)
{
	sUG_CHAR_SERVER_CHANGE_REQ* req = (sUG_CHAR_SERVER_CHANGE_REQ*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
}

//Not implemented yet
void CGameSession::SendGameCharChanneChangeReq(CNtlPacket* pPacket)
{
	sUG_CHAR_CHANNEL_CHANGE_REQ* req = (sUG_CHAR_CHANNEL_CHANGE_REQ*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);	
}

void CGameSession::SendGameCharExitReq(CNtlPacket* pPacket)
{
	sUG_CHAR_EXIT_REQ* req = (sUG_CHAR_EXIT_REQ*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	CNtlPacket packet(sizeof(sGU_CHAR_EXIT_RES));
	sGU_CHAR_EXIT_RES* res = (sGU_CHAR_EXIT_RES*)packet.GetPacketData();
	strcpy_s(res->aServerInfo[0].szCharacterServerIP, sizeof(res->aServerInfo[0].szCharacterServerIP), app->m_config.strCharServerIP.c_str());
	res->aServerInfo[0].wCharacterServerPortForClient = app->m_config.wCharServerPort;
	res->byServerInfoCount = 1;	
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = GU_CHAR_EXIT_RES;
	packet.SetPacketLen(sizeof(sGU_CHAR_EXIT_RES));
	app->Send(app->GetHSession(this->charId), &packet);
	app->Remove(this->GetHandle());
	app->RemovePlayer(this->charId, plr);
}

void CGameSession::SendGameCharGameExitReq(CNtlPacket* pPacket)
{
	sUG_GAME_EXIT_REQ* req = (sUG_GAME_EXIT_REQ*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	app->Remove(this->GetHandle());
	app->RemovePlayer(plr->GetCharID(), plr);
	CNtlPacket packet(sizeof(sGU_GAME_EXIT_RES));
	sGU_GAME_EXIT_RES* res = (sGU_GAME_EXIT_RES*)packet.GetPacketData();
	res->wOpCode = GU_GAME_EXIT_RES;
	packet.SetPacketLen(sizeof(sGU_GAME_EXIT_RES));
	app->Send(app->GetHSession(this->charId), &packet);
}

void CGameSession::SendGameServerCommand(CNtlPacket* pPacket)
{
	sUG_SERVER_COMMAND* req = (sUG_SERVER_COMMAND*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);

	char chBuffer[2048];
	::WideCharToMultiByte(GetACP(), 0, req->awchCommand, -1, chBuffer, 2048, NULL, NULL);	
	CNtlTokenizer lexer(chBuffer);
	int iOldLine = 0;
	int iLine;

	if (!lexer.IsSuccess())
		return;
	while (1)
	{
		std::string strToken = lexer.PeekNextToken(NULL, &iLine);

		if (strToken == "")
			break;
		
		if (strToken == "@createnpc")
		{
			lexer.PopToPeek();
			strToken = lexer.PeekNextToken(NULL, &iLine);
			TBLIDX npcID = (TBLIDX)atoi(strToken.c_str());
			CNtlPacket packet(sizeof(sGN_ADMIN_NPC_SPAWN));
			sGN_ADMIN_NPC_SPAWN* req = (sGN_ADMIN_NPC_SPAWN*)packet.GetPacketData();
			req->hLastSerial = app->AcquireSerialId();
			req->npcID = npcID;
			req->vPlayerDir = plr->GetDir();
			req->vPlayerPos = plr->GetPosition();
			req->worldIdx = plr->GetCurrentWorld();
			req->wOpCode = GN_ADMIN_NPC_SPAWN;
			packet.SetPacketLen(sizeof(sGN_ADMIN_NPC_SPAWN));
			app->GetNpcSession()->PushPacket(&packet);
			return;
		}
		if (strToken == "@updatems")
		{
			lexer.PopToPeek();
			strToken = lexer.PeekNextToken(NULL, &iLine);
			DWORD dwMoveSpeed = (DWORD)atoi(strToken.c_str());
			CNtlPacket packet(sizeof(sGU_UPDATE_CHAR_SPEED));
			sGU_UPDATE_CHAR_SPEED* res = (sGU_UPDATE_CHAR_SPEED*)packet.GetPacketData();
			res->handle = plr->GetHObject();
			res->fLastRunningSpeed = dwMoveSpeed;
			res->fLastWalkingSpeed = dwMoveSpeed;
			res->wOpCode = GU_UPDATE_CHAR_SPEED;
			packet.SetPacketLen(sizeof(sGU_UPDATE_CHAR_SPEED));
			app->SendAll(&packet);
			return;
		}
		if (strToken == "@createitem")
		{
			lexer.PopToPeek();
			strToken = lexer.PeekNextToken(NULL, &iLine);
			TBLIDX itemID = (TBLIDX)atoi(strToken.c_str());
			CNtlPacket packet(sizeof(sGU_SYSTEM_DISPLAY_TEXT));	
			sGU_SYSTEM_DISPLAY_TEXT* res = (sGU_SYSTEM_DISPLAY_TEXT*)packet.GetPacketData();			
			wcscpy_s(res->awGMChar, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1, plr->GetCharName());
			wcscpy_s(res->awchMessage, NTL_MAX_LENGTH_OF_CHAT_MESSAGE_UNICODE + 1, L"Not implemented yet");
			res->wMessageLengthInUnicode = 257;
			res->byDisplayType = SERVER_TEXT_EMERGENCY;
			res->wOpCode = GU_SYSTEM_DISPLAY_TEXT;
			packet.SetPacketLen(sizeof(sGU_SYSTEM_DISPLAY_TEXT));
			app->Send(app->GetHSession(plr->GetCharID()), &packet);
			return;
		}
		lexer.PopToPeek();
	}
}

void CGameSession::SendGameCharLeaveReq(CNtlPacket* pPacket)
{
	//Release the session in game server
	//Send packet to CharServer telling the player is out
	//Send packet to AuthServer Telling the player is out
}

void CGameSession::SendGameCharAwayReq(CNtlPacket* pPacket)
{
	sUG_CHAR_AWAY_REQ* req = (sUG_CHAR_AWAY_REQ*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);	
	CNtlPacket packet(sizeof(sGU_CHAR_AWAY_RES));
	sGU_CHAR_AWAY_RES* res = (sGU_CHAR_AWAY_RES*)packet.GetPacketData();	
	res->bIsAway = req->bIsAway;
	res->wOpCode = GU_CHAR_AWAY_RES;
	res->wResultCode = GAME_SUCCESS;
	packet.SetPacketLen(sizeof(sGU_CHAR_AWAY_RES));
	app->Send(app->GetHSession(this->charId), &packet);
	if (plr->IsAway())
	{
		plr->SetAway(req->bIsAway);
	}
	else
	{
		plr->SetAway(req->bIsAway);
	}
}
void CGameSession::SendGameCharDirectPlayAck(CNtlPacket* pPacket)
{
	sUG_CHAR_DIRECT_PLAY_ACK* req = (sUG_CHAR_DIRECT_PLAY_ACK*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	sWORLD_TBLDAT* pWorldTblData = (sWORLD_TBLDAT*)app->GetTableContainer()->GetWorldTable()->FindData(plr->GetCurrentWorld());
	sDUNGEON_TBLDAT* pDungeonData = (sDUNGEON_TBLDAT*)app->GetTableContainer()->GetDungeonTable()->FindData(pWorldTblData->worldRuleTbldx);
	if (INVALID_TBLIDX != pDungeonData->openCine)
	{
		CDirectionLinkTable* pLinkTbl = app->GetTableContainer()->GetDirectionLinkTable();
		sDIRECTION_LINK_TBLDAT* pLinkTblData = reinterpret_cast<sDIRECTION_LINK_TBLDAT*>(pLinkTbl->FindData(pDungeonData->openCine));
		CNtlPacket packet(sizeof(sGU_CHAR_DIRECT_PLAY));
		sGU_CHAR_DIRECT_PLAY* res = (sGU_CHAR_DIRECT_PLAY*)packet.GetPacketData();
		res->byPlayMode = true;
		res->directTblidx = pLinkTblData->tblidx;
		res->hSubject = plr->GetHObject();
		res->bSynchronize = true;
		res->wOpCode = GU_CHAR_DIRECT_PLAY;
		packet.SetPacketLen(sizeof(sGU_CHAR_DIRECT_PLAY));
		if (pWorldTblData->worldRuleTbldx == GAMERULE_TLQ)
		{
			//TODO: if(plr->GetParty()->IsInParty()) then throw in a loop and send to all HSESSION on party list
			app->Send(app->GetHSession(this->charId), &packet);
		}
		else
			app->SendAll(&packet);
	}
	else
	{
		CNtlPacket packet(sizeof(sGU_CHAR_DIRECT_PLAY_END));
		sGU_CHAR_DIRECT_PLAY_END* res = (sGU_CHAR_DIRECT_PLAY_END*)packet.GetPacketData();
		res->wOpCode = GU_CHAR_DIRECT_PLAY_END;
		packet.SetPacketLen(sizeof(sGU_CHAR_DIRECT_PLAY_END));
		app->Send(app->GetHSession(this->charId), &packet);
	}
}

void CGameSession::SendGameCharDirectPlayCancel(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	sUG_TUTORIAL_PLAY_QUIT_REQ* req = (sUG_TUTORIAL_PLAY_QUIT_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sGU_TUTORIAL_PLAY_QUIT_RES));
	sGU_TUTORIAL_PLAY_QUIT_RES* res = (sGU_TUTORIAL_PLAY_QUIT_RES*)packet.GetPacketData();
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = GU_TUTORIAL_PLAY_QUIT_RES;
	packet.SetPacketLen(sizeof(sGU_TUTORIAL_PLAY_QUIT_RES));
	app->Send(app->GetHSession(this->charId), &packet);
	plr->SwitchBindToLocal();
	plr->SendCharRevivalPopoStone();
}

//Need be checked wich process the client send this req
void CGameSession::SendGameCharTeleportReq(CNtlPacket* pPacket)
{
	sUG_CHAR_TELEPORT_REQ* req = (sUG_CHAR_TELEPORT_REQ*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	NTL_PRINT(PRINT_APP, "UG TELEPORT REQ MUST BE CHECKED \n");
}

void CGameSession::SendGameCharSkillLearn(CNtlPacket* pPacket)
{
	sUG_SKILL_LEARN_REQ* req = (sUG_SKILL_LEARN_REQ*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	sSKILL_TBLDAT* pSkillTbldat = reinterpret_cast<sSKILL_TBLDAT*>(app->GetTableContainer()->GetSkillTable()->FindData(req->skillTblidx));
	
	CNtlPacket packet(sizeof(sGQ_SKILL_ADD_REQ));
	sGQ_SKILL_ADD_REQ* res = (sGQ_SKILL_ADD_REQ*)packet.GetPacketData();
	res->byRpBonusType = DBO_RP_BONUS_TYPE_COUNT;
	res->charId = plr->GetCharID();
	res->dwNExp = 0;
	res->dwRemainSec = 0;
	res->dwSP = pSkillTbldat->wRequireSP;
	res->dwZenny = pSkillTbldat->dwRequire_Zenny;
	res->handle = plr->GetHObject();
	res->skillId = pSkillTbldat->tblidx;
	res->wOpCode = GQ_SKILL_ADD_REQ;
	packet.SetPacketLen(sizeof(sGQ_SKILL_ADD_REQ));
	app->AcquireQuerySession()->PushPacket(&packet);
}
void CGameSession::SendGameCharSkillUpgrade(CNtlPacket* pPacket)
{
	sUG_SKILL_UPGRADE_REQ* req = (sUG_SKILL_UPGRADE_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sGQ_SKILL_UPGRADE_REQ));
	sGQ_SKILL_UPGRADE_REQ* res = (sGQ_SKILL_UPGRADE_REQ*)packet.GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	sSKILL_TBLDAT* pOldSkill = reinterpret_cast<sSKILL_TBLDAT*>(app->GetTableContainer()->GetSkillTable()->FindData(plr->GetSkillContainer()->GetSkillBySlot(req->bySlotIndex)));
	sSKILL_TBLDAT* pNewSkill = reinterpret_cast<sSKILL_TBLDAT*>(app->GetTableContainer()->GetSkillTable()->FindData(pOldSkill->dwNextSkillTblidx));
	res->bIsRpBonusAuto = false;
	res->byRpBonusType = DBO_RP_BONUS_TYPE_COUNT;
	res->bySlot = req->bySlotIndex;
	res->oldSkillId = pOldSkill->tblidx;
	res->newSkillId = pNewSkill->tblidx;	
	res->charId = plr->GetCharID();
	res->handle = plr->GetHObject();
	res->bySlot = req->bySlotIndex;
	res->wOpCode = GQ_SKILL_UPGRADE_REQ;
	packet.SetPacketLen(sizeof(sGQ_SKILL_UPGRADE_REQ));
	app->GetQuerySession()->PushPacket(&packet);
}

void CGameSession::SendGameCharItemMove(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	sUG_ITEM_MOVE_REQ* req = (sUG_ITEM_MOVE_REQ*)pPacket->GetPacketData();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	plr->SendItemMove(req->bySrcPlace, req->byDestPlace, req->bySrcPos, req->byDestPos);
}

void CGameSession::SendGameShopBuy(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	sUG_SHOP_BUY_REQ* req = (sUG_SHOP_BUY_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sGN_NPC_SHOP_BUY));
	sGN_NPC_SHOP_BUY* res = (sGN_NPC_SHOP_BUY*)packet.GetPacketData();
	res->handle = req->handle;	
	res->hPlayer = plr->GetHObject();
	memcpy(res->sBuyData, req->sBuyData, (sizeof(sSHOP_BUY_CART)*NTL_MAX_BUY_SHOPPING_CART));
	res->wOpCode = GN_NPC_SHOP_BUY;
	packet.SetPacketLen(sizeof(sGN_NPC_SHOP_BUY));
	app->GetNpcSession()->PushPacket(&packet);
}

void CGameSession::SendGameShopStart(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	sUG_SHOP_START_REQ* req = (sUG_SHOP_START_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sGU_SHOP_START_RES));
	sGU_SHOP_START_RES* res = (sGU_SHOP_START_RES*)packet.GetPacketData();
	res->byType = 0;
	res->handle = req->handle;
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = GU_SHOP_START_RES;
	packet.SetPacketLen(sizeof(sGU_SHOP_START_RES));
	app->Send(app->GetHSession(plr->GetCharID()), &packet);
}

void CGameSession::SendGameShopEnd(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	sUG_SHOP_END_REQ* req = (sUG_SHOP_END_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sGU_SHOP_END_RES));
	sGU_SHOP_END_RES* res = (sGU_SHOP_END_RES*)packet.GetPacketData();
	res->wResultCode = GAME_SUCCESS;
	res->wOpCode = GU_SHOP_END_RES;
	packet.SetPacketLen(sizeof(sGU_SHOP_END_RES));
	app->Send(app->GetHSession(plr->GetCharID()), &packet);
}

void CGameSession::SendGameCharBankLoad(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	sUG_BANK_LOAD_REQ* req = (sUG_BANK_LOAD_REQ*)pPacket->GetPacketData();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	plr->SendBankItemInfo();
	plr->SendBankZennyInfo();
	plr->SendBankLoad(req->handle);
}

void CGameSession::SendGameCharBankStart(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	sUG_BANK_START_REQ* req = (sUG_BANK_START_REQ*)pPacket->GetPacketData();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	plr->SendBankStart(req->handle);	
}

void CGameSession::SendGameCharBankZennyInfo(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	sUG_BANK_ZENNY_REQ* req = (sUG_BANK_ZENNY_REQ*)pPacket->GetPacketData();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	plr->SendBankZennyRes(req->bIsSave,req->dwZenny,req->handle);
}

void CGameSession::SendGameCharBankEnd(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	sUG_BANK_END_REQ* req = (sUG_BANK_END_REQ*)pPacket->GetPacketData();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	plr->SendBankEnd();
}

void CGameSession::SendGameCharItemDelete(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	sUG_ITEM_DELETE_REQ* req = (sUG_ITEM_DELETE_REQ*)pPacket->GetPacketData();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	plr->SendItemDeleteRes(req->bySrcPlace, req->bySrcPos);
}

void CGameSession::SendGameCharPartyInvite(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	sUG_PARTY_INVITE_REQ* req = (sUG_PARTY_INVITE_REQ*)pPacket->GetPacketData();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	CGameCharacter* plr2 = app->GetPlayerByHandle(req->hTarget);
	CNtlPacket packet(sizeof(sGU_PARTY_INVITE_RES));
	CNtlPacket packet2(sizeof(sGU_PARTY_INVITE_NFY));
	sGU_PARTY_INVITE_RES* res = (sGU_PARTY_INVITE_RES*)packet.GetPacketData();
	sGU_PARTY_INVITE_NFY* notifyClient = (sGU_PARTY_INVITE_NFY*)packet2.GetPacketData();
	plr->GetParty();
	wcscpy_s(res->wszTargetName, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1, plr2->GetCharName());
	res->wResultCode = GAME_PARTY_NOT_AVAILABLE_OPERATION_RIGHT_NOW;
	res->wOpCode = GU_PARTY_INVITE_RES;
	packet.SetPacketLen(sizeof(sGU_PARTY_INVITE_RES));
	app->Send(app->GetHSession(plr->GetCharID()), &packet);
}

void CGameSession::SendGameCharPartyInviteCharId(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	sUG_PARTY_INVITE_CHARID_REQ* req = (sUG_PARTY_INVITE_CHARID_REQ*)pPacket->GetPacketData();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	CGameCharacter* plr2 = app->GetPlayerByCharID(req->targetCharId);
	CNtlPacket packet(sizeof(sGU_PARTY_INVITE_RES));
	CNtlPacket packet2(sizeof(sGU_PARTY_INVITE_NFY));
	sGU_PARTY_INVITE_RES* res = (sGU_PARTY_INVITE_RES*)packet.GetPacketData();
	sGU_PARTY_INVITE_NFY* notifyClient = (sGU_PARTY_INVITE_NFY*)packet2.GetPacketData();
	plr->GetParty();
	wcscpy_s(res->wszTargetName, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1, plr2->GetCharName());
	res->wResultCode = GAME_PARTY_NOT_AVAILABLE_OPERATION_RIGHT_NOW;
	res->wOpCode = GU_PARTY_INVITE_RES;
	packet.SetPacketLen(sizeof(sGU_PARTY_INVITE_RES));
	app->Send(app->GetHSession(plr->GetCharID()), &packet);
}

void CGameSession::SendGameCharPartyInviteCharName(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	sUG_PARTY_INVITE_CHAR_NAME_REQ* req = (sUG_PARTY_INVITE_CHAR_NAME_REQ*)pPacket->GetPacketData();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	CGameCharacter* plr2 = app->GetPlayerByCharName(req->wszTargetName);
	CNtlPacket packet(sizeof(sGU_PARTY_INVITE_RES));
	CNtlPacket packet2(sizeof(sGU_PARTY_INVITE_NFY));
	sGU_PARTY_INVITE_RES* res = (sGU_PARTY_INVITE_RES*)packet.GetPacketData();
	sGU_PARTY_INVITE_NFY* notifyClient = (sGU_PARTY_INVITE_NFY*)packet2.GetPacketData();
	plr->GetParty();
	wcscpy_s(res->wszTargetName, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1, plr2->GetCharName());
	res->wResultCode = GAME_PARTY_NOT_AVAILABLE_OPERATION_RIGHT_NOW;
	res->wOpCode = GU_PARTY_INVITE_RES;
	packet.SetPacketLen(sizeof(sGU_PARTY_INVITE_RES));
	app->Send(app->GetHSession(plr->GetCharID()), &packet);
}

void CGameSession::SendGameCharItemPickUp(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	sUG_ITEM_PICK_REQ* req = (sUG_ITEM_PICK_REQ*)pPacket->GetPacketData();
	CGameCharacter* plr = app->GetPlayerByCharID(this->charId);
	plr->SendItemPickUp(req->handle, req->byAvatarType);
}

///////////////////////QUERY HANDLING
void CGameSession::SendQueryLoadPcProfile(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	sQG_PC_DATA_LOAD_RES* req = (sQG_PC_DATA_LOAD_RES*)pPacket->GetPacketData();
	app->AddPlayer(req->charId, new CGameCharacter(req->accountId, req->charId));

	CGameCharacter* plr = app->GetPlayerByCharID(req->charId);
	plr->CreatePcProfile(req->sPcData);	
	plr->GetOtherParam()->SetBindDir(req->fBindDirX, req->fBindDirY, req->fBindDirZ);
	plr->GetOtherParam()->SetBindPosition(req->fBindLocX, req->fBindLocY, req->fBindLocZ);
	plr->GetOtherParam()->SetBindWorldID(req->sPcData.bindWorldId);
	plr->GetOtherParam()->SetBindObjectTblidx(req->sPcData.bindObjectTblidx);
	plr->GetOtherParam()->SetBindType((eDBO_BIND_TYPE)req->byBindType);
}
void CGameSession::SendQueryPcItemReq(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	sQG_PC_ITEM_LOAD_RES* req = (sQG_PC_ITEM_LOAD_RES*)pPacket->GetPacketData();
	CGameCharacter* plr = app->GetPlayerByCharID(req->charId);
	plr->CreateInventory(req->asItemData,req->byItemCount);
}
void CGameSession::SendQueryPcSkillReq(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	sQG_PC_SKILL_LOAD_RES* req = (sQG_PC_SKILL_LOAD_RES*)pPacket->GetPacketData();
	CGameCharacter* plr = app->GetPlayerByCharID(req->charId);
	plr->CreateSkills(req->asSkill,req->bySkillCount);
}
void CGameSession::SendQueryPcHtbReq(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	sQG_PC_HTB_LOAD_RES* req = (sQG_PC_HTB_LOAD_RES*)pPacket->GetPacketData();
	CGameCharacter* plr = app->GetPlayerByCharID(req->charId);
	plr->CreateHTB(req->asHTBSkill, req->byHTBSkillCount);
}
void CGameSession::SendQueryPcQuickSlot(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	sQG_PC_QUICK_SLOT_LOAD_RES* req = (sQG_PC_QUICK_SLOT_LOAD_RES*)pPacket->GetPacketData();
	CGameCharacter* plr = app->GetPlayerByCharID(req->charId);
	plr->GetSkillContainer()->CreateQuickSlot(req->asQuickSlotData, req->byQuickSlotCount,plr->GetInventory());
}
void CGameSession::SendQueryPcGmtReq(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	sQG_PC_GMT_LOAD_RES* req = (sQG_PC_GMT_LOAD_RES*)pPacket->GetPacketData();
	CGameCharacter* plr = app->GetPlayerByCharID(req->charId);			
}
void CGameSession::SendQueryPcBuffReq(CNtlPacket* pPacket)
{
	sQG_PC_BUFF_LOAD_RES* req = (sQG_PC_BUFF_LOAD_RES*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(req->charId);
	plr->CreateBuff(req->asBuff, req->byBuffCount);
}
void CGameSession::SendQueryPcWarfog(CNtlPacket* pPacket)
{
	sQG_PC_WAR_FOG_RES* req = (sQG_PC_WAR_FOG_RES*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(req->charId);
	memcpy(plr->GetWarFog().achWarFogFlag, req->sWarFogInfo.achWarFogFlag, NTL_MAX_SIZE_WAR_FOG);
}


void CGameSession::SendQueryCharSkillLearn(CNtlPacket* pPacket)
{
	sQG_SKILL_ADD_RES* req = (sQG_SKILL_ADD_RES*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(req->charId);
	CNtlPacket packet(sizeof(sGU_SKILL_LEARN_RES));
	sGU_SKILL_LEARN_RES* res = (sGU_SKILL_LEARN_RES*)packet.GetPacketData();
	res->wOpCode = GU_SKILL_LEARN_RES;
	res->wResultCode = req->wResultCode;
	packet.SetPacketLen(sizeof(sGU_SKILL_LEARN_RES));
	app->Send(app->GetHSession(req->charId), &packet);

	if (req->wResultCode == GAME_SUCCESS)
	{
		CNtlPacket packetNfy(sizeof(sGU_SKILL_LEARNED_NFY));
		sGU_SKILL_LEARNED_NFY* res2 = (sGU_SKILL_LEARNED_NFY*)packetNfy.GetPacketData();
		res2->bySlot = req->bySlot;
		res2->skillId = req->skillId;
		res2->wOpCode = GU_SKILL_LEARNED_NFY;
		packetNfy.SetPacketLen(sizeof(sGU_SKILL_LEARNED_NFY));
		app->Send(app->GetHSession(req->charId), &packet);
		plr->GetSkillContainer()->AddSkill(false, DBO_RP_BONUS_TYPE_COUNT, 0, 0, req->skillId, req->bySlot);
	}
}

void CGameSession::SendQueryCharSkillUpgrade(CNtlPacket* pPacket)
{
	sQG_SKILL_UPGRADE_RES* req = (sQG_SKILL_UPGRADE_RES*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByCharID(req->charId);
	CNtlPacket packet(sizeof(sGU_SKILL_UPGRADE_RES));
	sGU_SKILL_UPGRADE_RES* res = (sGU_SKILL_UPGRADE_RES*)packet.GetPacketData();
	res->bySlot = req->bySlot;
	res->skillId = req->newSkillId;
	res->wResultCode = req->wResultCode;
	res->wOpCode = GU_SKILL_UPGRADE_RES;
	packet.SetPacketLen(sizeof(sGU_SKILL_UPGRADE_RES));
	app->Send(app->GetHSession(req->charId), &packet);
	plr->GetSkillContainer()->UpgradeSkill(req->bySlot, req->newSkillId);
}
///////////////////////Community Handling
void CGameSession::SendCommunityAuthOk(CNtlPacket* pPacket)
{
	sTG_USER_AUTH_KEY_CREATED_ACK* req = (sTG_USER_AUTH_KEY_CREATED_ACK*)pPacket->GetPacketData();
}
///////////////////////NPC Handling
void CGameSession::SendNpcCreate(CNtlPacket* pPacket)
{
	sNG_NPC_SPAWN_RES* req = (sNG_NPC_SPAWN_RES*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByHandle(req->hPlayerHandle);
	CNtlPacket packet(sizeof(sGU_OBJECT_CREATE));
	sGU_OBJECT_CREATE* res = (sGU_OBJECT_CREATE*)packet.GetPacketData();
	CNPCTable* npcA = app->GetTableContainer()->GetNpcTable();
	sNPC_TBLDAT* data = reinterpret_cast<sNPC_TBLDAT*>(npcA->FindData(req->sNpcBrief.tblidx));
	res->sObjectInfo.objType = OBJTYPE_NPC;
	res->handle = req->hNpcHandle;
	res->sObjectInfo.npcBrief.tblidx = req->sNpcBrief.tblidx;
	res->sObjectInfo.npcBrief.wCurEP = req->sNpcBrief.wCurEP;
	res->sObjectInfo.npcBrief.wCurLP = req->sNpcBrief.wCurLP;
	res->sObjectInfo.npcBrief.wCurEP = req->sNpcBrief.wMaxEP;
	res->sObjectInfo.npcBrief.wMaxLP = req->sNpcBrief.wMaxLP;
	res->sObjectInfo.npcBrief.fLastRunningSpeed = req->sNpcBrief.fLastRunningSpeed;
	res->sObjectInfo.npcBrief.fLastWalkingSpeed = req->sNpcBrief.fLastWalkingSpeed;
	res->sObjectInfo.npcBrief.wAttackSpeedRate = req->sNpcBrief.wAttackSpeedRate;
	res->sObjectInfo.npcState.sCharStateBase.vCurLoc.x = req->sNpcState.sCharStateBase.vCurLoc.x;
	res->sObjectInfo.npcState.sCharStateBase.vCurLoc.y = req->sNpcState.sCharStateBase.vCurLoc.y;
	res->sObjectInfo.npcState.sCharStateBase.vCurLoc.z = req->sNpcState.sCharStateBase.vCurLoc.z;
	res->sObjectInfo.npcState.sCharStateBase.vCurDir.z = req->sNpcState.sCharStateBase.vCurDir.z;
	res->sObjectInfo.npcState.sCharStateBase.vCurDir.y = req->sNpcState.sCharStateBase.vCurDir.y;
	res->sObjectInfo.npcState.sCharStateBase.vCurDir.x = req->sNpcState.sCharStateBase.vCurDir.x;
	res->sObjectInfo.npcState.sCharStateBase.byStateID = req->sNpcState.sCharStateBase.byStateID;
	res->wOpCode = GU_OBJECT_CREATE;
	packet.SetPacketLen(sizeof(sGU_OBJECT_CREATE));
	app->Send(app->GetHSession(plr->GetCharID()), &packet);
}

void CGameSession::SendMobCreate(CNtlPacket* pPacket)
{
	sNG_MOB_SPAWN_RES* req = (sNG_MOB_SPAWN_RES*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByHandle(req->hPlayerHandle);
	CNtlPacket packet(sizeof(sGU_OBJECT_CREATE));
	sGU_OBJECT_CREATE* res = (sGU_OBJECT_CREATE*)packet.GetPacketData();
	res->handle = req->hMobHandle;
	res->sObjectInfo.objType = OBJTYPE_MOB;
	res->sObjectInfo.mobBrief.tblidx = req->sMobBrief.tblidx;
	res->sObjectInfo.mobBrief.wCurEP = req->sMobBrief.wCurEP;
	res->sObjectInfo.mobBrief.wCurLP = req->sMobBrief.wCurLP;
	res->sObjectInfo.mobBrief.wCurEP = req->sMobBrief.wMaxEP;
	res->sObjectInfo.mobBrief.wMaxLP = req->sMobBrief.wMaxLP;
	res->sObjectInfo.mobBrief.fLastRunningSpeed = req->sMobBrief.fLastRunningSpeed;
	res->sObjectInfo.mobBrief.fLastWalkingSpeed = req->sMobBrief.fLastWalkingSpeed;
	res->sObjectInfo.mobBrief.wAttackSpeedRate = req->sMobBrief.wAttackSpeedRate;
	res->sObjectInfo.mobBrief.actionpatternTblIdx = req->sMobBrief.actionpatternTblIdx;
	res->sObjectInfo.mobState.sCharStateBase.vCurLoc.x = req->sMobState.sCharStateBase.vCurLoc.x;
	res->sObjectInfo.mobState.sCharStateBase.vCurLoc.y = req->sMobState.sCharStateBase.vCurLoc.y;
	res->sObjectInfo.mobState.sCharStateBase.vCurLoc.z = req->sMobState.sCharStateBase.vCurLoc.z;
	res->sObjectInfo.mobState.sCharStateBase.vCurDir.z = req->sMobState.sCharStateBase.vCurDir.z;
	res->sObjectInfo.mobState.sCharStateBase.vCurDir.y = req->sMobState.sCharStateBase.vCurDir.y;
	res->sObjectInfo.mobState.sCharStateBase.vCurDir.x = req->sMobState.sCharStateBase.vCurDir.x;
	res->sObjectInfo.mobState.sCharStateBase.bFightMode = req->sMobState.sCharStateBase.bFightMode;
	res->sObjectInfo.mobState.sCharStateBase.byStateID = req->sMobState.sCharStateBase.byStateID;
	res->wOpCode = GU_OBJECT_CREATE;
	packet.SetPacketLen(sizeof(sGU_OBJECT_CREATE));
	app->Send(app->GetHSession(plr->GetCharID()), &packet);
}

void CGameSession::SendMobUnspawn(CNtlPacket* pPacket)
{
	sNG_MOB_UNSPAWN_RES* req = (sNG_MOB_UNSPAWN_RES*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByHandle(req->hPlayerHandle);
	CNtlPacket packet(sizeof(sGU_OBJECT_DESTROY));
	sGU_OBJECT_DESTROY* res = (sGU_OBJECT_DESTROY*)packet.GetPacketData();
	res->handle = req->hMobHandle;
	res->wOpCode = GU_OBJECT_DESTROY;
	packet.SetPacketLen(sizeof(sGU_OBJECT_DESTROY));
	app->Send(app->GetHSession(plr->GetCharID()), &packet);	
}

void CGameSession::SendNpcUnspawn(CNtlPacket* pPacket)
{
	sNG_NPC_UNSPAWN_RES* req = (sNG_NPC_UNSPAWN_RES*)pPacket->GetPacketData();
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	CGameCharacter* plr = app->GetPlayerByHandle(req->hPlayerHandle);
	CNtlPacket packet(sizeof(sGU_OBJECT_DESTROY));
	sGU_OBJECT_DESTROY* res = (sGU_OBJECT_DESTROY*)packet.GetPacketData();
	res->handle = req->hNpcHandle;
	res->wOpCode = GU_OBJECT_DESTROY;
	packet.SetPacketLen(sizeof(sGU_OBJECT_DESTROY));
	app->Send(app->GetHSession(plr->GetCharID()), &packet);
}

void CGameSession::SendGameNpcSpawn(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	sNG_ADMIN_NPC_SPAWN* req = (sNG_ADMIN_NPC_SPAWN*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sGU_OBJECT_CREATE));
	sGU_OBJECT_CREATE* res = (sGU_OBJECT_CREATE*)packet.GetPacketData();
	res->handle = req->hNpcHandle;
	res->sObjectInfo.objType = OBJTYPE_NPC;
	res->sObjectInfo.npcBrief.tblidx = req->sNpcBrief.tblidx;
	res->sObjectInfo.npcBrief.wCurEP = req->sNpcBrief.wCurEP;
	res->sObjectInfo.npcBrief.wCurLP = req->sNpcBrief.wCurLP;
	res->sObjectInfo.npcBrief.wCurEP = req->sNpcBrief.wMaxEP;
	res->sObjectInfo.npcBrief.wMaxLP = req->sNpcBrief.wMaxLP;
	res->sObjectInfo.npcBrief.fLastRunningSpeed = req->sNpcBrief.fLastRunningSpeed;
	res->sObjectInfo.npcBrief.fLastWalkingSpeed = req->sNpcBrief.fLastWalkingSpeed;
	res->sObjectInfo.npcBrief.wAttackSpeedRate = req->sNpcBrief.wAttackSpeedRate;
	res->sObjectInfo.npcState.sCharStateBase.vCurLoc.x = req->sNpcState.sCharStateBase.vCurLoc.x;
	res->sObjectInfo.npcState.sCharStateBase.vCurLoc.y = req->sNpcState.sCharStateBase.vCurLoc.y;
	res->sObjectInfo.npcState.sCharStateBase.vCurLoc.z = req->sNpcState.sCharStateBase.vCurLoc.z;
	res->sObjectInfo.npcState.sCharStateBase.vCurDir.z = req->sNpcState.sCharStateBase.vCurDir.z;
	res->sObjectInfo.npcState.sCharStateBase.vCurDir.y = req->sNpcState.sCharStateBase.vCurDir.y;
	res->sObjectInfo.npcState.sCharStateBase.vCurDir.x = req->sNpcState.sCharStateBase.vCurDir.x;
	res->sObjectInfo.npcState.sCharStateBase.byStateID = req->sNpcState.sCharStateBase.byStateID;
	res->wOpCode = GU_OBJECT_CREATE;
	packet.SetPacketLen(sizeof(sGU_OBJECT_CREATE));
	app->SendAll(&packet);
}

void CGameSession::SendGameMobSpawn(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	sNG_ADMIN_MOB_SPAWN* req = (sNG_ADMIN_MOB_SPAWN*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sGU_OBJECT_CREATE));
	sGU_OBJECT_CREATE* res = (sGU_OBJECT_CREATE*)packet.GetPacketData();
	res->handle = req->hMobHandle;
	res->sObjectInfo.objType = OBJTYPE_MOB;
	res->sObjectInfo.mobBrief.tblidx = req->sMobBrief.tblidx;
	res->sObjectInfo.mobBrief.wCurEP = req->sMobBrief.wCurEP;
	res->sObjectInfo.mobBrief.wCurLP = req->sMobBrief.wCurLP;
	res->sObjectInfo.mobBrief.wCurEP = req->sMobBrief.wMaxEP;
	res->sObjectInfo.mobBrief.wMaxLP = req->sMobBrief.wMaxLP;
	res->sObjectInfo.mobBrief.fLastRunningSpeed = req->sMobBrief.fLastRunningSpeed;
	res->sObjectInfo.mobBrief.fLastWalkingSpeed = req->sMobBrief.fLastWalkingSpeed;
	res->sObjectInfo.mobBrief.wAttackSpeedRate = req->sMobBrief.wAttackSpeedRate;
	res->sObjectInfo.mobBrief.actionpatternTblIdx = req->sMobBrief.actionpatternTblIdx;
	res->sObjectInfo.mobState.sCharStateBase.vCurLoc.x = req->sMobState.sCharStateBase.vCurLoc.x;
	res->sObjectInfo.mobState.sCharStateBase.vCurLoc.y = req->sMobState.sCharStateBase.vCurLoc.y;
	res->sObjectInfo.mobState.sCharStateBase.vCurLoc.z = req->sMobState.sCharStateBase.vCurLoc.z;
	res->sObjectInfo.mobState.sCharStateBase.vCurDir.z = req->sMobState.sCharStateBase.vCurDir.z;
	res->sObjectInfo.mobState.sCharStateBase.vCurDir.y = req->sMobState.sCharStateBase.vCurDir.y;
	res->sObjectInfo.mobState.sCharStateBase.vCurDir.x = req->sMobState.sCharStateBase.vCurDir.x;
	res->sObjectInfo.mobState.sCharStateBase.bFightMode = req->sMobState.sCharStateBase.bFightMode;
	res->sObjectInfo.mobState.sCharStateBase.byStateID = req->sMobState.sCharStateBase.byStateID;
	res->wOpCode = GU_OBJECT_CREATE;
	packet.SetPacketLen(sizeof(sGU_OBJECT_CREATE));
	app->SendAll(&packet);
}

void CGameSession::SendGameShopResult(CNtlPacket* pPacket)
{
	CGameServer * app = (CGameServer*)NtlSfxGetApp();
	sNG_SHOP_BUY* req = (sNG_SHOP_BUY*)pPacket->GetPacketData();
	CGameCharacter* plr = app->GetPlayerByHandle(req->hPlayer);
	DWORD dwResult = GAME_SUCCESS;
	if (!plr->GetWarehouse()->IsLessEqualsThan(req->dwTotalZenny))
	{
		if (plr->GetInventory()->IsPlayerHasAnySlotForShop(req->byItemsCount))
		{
			for (int i = 0; i < req->byItemsCount; i++)
			{
				sITEM_PROFILE* sNewItem = new sITEM_PROFILE;
				sITEM_TBLDAT* pItemData = reinterpret_cast<sITEM_TBLDAT*>(app->GetTableContainer()->GetItemTable()->FindData(req->sItemTblidx[i]));
				sNewItem->aOptionTblidx[0] = INVALID_TBLIDX;
				sNewItem->aOptionTblidx[1] = INVALID_TBLIDX;
				sNewItem->byStackcount = req->byOrderedStack[i];
				sNewItem->bNeedToIdentify = false;
				sNewItem->tblidx = pItemData->tblidx;
				sNewItem->byCurDur = pItemData->byDurability;
				sNewItem->byDurationType = pItemData->byDurationType;
				sNewItem->byGrade = 0;
				sNewItem->byBattleAttribute = 0;
				sNewItem->byRestrictType = 0;
				sNewItem->handle = app->AcquireSerialId();
				plr->GetInventory()->AddItem(sNewItem->handle, sNewItem);
				plr->SendItemCreate(sNewItem);
			}
		}
		else
		{
			dwResult = GAME_ITEM_INVEN_FULL;
		}
	}
	else
	{
		dwResult = GAME_ZENNY_NOT_ENOUGH;
	}
	CNtlPacket packet(sizeof(sGU_SHOP_BUY_RES));
	sGU_SHOP_BUY_RES* res = (sGU_SHOP_BUY_RES*)packet.GetPacketData();
	res->handle = req->hNpc;
	res->wResultCode = dwResult;
	res->wOpCode = GU_SHOP_BUY_RES;
	packet.SetPacketLen(sizeof(sGU_SHOP_BUY_RES));
	app->Send(app->GetHSession(plr->GetCharID()), &packet);
}