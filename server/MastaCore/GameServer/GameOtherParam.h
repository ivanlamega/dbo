#pragma once
class CGameOtherParam
{
private:
	sVECTOR3					vBindLoc;
	sVECTOR3					vBindDir;
	TBLIDX						bindObjectTblidx;
	WORLDID						bindWorldId;
	BYTE						byBindType;
public:
	CGameOtherParam();
	~CGameOtherParam();
	void					SetBindPosition(float x, float y, float z);
	sVECTOR3				GetBindPosition();
	void					SetBindDir(float x, float y, float z);
	sVECTOR3				GetBindDir();
	void					SetBindObjectTblidx(TBLIDX wd);
	void					SetBindWorldID(WORLDID wdId);
	TBLIDX					GetBindObjectTblidx();
	WORLDID					GetBindWorldID();
	void					SetBindType(BYTE byType);
	BYTE					GetBindType();
};

