#pragma once
#include "NtlBudokai.h"
#include "NtlMatch.h"
#include "BudokaiTable.h"
class CGameTBudokai
{
private:
	sBUDOKAI_UPDATE_STATE_INFO			m_sStateInfo;								///< MainState
	sBUDOKAI_UPDATE_MATCH_STATE_INFO	m_sMatchStateInfo[MAX_BUDOKAI_MATCH_TYPE];	///< MatchState

	sBUDOKAI_JOIN_INFO					m_sJoinInfo;

	sBUDOKAI_TBLINFO*					m_pBudokaiTblDat;
public:
	CGameTBudokai();
	~CGameTBudokai();	

	sBUDOKAI_UPDATE_STATE_INFO*			GetMainStateInfo()				{ return &m_sStateInfo; }
	sBUDOKAI_UPDATE_MATCH_STATE_INFO*	GetIndividualMatchStateInfo()	{ return &m_sMatchStateInfo[BUDOKAI_MATCH_TYPE_INDIVIDIAUL]; }
	sBUDOKAI_UPDATE_MATCH_STATE_INFO*	GetTeamMatchStateInfo()			{ return &m_sMatchStateInfo[BUDOKAI_MATCH_TYPE_TEAM]; }
	sBUDOKAI_JOIN_INFO*					GetBudokaiJoinInfo()			{ return &m_sJoinInfo; }

	sBUDOKAI_TBLINFO*					GetBudokaiTblData(VOID)			{ return m_pBudokaiTblDat; }

	BYTE								GetBudokaiMainState()			{ return m_sStateInfo.byState; }
	BUDOKAITIME							GetBudokaiMainStateNextTime()	{ return m_sStateInfo.tmNextStepTime; }
	BUDOKAITIME							GetBudokaiMainStateRemainTime()	{ return m_sStateInfo.tmRemainTime; }
};

