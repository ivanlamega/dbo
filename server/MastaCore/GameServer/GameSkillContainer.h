#pragma once
#include "NtlSkill.h"
#include "NtlCharacter.h"

class CGameInventory;

class CGameSkillContainer
{
private:
	BYTE				bySlotCounter;
	BYTE				bySkillCount;
	sSKILL_INFO			sSkillPf[NTL_MAX_PC_HAVE_SKILL];
	BYTE				byHTBCount;
	sHTB_SKILL_INFO		sHTBPf[NTL_HTB_MAX_PC_HAVE_HTB_SKILL];
	BYTE				byBuffCount;
	sBUFF_INFO			sBuffPf[NTL_MAX_BLESS_BUFF_CHARACTER_HAS + NTL_MAX_CURSE_BUFF_CHARACTER_HAS];
	sQUICK_SLOT_DATA	sQuickSlot[NTL_CHAR_QUICK_SLOT_MAX_COUNT];
	BYTE				byQuickSlotCount;
public:
	CGameSkillContainer();
	~CGameSkillContainer();
	void	Create(sSKILL_DATA* pSk, BYTE bySkillCount);
	void	CreateHTB(sHTB_SKILL_DATA* htbDt, BYTE byHtbCount);
	void	CreateBuff(sBUFF_DATA* sBuff, BYTE byBuffCount);
	void	CreateQuickSlot(sQUICK_SLOT_DATA* qcData, BYTE byQuickSlotCount, CGameInventory* inventory);
	sSKILL_INFO*		GetSkillInfo();
	sHTB_SKILL_INFO*	GetHTBSkillInfo();
	sBUFF_INFO*			GetBuffInfo();
	sQUICK_SLOT_DATA*	GetQuickSlotData();
	BYTE				GetSkillCount();
	BYTE				GetHTBSkillCount();
	BYTE				GetBuffCount();
	BYTE				GetQuickSlotCount();
	BYTE				GetTotalSlotCount();
	DWORD				GetSkillBySlot(BYTE bySlot);
	void				UpgradeSkill(BYTE bySlot,DWORD skillTblidx);
	void				AddSkill(bool bIsRpAuto,BYTE byRpBonusType, DWORD dwTimeRemaining, DWORD nExp, TBLIDX tbl, BYTE bySlot);
};

inline sSKILL_INFO*		 CGameSkillContainer::GetSkillInfo()
{
	return sSkillPf;
}
inline sHTB_SKILL_INFO*	 CGameSkillContainer::GetHTBSkillInfo()
{
	return sHTBPf;
}
inline sBUFF_INFO*		 CGameSkillContainer::GetBuffInfo()
{
	return sBuffPf;
}
inline sQUICK_SLOT_DATA* CGameSkillContainer::GetQuickSlotData()
{
	return sQuickSlot;
}
inline BYTE				 CGameSkillContainer::GetSkillCount()
{
	return bySkillCount;
}
inline BYTE				 CGameSkillContainer::GetHTBSkillCount()
{
	return byHTBCount;
}
inline BYTE				 CGameSkillContainer::GetBuffCount()
{
	return byBuffCount;
}
inline BYTE				 CGameSkillContainer::GetQuickSlotCount()
{
	return byQuickSlotCount;
}
inline BYTE				 CGameSkillContainer::GetTotalSlotCount()
{
	return bySlotCounter;
}