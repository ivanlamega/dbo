#include "stdafx.h"
#include "GameWarehouse.h"


CGameWarehouse::CGameWarehouse()
{
	this->dwCurrentMoney = 0;
	this->dwBankMoney = 0;
	this->byItemCount = 0;
}


CGameWarehouse::~CGameWarehouse()
{
}

void CGameWarehouse::Create(DWORD* dwMoney, DWORD dwMoneyBank)
{
	this->dwCurrentMoney = dwMoney;
	this->dwBankMoney = dwMoneyBank;
}