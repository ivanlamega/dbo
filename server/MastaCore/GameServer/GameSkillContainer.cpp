#include "stdafx.h"
#include "GameSkillContainer.h"
#include "GameInventory.h"

CGameSkillContainer::CGameSkillContainer()
{
	bySlotCounter = 0;
}


CGameSkillContainer::~CGameSkillContainer()
{
}

void CGameSkillContainer::CreateHTB(sHTB_SKILL_DATA* htbDt, BYTE byHtbCount)
{
	for (int i = 0; i < byHtbCount; i++)
	{
		if (htbDt[i].skillId != INVALID_TBLIDX)
		{
			this->sHTBPf[i].bySlotId = this->bySlotCounter;
			this->sHTBPf[i].dwTimeRemaining = htbDt[i].nSkillTime;
			this->sHTBPf[i].skillId = htbDt[i].skillId;
			this->bySlotCounter++;
			this->byHTBCount++;
		}
	}
}

void CGameSkillContainer::CreateBuff(sBUFF_DATA* sBuff, BYTE byBuffCount)
{
	for (int z = 0; z < byBuffCount; z++)
	{
		this->sBuffPf[z].afEffectValue[0] = sBuff[z].fEffectValue1;
		this->sBuffPf[z].afEffectValue[1] = sBuff[z].fEffectValue2;
		this->sBuffPf[z].bySourceType = sBuff[z].bySourceType;
		this->sBuffPf[z].dwInitialDuration = sBuff[z].dwInitialDuration;
		this->sBuffPf[z].dwTimeRemaining = sBuff[z].dwTimeRemaining;
		this->sBuffPf[z].sourceTblidx = sBuff[z].sourceTblidx;
	}
}

void CGameSkillContainer::AddSkill(bool bIsRpAuto, BYTE byRpBonusType, DWORD dwTimeRemaining, DWORD nExp, TBLIDX tbl, BYTE bySlot)
{
	this->sSkillPf[this->bySlotCounter].bIsRpBonusAuto = bIsRpAuto;
	this->sSkillPf[this->bySlotCounter].byRpBonusType = byRpBonusType;
	this->sSkillPf[this->bySlotCounter].dwTimeRemaining = dwTimeRemaining;
	this->sSkillPf[this->bySlotCounter].nExp = nExp;
	this->sSkillPf[this->bySlotCounter].tblidx = tbl;
	this->sSkillPf[this->bySlotCounter].bySlotId = bySlot;
	this->bySlotCounter++;	
	this->bySkillCount++;
}

void CGameSkillContainer::Create(sSKILL_DATA* pSk, BYTE bySkillCount)
{
	for (int p = 0; p < bySkillCount; p++)
	{
		this->bySlotCounter++;
		this->sSkillPf[p].bIsRpBonusAuto = pSk[p].bIsRpBonusAuto;
		this->sSkillPf[p].byRpBonusType = pSk[p].byRpBonusType;
		this->sSkillPf[p].dwTimeRemaining = pSk[p].nRemainSec;
		this->sSkillPf[p].nExp = pSk[p].nExp;
		this->sSkillPf[p].tblidx = pSk[p].skillId;
		this->sSkillPf[p].bySlotId = this->bySlotCounter;
		this->bySkillCount++;
	}
	
}

DWORD CGameSkillContainer::GetSkillBySlot(BYTE bySlot)
{
	for (int i = 0; i < this->bySlotCounter; i++)
	{
		if (this->sSkillPf[i].bySlotId == bySlot)
		{
			return this->sSkillPf[i].tblidx;
		}
	}
	return INVALID_TBLIDX;
}

void CGameSkillContainer::CreateQuickSlot(sQUICK_SLOT_DATA* qcData, BYTE byQuickSlotCount, CGameInventory* inventory)
{
	for (int i = 0; i < byQuickSlotCount; i++)
	{
		if (qcData[i].tblidx != INVALID_TBLIDX)
		{
			this->sQuickSlot[i].bySlot = qcData[i].bySlot;
			this->sQuickSlot[i].byType = qcData[i].byType;
			this->sQuickSlot[i].itemID = qcData[i].itemID;
			this->sQuickSlot[i].tblidx = qcData[i].tblidx;
			this->sQuickSlot[i].hItem = inventory->GetHObjectFromTblidx(qcData[i].tblidx);
		}
	}
}
void CGameSkillContainer::UpgradeSkill(BYTE bySlot,DWORD skillTblidx)
{
	for (int i = 0; i < this->bySlotCounter; i++)
	{
		if (this->sSkillPf[i].bySlotId == bySlot)
		{
			this->sSkillPf[i].tblidx = skillTblidx;
		}
	}
}