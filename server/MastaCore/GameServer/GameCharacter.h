#pragma once
#include "NtlSharedType.h"

#include "PCTable.h"
#include "NtlAdmin.h"
#include "NtlCharacter.h"
#include "NtlCharacterState.h"

class CGameBlackList;
class CGameFriendList;
class CGameInventory;
class CGameGuild;
class CGameQuestInventory;
class CGameDojo;
class CGameHoiPoiMix;
class CGamePrivateShop;
class CGameTBudokai;
class CGameOtherParam;
class CGameWarehouse;
class CGameGuildWarehouse;
class CGameParty;
class CGameRankBattle;
class CGameTMQ;
class CGameMailSystem;
class CGameSkillContainer;
class CGameQuestAgency;
class CGameTriggerAgency;
class CGameObjectAgency;

//Friend List
#include "GameFriendList.h"
#include "GameBlackList.h"
//Guild
#include "GameGuild.h"
//Inventory
#include "GameInventory.h"
#include "GameQuestInventory.h"
//Dojo
#include "GameDojo.h"
//HoiPoiMix
#include "GameHoiPoiMix.h"
//PrivateShop
#include "GamePrivateShop.h"
//TBudokai
#include "GameTBudokai.h"
//OtherParam
#include "GameOtherParam.h"
//Warehouse
#include "GameWarehouse.h"
#include "GameGuildWarehouse.h"
//Party
#include "GameParty.h"
//Rankbattle
#include "GameRankBattle.h"
//TMQ
#include "GameTMQ.h"
//MailSystem
#include "GameMailSystem.h"
//SkillContainer
#include "GameSkillContainer.h"
//Quest
#include "GameQuestAgency.h"
#include "GameTriggerAgency.h"
#include "GameObjectAgency.h"

class CGameCharacter
{
	//Members	
private:
	CHARACTERID					m_charID;
	ACCOUNTID					m_accountID;
	HOBJECT						m_SerialHandle;//this will be generated m_charID + last serial object from object manager
	BYTE						abyAuthKey[NTL_MAX_SIZE_AUTH_KEY];
	bool						standup;
	bool						away;
	bool						blockMode;
	bool						charge;
	BYTE						byRpBallCounter;
	BYTE						byRace;
	BYTE						byClass;
	BYTE						byGender;
	bool						bTutorial;
	//Structs in this class
	sPC_PROFILE					m_sPcProfile;
	sAVATAR_ATTRIBUTE_LINK		m_sAttributeLink;
	sCHARSTATE					m_sCharState;
	sCHAR_WAR_FOG_FLAG			m_warFog;
	sVECTOR3					vCurLoc;
	sVECTOR3					vCurDir;
	TBLIDX						currentWorld;
	WORLDID						currentWorldID;
	ZONEID						currentZoneID;
	//Propertys
	CGameFriendList				m_FriendList;
	CGameBlackList				m_BlackList;
	CGameDojo					m_Dojo; //TODO: Implement Class
	CGameGuild					m_Guild;
	CGameGuildWarehouse			m_GuildWarehouse;
	CGameHoiPoiMix				m_HoipoiMix;
	CGameInventory				m_Inventory;
	CGameOtherParam				m_OtherParam;
	CGameParty					m_Party;
	CGamePrivateShop			m_PrivateShop;
	CGameQuestInventory			m_QuestInventory;
	CGameTBudokai				m_TBudokai;
	CGameWarehouse				m_Warehouse;
	CGameSkillContainer			m_SkillContainer;
	CGameTMQ					m_TMQ; //TODO: Implement Class
	CGameMailSystem				m_MailSystem;
	CGameRankBattle				m_RankBattle;//TODO: Implement Class
	CGameQuestAgency*			m_pQAgency;
	CGameTriggerAgency*			m_pPCTAgency;
	CGameObjectAgency*			m_pObjAgency;
	
	void						LoadAttributes(sAVATAR_ATTRIBUTE* pAttributes, sPC_TBLDAT* pTbl);
	//TODO: Add Quest Agencys(Quest,Trigger,Object)

public:
	CGameCharacter(ACCOUNTID accID, CHARACTERID charID);
	~CGameCharacter();
	CHARACTERID	GetCharID();

	CGameFriendList*		GetFriendList(void);
	CGameBlackList*			GetBlackList(void);
	CGameDojo*				GetDojo(void);
	CGameGuild*				GetGuild(void);
	CGameGuildWarehouse*	GetGuildWarehouse(void);
	CGameHoiPoiMix*			GetHoipoiMix(void);
	CGameInventory*			GetInventory(void);
	CGameOtherParam*		GetOtherParam(void);
	CGameParty*				GetParty(void);
	CGamePrivateShop*		GetPrivateShop(void);
	CGameWarehouse*			GetWarehouse(void);
	CGameQuestInventory*	GetQuestInventory(void);
	CGameTBudokai*			GetTenkaichiBudokai(void);
	CGameSkillContainer*	GetSkillContainer(void);
	
	CGameTMQ*				GetTMQ(void);
	CGameMailSystem*		GetMailSystem(void);
	CGameRankBattle*		GetRankBattle(void);
	ACCOUNTID				GetAccountID();
	HOBJECT					GetHObject();
	void					SetWarFog(DWORD index);
	sCHAR_WAR_FOG_FLAG		GetWarFog();
	void					SetPosition(float x, float y, float z);
	sVECTOR3				GetPosition();
	void					SetDir(float x, float y, float z);
	sVECTOR3				GetDir();
	void					SetPlayerTutorialFlag(bool b);
	
	//Load this things when char enter in game world	
	void					SendCharStateBase(bool bFightMode = false, eCHARSTATE state = CHARSTATE_SPAWNING, eCHARCONDITION c = INVALID_CHARCONDITION, DWORD stateTime = 0);
	void					SendCharStateDetailed(sCHARSTATE_BASE sBase, sCHARSTATE_DETAIL sDetail);
	void					SendCharInfo();
	void					SendItemInfo();
	void					SendSkillInfo();
	void					SendHTBSkillInfo();
	void					SendBuffInfo();
	void					SendQuickSlotInfo();
	void					SendWarFogInfo();
	void					SendRecipeInfo();
	void					SendWorldZoneInfo(bool IsDark,ZONEID zd,bool sendAll = false);
	void					SendWorldInfo(TBLIDX world=0,WORLDID id=0,eGAMERULE_TYPE ruleType = GAMERULE_NORMAL);
	void					SendCharInfoEnd();
	void					SendCreatePlayer();
	void					SendCommunityKeyReq(BYTE* abyKey);
	void					SetStandUp(bool b){ this->standup = b; }
	void					SetAway(bool b){ this->away = b; }
	void					SetBlockMode(bool b){ this->blockMode = b; }
	void					SetCharging(bool b){ this->charge = b; }
	void					SetPlayerInTutorial(bool b){ this->bTutorial = b; }
	void					IncreaseRP();
	bool					IsCharCanCharge();
	bool					IsCharging(){ return this->charge; }
	bool					IsStandup(){ return this->standup; }
	bool					IsAway(){ return this->away; }
	bool					IsBlocking(){ return this->blockMode; }
	bool					IsPlayerInTutorial(){ return this->bTutorial; };
	TBLIDX					GetCurrentWorld();
	//Character Behavior - Movement
	void					SendCharMove(sVECTOR3* Dir, sVECTOR3* Pos, BYTE eMove);
	void					SendCharMoveSync(sVECTOR3* Dir, sVECTOR3* Pos);
	void					SendCharChangeDirFloating(sVECTOR3* vect3, BYTE byMoveDir);
	void					SendCharChangeHeading(sVECTOR3* vect3, sVECTOR3* vect);
	void					SendCharJump(sVECTOR3* Dir, BYTE byMoveDir);
	void					SendCharJumpEnd();
	void					SendCharDestMove(sVECTOR3 vDestLoc, DWORD dwTimeStamp);
	void					SendCharFollowMove(float fDistance,BYTE byMovementDir, BYTE byMovementReason,DWORD dwTimeStamp, HOBJECT hTarget);
	void					SendCharToggSitDown();
	void					SendCharDestroyObj();
	void					SendCharToggStandUp();
	void					SendCharCharging(bool bHitDelay = false);
	void					SendCharChargingCancel();
	void					SendCharUpdateRp();
	void					SendCharUpdateRpBallMax();
	void					SwitchBindToLocal();
	//NetPy
	void					SendNetPyNotification();
	//Character Bank/Warehouse
	void					SendBankLoad(HOBJECT hWarehouse);
	void					SendBankStart(HOBJECT hWarehouse);
	void					SendBankZennyInfo();
	void					SendBankZennyRes(bool IsSave, DWORD amount, HOBJECT hWarehouse);
	void					SendBankItemInfo();
	void					SendBankEnd();
	//Character Inventory
	void					SendItemMove(BYTE byPlaceSource, BYTE byPlaceDestination, BYTE byPositionSource, BYTE byPositionDestination);
	void					SendItemStack(BYTE byStack, HOBJECT hItem, bool IsNew = false);
	void					SendItemDeleteRes(BYTE byPlace, BYTE byPos);
	void					SendItemDelete(BYTE byPlaceSource, BYTE byPositionSource, HOBJECT hItemObj);
	void					SendItemPickUp(HOBJECT hItemHandle, BYTE byAvatarType);
	void					SendItemCreate(sITEM_PROFILE* pItem);
	//Send Npc & Mob Spawn/Unspawn
	void					SendNpcSpawn();
	void					SendMobSpawn();
	void					SendNpcCheck();
	void					SendMobCheck();
	//Character Revival
	void					SendCharRevivalPopoStone();
	void					SendCharRevivalCurrentLoc();
	//Character Traits
	void						CreatePcProfile(sPC_DATA sum);
	void						CreateInventory(sITEM_DATA* itDt, BYTE byCount);
	void						CreateSkills(sSKILL_DATA* sk, BYTE byCount);
	void						CreateHTB(sHTB_SKILL_DATA* pHtbData, BYTE byHtbCount);
	void						CreateBuff(sBUFF_DATA* buffDt, BYTE byBuffCount);
	void						CreateHoiPoiMix(sRECIPE_DATA* sRecipe, BYTE byCount);
	//////////////////////////////////////////////////////
	void					CreateAvatarInfo(sPC_DATA sPCData, 
											 sITEM_DATA* sItem, 
											 sSKILL_DATA* sSkill, 
											 sHTB_SKILL_DATA* sHtbSkill, 
											 sBUFF_DATA* sBuff,
											 sQUEST_PROGRESS_DATA* sProgress,
											 sQUEST_INVENTORY_DATA* sInventoryQuest,
											 sQUICK_SLOT_DATA* sQuickSlot,
											 sPUNISH_DATA* sPunish,
											 sSHORTCUT_DATA* sShortcut,
											 sRECIPE_DATA* sRecipe,
											 BYTE byItemCount,
											 BYTE bySkillCount,
											 BYTE byBuffCount,
											 BYTE byHTBSkillCount,
											 BYTE byQuestProgressCount,
											 BYTE byQuestInventoryCount,
											 BYTE byQuickSlotCount,
											 BYTE byPunishCount,
											 BYTE byShortcutCount,
											 BYTE byRecipeCount);
	

	CGameQuestAgency*		GetQuestAgency(void);
	CGameTriggerAgency*		GetPCTriggerAgency(void);
	int						GetLevel();
	int						GetRace();
	int						GetClass();
	int						GetGender();
	TBLIDX					GetMapNameTblidx();
	DWORD					GetReputation();
	WCHAR*					GetCharName();
};

inline CHARACTERID CGameCharacter::GetCharID()
{
	return m_charID;
}

inline CGameInventory* CGameCharacter::GetInventory(void)
{
	return &m_Inventory;
}

inline CGameWarehouse* CGameCharacter::GetWarehouse(void)
{
	return &m_Warehouse;
}

inline CGameQuestInventory* CGameCharacter::GetQuestInventory(void)
{
	return &m_QuestInventory;
}

inline CGameSkillContainer* CGameCharacter::GetSkillContainer(void)
{
	return &m_SkillContainer;
}

inline CGameParty* CGameCharacter::GetParty(void)
{
	return &m_Party;
}

inline CGamePrivateShop* CGameCharacter::GetPrivateShop(void)
{
	return &m_PrivateShop;
}

inline CGameGuild* CGameCharacter::GetGuild(void)
{
	return &m_Guild;
}

inline CGameOtherParam* CGameCharacter::GetOtherParam(void)
{
	return &m_OtherParam;
}

inline CGameTMQ* CGameCharacter::GetTMQ()
{
	return &m_TMQ;
}

inline CGameMailSystem* CGameCharacter::GetMailSystem(void)
{
	return &m_MailSystem;
}

inline CGameRankBattle* CGameCharacter::GetRankBattle(void)
{
	return &m_RankBattle;
}

inline CGameFriendList* CGameCharacter::GetFriendList()
{
	return &m_FriendList;
}

inline CGameBlackList* CGameCharacter::GetBlackList()
{
	return &m_BlackList;
}

inline CGameGuildWarehouse* CGameCharacter::GetGuildWarehouse()
{
	return &m_GuildWarehouse;
}

inline CGameTBudokai* CGameCharacter::GetTenkaichiBudokai()
{
	return &m_TBudokai;
}

inline CGameHoiPoiMix* CGameCharacter::GetHoipoiMix()
{
	return &m_HoipoiMix;
}

inline CGameDojo* CGameCharacter::GetDojo()
{
	return &m_Dojo;
}

inline HOBJECT CGameCharacter::GetHObject()
{
	return m_SerialHandle;
}
inline ACCOUNTID CGameCharacter::GetAccountID()
{
	return m_accountID;
}
inline sCHAR_WAR_FOG_FLAG CGameCharacter::GetWarFog()
{
	return m_warFog;
}
inline sVECTOR3 CGameCharacter::GetPosition()
{
	return vCurLoc;
}
inline sVECTOR3 CGameCharacter::GetDir()
{
	return vCurDir;
}
inline TBLIDX CGameCharacter::GetCurrentWorld()
{
	return currentWorld;
}
inline int CGameCharacter::GetRace()
{
	return byRace;
}
inline int CGameCharacter::GetClass()
{
	return byClass;
}
inline int CGameCharacter::GetGender()
{
	return byGender;
}
inline int CGameCharacter::GetLevel()
{
	return m_sPcProfile.byLevel;
}
inline TBLIDX CGameCharacter::GetMapNameTblidx()
{
	return m_sPcProfile.bindObjectTblidx;
}
inline DWORD CGameCharacter::GetReputation()
{
	return m_sPcProfile.dwReputation;
}
inline WCHAR* CGameCharacter::GetCharName()
{
	return m_sPcProfile.awchName;
}
inline CGameQuestAgency* CGameCharacter::GetQuestAgency(void)
{
	CQuestWrapper* pTSMain = GetTSCMain();
	if (NULL == pTSMain || !pTSMain->IsCreated()) return NULL;

	return m_pQAgency;
}

inline CGameTriggerAgency* CGameCharacter::GetPCTriggerAgency(void)
{
	CQuestWrapper* pTSMain = GetTSCMain();
	if (NULL == pTSMain || !pTSMain->IsCreated()) return NULL;

	return m_pPCTAgency;
}