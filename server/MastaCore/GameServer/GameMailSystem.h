#pragma once
#include "NtlMail.h"
#include <vector>
#include <map>
class CGameMailSystem
{
public:
	typedef std::vector<MAILID>					MAIL_ID_VEC;
	typedef std::map<MAILID, sMAIL_PROFILE*>	MAIL_PROFILE_MAP;
	CGameMailSystem();
	~CGameMailSystem();
private:
	MAIL_ID_VEC			m_vecMailID;
	MAIL_PROFILE_MAP	m_mapMailProfile;

	UINT32				m_uiMailNum;

	bool				m_bAway;
	UINT32				m_uiNotify;
	bool				m_bMailStart;

	UINT32				m_uiMailCount;
	UINT32				m_uiUnreadMailCountNormal;
	UINT32				m_uiUnreadMailCountManager;
	UINT32				m_uiOldUnreadMailCountNormal;
};

