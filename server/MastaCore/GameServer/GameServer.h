#pragma once
#include "NtlSfx.h"
#include "GameSession.h"
#include "GameSessionFactory.h"

#ifndef CGAME_SERVER_H
#define CGAME_SERVER_H

class CMasterSession;
class CQuerySession;
class CCommunitySession;
class CNpcSession;
class CTableContainer;
class CNtlBitFlagManager;
class CTableFileNameList;

#include "MasterSession.h"
#include "QuerySession.h"
#include "CommunitySession.h"
#include "NpcSession.h"


#include "TableContainer.h"
#include "NtlBitFlagManager.h"
#include "TableFileNameList.h"
//List Tables
#include "ExpTable.h"
#include "HelpTable.h"
#include "MerchantTable.h"
#include "MobTable.h"
#include "NewbieTable.h"
#include "NPCTable.h"
#include "PCTable.h"
#include "StatusTransformTable.h"
#include "GameManiaTimeTable.h"
#include "GuideHintTable.h"
#include "BasicDropTable.h"
#include "ItemOptionTable.h"
#include "ItemTable.h"
#include "NormalDropTable.h"
#include "LegendaryDropTable.h"
#include "SuperiorDropTable.h"
#include "UseItemTable.h"
#include "SetItemTable.h"
#include "EachDropTable.h"
#include "TypeDropTable.h"
#include "ExcellentDropTable.h"
#include "DragonBallTable.h"
#include "DragonBallRewardTable.h"
#include "ActionTable.h"
#include "ChatCommandTable.h"
#include "DirectionLinkTable.h"
#include "FormulaTable.h"
#include "CharmTable.h"
#include "QuestDropTable.h"
#include "QuestItemTable.h"
#include "QuestProbabilityTable.h"
#include "QuestTextDataTable.h"
#include "QuestRewardTable.h"
#include "HTBSetTable.h"
#include "SkillTable.h"
#include "SystemEffectTable.h"
#include "TextAllTable.h"
#include "ChattingFilterTable.h"
#include "TextServerTable.h"
#include "ObjectTable.h"
#include "SpawnTable.h"
#include "WorldTable.h"
#include "WorldZoneTable.h"
#include "WorldMapTable.h"
#include "LandMarkTable.h"
#include "WorldPathTable.h"
#include "WorldPlayTable.h"
#include "PortalTable.h"
#include "NpcSpeechTable.h"
#include "SetItemTable.h"
#include "TimeQuestTable.h"
#include "RankBattleTable.h"
#include "BudokaiTable.h"
#include "ScriptLinkTable.h"
#include "DungeonTable.h"
#include "ModelToolCharDataTable.h"
#include "ModelToolObjDataTable.h"
#include "QuestNarrationTable.h"
#include "VehicleTable.h"
#include "ItemRecipeTable.h"
#include "DynamicObjectTable.h"
#include "MobMovePatternTable.h"
#include "DojoTable.h"
#include "ItemUpgradeTable.h"
#include "ItemMixMachineTable.h"
#include "HLSItemTable.h"
#include "HLSMerchantTable.h"
//END TABLES
//Quest Wrapper
#include "QuestWrapper.h"
#include "GameCharacter.h"
#include "NtlTokenizer.h"

#define HTRIGGER_OBJECT_WORLD 100000

class CGameServer :	public CNtlServerApp
{
	//Members
public:
	CNtlString				ServersConfig[99][2];//For Config of multiple server 0->{Ip,Port} - Luiz45
	sSERVERCONFIG			m_config;
private:
	CNtlAcceptor			m_clientAcceptor;
	CNtlLog  				m_log;	
	CNtlString				EnableMultipleServers;//Added for enabling multiple server - Luiz45
	DWORD					m_uiSerialId;
	CQuestWrapper*			g_pQuestWrapper;

	typedef std::map<CHARACTERID, CGameCharacter*> PLAYER;
	typedef std::map<CHARACTERID, CGameCharacter*>::iterator PLAYER_IT;

	//typedef std::map<WORLDID, sDBO_DOJO_BRIEF*> DOJO_LIST;
	//typedef std::map<WORLDID, sDBO_DOJO_BRIEF*>::iterator DOJO_IT;

	typedef std::map<HOBJECT, sOBJECT_TBLDAT*> OBJECT_LIST;
	typedef std::map<HOBJECT, sOBJECT_TBLDAT*>::iterator OBJECT_IT;

	typedef std::map<WORLDID, sWORLD_TBLDAT*> WORLD_LIST;
	typedef std::map<WORLDID, sWORLD_TBLDAT*>::iterator WORLD_IT;

	typedef std::map<HOBJECT, sITEM_PROFILE*> DROP_LIST;
	typedef std::map<HOBJECT, sITEM_PROFILE*>::iterator DROP_LIST_IT;

	GAME_CLIENT_SESSION		m_sessionList;
	PLAYER					m_playerList;
	//DOJO_LIST				m_dojoList;
	OBJECT_LIST				m_gameObject;
	WORLD_LIST				m_worldList;
	DROP_LIST				m_dropList;

	//Game Server Ptr Refresh
	CGameSession*				m_pServerSession;
	//Client Session
	CGameSession*				m_pClientSession;
	//Community Server Connection
	CNtlConnector				m_communityServerConnector;
	CNtlMutex					m_communityServerMutex;
	CCommunitySession*			m_pCommunityServerSession;
	//NPC Server Connection
	CNtlConnector				m_npcServerConnector;
	CNtlMutex					m_npcServerMutex;
	CNpcSession*			    m_pNpcServerSession;
	//Master Server Connection
	CNtlConnector				m_masterServerConnector;
	CNtlMutex					m_masterServerMutex;
	CMasterSession*				m_pMasterServerSession;
	//Query Server Connection
	CNtlConnector				m_queryServerConnector;
	CNtlMutex					m_queryServerMutex;
	CQuerySession*			    m_pQueryServerSession;
	//Connected
	bool					m_bMasterServerConnected;
	bool					m_bQueryServerConnected;
	bool					m_bCommunityServerConnected;
	bool					m_bNpcServerConnected;
	void					DoAliveTime();
	CTableContainer*		g_pTableContainer;
public:		
	//For Multiple Server - Luiz45
	const std::string		GetConfigFileEnabledMultipleServers();
	const DWORD				GetConfigFileMaxServers();
	int						CreateTableContainer();
	int						OnInitApp();
	int						OnCreate();
	void					OnDestroy();
	int						OnCommandArgument(int argc, _TCHAR* argv[]);
	int						OnConfiguration(const char * lpszConfigFile);
	int						OnAppStart();
	int						GetSessionCount();
	void					Run();
	void					LoadQuests();
	///Players
	bool					AddPlayer(CHARACTERID charID, CGameCharacter* plr);
	void					RemovePlayer(CHARACTERID charID, CGameCharacter* plr);
	bool					FindPlayer(CHARACTERID charID);
	CGameCharacter*			GetPlayerByCharID(CHARACTERID charID);
	CGameCharacter*			GetPlayerByHandle(HOBJECT hHandle);
	CGameCharacter*			GetPlayerByCharName(WCHAR* charName);
	///
	bool					Add(CHARACTERID charID, HSESSION hSession);
	void					Remove(HSESSION hSession);
	bool					Find(CHARACTERID charID, HSESSION hSession);
	HSESSION				GetHSession(CHARACTERID charID);
	///////////////////////
	void					SetMasterSession(CMasterSession* pServerSession);
	CMasterSession*			AcquireMasterSession();
	CMasterSession*			GetMasterSession();
	void					SetCommunitySession(CCommunitySession* pServerSession);
	CCommunitySession*		AcquireCommunitySession();
	CCommunitySession*		GetCommunitySession();
	void					SetNpcSession(CNpcSession* pServerSession);
	CNpcSession*			AcquireNpcSession();
	CNpcSession*			GetNpcSession();
	void					SetQuerySession(CQuerySession* pServerSession);
	CQuerySession*			AcquireQuerySession();
	CQuerySession*			GetQuerySession();
	void					SetClientSession(CGameSession* pClientSession);
	void					SetQueryServerConnected(bool c){ m_bQueryServerConnected = c; }
	void					SetMasterServerConnected(bool c){ m_bMasterServerConnected = c; }
	void					SetCommunityServerConnected(bool c){ m_bCommunityServerConnected = c; }
	void					SetNpcServerConnected(bool c){ m_bNpcServerConnected = c; }
	bool					IsQueryServerConnected() { return m_bQueryServerConnected; }
	bool					IsMasterServerConnected(){ return m_bMasterServerConnected; }
	bool					IsCommunityServerConnected(){ return m_bCommunityServerConnected; }
	bool					IsNpcServerConnected(){ return m_bNpcServerConnected; }
	CGameSession*			GetClientSession();
	HOBJECT					AcquireSerialId(void);
	void					SendAll(CNtlPacket* pPacket,HSESSION except = INVALID_HSESSION);
	sOBJECT_TBLDAT*			GetObjectTbl(HOBJECT hObject);
	sWORLD_TBLDAT*			GetWorld(WORLDID WorldId);
	CTableContainer*		GetTableContainer();
	CQuestWrapper*			GetQuestWrapper();
	void					AddItemDrop(HOBJECT hItem, sITEM_PROFILE* itemProf);
	void					RemoveItemDrop(HOBJECT hItem);
	TBLIDX					FindTblidxOnDropList(HOBJECT hObj);
	sITEM_PROFILE*			FindItemOnDropList(HOBJECT hItemProfile);
	void					RetrievePlayers();
	void					RefreshSerial(HOBJECT serial);
	void					ReconnectNpcServer();
};

#endif

inline CTableContainer* CGameServer::GetTableContainer()
{
	return g_pTableContainer;
}
inline CQuestWrapper* CGameServer::GetQuestWrapper()
{
	return g_pQuestWrapper;
}