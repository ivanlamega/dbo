#pragma once
#include "CharServer.h"
#include "NtlSession.h"
#include "NtlPacketEncoder_RandKey.h"
///////////////////////////////////////RESPONSE PACKETS///////////////////////////
//Char Server -> Query Server
#include "NtlPacketCQ.h"
//Char Server -> Master Server
#include "NtlPacketCM.h"
//Char Server -> Client(EXE)
#include "NtlPacketCU.h"
/////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////REQUEST PACKETS///////////////////////////
//Query Server -> Char Server
#include "NtlPacketQC.h"
//Master Server -> Char Server
#include "NtlPacketMC.h"
//Client(EXE) -> Char Server
#include "NtlPacketUC.h"
/////////////////////////////////////////////////////////////////////////////////
#include "NtlPacketUtil.h"

#ifndef CCHARSESSION_H
#define CCHARSESSION_H

class CCharSession :	public CNtlSession
{
private:
	ACCOUNTID		m_dwAccountID;
	CHARACTERID		m_dwCharID;
public:
	CCharSession(bool bAliveCheck = true, bool bOpcodeCheck = true);
	~CCharSession();
	int							OnAccept();
	void						OnClose();
	int							OnConnect();
	int							OnDispatch(CNtlPacket * pPacket);
	// Packet functions
	void						SendCharLoginReq(CNtlPacket* pPacket);
	void						SendCharServerListOne(CNtlPacket* pPacket);
	void						SendCharLoadReq(CNtlPacket* pPacket);
	void						SendCharAddReq(CNtlPacket* pPacket);
	void						SendCharSelectReq(CNtlPacket* pPacket);
	void						SendCharConnectCheck(CNtlPacket* pPacket);
	void						SendCharConnectCheckCancel(CNtlPacket* pPacket);
	void						SendCharDisconnectReq(CNtlPacket* pPacket);
	void						SendQueryLoadInfo(CNtlPacket* pPacket);
	void						SendQueryAddRes(CNtlPacket* pPacket);
	void						SendQueryConnectCheck(CNtlPacket* pPacket);
	void						SendQueryConnectCheckCancel(CNtlPacket* pPacket);
	// End Packet functions
	void						SendQueryLoadChar(ACCOUNTID id, CHARACTERID idChar);
private:
	CNtlPacketEncoder_RandKey	m_packetEncoder;
};

#endif