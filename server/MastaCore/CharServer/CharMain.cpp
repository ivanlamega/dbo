// CharMain.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "NtlSfx.h"
#include "CharServer.h"
#include "NtlFile.h"


int _tmain(int argc, _TCHAR* argv[])
{
	NtlSetPrintFlag(PRINT_APP);

	CCharServer app;
	CNtlFileStream traceFileStream;

	// LOG FILE
	std::cout << "Loading Server.ini... \n";
	int rc = traceFileStream.Create("CharServerLog");
	rc = app.Create(argc, argv, ".\\Server.ini");
	NtlSetPrintStream(traceFileStream.GetFilePtr());
	NtlSetPrintFlag(PRINT_APP | PRINT_SYSTEM);

	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "log file CreateFile error %d(%s)", rc, NtlGetErrorMessage(rc));
		return rc;
	}
	std::cout << "Loaded!\n";
	app.Start();
	std::cout << "Loading Tables... \n";
	if (!app.CreateTableContainer())
	{
		NTL_PRINT(PRINT_APP, "Cannot Load the Tables see more: %d(%s)", rc, NtlGetErrorMessage(rc));
		return rc;
	}
	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "Server Application Create Fail %d(%s)", rc, NtlGetErrorMessage(rc));
		return rc;
	}	
	std::cout << "Loading Tables - DONE \n";
	std::cout << "Running... \n";
	app.WaitForTerminate();


	return 0;
}

