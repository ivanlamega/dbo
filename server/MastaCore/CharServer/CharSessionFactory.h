#pragma once
#include "CharServer.h"
#include "NtlSessionFactory.h"

#ifndef CCHARSESSION_FACTORY_H
#define CCHARSESSION_FACTORY_H

class CCharSessionFactory : public CNtlSessionFactory
{
public:
	CCharSessionFactory();
	~CCharSessionFactory();
	CNtlSession * CreateSession(SESSIONTYPE sessionType);
	void DestroySession(CNtlSession * pSession);
};
#endif

