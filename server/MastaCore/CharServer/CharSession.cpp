#include "stdafx.h"
#include "CharServer.h"
#include "CharSession.h"


CCharSession::CCharSession(bool bAliveCheck, bool bOpcodeCheck) :CNtlSession(SESSION_CHAR_SERVER)
{
	SetControlFlag(CONTROL_FLAG_USE_SEND_QUEUE);

	if (bAliveCheck)
	{
		SetControlFlag(CONTROL_FLAG_CHECK_ALIVE);
	}
	if (bOpcodeCheck)
	{
		SetControlFlag(CONTROL_FLAG_CHECK_OPCODE);
	}

	SetPacketEncoder(&m_packetEncoder);
}

//-----------------------------------------------------------------------------------
//		클라이언트 소멸
//-----------------------------------------------------------------------------------
CCharSession::~CCharSession()
{
	NTL_PRINT(PRINT_APP, "CCharSession Destructor Called");

	CCharServer * app = (CCharServer*)NtlSfxGetApp();
	app->Remove(this->GetHandle());
}

int CCharSession::OnAccept()
{
	NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );		
	CCharServer* app = (CCharServer*)NtlSfxGetApp();
	for (int i = 0; i < app->GetNetwork()->GetSessionList()->GetMaxCount(); i++)
	{
		CNtlSession* first = app->GetNetwork()->GetSessionList()->Find(i);
		if (first)
		{
			if (first->GetSessionType() == SESSION_QUERY_SERVER)
			{
				app->SetQuerySession((CQuerySession*)first);
			}
			if (first->GetSessionType() == SESSION_MASTER_SERVER)
			{
				app->SetMasterSession((CMasterSession*)first);
			}
		}
	}
	if ((app->GetQuerySession() != NULL) && (!app->IsQueryServerConnected()))
	{
		CNtlPacket packetC(sizeof(sCQ_NOTIFY_SERVER_BEGIN));
		sCQ_NOTIFY_SERVER_BEGIN* msgC = (sCQ_NOTIFY_SERVER_BEGIN *)packetC.GetPacketData();
		msgC->byServerIndex = app->GetServerIndex();
		msgC->wOpCode = CQ_NOTIFY_SERVER_BEGIN;
		packetC.SetPacketLen(sizeof(sCQ_NOTIFY_SERVER_BEGIN));
		app->GetQuerySession()->PushPacket(&packetC);
	}
	if ((app->GetMasterSession() != NULL) && (!app->IsMasterServerConnected()))
	{
		CNtlPacket packetM(sizeof(sCM_HEARTBEAT));
		sCM_HEARTBEAT* msgM = (sCM_HEARTBEAT*)packetM.GetPacketData();
		msgM->wOpCode = CM_HEARTBEAT;
		packetM.SetPacketLen(sizeof(sCM_HEARTBEAT));
		app->GetMasterSession()->PushPacket(&packetM);
	}
	return NTL_SUCCESS;
}


void CCharSession::OnClose()
{
	NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
}

int CCharSession::OnConnect()
{
	NTL_PRINT(PRINT_APP, "%s", __FUNCTION__);
	CCharServer* app = (CCharServer*)NtlSfxGetApp();
	do{
		this->OnAccept();
		Sleep(5000);
	} while ((!app->GetQuerySession()) ||
		(!app->GetMasterSession()));
	return 0;
}
int CCharSession::OnDispatch(CNtlPacket * pPacket)
{
	CCharServer * app = (CCharServer*)NtlSfxGetApp();
	if ((!app->GetQuerySession()) ||
		(!app->GetMasterSession()))
		this->OnConnect();

	sNTLPACKETHEADER * pHeader = (sNTLPACKETHEADER *)pPacket->GetPacketData();
	switch (pHeader->wOpCode)
	{
		case UC_LOGIN_REQ:
		{
			CCharSession::SendCharLoginReq(pPacket);
		}
		break;
		case UC_CHARACTER_SERVERLIST_ONE_REQ:
		{
			CCharSession::SendCharServerListOne(pPacket);
		}
		break;
		case UC_CHARACTER_LOAD_REQ:
		{
			CCharSession::SendCharLoadReq(pPacket);
		}
		break;
		case UC_CHARACTER_ADD_REQ:
		{
			CCharSession::SendCharAddReq(pPacket);
		}
		break;
		case UC_CHARACTER_SELECT_REQ:
		{
			CCharSession::SendCharSelectReq(pPacket);
		}
		break;
		case UC_CONNECT_WAIT_CHECK_REQ:
		{
			CCharSession::SendCharConnectCheck(pPacket);
		}
		break;
		case UC_CONNECT_WAIT_CANCEL_REQ:
		{
			CCharSession::SendCharConnectCheckCancel(pPacket);
		}
		break;
		case UC_CHARACTER_EXIT_REQ:
		{
			CCharSession::SendCharDisconnectReq(pPacket);
		}
		break;
		case QC_HEARTBEAT:
		{
			if (app->GetQuerySession() != NULL)
				app->GetQuerySession()->ResetAliveTime();
		}
		break;
		case QC_NOTIFY_SERVER_BEGIN_ACK:
		{
			sQC_NOTIFY_SERVER_BEGIN_ACK* req = (sQC_NOTIFY_SERVER_BEGIN_ACK*)pPacket->GetPacketData();
			app->byServerIndex = req->serverFarmId;
		}
		break;
		case QC_CHARACTER_LOAD_RES:
		{
			CCharSession::SendQueryLoadInfo(pPacket);
		}
		break;
		case QC_CHARACTER_ADD_RES:
		{
			CCharSession::SendQueryAddRes(pPacket);
		}
		break;
		case QC_CONNECT_WAIT_CHECK_RES:
		{
			CCharSession::SendQueryConnectCheck(pPacket);
		}
		break;
		case QC_CONNECT_WAIT_CANCEL_RES:
		{
			CCharSession::SendQueryConnectCheckCancel(pPacket);
		}
		break;
		case MC_HEARTBEAT:
		{
			if (app->GetMasterSession() != NULL)
				app->GetMasterSession()->ResetAliveTime();
		}
		break;
		default:
		{
			if (pHeader->wOpCode != SYS_ALIVE)
				NTL_PRINT(PRINT_APP, "The following packet was not handled: %s", NtlGetPacketName(pHeader->wOpCode));
			return CNtlSession::OnDispatch(pPacket);
		}
	}

	return NTL_SUCCESS;
}

void CCharSession::SendCharLoginReq(CNtlPacket* pPacket)
{
	CCharServer * app = (CCharServer*)NtlSfxGetApp();
	sUC_LOGIN_REQ* req = (sUC_LOGIN_REQ*)pPacket->GetPacketData();
	if (app->byServerIndex == INVALID_BYTE)
		app->byServerIndex = req->serverID;
	CNtlPacket packet(sizeof(sCU_LOGIN_RES));
	sCU_LOGIN_RES* res = (sCU_LOGIN_RES*)packet.GetPacketData();
	res->lastServerFarmId = app->byServerIndex;
	res->wResultCode = CHARACTER_SUCCESS;
	res->wOpCode = CU_LOGIN_RES;
	packet.SetPacketLen(sizeof(sCU_LOGIN_RES));
	app->Send(this->GetHandle(), &packet);
	app->Add(req->accountId, this->GetHandle());
	this->m_dwAccountID = req->accountId;
}

void CCharSession::SendCharServerListOne(CNtlPacket* pPacket)
{
	CCharServer * app = (CCharServer*)NtlSfxGetApp();
	sUC_CHARACTER_SERVERLIST_ONE_REQ* req = (sUC_CHARACTER_SERVERLIST_ONE_REQ*)pPacket->GetPacketData();

	//Send Game Server information
	CNtlPacket packet(sizeof(sCU_SERVER_FARM_INFO));
	sCU_SERVER_FARM_INFO * res = (sCU_SERVER_FARM_INFO *)packet.GetPacketData();

	res->wOpCode = CU_SERVER_FARM_INFO;
	res->serverFarmInfo.serverFarmId = app->byServerIndex;
	wcscpy_s((wchar_t*)res->serverFarmInfo.wszGameServerFarmName, NTL_MAX_SIZE_SERVER_FARM_NAME_UNICODE + 1, L"KameCore");
	res->serverFarmInfo.byServerStatus = DBO_SERVER_STATUS_UP;
	res->serverFarmInfo.dwLoad = app->dwCurrentLoaded;
	res->serverFarmInfo.dwMaxLoad = app->dwMaxLoad;

	packet.SetPacketLen(sizeof(sCU_SERVER_FARM_INFO));
	g_pApp->Send(app->GetHSession(this->m_dwAccountID), &packet);

	//Send Char Server OK Answer
	CNtlPacket packet2(sizeof(sCU_CHARACTER_SERVERLIST_ONE_RES));
	sCU_CHARACTER_SERVERLIST_ONE_RES* res2 = (sCU_CHARACTER_SERVERLIST_ONE_RES*)packet2.GetPacketData();	
	res2->wResultCode = CHARACTER_SUCCESS;
	res2->wOpCode = CU_CHARACTER_SERVERLIST_ONE_RES;
	packet2.SetPacketLen(sizeof(sCU_CHARACTER_SERVERLIST_ONE_RES));
	app->Send(app->GetHSession(this->m_dwAccountID), &packet2);

	//Send How many char servers are
	CNtlPacket packet3(sizeof(sCU_SERVER_CHANNEL_INFO));
	sCU_SERVER_CHANNEL_INFO * res3 = (sCU_SERVER_CHANNEL_INFO *)packet3.GetPacketData();
	
	//memcpy(res3->serverChannelInfo, app->serverChannelInfo, MAX_NUMOF_COMMUNITY_SERVER);
	res3->serverChannelInfo[0].serverFarmId = 0;
	res3->serverChannelInfo[0].byServerChannelIndex = 0;
	res3->serverChannelInfo[0].dwLoad = 10;
	res3->serverChannelInfo[0].bIsVisible = true;
	res3->serverChannelInfo[0].dwMaxLoad = 100;
	res3->serverChannelInfo[0].byServerStatus = DBO_SERVER_STATUS_UP;
	res3->wOpCode = CU_SERVER_CHANNEL_INFO;
	res3->byCount = MAX_NUMOF_COMMUNITY_SERVER;

	packet3.SetPacketLen(sizeof(sCU_SERVER_CHANNEL_INFO));
	app->Send(app->GetHSession(this->m_dwAccountID), &packet3);
}

void CCharSession::SendCharLoadReq(CNtlPacket* pPacket)
{
	CCharServer * app = (CCharServer*)NtlSfxGetApp();
	sUC_CHARACTER_LOAD_REQ* req = (sUC_CHARACTER_LOAD_REQ*)pPacket->GetPacketData();

	CNtlPacket packetQuery(sizeof(sCQ_CHARACTER_LOAD_REQ));
	sCQ_CHARACTER_LOAD_REQ* reqQuery = (sCQ_CHARACTER_LOAD_REQ*)packetQuery.GetPacketData();
	reqQuery->accountId = req->accountId;
	reqQuery->serverFarmId = req->serverFarmId;
	reqQuery->wOpCode = CQ_CHARACTER_LOAD_REQ;
	packetQuery.SetPacketLen(sizeof(sCQ_CHARACTER_LOAD_REQ));
	app->AcquireQuerySession()->PushPacket(&packetQuery);
}

void CCharSession::SendCharAddReq(CNtlPacket* pPacket)
{
	CCharServer * app = (CCharServer*)NtlSfxGetApp();
	sUC_CHARACTER_ADD_REQ* req = (sUC_CHARACTER_ADD_REQ*)pPacket->GetPacketData();
	CNtlPacket packetQuery(sizeof(sCQ_CHARACTER_ADD_REQ));
	sCQ_CHARACTER_ADD_REQ* res = (sCQ_CHARACTER_ADD_REQ*)packetQuery.GetPacketData();
	CNewbieTable* pNewbieTable = app->g_pTableContainer->GetNewbieTable();
	CWorldTable* pWorldTable = app->g_pTableContainer->GetWorldTable();
	sNEWBIE_TBLDAT *pNewbieTblData = reinterpret_cast<sNEWBIE_TBLDAT*>(pNewbieTable->GetNewbieTbldat(req->byRace, req->byClass));
	sWORLD_TBLDAT* pWorldTblData = reinterpret_cast<sWORLD_TBLDAT*>(pWorldTable->FindData(pNewbieTblData->tutorialWorld));	
	res->accountId = this->m_dwAccountID;
	wcscpy_s(res->awchCharName, NTL_MAX_SIZE_CHAR_NAME_UNICODE+1, req->awchCharName);
	res->byBlood = req->byBlood;
	res->byClass = req->byClass;
	res->byGender = req->byGender;
	res->byFace = req->byFace;
	res->byHair = req->byHair;
	res->byHairColor = req->byHairColor;
	res->byRace = req->byRace;
	res->bySkinColor = req->bySkinColor;
	res->serverFarmId = app->byServerIndex;	
	for (int i = 0; i < NTL_MAX_NEWBIE_ITEM; i++)
	{
		if (pNewbieTblData->aitem_Tblidx[i] == INVALID_TBLIDX)
			continue;

		sITEM_TBLDAT* pItemTbldat = reinterpret_cast<sITEM_TBLDAT*>(app->g_pTableContainer->GetItemTable()->FindData(pNewbieTblData->aitem_Tblidx[i]));
		if ((pItemTbldat->byItem_Type == ITEM_TYPE_WAREHOUSE) || (pItemTbldat->byItem_Type == ITEM_TYPE_NETPYSTORE))
			continue;

		res->sItem[i].itemNo = app->AcquireSerialId();
		res->sItem[i].itemId = pNewbieTblData->aitem_Tblidx[i];		
		if ((pItemTbldat->byItem_Type >= ITEM_TYPE_WEAPON_FIRST && pItemTbldat->byItem_Type <= ITEM_TYPE_WEAPON_LAST) ||
			(pItemTbldat->byItem_Type >= ITEM_TYPE_ARMOR_FIRST && pItemTbldat->byItem_Type <= ITEM_TYPE_ARMOR_LAST)||
			(pItemTbldat->byItem_Type == ITEM_TYPE_SCOUTER))
		{
			res->sItem[i].byPlace = CONTAINER_TYPE_EQUIP;
			res->sItem[i].byPosition = pNewbieTblData->abyPos[i];
		}
		if (pItemTbldat->byItem_Type == ITEM_TYPE_BAG)
		{
			res->sItem[i].byPlace = CONTAINER_TYPE_BAGSLOT;
			res->sItem[i].byPosition = 0;
		}		
		res->sItem[i].byStackcount = 1;
		res->sItem[i].byRank = pItemTbldat->byRank;
		res->sItem[i].byCurrentDurability = pItemTbldat->byDurability;
		res->sItem[i].aOptionTblidx[0] = INVALID_TBLIDX;
		res->sItem[i].aOptionTblidx[1] = INVALID_TBLIDX;
		res->sItem[i].bNeedToIdentify = FALSE;
		res->sItem[i].byGrade = 0;
	}
	for (int i = 0; i < NTL_MAX_NEWBIE_SKILL; i++)
	{
		res->aSkill[i] = pNewbieTblData->aSkillTblidx[i];
	}
	for (int i = 0; i < NTL_MAX_NEWBIE_QUICKSLOT_COUNT; i++)
	{
		res->asQuickSlotData[i] = pNewbieTblData->asQuickData[i];
	}
	res->bindWorldId = pNewbieTblData->world_Id;//If you change here you need change in game server
	res->worldID = pNewbieTblData->tutorialWorld;
	res->worldTblix = pNewbieTblData->tutorialWorld;
	res->mapNameTblidx = pNewbieTblData->mapNameTblidx;
	res->vSpawn_Loc.x = pWorldTblData->vStart1Loc.x;//Maybe in the future i will be checking on CWorldTable to see if they implemented that
	res->vSpawn_Loc.y = pWorldTblData->vStart1Loc.y;
	res->vSpawn_Loc.z = pWorldTblData->vStart1Loc.z;
	res->vSpawn_Dir.x = pWorldTblData->vStart1Dir.x;
	res->vSpawn_Dir.y = pWorldTblData->vStart1Dir.y;
	res->vSpawn_Dir.z = pWorldTblData->vStart1Dir.z;
	res->vBind_Dir.x = pWorldTblData->outWorldDir.x;
	res->vBind_Dir.y = pWorldTblData->outWorldDir.y;
	res->vBind_Dir.z = pWorldTblData->outWorldDir.z;
	res->vBind_Loc.x = pWorldTblData->outWorldLoc.x;
	res->vBind_Loc.y = pWorldTblData->outWorldLoc.y;
	res->vBind_Loc.z = pWorldTblData->outWorldLoc.z;
	res->byBindType = DBO_BIND_TYPE_INITIAL_LOCATION;
	res->wOpCode = CQ_CHARACTER_ADD_REQ;
	packetQuery.SetPacketLen(sizeof(sCQ_CHARACTER_ADD_REQ));
	app->AcquireQuerySession()->PushPacket(&packetQuery);
}

void CCharSession::SendCharSelectReq(CNtlPacket* pPacket)
{
	CCharServer * app = (CCharServer*)NtlSfxGetApp();
	sUC_CHARACTER_SELECT_REQ* req = (sUC_CHARACTER_SELECT_REQ*)pPacket->GetPacketData();
	SendQueryLoadChar(this->m_dwAccountID, req->charId);
	CNtlPacket packet(sizeof(sCU_CHARACTER_SELECT_RES));
	sCU_CHARACTER_SELECT_RES* res = (sCU_CHARACTER_SELECT_RES*)packet.GetPacketData();	
	this->m_dwCharID = req->charId;
	res->charId = req->charId;		

	if (app->AddCharLogged(this->m_dwAccountID,this->m_dwCharID))
		res->wResultCode = CHARACTER_SUCCESS;
	else
		res->wResultCode = CHARACTER_USER_EXIST_IN_GAME_SERVER;

	res->wOpCode = CU_CHARACTER_SELECT_RES;
	strcpy_s((char*)res->abyAuthKey, NTL_MAX_SIZE_AUTH_KEY, "Dbo");
	strcpy_s(res->szGameServerIP, sizeof(res->szGameServerIP), app->m_config.strGameServerIP.c_str());
	//switch(req->byServerChannelIndex)
	res->wGameServerPortForClient = app->m_config.wGameServerPort;	
	packet.SetPacketLen(sizeof(sCU_CHARACTER_SELECT_RES));
	app->Send(app->GetHSession(this->m_dwAccountID), &packet);
}

void CCharSession::SendCharConnectCheck(CNtlPacket* pPacket)
{
	CCharServer * app = (CCharServer*)NtlSfxGetApp();
	sUC_CONNECT_WAIT_CHECK_REQ* req = (sUC_CONNECT_WAIT_CHECK_REQ*)pPacket->GetPacketData();
	
	CNtlPacket packet(sizeof(sCQ_CONNECT_WAIT_CHECK_REQ));
	sCQ_CONNECT_WAIT_CHECK_REQ* res = (sCQ_CONNECT_WAIT_CHECK_REQ*)packet.GetPacketData();
	res->accountId = this->m_dwAccountID;
	res->ServerFarmId = app->byServerIndex;
	res->byCharacterServerIndex = app->byServerIndex;
	res->byChannelIdx = app->byServerIndex;
	res->byGameServerIndex = app->byServerIndex;
	res->wOpCode = CQ_CONNECT_WAIT_CHECK_REQ;
	packet.SetPacketLen(sizeof(sCQ_CONNECT_WAIT_CHECK_REQ));
	app->AcquireQuerySession()->PushPacket(&packet);
}

void CCharSession::SendCharConnectCheckCancel(CNtlPacket* pPacket)
{
	CCharServer * app = (CCharServer*)NtlSfxGetApp();
	sUC_CONNECT_WAIT_CANCEL_REQ* req = (sUC_CONNECT_WAIT_CANCEL_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sCQ_CONNECT_WAIT_CANCEL_REQ));
	sCQ_CONNECT_WAIT_CANCEL_REQ* res = (sCQ_CONNECT_WAIT_CANCEL_REQ*)packet.GetPacketData();
	res->accountId = this->m_dwAccountID;
	res->ServerFarmId = app->byServerIndex;
	res->byCharacterServerIndex = app->byServerIndex;
	res->byChannelIdx = app->byServerIndex;
	res->byGameServerIndex = app->byServerIndex;
	res->wOpCode = CQ_CONNECT_WAIT_CANCEL_REQ;
	packet.SetPacketLen(sizeof(sCQ_CONNECT_WAIT_CANCEL_REQ));
	app->AcquireQuerySession()->PushPacket(&packet);
}

void	CCharSession::SendCharDisconnectReq(CNtlPacket* pPacket)
{
	CCharServer * app = (CCharServer*)NtlSfxGetApp();
	sUC_CHARACTER_EXIT_REQ* req = (sUC_CHARACTER_EXIT_REQ*)pPacket->GetPacketData();

	//Now you disconnect
	CNtlPacket packet(sizeof(sCU_CHARACTER_EXIT_RES));
	sCU_CHARACTER_EXIT_RES* res = (sCU_CHARACTER_EXIT_RES*)packet.GetPacketData();
	res->wResultCode = CHARACTER_SUCCESS;
	res->wOpCode = CU_CHARACTER_EXIT_RES;
	packet.SetPacketLen(sizeof(sCU_CHARACTER_EXIT_RES));
 	app->Send(app->GetHSession(this->m_dwAccountID), &packet);	

	//Send notification to Query
	CNtlPacket packetQuery(sizeof(sCQ_CHARACTER_EXIT));
	sCQ_CHARACTER_EXIT* query = (sCQ_CHARACTER_EXIT*)packetQuery.GetPacketData();
	query->accountId = this->m_dwAccountID;
	query->bIsGameMove = req->bIsGameMove;
	query->serverFarmId = app->byServerIndex;
	query->charId = this->m_dwCharID;
	query->wOpCode = CQ_CHARACTER_EXIT;
	packetQuery.SetPacketLen(sizeof(sCQ_CHARACTER_EXIT));
	app->AcquireQuerySession()->PushPacket(&packetQuery);

	if (!req->bIsGameMove)
		app->RemoveCharaLogged(this->m_dwAccountID, 0);
}
////////////Query Server Packet handling
void CCharSession::SendQueryLoadChar(ACCOUNTID id, CHARACTERID charID)
{
	CCharServer * app = (CCharServer*)NtlSfxGetApp();
	//Char Info
	CNtlPacket packetQ(sizeof(sGQ_PC_DATA_LOAD_REQ));
	sGQ_PC_DATA_LOAD_REQ* res1 = (sGQ_PC_DATA_LOAD_REQ*)packetQ.GetPacketData();
	res1->accountId = id;
	res1->charId = charID;
	res1->wOpCode = GQ_PC_DATA_LOAD_REQ;
	packetQ.SetPacketLen(sizeof(sGQ_PC_DATA_LOAD_REQ));
	app->AcquireQuerySession()->PushPacket(&packetQ);
	//Item info
	CNtlPacket packetQ2(sizeof(sGQ_PC_ITEM_LOAD_REQ));
	sGQ_PC_ITEM_LOAD_REQ* res2 = (sGQ_PC_ITEM_LOAD_REQ*)packetQ2.GetPacketData();
	res2->accountId = id;
	res2->charId = charID;
	res2->wOpCode = GQ_PC_ITEM_LOAD_REQ;
	packetQ2.SetPacketLen(sizeof(sGQ_PC_DATA_LOAD_REQ));
	app->AcquireQuerySession()->PushPacket(&packetQ2);
	//Skills Info
	CNtlPacket packetQ3(sizeof(sGQ_PC_SKILL_LOAD_REQ));
	sGQ_PC_SKILL_LOAD_REQ* res3 = (sGQ_PC_SKILL_LOAD_REQ*)packetQ3.GetPacketData();
	res3->accountId = id;
	res3->charId = charID;
	res3->wOpCode = GQ_PC_SKILL_LOAD_REQ;
	packetQ3.SetPacketLen(sizeof(sGQ_PC_SKILL_LOAD_REQ));
	app->AcquireQuerySession()->PushPacket(&packetQ3);
	//Buff Info
	CNtlPacket packetQ4(sizeof(sGQ_PC_BUFF_LOAD_REQ));
	sGQ_PC_BUFF_LOAD_REQ* res4 = (sGQ_PC_BUFF_LOAD_REQ*)packetQ4.GetPacketData();
	res4->accountId = id;
	res4->charId = charID;
	res4->wOpCode = GQ_PC_BUFF_LOAD_REQ;
	packetQ4.SetPacketLen(sizeof(sGQ_PC_BUFF_LOAD_REQ));
	app->AcquireQuerySession()->PushPacket(&packetQ4);
	//HTB Info
	CNtlPacket packetQ5(sizeof(sGQ_PC_HTB_LOAD_REQ));
	sGQ_PC_HTB_LOAD_REQ* res5 = (sGQ_PC_HTB_LOAD_REQ*)packetQ5.GetPacketData();
	res5->accountId = id;
	res5->charId = charID;
	res5->wOpCode = GQ_PC_HTB_LOAD_REQ;
	packetQ5.SetPacketLen(sizeof(sGQ_PC_HTB_LOAD_REQ));
	app->AcquireQuerySession()->PushPacket(&packetQ5);
	//Quest Item Info
	CNtlPacket packetQ6(sizeof(sGQ_PC_QUEST_ITEM_LOAD_REQ));
	sGQ_PC_QUEST_ITEM_LOAD_REQ* res6 = (sGQ_PC_QUEST_ITEM_LOAD_REQ*)packetQ6.GetPacketData();
	res6->accountId = id;
	res6->charId = charID;
	res6->wOpCode = GQ_PC_QUEST_ITEM_LOAD_REQ;
	packetQ6.SetPacketLen(sizeof(sGQ_PC_QUEST_ITEM_LOAD_REQ));
	app->AcquireQuerySession()->PushPacket(&packetQ6);
	//Quest Complete Info
	CNtlPacket packetQ7(sizeof(sGQ_PC_QUEST_COMPLETE_LOAD_REQ));
	sGQ_PC_QUEST_COMPLETE_LOAD_REQ* res7 = (sGQ_PC_QUEST_COMPLETE_LOAD_REQ*)packetQ7.GetPacketData();
	res7->accountId = id;
	res7->charId = charID;
	res7->wOpCode = GQ_PC_QUEST_COMPLETE_LOAD_REQ;
	packetQ7.SetPacketLen(sizeof(sGQ_PC_QUEST_COMPLETE_LOAD_REQ));
	app->AcquireQuerySession()->PushPacket(&packetQ7);
	//Quest Progress Info
	CNtlPacket packetQ8(sizeof(sGQ_PC_QUEST_PROGRESS_LOAD_REQ));
	sGQ_PC_QUEST_PROGRESS_LOAD_REQ* res8 = (sGQ_PC_QUEST_PROGRESS_LOAD_REQ*)packetQ8.GetPacketData();
	res8->accountId = id;
	res8->charId = charID;
	res8->wOpCode = GQ_PC_QUEST_PROGRESS_LOAD_REQ;
	packetQ8.SetPacketLen(sizeof(sGQ_PC_QUEST_PROGRESS_LOAD_REQ));
	app->AcquireQuerySession()->PushPacket(&packetQ8);
	//Quick Slot Load
	CNtlPacket packetQ9(sizeof(sGQ_PC_QUICK_SLOT_LOAD_REQ));
	sGQ_PC_QUICK_SLOT_LOAD_REQ* res9 = (sGQ_PC_QUICK_SLOT_LOAD_REQ*)packetQ9.GetPacketData();
	res9->accountId = id;
	res9->charId = charID;
	res9->wOpCode = GQ_PC_QUICK_SLOT_LOAD_REQ;
	packetQ9.SetPacketLen(sizeof(sGQ_PC_QUICK_SLOT_LOAD_REQ));
	app->AcquireQuerySession()->PushPacket(&packetQ9);
	//Shortcut Load
	CNtlPacket packetQ10(sizeof(sGQ_PC_SHORTCUT_LOAD_REQ));
	sGQ_PC_SHORTCUT_LOAD_REQ* res10 = (sGQ_PC_SHORTCUT_LOAD_REQ*)packetQ10.GetPacketData();
	res10->accountId = id;
	res10->wOpCode = GQ_PC_SHORTCUT_LOAD_REQ;
	packetQ10.SetPacketLen(sizeof(sGQ_PC_SHORTCUT_LOAD_REQ));
	app->AcquireQuerySession()->PushPacket(&packetQ10);
	//Quest Progress Info
	CNtlPacket packetQ11(sizeof(sGQ_PC_ITEM_RECIPE_LOAD_REQ));
	sGQ_PC_ITEM_RECIPE_LOAD_REQ* res11 = (sGQ_PC_ITEM_RECIPE_LOAD_REQ*)packetQ11.GetPacketData();
	res11->accountId = id;
	res11->charId = charID;
	res11->wOpCode = GQ_PC_ITEM_RECIPE_LOAD_REQ;
	packetQ11.SetPacketLen(sizeof(sGQ_PC_ITEM_RECIPE_LOAD_REQ));
	app->AcquireQuerySession()->PushPacket(&packetQ11);
	//Quest Progress Info
	CNtlPacket packetQ12(sizeof(sGQ_PC_GMT_LOAD_REQ));
	sGQ_PC_GMT_LOAD_REQ* res12 = (sGQ_PC_GMT_LOAD_REQ*)packetQ12.GetPacketData();
	res12->accountId = id;
	res12->charId = charID;
	res12->dwGMTCurTime = 0;
	res12->wOpCode = GQ_PC_GMT_LOAD_REQ;
	packetQ12.SetPacketLen(sizeof(sGQ_PC_GMT_LOAD_REQ));
	app->AcquireQuerySession()->PushPacket(&packetQ12);
	//WarFog Info
	CNtlPacket packetQ13(sizeof(sGQ_PC_WAR_FOG_REQ));
	sGQ_PC_WAR_FOG_REQ* res13 = (sGQ_PC_WAR_FOG_REQ*)packetQ13.GetPacketData();
	res13->accountId = id;
	res13->charId = charID;
	res13->wOpCode = GQ_PC_WAR_FOG_REQ;
	packetQ13.SetPacketLen(sizeof(sGQ_PC_WAR_FOG_REQ));
	app->AcquireQuerySession()->PushPacket(&packetQ13);
}
void CCharSession::SendQueryLoadInfo(CNtlPacket* pPacket)
{
	CCharServer * app = (CCharServer*)NtlSfxGetApp();
	sQC_CHARACTER_LOAD_RES* req = (sQC_CHARACTER_LOAD_RES*)pPacket->GetPacketData();
	
	CNtlPacket packet(sizeof(sCU_CHARACTER_INFO));
	sCU_CHARACTER_INFO * res = (sCU_CHARACTER_INFO *)packet.GetPacketData();
	if (req->byCharCount != INVALID_BYTE)
	{
		for (int i = 0; i < req->byCharCount;i++)
		{
			memcpy(res->sPcData[i].awchName, req->sPcData[i].awchName, NTL_MAX_SIZE_CHAR_NAME_UNICODE + 1);
			res->sPcData[i].bIsAdult = req->sPcData[i].bIsAdult;
			res->sPcData[i].bNeedNameChange = req->sPcData[i].bNeedNameChange;
			res->sPcData[i].bTutorialFlag = req->sPcData[i].bTutorialFlag;
			res->sPcData[i].byClass = req->sPcData[i].byClass;
			res->sPcData[i].byFace = req->sPcData[i].byFace;
			res->sPcData[i].byGender = req->sPcData[i].byGender;
			res->sPcData[i].byHair = req->sPcData[i].byHair;
			res->sPcData[i].byHairColor = req->sPcData[i].byHairColor;
			res->sPcData[i].byLevel = req->sPcData[i].byLevel;
			res->sPcData[i].byRace = req->sPcData[i].byRace;
			res->sPcData[i].bySkinColor = req->sPcData[i].bySkinColor;
			res->sPcData[i].worldId = req->sPcData[i].worldId;
			res->sPcData[i].worldTblidx = req->sPcData[i].worldTblidx;
			res->sPcData[i].dwMoney = req->sPcData[i].dwMoney;
			res->sPcData[i].dwMoneyBank = req->sPcData[i].dwMoneyBank;
			res->sPcData[i].dwMapInfoIndex = req->sPcData[i].dwMapInfoIndex;
			res->sPcData[i].charId = req->sPcData[i].charId;
			for (int p = 0; p < EQUIP_SLOT_TYPE_COUNT; p++)
			{
				res->sPcData[i].sItem[p].aOptionTblidx[0] = req->sPcData[i].sItem[p].aOptionTblidx[0];
				res->sPcData[i].sItem[p].aOptionTblidx[1] = req->sPcData[i].sItem[p].aOptionTblidx[1];
				res->sPcData[i].sItem[p].byBattleAttribute = req->sPcData[i].sItem[p].byBattleAttribute;
				res->sPcData[i].sItem[p].byGrade = req->sPcData[i].sItem[p].byGrade;
				res->sPcData[i].sItem[p].byRank = req->sPcData[i].sItem[p].byRank;
				res->sPcData[i].sItem[p].tblidx = req->sPcData[i].sItem[p].tblidx;
			}
			res->sPcData[i].sMarking.byCode = req->sPcData[i].sMarking.byCode;
			res->sPcData[i].sDogi.guildId = req->sPcData[i].sDogi.guildId;
			res->sPcData[i].sDogi.byDojoColor = req->sPcData[i].sDogi.byDojoColor;
			res->sPcData[i].sDogi.byGuildColor = req->sPcData[i].sDogi.byGuildColor;
			res->sPcData[i].sDogi.byType = req->sPcData[i].sDogi.byType;
		}
	}
	if (req->byDelCount != INVALID_BYTE)
	{
		for (int p = 0; p < req->byDelCount; p++)
		{
			res->asDelData[p] = req->sDelData[p];
		}
	}
	res->byCount = (req->byCharCount + req->byDelCount);
	res->wOpCode = CU_CHARACTER_INFO;
	packet.SetPacketLen(sizeof(sCU_CHARACTER_INFO));
	app->Send(app->GetHSession(req->accountId), &packet);

	CNtlPacket packet2(sizeof(sCU_NETMARBLEMEMBERIP_NFY));
	sCU_NETMARBLEMEMBERIP_NFY* res2 = (sCU_NETMARBLEMEMBERIP_NFY*)packet2.GetPacketData();
	res2->wOpCode = CU_NETMARBLEMEMBERIP_NFY;
	packet2.SetPacketLen(sizeof(sCU_NETMARBLEMEMBERIP_NFY));
	app->Send(app->GetHSession(req->accountId), &packet2);

	// load characters
	CNtlPacket packet3(sizeof(sCU_CHARACTER_LOAD_RES));
	sCU_CHARACTER_LOAD_RES * res3 = (sCU_CHARACTER_LOAD_RES *)packet3.GetPacketData();
	res3->wOpCode = CU_CHARACTER_LOAD_RES;
	res3->wResultCode = req->wResultCode;
	packet3.SetPacketLen(sizeof(sCU_CHARACTER_LOAD_RES));
	app->Send(app->GetHSession(req->accountId), &packet3);
}

void CCharSession::SendQueryAddRes(CNtlPacket* pPacket)
{
	CCharServer * app = (CCharServer*)NtlSfxGetApp();
	sQC_CHARACTER_ADD_RES* req = (sQC_CHARACTER_ADD_RES*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sCU_CHARACTER_ADD_RES));
	sCU_CHARACTER_ADD_RES* res = (sCU_CHARACTER_ADD_RES*)packet.GetPacketData();
	res->sPcDataSummary = req->sPcDataSummary;
	res->wResultCode = req->wResultCode;
	res->wOpCode = CU_CHARACTER_ADD_RES;
	packet.SetPacketLen(sizeof(sCU_CHARACTER_ADD_RES));
	app->Send(app->GetHSession(req->accountId), &packet);
}

void CCharSession::SendQueryConnectCheck(CNtlPacket* pPacket)
{
	CCharServer * app = (CCharServer*)NtlSfxGetApp();
	sQC_CONNECT_WAIT_CHECK_RES* req = (sQC_CONNECT_WAIT_CHECK_RES*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sCU_CONNECT_WAIT_CHECK_RES));
	sCU_CONNECT_WAIT_CHECK_RES* res = (sCU_CONNECT_WAIT_CHECK_RES*)packet.GetPacketData();
	res->wResultCode = req->wResultCode;
	res->wOpCode = CU_CONNECT_WAIT_CHECK_RES;
	packet.SetPacketLen(sizeof(sCU_CONNECT_WAIT_CHECK_RES));
	app->Send(app->GetHSession(req->accountId), &packet);
}

void CCharSession::SendQueryConnectCheckCancel(CNtlPacket* pPacket)
{
	CCharServer * app = (CCharServer*)NtlSfxGetApp();
	sQC_CONNECT_WAIT_CANCEL_RES* req = (sQC_CONNECT_WAIT_CANCEL_RES*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sCU_CONNECT_WAIT_CANCEL_RES));
	sCU_CONNECT_WAIT_CANCEL_RES* res = (sCU_CONNECT_WAIT_CANCEL_RES*)packet.GetPacketData();
	res->wResultCode = req->wResultCode;
	res->wOpCode = CU_CONNECT_WAIT_CANCEL_RES;
	packet.SetPacketLen(sizeof(sCU_CONNECT_WAIT_CANCEL_RES));
	app->Send(app->GetHSession(req->accountId), &packet);
}