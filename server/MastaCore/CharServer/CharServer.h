#pragma once
#include "NtlSfx.h"
#include "CharSession.h"
#include "CharSessionFactory.h"

#ifndef CCHAR_SERVER_H
#define CCHAR_SERVER_H

class CMasterSession;
class CQuerySession;
class CTableContainer;
class CNtlBitFlagManager;
class CTableFileNameList;

#include "MasterSession.h"
#include "QuerySession.h"
#include "TableContainer.h"
#include "TableFileNameList.h"
#include "NewbieTable.h"
#include "NtlBitFlagManager.h"

class CCharServer :	public CNtlServerApp
{
	//Members
public:
	CNtlString				ServersConfig[99][2];//For Config of multiple server 0->{Ip,Port} - Luiz45
	DWORD					dwMaxLoad;
	DWORD					dwCurrentLoaded;
	BYTE					byServerIndex;
	sDBO_GAME_SERVER_CHANNEL_INFO		serverChannelInfo[MAX_NUMOF_COMMUNITY_SERVER];
	CTableContainer*		g_pTableContainer;
	sSERVERCONFIG			m_config;
private:
	CNtlAcceptor			m_clientAcceptor;
	CNtlLog  				m_log;
	DWORD					m_uiSerialId;
	CNtlString				EnableMultipleServers;//Added for enabling multiple server - Luiz45

	typedef std::map<ACCOUNTID, CHARACTERID> IN_CHAR_SERVER;
	typedef std::map<ACCOUNTID, CHARACTERID>::iterator IN_CHAR_SERVER_IT;

	CHAR_CLIENT_SESSION		m_sessionList;
	IN_CHAR_SERVER			m_charList;
	//Methods
	//Char Server ptr refresh
	CCharSession*				m_pCharSession;
	//Client Session
	CCharSession*				m_pClientSession;
	//Master Server Connection
	CNtlConnector				m_masterServerConnector;
	CNtlMutex					m_masterServerMutex;
	CMasterSession*				m_pMasterServerSession;
	//Query Server Connection
	CNtlConnector				m_queryServerConnector;
	CNtlMutex					m_queryServerMutex;
	CQuerySession*			    m_pQueryServerSession;
	//Connected
	bool					m_bMasterServerConnected;
	bool					m_bQueryServerConnected;
	void					DoAliveTime();
public:	
	//For Multiple Server - Luiz45
	const std::string		GetConfigFileEnabledMultipleServers();
	const DWORD				GetConfigFileMaxServers();
	int						OnInitApp();
	int						OnCreate();
	void					OnDestroy();
	int						OnCommandArgument(int argc, _TCHAR* argv[]);
	int						OnConfiguration(const char * lpszConfigFile);
	int						OnAppStart();
	int						GetSessionCount();
	void					Run();
	bool					Add(ACCOUNTID accID, HSESSION hSession);
	void					RemoveClientSession(ACCOUNTID accID);
	void					Remove(HSESSION session);
	bool					Find(ACCOUNTID accID);
	bool					AddCharLogged(ACCOUNTID accID, CHARACTERID charID);
	bool					IsCharLogged(ACCOUNTID accID, CHARACTERID charID);
	void					RemoveCharaLogged(ACCOUNTID accID, CHARACTERID charID);
	HSESSION				GetHSession(ACCOUNTID accID);
	void					SetMasterSession(CMasterSession* pServerSession);
	CMasterSession*			AcquireMasterSession();
	CMasterSession*			GetMasterSession();
	void					SetQuerySession(CQuerySession* pServerSession);
	CQuerySession*			AcquireQuerySession();
	CQuerySession*			GetQuerySession();
	void					SetClientSession(CCharSession* pClientSession);
	CCharSession*			GetClientSession();
	DWORD					GetCurrentLoaded(){ return dwCurrentLoaded; }
	DWORD					GetMaxLoad(){ return dwMaxLoad; }
	DWORD					GetServerIndex(){ return byServerIndex; }
	void					IncrementCurrentLoad(){ dwCurrentLoaded++; }
	void					DecreaseCurrentLoad(){ dwCurrentLoaded--; }
	void					SetQueryServerConnected(bool c){ m_bQueryServerConnected = c; }
	void					SetMasterServerConnected(bool c){ m_bMasterServerConnected = c; }
	bool					IsQueryServerConnected() { return m_bQueryServerConnected; }
	bool					IsMasterServerConnected(){ return m_bMasterServerConnected; }
	bool					CreateTableContainer();
	HOBJECT					AcquireSerialId(void);
};

#endif