#include "stdafx.h"
#include "CharSessionFactory.h"


CCharSessionFactory::CCharSessionFactory()
{
}


CCharSessionFactory::~CCharSessionFactory()
{
}

CNtlSession * CCharSessionFactory::CreateSession(SESSIONTYPE sessionType)
{
	CNtlSession * pSession = NULL;
	switch (sessionType)
	{
	case SESSION_CLIENT:
	case SESSION_CHAR_SERVER:
	{
		pSession = new CCharSession(sessionType);
	}
	break;
	case SESSION_MASTER_SERVER:
	{
		pSession = new CNtlSession(sessionType);
	}
		break;
	case SESSION_QUERY_SERVER:
	{
		pSession = new CNtlSession(sessionType);
	}
		break;
	default:
		break;
	}

	return pSession;
}



void CCharSessionFactory::DestroySession(CNtlSession * pSession)
{
	switch (pSession->GetSessionType())
	{
	case SESSION_CLIENT:
	case SESSION_MASTER_SERVER:
	case SESSION_QUERY_SERVER:
	{
		SAFE_DELETE(pSession);
	}
	break;

	default:
		break;
	}
}