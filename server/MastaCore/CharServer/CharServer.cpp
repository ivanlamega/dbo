#include "stdafx.h"
#include "CharServer.h"

//For Multiple Server - Luiz45
const std::string CCharServer::GetConfigFileEnabledMultipleServers()
{
	return EnableMultipleServers.GetString();
}
const DWORD CCharServer::GetConfigFileMaxServers()
{
	return MAX_NUMOF_CHAR_SERVER;
}
int	CCharServer::OnInitApp()
{
	m_nMaxSessionCount = MAX_NUMOF_CHAR_SESSION;

	m_pSessionFactory = new CCharSessionFactory;
	if (NULL == m_pSessionFactory)
	{
		return NTL_ERR_SYS_MEMORY_ALLOC_FAIL;
	}

	m_pMasterServerSession = NULL;
	m_pQueryServerSession = NULL;	
	m_pCharSession = (CCharSession*)m_pSessionFactory->CreateSession(SESSION_CHAR_SERVER);

	return NTL_SUCCESS;
}

int	CCharServer::OnCreate()
{
	int rc = NTL_SUCCESS;
	rc = m_clientAcceptor.Create(m_config.strCharServerIP.c_str(), m_config.wCharServerPort, SESSION_CHAR_SERVER,
		MAX_NUMOF_CHAR_GAME_CLIENT, 5,  15, MAX_NUMOF_CHAR_GAME_CLIENT);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_clientAcceptor, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_masterServerConnector.Create(m_config.strMasterServerIP.c_str(), m_config.wMasterServerPort, SESSION_MASTER_SERVER);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_masterServerConnector, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_queryServerConnector.Create(m_config.strQueryServerIP.c_str(), m_config.wQueryServerPort, SESSION_QUERY_SERVER);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_queryServerConnector, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	return NTL_SUCCESS;

}

void	CCharServer::OnDestroy()
{
	SAFE_DELETE(m_pSessionFactory);
}

int	CCharServer::OnCommandArgument(int argc, _TCHAR* argv[])
{
	return NTL_SUCCESS;
}

int	CCharServer::OnConfiguration(const char * lpszConfigFile)
{
	CNtlIniFile file;

	int rc = file.Create(lpszConfigFile);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}
	//For multiple servers - Luiz45
	if (!file.Read("ServerOptions", "EnableMultipleServers", EnableMultipleServers))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	CNtlString sTmpCountServer;
	if (!file.Read("ServerOptions", "MaxServerAllowed", sTmpCountServer))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (GetConfigFileEnabledMultipleServers() == "true")
	{
		int maxServer = atoi(sTmpCountServer.c_str());
		int i = 0;
		for (i = 0; i < maxServer; i++)
		{
			if (!file.Read("CharServer" + i, "Address", ServersConfig[i][0]))
			{
				return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
			}
			if (!file.Read("CharServer" + i, "Port", ServersConfig[i][1]))
			{
				return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
			}			
		}
	}
	else
	{
		if (!file.Read("CharServer", "Address", m_config.strCharServerIP))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("CharServer", "Port", m_config.wCharServerPort))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
	}
	if (!file.Read("GameServer", "Address", m_config.strGameServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("GameServer", "Port", m_config.wGameServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("MasterServer", "Address", m_config.strMasterServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("MasterServer", "Port", m_config.wMasterServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("OperatingServer", "Address", m_config.strOperatingServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("OperatingServer", "Port", m_config.wOperatingServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("QueryServer", "Address", m_config.strQueryServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("QueryServer", "Port", m_config.wQueryServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	return NTL_SUCCESS;
}

int	CCharServer::OnAppStart()
{
	return NTL_SUCCESS;
}

bool	CCharServer::CreateTableContainer()
{
	CNtlBitFlagManager flagManager;
	if (false == flagManager.Create(CTableContainer::TABLE_COUNT))
	{
		return false;
	}

	CTableFileNameList fileNameList;
	if (false == fileNameList.Create())
	{
		return false;
	}

	flagManager.Set(CTableContainer::TABLE_HELP);
	flagManager.Set(CTableContainer::TABLE_MERCHANT);
	flagManager.Set(CTableContainer::TABLE_MOB);
	flagManager.Set(CTableContainer::TABLE_ITEM);
	flagManager.Set(CTableContainer::TABLE_ITEM_OPTION);
	flagManager.Set(CTableContainer::TABLE_NEWBIE);
	flagManager.Set(CTableContainer::TABLE_WORLD);
	flagManager.Set(CTableContainer::TABLE_WORLD_MAP);
	flagManager.Set(CTableContainer::TABLE_WORLD_ZONE);
	flagManager.Set(CTableContainer::TABLE_TEXT_ALL);
	flagManager.Set(CTableContainer::TABLE_USE_ITEM);
	flagManager.Set(CTableContainer::TABLE_SET_ITEM);
	fileNameList.SetFileName(CTableContainer::TABLE_ITEM, "Table_Item_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_ITEM_OPTION, "Table_Item_Option_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_USE_ITEM, "Table_Use_Item_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_SET_ITEM, "Table_Set_Item_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_WORLD, "Table_World_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_WORLD_MAP, "Table_Worldmap_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_WORLD_ZONE, "Table_World_Zone_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_HELP, "Table_Help_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_MERCHANT, "Table_Merchant_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_MOB, "Table_MOB_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_NEWBIE, "Table_Newbie_Data");
	fileNameList.SetFileName(CTableContainer::TABLE_TEXT_ALL, "Table_Text_All_Data");

	g_pTableContainer = new CTableContainer;
	std::string str = ".\\data";
	return g_pTableContainer->Create(flagManager, (char*)str.c_str(), &fileNameList, CTable::LOADING_METHOD_BINARY, GetACP(),NULL);
}

void	CCharServer::Run()
{
	DWORD dwTickCur, dwTickOld = ::GetTickCount();

	while (IsRunnable())
	{
		dwTickCur = ::GetTickCount();
		if (dwTickCur - dwTickOld >= 100000)
		{			
			DoAliveTime();
			dwTickOld = dwTickCur;
			if (!m_pQueryServerSession)
				this->m_pCharSession->OnAccept();
			if (!m_pMasterServerSession)
				this->m_pCharSession->OnAccept();
		}
		Sleep(2);
		DoAliveTime();
	}
}

bool	CCharServer::Add(ACCOUNTID accID, HSESSION hSession)
{
	if (Find(accID))
		return false;

	m_sessionList.insert(std::pair<ACCOUNTID, HSESSION>(accID, hSession));

	return true;
}

void	CCharServer::RemoveClientSession(ACCOUNTID accID)
{
	for (CHAR_CLIENT_IT it = m_sessionList.begin(); it != m_sessionList.end(); it++)
	{
		if (accID == it->first)
		{
			m_sessionList.erase(it->first);
			break;
		}
	}
}

void	CCharServer::Remove(HSESSION session)
{
	for (CHAR_CLIENT_IT it = m_sessionList.begin(); it != m_sessionList.end(); it++)
	{
		if (session == it->second)
		{
			m_sessionList.erase(it);
			break;
		}
	}
}

bool	CCharServer::Find(ACCOUNTID accID)
{
	for (CHAR_CLIENT_IT it = m_sessionList.begin(); it != m_sessionList.end(); it++)
	{
		if (accID == it->first)
		{
			return true;
		}
	}

	return false;
}

HSESSION CCharServer::GetHSession(ACCOUNTID accID)
{
	for (CHAR_CLIENT_IT it = m_sessionList.begin(); it != m_sessionList.end(); it++)
	{
		if (accID == it->first)
		{
			return it->second;
		}
	}
	return INVALID_HSESSION;
}

bool	CCharServer::AddCharLogged(ACCOUNTID accID, CHARACTERID charID)
{
	if (IsCharLogged(accID, charID))
		return false;

	m_charList.insert(std::pair<ACCOUNTID, CHARACTERID>(accID,charID));
	return true;
}
bool	CCharServer::IsCharLogged(ACCOUNTID accID, CHARACTERID charID)
{
	for (IN_CHAR_SERVER_IT it = m_charList.begin(); it != m_charList.end(); it++)
	{
		if (accID == it->first)
		{
			if (charID == it->second)
				return true;
		}
	}
	return false;
}
void	CCharServer::RemoveCharaLogged(ACCOUNTID accID, CHARACTERID charID)
{
	for (IN_CHAR_SERVER_IT it = m_charList.begin(); it != m_charList.end(); it++)
	{
		if (accID == it->first)
		{
			if (charID == it->second)
			{
				m_charList.erase(it);
				break;
			}
		}
	}
}

void	CCharServer::SetMasterSession(CMasterSession * pServerSession)
{
	CNtlAutoMutex mutex(&m_masterServerMutex);
	mutex.Lock();

	if (NULL == pServerSession)
	{
		m_pMasterServerSession->Release();
		m_pMasterServerSession = pServerSession;
	}
	else
	{
		pServerSession->Acquire();
		m_pMasterServerSession = pServerSession;
	}
}

CMasterSession * CCharServer::AcquireMasterSession()
{
	CNtlAutoMutex mutex(&m_masterServerMutex);
	mutex.Lock();

	if (NULL == m_pMasterServerSession) return NULL;

	m_pMasterServerSession->Acquire();
	return m_pMasterServerSession;
}

CMasterSession * CCharServer::GetMasterSession()
{
	return m_pMasterServerSession;
}
void	CCharServer::SetQuerySession(CQuerySession * pServerSession)
{
	CNtlAutoMutex mutex(&m_queryServerMutex);
	mutex.Lock();

	if (NULL == pServerSession)
	{
		m_pQueryServerSession->Release();
		m_pQueryServerSession = pServerSession;
	}
	else
	{
		pServerSession->Acquire();
		m_pQueryServerSession = pServerSession;
	}
}

CQuerySession * CCharServer::AcquireQuerySession()
{
	CNtlAutoMutex mutex(&m_queryServerMutex);
	mutex.Lock();

	if (NULL == m_pQueryServerSession) return NULL;

	m_pQueryServerSession->Acquire();
	return m_pQueryServerSession;
}
CQuerySession * CCharServer::GetQuerySession()
{
	return m_pQueryServerSession;
}
void	CCharServer::SetClientSession(CCharSession* pSession)
{
	this->m_pClientSession = pSession;
}
CCharSession* CCharServer::GetClientSession()
{
	return this->m_pClientSession;
}

HOBJECT	CCharServer::AcquireSerialId(void)
{
	if (m_uiSerialId++)
	{
		if (m_uiSerialId == 0xffffffff)
			m_uiSerialId = 0;
	}

	return m_uiSerialId;
}
void CCharServer::DoAliveTime()
{
	for (int i = 0; i < this->GetNetwork()->GetSessionList()->GetMaxCount(); i++)
	{
		CNtlSession* first = this->GetNetwork()->GetSessionList()->Find(i);
		if (first)
		{
			if (first->GetSessionType() != SESSION_CHAR_SERVER)
			{
				PACKETDATA res;
				res.wOpCode = SYS_ALIVE;
				CNtlPacket packet((BYTE*)&res, sizeof(res));
				first->PushPacket(&packet);
			}
		}
	}
}