#include "stdafx.h"
#include "MasterSessionFactory.h"


CMasterSessionFactory::CMasterSessionFactory()
{
}


CMasterSessionFactory::~CMasterSessionFactory()
{
}

CNtlSession * CMasterSessionFactory::CreateSession(SESSIONTYPE sessionType)
{
	CNtlSession * pSession = NULL;
	switch (sessionType)
	{
		case SESSION_MASTER_SERVER:
		{
			pSession = new CMasterSession(sessionType);
		}
		break;
		case SESSION_AUTH_SERVER:
		{
			pSession = new CNtlSession(sessionType);
		}
		break;
		case SESSION_QUERY_SERVER:
		{
			pSession = new CNtlSession(sessionType);
		}
		break;
		case SESSION_CHAR_SERVER:
		{
			pSession = new CNtlSession(sessionType);
		}
		break;
		case SESSION_COMMUNITY_SERVER:
		{
			pSession = new CNtlSession(sessionType);
		}
		break;
		case SESSION_GAME_SERVER:
		{
			pSession = new CNtlSession(sessionType);
		}
		break;
		case SESSION_NPC_SERVER:
		{
			pSession = new CNtlSession(sessionType);
		}
		break;
		default:
			break;
	}

	return pSession;
}



void CMasterSessionFactory::DestroySession(CNtlSession * pSession)
{
	switch (pSession->GetSessionType())
	{
	case SESSION_MASTER_SERVER:
	case SESSION_QUERY_SERVER:
	case SESSION_AUTH_SERVER:
	case SESSION_COMMUNITY_SERVER:
	case SESSION_CHAR_SERVER:
	case SESSION_GAME_SERVER:
	case SESSION_NPC_SERVER:
	{
		SAFE_DELETE(pSession);
	}
	break;

	default:
		break;
	}
}