#pragma once
#include "MasterServer.h"
#include "NtlSessionFactory.h"

#ifndef CMASTERSESSION_FACTORY_H
#define CMASTERSESSION_FACTORY_H

class CMasterSessionFactory : public CNtlSessionFactory
{
public:
	CMasterSessionFactory();
	~CMasterSessionFactory();
	CNtlSession * CreateSession(SESSIONTYPE sessionType);
	void DestroySession(CNtlSession * pSession);
};
#endif

