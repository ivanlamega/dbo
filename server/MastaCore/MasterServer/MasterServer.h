#pragma once
#include "NtlSfx.h"
#include "ServerGeneralDef.h"
#include "MasterSession.h"
#include "MasterSessionFactory.h"

#ifndef CMASTER_SERVER_H
#define CMASTER_SERVER_H

class CAuthSession;
class CQuerySession;
class CCharSession;
class CCommunitySession;
class CGameSession;
class CNpcSession;

#include "AuthSession.h"
#include "QuerySession.h"
#include "CharSession.h"
#include "CommunitySession.h"
#include "GameSession.h"
#include "NpcSession.h"

class CMasterServer :	public CNtlServerApp
{
	//Members
public:
	CNtlString				ServersConfig[99][2];//For Config of multiple server 0->{Ip,Port} - Luiz45
private:
	CNtlAcceptor			m_clientAcceptor;
	CNtlLog  				m_log;
	sSERVERCONFIG			m_config;
	CNtlString				EnableMultipleServers;//Added for enabling multiple server - Luiz45

	typedef std::list<HSESSION> SESSIONLIST;
	typedef SESSIONLIST::iterator SESSIONIT;

	SESSIONLIST				m_sessionList;	
	//Master Server ptr Refresh
	CMasterSession*				m_pMasterSession;
	//Query Server Connection
	CNtlConnector				m_queryServerConnector;
	CNtlMutex					m_queryServerMutex;
	CQuerySession*			    m_pQueryServerSession;
	//Auth Server Connection
	CNtlConnector				m_authServerConnector;
	CNtlMutex					m_authServerMutex;
	CAuthSession*			    m_pAuthServerSession;
	//Char Server Connection
	CNtlConnector				m_charServerConnector;
	CNtlMutex					m_charServerMutex;
	CCharSession*				m_pCharServerSession;
	//Community Server Connection
	CNtlConnector				m_communityServerConnector;
	CNtlMutex					m_communityServerMutex;
	CCommunitySession*			m_pCommunityServerSession;
	//Game Server Connection
	CNtlConnector				m_gameServerConnector;
	CNtlMutex					m_gameServerMutex;
	CGameSession*				m_pGameServerSession;
	//Game Server Connection
	CNtlConnector				m_npcServerConnector;
	CNtlMutex					m_npcServerMutex;
	CNpcSession*				m_pNpcServerSession;
	//Methods
	//Connected
	bool					m_bAuthServerConnected;
	bool					m_bQueryServerConnected;
	bool					m_bCharServerConnected;
	bool					m_bCommunityServerConnected;
	bool					m_bGameServerConnected;
	bool					m_bNpcServerConnected;
	void					DoAliveTime();
public:	
	//For Multiple Server - Luiz45
	const std::string		GetConfigFileEnabledMultipleServers();
	const DWORD				GetConfigFileMaxServers();	
	int						OnInitApp();
	int						OnCreate();
	void					OnDestroy();
	int						OnCommandArgument(int argc, _TCHAR* argv[]);
	int						OnConfiguration(const char * lpszConfigFile);
	int						OnAppStart();
	int						GetSessionCount();
	void					Run();
	bool					Add(HSESSION hSession);
	void					Remove(HSESSION hSession);
	bool					Find(HSESSION hSession);
	void					SetQuerySession(CQuerySession* pServerSession);
	CQuerySession*			AcquireQuerySession();
	CQuerySession*			GetQuerySession();
	void					SetAuthSession(CAuthSession * pServerSession);
	CAuthSession*			AcquireAuthSession();
	CAuthSession*			GetAuthSession();
	void					SetCharSession(CCharSession * pServerSession);
	CCharSession*			AcquireCharSession();
	CCharSession*			GetCharSession();
	void					SetCommunitySession(CCommunitySession* pServerSession);
	CCommunitySession*		AcquireCommunitySession();
	CCommunitySession*		GetCommunitySession();
	void					SetGameSession(CGameSession* pServerSession);
	CGameSession*			AcquireGameSession();
	CGameSession*			GetGameSession();
	void					SetNpcSession(CNpcSession* pServerSession);
	CNpcSession*			AcquireNpcSession();
	CNpcSession*			GetNpcSession();
	void					SetAuthServerConnected(bool c){ m_bAuthServerConnected = c; }
	void					SetQueryServerConnected(bool c){ m_bQueryServerConnected = c; }
	void					SetCharServerConnected(bool c){ m_bCharServerConnected = c; }
	void					SetCommunityServerConnected(bool c){ m_bCommunityServerConnected = c; }
	void					SetGameServerConnected(bool c){ m_bGameServerConnected = c; }
	void					SetNpcServerConnected(bool c){ m_bNpcServerConnected = c; }
	bool					IsAuthServerConnected() { return m_bAuthServerConnected; }
	bool					IsQueryServerConnected(){ return m_bQueryServerConnected; }
	bool					IsCharServerConnected(){ return m_bCharServerConnected; }
	bool					IsCommunityServerConnected(){ return m_bCommunityServerConnected; }
	bool					IsGameServerConnected(){ return m_bGameServerConnected; }
	bool					IsNpcServerConnected(){ return m_bNpcServerConnected; }
	void					ReconnectNpcServer();
};

#endif