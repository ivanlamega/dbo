#pragma once
#include "MasterServer.h"
#include "NtlSession.h"
#include "NtlPacketEncoder_RandKey.h"
///////////////////////////////////////RESPONSE PACKETS///////////////////////////
//Master -> Auth Server
#include "NtlPacketMA.h"
//Master -> Char Server
#include "NtlPacketMC.h"
//Master -> Server Agent
#include "NtlPacketME.h"
//Master -> Game Server
#include "NtlPacketMG.h"
//Master -> Log server
#include "NtlPacketML.h"
//Master -> NPC Server
#include "NtlPacketMN.h"
//Master -> Operating Server
#include "NtlPacketMP.h"
//Master -> Query Server
#include "NtlPacketMQ.h"
//Master -> Game Report Server
#include "NtlPacketMR.h"
//Master -> System Tool
#include "NtlPacketMS.h"
//Master -> Community server
#include "NtlPacketMT.h"
/////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////REQUEST PACKETS///////////////////////////
//Auth Server -> Master Server
#include "NtlPacketAM.h"
//Char Server -> Master Server
#include "NtlPacketCM.h"
//Server Agent -> Master Server
#include "NtlPacketEM.h"
//Game Server -> Master Server
#include "NtlPacketGM.h"
//Log server -> Master Server
#include "NtlPacketLM.h"
//NPC Server -> Master Server
#include "NtlPacketNM.h"
//Operating Server -> Master Server
#include "NtlPacketPM.h"
//Query Server -> Master Server
#include "NtlPacketQM.h"
//Game Report Server -> Master Server
#include "NtlPacketRM.h"
//System Tool -> Master Server
#include "NtlPacketSM.h"
//Community Server -> Master Server
#include "NtlPacketTM.h"
/////////////////////////////////////////////////////////////////////////////////

#include "NtlPacketUtil.h"

class CMasterSession :	public CNtlSession
{
public:
	CMasterSession(bool bAliveCheck = true, bool bOpcodeCheck = true);
	~CMasterSession();
	int							OnAccept();
	void						OnClose();
	int							OnConnect();
	int							OnDispatch(CNtlPacket * pPacket);
	// Packet functions
	
	// End Packet functions
private:
	CNtlPacketEncoder_RandKey	m_packetEncoder;
};