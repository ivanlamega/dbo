#include "stdafx.h"
#include "MasterServer.h"
#include "MasterSession.h"

CMasterSession::CMasterSession(bool bAliveCheck, bool bOpcodeCheck) :CNtlSession(SESSION_MASTER_SERVER)
{
	SetControlFlag(CONTROL_FLAG_USE_SEND_QUEUE);

	if (bAliveCheck)
	{
		SetControlFlag(CONTROL_FLAG_CHECK_ALIVE);
	}
	if (bOpcodeCheck)
	{
		SetControlFlag(CONTROL_FLAG_CHECK_OPCODE);
	}

	SetPacketEncoder(&m_packetEncoder);
}

//-----------------------------------------------------------------------------------
//		클라이언트 소멸
//-----------------------------------------------------------------------------------
CMasterSession::~CMasterSession()
{
	NTL_PRINT(PRINT_APP, "CMasterSession Destructor Called");
	CMasterServer * app = (CMasterServer*)NtlSfxGetApp();
	app->Remove(this->GetHandle());
}

int CMasterSession::OnAccept()
{
	NTL_PRINT(PRINT_APP, "%s", __FUNCTION__);
	CMasterServer* app = (CMasterServer*)NtlSfxGetApp();
	for (int i = 0; i < app->GetNetwork()->GetSessionList()->GetMaxCount(); i++)
	{
		CNtlSession* first = app->GetNetwork()->GetSessionList()->Find(i);
		if (first)
		{
			if (first->GetSessionType() == SESSION_AUTH_SERVER)
			{
				app->SetAuthSession((CAuthSession*)first);
			}
			if (first->GetSessionType() == SESSION_QUERY_SERVER)
			{
				app->SetQuerySession((CQuerySession*)first);
			}
			if (first->GetSessionType() == SESSION_CHAR_SERVER)
			{
				app->SetCharSession((CCharSession*)first);
			}
			if (first->GetSessionType() == SESSION_COMMUNITY_SERVER)
			{
				app->SetCommunitySession((CCommunitySession*)first);
			}
			if (first->GetSessionType() == SESSION_GAME_SERVER)
			{
				app->SetGameSession((CGameSession*)first);
			}
			if (first->GetSessionType() == SESSION_NPC_SERVER)
			{
				app->SetNpcSession((CNpcSession*)first);
			}
		}
	}
	if ((app->GetAuthSession() != NULL) && (!app->IsAuthServerConnected()))
	{
		CNtlPacket packetA(sizeof(sMA_HEARTBEAT));
		sMA_HEARTBEAT* msgA = (sMA_HEARTBEAT *)packetA.GetPacketData();
		msgA->wOpCode = MA_HEARTBEAT;
		packetA.SetPacketLen(sizeof(sMA_HEARTBEAT));
		app->GetAuthSession()->PushPacket(&packetA);
		app->SetAuthServerConnected(true);
	}
	if ((app->GetQuerySession() != NULL) && (!app->IsQueryServerConnected()))
	{
		CNtlPacket packetQ(sizeof(sMQ_HEARTBEAT));
		sMQ_HEARTBEAT* msgQ = (sMQ_HEARTBEAT*)packetQ.GetPacketData();
		msgQ->wOpCode = MQ_HEARTBEAT;
		packetQ.SetPacketLen(sizeof(sMQ_HEARTBEAT));
		app->GetQuerySession()->PushPacket(&packetQ);
		app->SetQueryServerConnected(true);
	}
	if ((app->GetCharSession() != NULL) && (!app->IsCharServerConnected()))
	{
		CNtlPacket packetC(sizeof(sMC_HEARTBEAT));
		sMC_HEARTBEAT* msgC = (sMC_HEARTBEAT*)packetC.GetPacketData();
		msgC->wOpCode = MC_HEARTBEAT;
		packetC.SetPacketLen(sizeof(sMC_HEARTBEAT));
		app->GetCharSession()->PushPacket(&packetC);
		app->SetCharServerConnected(true);
	}
	if ((app->GetCommunitySession() != NULL) && (!app->IsCommunityServerConnected()))
	{
		CNtlPacket packetT(sizeof(sMT_HEARTBEAT));
		sMT_HEARTBEAT* msgT = (sMT_HEARTBEAT*)packetT.GetPacketData();
		msgT->wOpCode = MT_HEARTBEAT;
		packetT.SetPacketLen(sizeof(sMT_HEARTBEAT));
		app->GetCommunitySession()->PushPacket(&packetT);
		app->SetCommunityServerConnected(true);
	}
	if ((app->GetGameSession() != NULL) && (!app->IsGameServerConnected()))
	{
		CNtlPacket packetG(sizeof(sMG_HEARTBEAT));
		sMG_HEARTBEAT* msgG = (sMG_HEARTBEAT*)packetG.GetPacketData();
		msgG->wOpCode = MG_HEARTBEAT;
		packetG.SetPacketLen(sizeof(sMG_HEARTBEAT));
		app->GetGameSession()->PushPacket(&packetG);
		app->SetGameServerConnected(true);
	}
	if ((app->GetNpcSession() != NULL) && (!app->IsNpcServerConnected()))
	{
		CNtlPacket packetG(sizeof(sMN_HEARTBEAT));
		sMN_HEARTBEAT* msgG = (sMN_HEARTBEAT*)packetG.GetPacketData();
		msgG->wOpCode = MN_HEARTBEAT;
		packetG.SetPacketLen(sizeof(sMN_HEARTBEAT));
		app->GetNpcSession()->PushPacket(&packetG);
		app->SetNpcServerConnected(true);
	}
	return NTL_SUCCESS;
}


void CMasterSession::OnClose()
{
	NTL_PRINT(PRINT_APP, "%s", __FUNCTION__);
}

int CMasterSession::OnConnect()
{
	NTL_PRINT(PRINT_APP, "%s", __FUNCTION__);
	CMasterServer* app = (CMasterServer*)NtlSfxGetApp();
	do{
		this->OnAccept();
		Sleep(5000);
	} while ((!app->GetAuthSession()) ||
		(!app->GetCharSession()) ||
		(!app->GetQuerySession())||
		(!app->GetGameSession()) ||
		(!app->AcquireNpcSession()) ||
		(!app->GetCommunitySession()));
	return 0;
}

int CMasterSession::OnDispatch(CNtlPacket * pPacket)
{
	CMasterServer * app = (CMasterServer*)NtlSfxGetApp();
	if (((!app->GetAuthSession()) ||
		(!app->GetCharSession()) ||
		(!app->GetQuerySession()) ||
		(!app->GetGameSession()) ||
		(!app->GetNpcSession()) ||
		(!app->GetCommunitySession())))
		this->OnConnect();

	sNTLPACKETHEADER * pHeader = (sNTLPACKETHEADER *)pPacket->GetPacketData();
	switch (pHeader->wOpCode)
	{		
		case AM_HEARTBEAT:
		{
			if (app->GetAuthSession() != NULL)
				app->GetAuthSession()->ResetAliveTime();
		}
		break;
		case QM_HEARTBEAT:
		{
			if (app->GetQuerySession() != NULL)
				app->GetQuerySession()->ResetAliveTime();
		}
		break;
		case CM_HEARTBEAT:
		{
			if (app->GetCharSession() != NULL)
				app->GetCharSession()->ResetAliveTime();
		}
		break;
		case TM_HEARTBEAT:
		{
			if (app->GetCommunitySession() != NULL)
				app->GetCommunitySession()->ResetAliveTime();
		}
		break;
		case GM_HEARTBEAT:
		{
			if (app->GetGameSession() != NULL)
				app->GetGameSession()->ResetAliveTime();
		}
		break;
		case NM_HEARTBEAT:
		{
			if (app->GetNpcSession() != NULL)
				app->GetNpcSession()->ResetAliveTime();
		}
		break;
		default:
		{
			if (pHeader->wOpCode != SYS_ALIVE)
			{
				NTL_PRINT(PRINT_APP, "The following packet was not handled: %s", NtlGetPacketName(pHeader->wOpCode));
			}
			return CNtlSession::OnDispatch(pPacket);
		}
	}

	return NTL_SUCCESS;
}