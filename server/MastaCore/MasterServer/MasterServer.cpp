#include "stdafx.h"
#include "MasterServer.h"

//For Multiple Server - Luiz45
const std::string CMasterServer::GetConfigFileEnabledMultipleServers()
{
	return EnableMultipleServers.GetString();
}
const DWORD CMasterServer::GetConfigFileMaxServers()
{
	return MAX_NUMOF_MASTER_SERVER;
}
int	CMasterServer::OnInitApp()
{
	m_nMaxSessionCount = MAX_NUMOF_MASTER_SESSION;

	m_pSessionFactory = new CMasterSessionFactory;
	if (NULL == m_pSessionFactory)
	{
		return NTL_ERR_SYS_MEMORY_ALLOC_FAIL;
	}

	m_pAuthServerSession = NULL;
	m_pCharServerSession = NULL;
	m_pQueryServerSession = NULL;
	m_pCommunityServerSession = NULL;	
	m_pGameServerSession = NULL;
	m_pNpcServerSession = NULL;
	m_pMasterSession = (CMasterSession*)m_pSessionFactory->CreateSession(SESSION_MASTER_SERVER);
	return NTL_SUCCESS;
}

int	CMasterServer::OnCreate()
{
	int rc = NTL_SUCCESS;
	rc = m_clientAcceptor.Create(m_config.strMasterServerIP.c_str(), m_config.wMasterServerPort, SESSION_MASTER_SERVER,
		MAX_NUMOF_MASTER_GAME_CLIENT, 10, 15, MAX_NUMOF_MASTER_GAME_CLIENT);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_clientAcceptor, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_queryServerConnector.Create(m_config.strQueryServerIP.c_str(), m_config.wQueryServerPort, SESSION_QUERY_SERVER);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_queryServerConnector, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_authServerConnector.Create(m_config.strAuthServerIP.c_str(), m_config.wAuthServerPort, SESSION_AUTH_SERVER);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_authServerConnector, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_charServerConnector.Create(m_config.strCharServerIP.c_str(), m_config.wCharServerPort, SESSION_CHAR_SERVER);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_charServerConnector, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_communityServerConnector.Create(m_config.strCommunityServerIP.c_str(), m_config.wCommunityServerPort, SESSION_COMMUNITY_SERVER);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_communityServerConnector, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_gameServerConnector.Create(m_config.strGameServerIP.c_str(), m_config.wGameServerPort, SESSION_GAME_SERVER);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_gameServerConnector, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_npcServerConnector.Create(m_config.strNpcServerIP.c_str(), m_config.wNpcServerPort, SESSION_NPC_SERVER,5000);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_npcServerConnector, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	return NTL_SUCCESS;

}

void	CMasterServer::OnDestroy()
{
	SAFE_DELETE(m_pSessionFactory);
}

int	CMasterServer::OnCommandArgument(int argc, _TCHAR* argv[])
{
	return NTL_SUCCESS;
}

int	CMasterServer::OnConfiguration(const char * lpszConfigFile)
{
	CNtlIniFile file;

	int rc = file.Create(lpszConfigFile);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}
	//For multiple servers - Luiz45
	if (!file.Read("ServerOptions", "EnableMultipleServers", EnableMultipleServers))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	/*if (!file.Read("ServerOptions", "MaxServerAllowed", MAX_NUMOF_MASTER_SERVER))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}*/
	if (!file.Read("MasterServer", "Address", m_config.strMasterServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("MasterServer", "Port", m_config.wMasterServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("AuthServer", "Address", m_config.strAuthServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("AuthServer", "Port", m_config.wAuthServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("CharServer", "Address", m_config.strCharServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("CharServer", "Port", m_config.wCharServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("CommunityServer", "Address", m_config.strCommunityServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("CommunityServer", "Port", m_config.wCommunityServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("QueryServer", "Address", m_config.strQueryServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("QueryServer", "Port", m_config.wQueryServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("GameServer", "Address", m_config.strGameServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("GameServer", "Port", m_config.wGameServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("NpcServer", "Address", m_config.strNpcServerIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("NpcServer", "Port", m_config.wNpcServerPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	return NTL_SUCCESS;
}

int	CMasterServer::OnAppStart()
{
	return NTL_SUCCESS;
}

void	CMasterServer::Run()
{
	DWORD dwTickCur, dwTickOld = ::GetTickCount();

	while (IsRunnable())
	{
		dwTickCur = ::GetTickCount();		
		if (dwTickCur - dwTickOld >= 100000)
		{
			//	NTL_PRINT(PRINT_APP, "Auth Server Run()");
			DoAliveTime();
			dwTickOld = dwTickCur;
			if (!m_pAuthServerSession)
				this->m_pMasterSession->OnAccept();
			if (!m_pQueryServerSession)
				this->m_pMasterSession->OnAccept();
			if (!m_pCharServerSession)
				this->m_pMasterSession->OnAccept();
			if (!m_pCommunityServerSession)
				this->m_pMasterSession->OnAccept();
			if (!m_pGameServerSession)
				this->m_pMasterSession->OnAccept();
			if (!m_pNpcServerSession)
				this->m_pMasterSession->OnAccept();
		}
		Sleep(2);
		DoAliveTime();
	}
}

bool	CMasterServer::Add(HSESSION hSession)
{
	if (Find(hSession))
		return false;

	m_sessionList.push_back(hSession);

	return true;
}

void	CMasterServer::Remove(HSESSION hSession)
{
	for (SESSIONIT it = m_sessionList.begin(); it != m_sessionList.end(); it++)
	{
		if (hSession == *it)
		{
			m_sessionList.erase(it);
			break;
		}
	}
}

bool	CMasterServer::Find(HSESSION hSession)
{
	for (SESSIONIT it = m_sessionList.begin(); it != m_sessionList.end(); it++)
	{
		if (hSession == *it)
		{
			return true;
		}
	}

	return false;
}

void	CMasterServer::SetQuerySession(CQuerySession * pServerSession)
{
	CNtlAutoMutex mutex(&m_queryServerMutex);
	mutex.Lock();

	if (NULL == pServerSession)
	{
		m_pQueryServerSession->Release();
		m_pQueryServerSession = pServerSession;
	}
	else
	{
		pServerSession->Acquire();
		m_pQueryServerSession = pServerSession;
	}
}

CQuerySession * CMasterServer::AcquireQuerySession()
{
	CNtlAutoMutex mutex(&m_queryServerMutex);
	mutex.Lock();

	if (NULL == m_pQueryServerSession) return NULL;

	m_pQueryServerSession->Acquire();
	return m_pQueryServerSession;
}
CQuerySession * CMasterServer::GetQuerySession()
{
	return m_pQueryServerSession;
}

void	CMasterServer::SetAuthSession(CAuthSession * pServerSession)
{
	CNtlAutoMutex mutex(&m_authServerMutex);
	mutex.Lock();

	if (NULL == pServerSession)
	{
		m_pAuthServerSession->Release();
		m_pAuthServerSession = pServerSession;
	}
	else
	{
		pServerSession->Acquire();
		m_pAuthServerSession = pServerSession;
	}
}

CAuthSession* CMasterServer::AcquireAuthSession()
{
	CNtlAutoMutex mutex(&m_authServerMutex);
	mutex.Lock();

	if (NULL == m_pAuthServerSession) return NULL;

	m_pAuthServerSession->Acquire();
	return m_pAuthServerSession;
}

CAuthSession* CMasterServer::GetAuthSession()
{
	return m_pAuthServerSession;
}

void	CMasterServer::SetCharSession(CCharSession * pServerSession)
{
	CNtlAutoMutex mutex(&m_charServerMutex);
	mutex.Lock();

	if (NULL == pServerSession)
	{
		m_pCharServerSession->Release();
		m_pCharServerSession = pServerSession;
	}
	else
	{
		pServerSession->Acquire();
		m_pCharServerSession = pServerSession;
	}
}

CCharSession* CMasterServer::AcquireCharSession()
{
	CNtlAutoMutex mutex(&m_charServerMutex);
	mutex.Lock();

	if (NULL == m_pCharServerSession) return NULL;

	m_pCharServerSession->Acquire();
	return m_pCharServerSession;
}

CCharSession* CMasterServer::GetCharSession()
{
	return m_pCharServerSession;
}

void	CMasterServer::SetCommunitySession(CCommunitySession * pServerSession)
{
	CNtlAutoMutex mutex(&m_communityServerMutex);
	mutex.Lock();

	if (NULL == pServerSession)
	{
		m_pCommunityServerSession->Release();
		m_pCommunityServerSession = pServerSession;
	}
	else
	{
		pServerSession->Acquire();
		m_pCommunityServerSession = pServerSession;
	}
}

CCommunitySession* CMasterServer::AcquireCommunitySession()
{
	CNtlAutoMutex mutex(&m_communityServerMutex);
	mutex.Lock();

	if (NULL == m_pCommunityServerSession) return NULL;

	m_pCommunityServerSession->Acquire();
	return m_pCommunityServerSession;
}

CCommunitySession* CMasterServer::GetCommunitySession()
{
	return m_pCommunityServerSession;
}

void	CMasterServer::SetGameSession(CGameSession * pServerSession)
{
	CNtlAutoMutex mutex(&m_gameServerMutex);
	mutex.Lock();

	if (NULL == pServerSession)
	{
		m_pGameServerSession->Release();
		m_pGameServerSession = pServerSession;
	}
	else
	{
		pServerSession->Acquire();
		m_pGameServerSession = pServerSession;
	}
}

CGameSession* CMasterServer::AcquireGameSession()
{
	CNtlAutoMutex mutex(&m_gameServerMutex);
	mutex.Lock();

	if (NULL == m_pGameServerSession) return NULL;

	m_pGameServerSession->Acquire();
	return m_pGameServerSession;
}

CGameSession* CMasterServer::GetGameSession()
{
	return m_pGameServerSession;
}

void	CMasterServer::SetNpcSession(CNpcSession * pServerSession)
{
	CNtlAutoMutex mutex(&m_npcServerMutex);
	mutex.Lock();

	if (NULL == pServerSession)
	{
		m_pNpcServerSession->Release();
		m_pNpcServerSession = pServerSession;
	}
	else
	{
		pServerSession->Acquire();
		m_pNpcServerSession = pServerSession;
	}
}

CNpcSession* CMasterServer::AcquireNpcSession()
{
	CNtlAutoMutex mutex(&m_npcServerMutex);
	mutex.Lock();

	if (NULL == m_pNpcServerSession) return NULL;

	m_pNpcServerSession->Acquire();
	return m_pNpcServerSession;
}

CNpcSession* CMasterServer::GetNpcSession()
{
	return m_pNpcServerSession;
}

void CMasterServer::DoAliveTime()
{
	for (int i = 0; i < this->GetNetwork()->GetSessionList()->GetMaxCount(); i++)
	{
		CNtlSession* first = this->GetNetwork()->GetSessionList()->Find(i);
		if (first)
		{
			if (first->GetSessionType() != SESSION_MASTER_SERVER)
			{
				PACKETDATA res;
				res.wOpCode = SYS_ALIVE;
				CNtlPacket packet((BYTE*)&res, sizeof(res));
				first->PushPacket(&packet);
			}
		}
	}
}

void CMasterServer::ReconnectNpcServer()
{
	int rc;
	m_npcServerConnector.Destroy();
	m_network.GetConnector(SESSION_NPC_SERVER, m_config.strNpcServerIP.c_str(), m_config.wNpcServerPort)->Destroy();
	rc = m_npcServerConnector.Create(m_config.strNpcServerIP.c_str(), m_config.wNpcServerPort, SESSION_NPC_SERVER, 5000);
	rc = m_network.Associate(&m_npcServerConnector, true);
}