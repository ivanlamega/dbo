#include "stdafx.h"
#include "BotServer.h"

//For Multiple Server - Luiz45
const std::string CBotServer::GetConfigFileEnabledMultipleServers()
{
	return EnableMultipleServers.GetString();
}
const DWORD CBotServer::GetConfigFileMaxServers()
{
	return MAX_NUMOF_BM_SERVER;
}
int	CBotServer::OnInitApp()
{
	m_nMaxSessionCount = MAX_NUMOF_BM_SESSION;

	m_pSessionFactory = new CBotSessionFactory;
	if (NULL == m_pSessionFactory)
	{
		return NTL_ERR_SYS_MEMORY_ALLOC_FAIL;
	}

	return NTL_SUCCESS;
}

int	CBotServer::OnCreate()
{
	int rc = NTL_SUCCESS;
	rc = m_clientAcceptor.Create(m_config.strBotMasterIP.c_str(), m_config.wBotMasterPort, SESSION_BOT_MASTER_SERVER,
		MAX_NUMOF_BM_GAME_CLIENT, 5, 15, MAX_NUMOF_BM_GAME_CLIENT);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_clientAcceptor, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_serverConnector.Create(m_config.strBotAgentIP.c_str(), m_config.wBotAgentPort, SESSION_BOT_AGENT_SERVER);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	rc = m_network.Associate(&m_serverConnector, true);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}

	return NTL_SUCCESS;

}

void	CBotServer::OnDestroy()
{
	SAFE_DELETE(m_pSessionFactory);
}

int	CBotServer::OnCommandArgument(int argc, _TCHAR* argv[])
{
	return NTL_SUCCESS;
}

int	CBotServer::OnConfiguration(const char * lpszConfigFile)
{
	CNtlIniFile file;

	int rc = file.Create(lpszConfigFile);
	if (NTL_SUCCESS != rc)
	{
		return rc;
	}
	//For multiple servers - Luiz45
	if (!file.Read("ServerOptions", "EnableMultipleServers", EnableMultipleServers))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	/*if (!file.Read("ServerOptions", "MaxServerAllowed", MAX_NUMOF_SERVER))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}*/
	if (!file.Read("BotAgent", "Address", m_config.strBotAgentIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("BotAgent", "Port", m_config.wBotAgentPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("BotMaster", "Address", m_config.strBotMasterIP))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	if (!file.Read("BotMaster", "Port", m_config.wBotMasterPort))
	{
		return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
	}
	return NTL_SUCCESS;
}

int	CBotServer::OnAppStart()
{
	return NTL_SUCCESS;
}

void	CBotServer::Run()
{
	DWORD dwTickCur, dwTickOld = ::GetTickCount();

	while (IsRunnable())
	{
		dwTickCur = ::GetTickCount();
		if (dwTickCur - dwTickOld >= 10000)
		{
			//	NTL_PRINT(PRINT_APP, "Auth Server Run()");
			DoAliveTime();
			dwTickOld = dwTickCur;
		}
		Sleep(1500);
		DoAliveTime();
	}
}

bool	CBotServer::Add(HSESSION hSession)
{
	if (Find(hSession))
		return false;

	m_sessionList.push_back(hSession);

	return true;
}

void	CBotServer::Remove(HSESSION hSession)
{
	for (SESSIONIT it = m_sessionList.begin(); it != m_sessionList.end(); it++)
	{
		if (hSession == *it)
		{
			m_sessionList.erase(it);
			break;
		}
	}
}

bool	CBotServer::Find(HSESSION hSession)
{
	for (SESSIONIT it = m_sessionList.begin(); it != m_sessionList.end(); it++)
	{
		if (hSession == *it)
		{
			return true;
		}
	}

	return false;
}

void	CBotServer::DoAliveTime()
{
	for (int i = 0; i < this->GetNetwork()->GetSessionList()->GetMaxCount(); i++)
	{
		CNtlSession* first = this->GetNetwork()->GetSessionList()->Find(i);
		if (first)
		{
			if (first->GetSessionType() == SESSION_BOT_AGENT_SERVER)
			{
				PACKETDATA res;
				res.wOpCode = SYS_ALIVE;
				CNtlPacket packet((BYTE*)&res, sizeof(res));
				first->PushPacket(&packet);
			}
		}
	}
}