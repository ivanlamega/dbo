#include "stdafx.h"
#include "BotSessionFactory.h"


CBotSessionFactory::CBotSessionFactory()
{
}


CBotSessionFactory::~CBotSessionFactory()
{
}

CNtlSession * CBotSessionFactory::CreateSession(SESSIONTYPE sessionType)
{
	CNtlSession * pSession = NULL;
	switch (sessionType)
	{
	case SESSION_BOT_MASTER_SERVER:
	{
		pSession = new CBotMasterSession(sessionType);
	}
	break;
	case SESSION_BOT_AGENT_SERVER:
	{
		pSession = new CNtlSession(sessionType);
	}
	break;

	default:
		break;
	}

	return pSession;
}



void CBotSessionFactory::DestroySession(CNtlSession * pSession)
{
	switch (pSession->GetSessionType())
	{
	case SESSION_BOT_MASTER_SERVER:
	case SESSION_BOT_AGENT_SERVER:
	{
		SAFE_DELETE(pSession);
	}
	break;

	default:
		break;
	}
}