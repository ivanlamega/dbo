#pragma once
#include "BotServer.h"
#include "NtlSession.h"
#include "NtlPacketEncoder_RandKey.h"
#include "NTLBotSystemResultCode.h"
///////////////////////////////////////RESPONSE PACKETS///////////////////////////
//Bot Master -> Bot Agent
#include "NtlPacketBMBA.h"
//Bot Master -> Bot Client
#include "NtlPacketBMBC.h"
/////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////REQUEST PACKETS///////////////////////////
//Bot Agent -> Bot Master
#include "NtlPacketBABM.h"
//Bot Client -> Bot Master
#include "NtlPacketBCBM.h"
/////////////////////////////////////////////////////////////////////////////////
#include "NtlPacketUtil.h"

#ifndef CBOTSESSION_H
#define CBOTSESSION_H

class CClientSession :	public CNtlSession
{
public:
	CClientSession(bool bAliveCheck = false, bool bOpcodeCheck = false);
	~CClientSession();
	int							OnAccept();
	void						OnClose();
	int							OnDispatch(CNtlPacket * pPacket);
	// Packet functions
	void						SendBotAgentEnterReq(CNtlPacket* pPacket);
	// End Packet functions
private:
	CNtlPacketEncoder_RandKey	m_packetEncoder;
};

#endif