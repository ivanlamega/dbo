#pragma once
#include "BotServer.h"
#include "NtlSession.h"
#include "NtlPacketEncoder_RandKey.h"
///////////////////////////////////////RESPONSE PACKETS///////////////////////////
/////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////REQUEST PACKETS///////////////////////////
/////////////////////////////////////////////////////////////////////////////////
#include "NtlPacketUtil.h"

#ifndef CBOTSESSION_H
#define CBOTSESSION_H

class CClientSession :	public CNtlSession
{
public:
	CClientSession(bool bAliveCheck = true, bool bOpcodeCheck = true);
	~CClientSession();
	int							OnAccept();
	void						OnClose();
	int							OnDispatch(CNtlPacket * pPacket);
	// Packet functions
	void						SendBotAgentEnterReq(CNtlPacket* pPacket);
	// End Packet functions
private:
	CNtlPacketEncoder_RandKey	m_packetEncoder;
};

#endif