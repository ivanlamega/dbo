#pragma once
#include "NtlSfx.h"
#include "ClientSession.h"
#include "BotSessionFactory.h"

#ifndef CBOT_SERVER_H
#define CBOT_SERVER_H

enum APP_LOG
{
	PRINT_APP = 2,
};
enum BOT_SESSION
{
	SESSION_CLIENT,
	SESSION_SERVER_ACTIVE,
};
struct sSERVERCONFIG
{
	CNtlString		strBotAgentIP;
	WORD			wBotAgentPort;
	CNtlString      strBotMasterIP;
	WORD			wBotMasterPort;
};

const DWORD					MAX_NUMOF_GAME_CLIENT = 50;
const DWORD					MAX_NUMOF_SERVER = 1;
const DWORD					MAX_NUMOF_SESSION = MAX_NUMOF_GAME_CLIENT + MAX_NUMOF_SERVER;

class CBotServer :	public CNtlServerApp
{
	//Members
public:
	CNtlString				ServersConfig[99][2];//For Config of multiple server 0->{Ip,Port} - Luiz45
private:
	CNtlAcceptor			m_clientAcceptor;
	CNtlLog  				m_log;
	sSERVERCONFIG			m_config;
	DWORD					MAX_NUMOF_SERVER = 1;//This will be defined how many servers we can load
	CNtlString				EnableMultipleServers;//Added for enabling multiple server - Luiz45
	DWORD					MAX_NUMOF_GAME_CLIENT = 3;
	DWORD					MAX_NUMOF_SESSION = MAX_NUMOF_GAME_CLIENT + MAX_NUMOF_SERVER;

	typedef std::list<HSESSION> SESSIONLIST;
	typedef SESSIONLIST::iterator SESSIONIT;

	SESSIONLIST				m_sessionList;
	//Methods
public:	
	//For Multiple Server - Luiz45
	const std::string		GetConfigFileEnabledMultipleServers();
	const DWORD				GetConfigFileMaxServers();
	int						OnInitApp();
	int						OnCreate();
	void					OnDestroy();
	int						OnCommandArgument(int argc, _TCHAR* argv[]);
	int						OnConfiguration(const char * lpszConfigFile);
	int						OnAppStart();
	int						GetSessionCount();
	void					Run();
	bool					Add(HSESSION hSession);
	void					Remove(HSESSION hSession);
	bool					Find(HSESSION hSession);
};

#endif