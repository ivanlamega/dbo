#include "stdafx.h"
#include "BotSessionFactory.h"


CBotSessionFactory::CBotSessionFactory()
{
}


CBotSessionFactory::~CBotSessionFactory()
{
}

CNtlSession * CBotSessionFactory::CreateSession(SESSIONTYPE sessionType)
{
	CNtlSession * pSession = NULL;
	switch (sessionType)
	{
	case SESSION_CLIENT:
	{
		pSession = new CClientSession(sessionType);
	}
	break;

	default:
		break;
	}

	return pSession;
}



void CBotSessionFactory::DestroySession(CNtlSession * pSession)
{
	switch (pSession->GetSessionType())
	{
	case SESSION_CLIENT:
	{
		SAFE_DELETE(pSession);
	}
	break;

	default:
		break;
	}
}