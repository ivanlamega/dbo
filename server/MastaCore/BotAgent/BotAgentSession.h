#pragma once
#include "BotServer.h"
#include "NtlSession.h"
#include "NtlPacketEncoder_RandKey.h"
#include "NTLBotSystemResultCode.h"
///////////////////////////////////////RESPONSE PACKETS///////////////////////////
#include "NtlPacketBABM.h"
/////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////REQUEST PACKETS///////////////////////////
#include "NtlPacketBMBA.h"
/////////////////////////////////////////////////////////////////////////////////
#include "NtlPacketSYS.h"
#include "NtlPacketUtil.h"

#ifndef CBOTAGENTSESSION_H
#define CBOTAGENTSESSION_H

class CBotAgentSession :	public CNtlSession
{
public:
	CBotAgentSession(bool bAliveCheck = true, bool bOpcodeCheck = true);
	~CBotAgentSession();
	int							OnAccept();
	void						OnClose();
	int							OnDispatch(CNtlPacket * pPacket);
	// Packet functions
	void						SendBotAgentEnterReq(CNtlPacket* pPacket);
	// End Packet functions
private:
	CNtlPacketEncoder_RandKey	m_packetEncoder;
};

#endif