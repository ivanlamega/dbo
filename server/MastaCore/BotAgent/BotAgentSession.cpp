#include "stdafx.h"
#include "BotServer.h"
#include "BotAgentSession.h"


CBotAgentSession::CBotAgentSession(bool bAliveCheck, bool bOpcodeCheck) :CNtlSession(SESSION_BOT_AGENT_SERVER)
{
	SetControlFlag(CONTROL_FLAG_USE_SEND_QUEUE);

	if (bAliveCheck)
	{
		SetControlFlag(CONTROL_FLAG_CHECK_ALIVE);
	}
	if (bOpcodeCheck)
	{
		SetControlFlag(CONTROL_FLAG_CHECK_OPCODE);
	}

	SetPacketEncoder(&m_packetEncoder);
}

//-----------------------------------------------------------------------------------
//		클라이언트 소멸
//-----------------------------------------------------------------------------------
CBotAgentSession::~CBotAgentSession()
{
	NTL_PRINT(PRINT_APP, "CBotAgentSession Destructor Called");

	CBotServer * app = (CBotServer*)NtlSfxGetApp();
	app->Remove(this->GetHandle());
}

int CBotAgentSession::OnAccept()
{
	NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
	return NTL_SUCCESS;
}


void CBotAgentSession::OnClose()
{
	NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
}

int CBotAgentSession::OnDispatch(CNtlPacket * pPacket)
{
	CBotServer * app = (CBotServer*)NtlSfxGetApp();

	sNTLPACKETHEADER * pHeader = (sNTLPACKETHEADER *)pPacket->GetPacketData();
	switch (pHeader->wOpCode)
	{
		case BABM_ENTER_REQ:
			SendBotAgentEnterReq(pPacket);
		break;
		default:
		{
			if (pHeader->wOpCode != SYS_ALIVE)
			{
				NTL_PRINT(PRINT_APP, "The following packet was not handled: %s", NtlGetPacketName(pHeader->wOpCode));
			}
			return CNtlSession::OnDispatch(pPacket);
		}
	}

	return NTL_SUCCESS;
}
void CBotAgentSession::SendBotAgentEnterReq(CNtlPacket* pPacket)
{
	CBotServer * app = (CBotServer*)NtlSfxGetApp();
	sBABM_ENTER_REQ* req = (sBABM_ENTER_REQ*)pPacket->GetPacketData();
	CNtlPacket packet(sizeof(sBMBA_ENTER_RES));
	sBMBA_ENTER_RES* res = (sBMBA_ENTER_RES*)packet.GetPacketData();
	res->botgroupID = BOTAGENT;
	res->botagentID = 1;
	res->wResultCode = BOTSYSTEM_SUCCESS;
	res->wOpCode = BMBA_ENTER_RES;
	packet.SetPacketLen(sizeof(sBMBA_ENTER_RES));
	app->Send(this->GetHandle(), &packet);	
}