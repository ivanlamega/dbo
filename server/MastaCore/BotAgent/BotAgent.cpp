// BotAgent.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "NtlSfx.h"
#include "BotServer.h"
#include "NtlFile.h"


int _tmain(int argc, _TCHAR* argv[])
{
	NtlSetPrintFlag(PRINT_APP);

	CBotServer app;
	CNtlFileStream traceFileStream;

	// LOG FILE
	std::cout << "Loading BotServer.ini... \n";
	int rc = traceFileStream.Create("BotAgentLog");
	rc = app.Create(argc, argv, ".\\BotServer.ini");
	NtlSetPrintStream(traceFileStream.GetFilePtr());
	NtlSetPrintFlag(PRINT_APP | PRINT_SYSTEM);

	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "log file CreateFile error %d(%s)", rc, NtlGetErrorMessage(rc));
		return rc;
	}
	std::cout << "Loaded!\n";

	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "Server Application Create Fail %d(%s)", rc, NtlGetErrorMessage(rc));
		return rc;
	}
	app.Start();
	Sleep(500);
	std::cout << "Running... \n";
	app.WaitForTerminate();


	return 0;
}

