#pragma once
#include "NtlSfx.h"
#include "ServerGeneralDef.h"
#include "BotAgentSession.h"
#include "BotSessionFactory.h"

#ifndef CBOT_SERVER_H
#define CBOT_SERVER_H

class CBotServer :	public CNtlServerApp
{
	//Members
public:
	CNtlString				ServersConfig[99][2];//For Config of multiple server 0->{Ip,Port} - Luiz45
private:
	CNtlAcceptor			m_clientAcceptor;
	CNtlConnector			m_serverConnector;	
	CNtlLog  				m_log;
	sSERVERCONFIG			m_config;
	CNtlString				EnableMultipleServers;//Added for enabling multiple server - Luiz45

	typedef std::list<HSESSION> SESSIONLIST;
	typedef SESSIONLIST::iterator SESSIONIT;

	SESSIONLIST				m_sessionList;
	//Methods
	void					DoAliveTime();
public:	
	//For Multiple Server - Luiz45
	const std::string		GetConfigFileEnabledMultipleServers();
	const DWORD				GetConfigFileMaxServers();
	int						OnInitApp();
	int						OnCreate();
	void					OnDestroy();
	int						OnCommandArgument(int argc, _TCHAR* argv[]);
	int						OnConfiguration(const char * lpszConfigFile);
	int						OnAppStart();
	int						GetSessionCount();
	void					Run();
	bool					Add(HSESSION hSession);
	void					Remove(HSESSION hSession);
	bool					Find(HSESSION hSession);
};

#endif